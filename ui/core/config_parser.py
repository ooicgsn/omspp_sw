import datetime
from typing import Optional
from pydantic import BaseModel
import yaml
from core.models.asset import AssetCategory, AssetSubcategory


class PlatformConfig(BaseModel):
    is_valid: bool = False
    error: str = ''

    # Metadata
    category: Optional[AssetCategory]
    subcategory: Optional[AssetSubcategory]
    deployed_date: Optional[datetime.datetime]
    recovered_date: Optional[datetime.datetime]
    latitude: Optional[float]
    longitude: Optional[float]
    depth: Optional[float]
    watch_circle: Optional[float]
    deployed_cruise: str = ''
    recovered_cruise: str = ''

    class Config:
        arbitrary_types_allowed = True


class ConfigParser:
    def parse_bytes(self, data: bytes) -> PlatformConfig:
        platform_config = PlatformConfig()

        try:
            config = yaml.safe_load(data)
        except Exception as e:
            platform_config.is_valid = False
            platform_config.error = "Invalid YAML:\n" + str(e)

            return platform_config

        if not 'meta' in config or not 'platforms' in config:
            platform_config.is_valid = False
            platform_config.error = "The file is Valid YAML but not a valid platform configuration file"

            return platform_config

        meta = config['meta']
        category_name = parse_string(meta, 'category')
        subcategory_name = parse_string(meta, 'subcategory')

        platform_config.category = AssetCategory.objects.filter(name=category_name).first()
        platform_config.subcategory = AssetSubcategory.objects.filter(name=subcategory_name).first()
        platform_config.deployed_date = parse_date(meta, 'start_date')
        platform_config.recovered_date = parse_date(meta, 'stop_date')
        platform_config.latitude = parse_float(meta, 'latitude')
        platform_config.longitude = parse_float(meta, 'longitude')
        platform_config.depth = parse_float(meta, 'site_depth')
        platform_config.watch_circle = parse_float(meta, 'watch_circle')
        platform_config.deployed_cruise = parse_string(meta, 'deploy_cruise')
        platform_config.recovered_cruise = parse_string(meta, 'recover_cruise')

        platform_config.is_valid = True

        return platform_config

# region " Helpers "

def parse_string(config: dict, key: str) -> str:
    value = config.get(key, '')

    return value if value.lower() != 'none' else ''

def parse_float(config: dict, key: str) -> Optional[float]:
    value = config.get(key, None)
    if not value or (type(value) is not float and type(value) is not int):
        return None

    return float(value)

def parse_int(config: dict, key: str) -> Optional[int]:
    value = config.get(key, None)
    if not value or type(value) is not int:
        return None

    return value

def parse_date(config: dict, key: str) -> Optional[datetime.datetime]:
    value = config.get(key, None)
    if not value or type(value) is not datetime.datetime:
        return None

    return value

# endregion
