from enum import Enum
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import get_template
from django.template import Context
from django.urls import reverse
from .models.account import Profile, MobileCarrier
from .models.alert import Alert, AlertStatuses, Notification
from .models.asset import Asset, AssetLink, StatusHistory
from .models.alert_trigger import DeliveryMethod, DeliveryMethods


def recalculate_asset_status(asset_id, reason='', reported_by=None, was_manually_set=False, alert_id=None):
    asset = get_object_or_404(Asset, pk=asset_id)

    # try:
    #     # Retrieve either the passed in alert or the latest alert
    #     if alert_id is None:
    #         latest_alert = Alert.objects.filter(
    #                        asset_id=asset_id, status_id=AlertStatuses.OPEN.value).latest('created')
    #     else:
    #         latest_alert = get_object_or_404(Alert, pk=alert_id)
    # except Alert.DoesNotExist:
    #     pass

    # Reason is not nullable but could be passed in as none depending on the trigger
    if reason is None:
        reason = ''

    # If the alert rolls up or down then process linked items
    if alert_id is not None:
        alert = get_object_or_404(Alert, pk=alert_id)
        resolved = (alert.status_id == AlertStatuses.RESOLVED.value)

        if alert.roll_up:
            roll_status(RollDirection.UP.value, asset_id, resolved, reason, reported_by, was_manually_set)

        if alert.roll_down:
            roll_status(RollDirection.DOWN.value, asset_id, resolved, reason, reported_by, was_manually_set)

    # Set main asset to the correct status id, based on it's own alerts or linked alerts
    status_id, status_percent = _get_status_from_open_alerts(asset_id)
    if asset.status_percent != status_percent:
        asset.status_percent = status_percent
        asset.save()

    # TODO(vpeou): If reported_by is none then load the first user (ID=1), else use reported_by.
    # TODO(vpeou): We need to determine a better default user than 1.
    reported_by_id = reported_by.id if reported_by else 1

    # severity_id = Alert().get_highest_alert_severity_for_asset(asset_id)
    # Only update the status if its different than what it already was
    if asset.status_id != status_id:
        # Save status change history detail
        asset_history = StatusHistory()
        asset_history.created = timezone.now()
        asset_history.asset = asset
        asset_history.user_id = reported_by_id
        asset_history.status_id = status_id
        asset_history.prev_status_id = asset.status_id
        asset_history.reason = reason
        asset_history.save()

        # Change status on the actual asset
        asset.status_id = status_id
        asset.status_percent = status_percent
        asset.status_reason = reason
        asset.status_reported_by_id = reported_by_id
        asset.status_is_manually_set = was_manually_set
        asset.save()


def _get_status_from_open_alerts(asset_id):
    # Start at 100%
    status_percent = 1.0

    for multiplier in Asset().get_asset_severity_multipliers(asset_id):
        status_percent *= multiplier

    # Thresholds are based on 100 (not 1) so multiply by 100 before passing it to the function
    status_id = Alert().get_status_from_value(status_percent * 100.0)

    return status_id, status_percent


def roll_status(direction, asset_id, resolved=False, reason='', reported_by=None, was_manually_set=False):
    try:
        # Retrieve linked items
        if direction is RollDirection.UP.value:
            linked_assets = AssetLink.objects.filter(is_parent=True, asset_id=asset_id)
        else:
            linked_assets = AssetLink.objects.filter(is_child=True, asset_id=asset_id)

        # Get status of main asset
        status_id, status_percent = _get_status_from_open_alerts(asset_id)

        for linked_asset in linked_assets:
            if linked_asset.linked_asset_id != linked_asset.asset_id:
                # TODO(vpeou): If reported_by is none then load the first user (ID=1), else use reported_by.
                # TODO(vpeou): We need to determine a better default user than 1.
                reported_by_id = reported_by.id if reported_by else 1

                # Set main asset to the correct status id, based on it's own alerts or linked alerts
                linked_status_id, linked_status_percent = _get_status_from_open_alerts(linked_asset.linked_asset_id)

                # Load the asset in question to update it's status percent, check it's status, and prep update
                asset = get_object_or_404(Asset, pk=linked_asset.linked_asset_id)
                original_status_id = asset.status_id
                if asset.status_percent != linked_status_percent:
                    asset.status_percent = linked_status_percent
                    asset.save()

                asset.status_reason = reason
                asset.status_reported_by_id = reported_by_id
                asset.status_is_manually_set = was_manually_set

                if (resolved is False and status_id > asset.status_id) or linked_status_id != asset.status_id:
                    # If this is a new alert and new status is higher than current one update the current one
                    if status_id > linked_status_id:
                        asset.status_id = status_id
                        asset.status_percent = status_percent
                    else:
                        asset.status_id = linked_status_id
                    asset.save()

                # Only do a save if the value has changed
                if asset.status_id != original_status_id:
                    asset_history = StatusHistory()
                    asset_history.created = timezone.now()
                    asset_history.asset = asset
                    asset_history.user_id = reported_by_id
                    asset_history.status_id = asset.status_id
                    asset_history.prev_status_id = original_status_id
                    asset_history.reason = reason
                    asset_history.save()

    except AssetLink.DoesNotExist:
        pass


class RollDirection(Enum):
    UP = 1
    DOWN = 2


def send_user_alert_notification(user, alert, delivery_method_id):
    try:
        if delivery_method_id is DeliveryMethods.EMAIL.value and user.email is not None:
            trigger_name = alert.trigger.name if alert.trigger else alert.name
            if alert.is_manual:
                trigger_name += " (Manual)"

            content = get_template('alert/emails/alert.html').render(
                {
                    "name": user.first_name + " " + user.last_name,
                    "platform": alert.asset.platform.name,
                    "asset": alert.asset.name,
                    "url": settings.SITE_URL + reverse('alert.detail', kwargs={'alert_id': alert.id}),
                    "trigger": trigger_name,
                    "expression": alert.trigger.expression if alert.trigger else None,
                    "comments": alert.trigger.comments if alert.trigger else None,
                    "details": alert.details
                }
            )

            msg = EmailMessage(
                "OMS++ Alert: {} > {}, {}".format(alert.asset.platform.name, alert.asset.name, trigger_name),
                content,
                settings.DEFAULT_FROM_EMAIL,
                [user.email]
            )
            msg.content_subtype = 'html'
            msg.send()

        if delivery_method_id is DeliveryMethods.TEXT.value:
            profile = Profile.objects.get(pk=user.id)
            if profile.sms_number and profile.sms_carrier:
                carrier = MobileCarrier.objects.get(pk=profile.sms_carrier_id)
                trigger_name = alert.trigger.name if alert.trigger else alert.name
                if alert.is_manual:
                    trigger_name += " (Manual)"

                msg_subject = "OMS++ Alert"
                msg_body = "An alert has been raised for the asset {} > {}: {}" \
                           .format(alert.asset.platform.name, alert.asset.name, trigger_name)
                msg_email_address = carrier.get_email(profile.sms_number)
                msg = EmailMessage(msg_subject, msg_body, settings.DEFAULT_FROM_EMAIL, [msg_email_address])
                msg.send()

        if delivery_method_id is DeliveryMethods.SUMMARY.value:
            # TODO(vpeou): add email summary case
            pass

        # Save to the alert_notifications table
        time_stamp = timezone.now()
        alert_note_email = Notification()
        alert_note_email.created = time_stamp
        alert_note_email.modified = time_stamp
        alert_note_email.has_acknowledged = False
        alert_note_email.alert = alert
        alert_note_email.user = user
        alert_note_email.delivery_method_id = delivery_method_id
        alert_note_email.save()

        return True

    except Exception as e:
        print("Error in send_user_alert_notification:")
        print(e)
        return False


def purge_alerts_for_deployment(deployment):
    alerts = Alert.objects.filter(deployment=deployment)
    amount = 0
    failed = 0

    if len(alerts) > 0:
        for alert in alerts:
            try:
                Alert.delete(alert.id)
                amount += 1
            except:
                failed += 1

    return amount, failed
