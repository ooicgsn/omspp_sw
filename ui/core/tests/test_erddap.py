from django.test import TestCase
from core.models.asset import Asset, AssetType, AssetStatus, Group, Deployment
from core.models.variable import Variable
from core.erddap import Erddap
from urllib.parse import urljoin
import pandas as pd
from collections import OrderedDict
import numpy as np
import numpy.testing as npt

import requests_mock

class TestErddap(TestCase):
    def setUp(self):
        test_url = 'https://cgoms.coas.oregonstate.edu/erddap/'
        # Rely on default ERDDAP URL in livesettings for the time being.
        # A bit of a hack, perhaps, but there didn't seem to be an easy way to
        # alter livesettings in Python
        deployment = Deployment(name='Test Deployment', code='test_deploy')
        deployment.save()

        self.assetPlatform = Asset(name='Test Platform', code='test-platform',
                      type=AssetType.objects.get(name='Platform'),
                      group=Group.objects.get(name='Coastal - Pioneer'),
                      status=AssetStatus.objects.get(name='Ok'),
                      deployment=deployment, erddap_id='test-platform')
        self.assetPlatform.save()

        self.ds_url = urljoin(test_url, 'tabledap/test-asset.csv?time,voltage,int_temp&time%3E=max(time)-2days')
        self.asset = Asset(name='Test Asset', code='test-asset',
                      type=AssetType.objects.get(name='Instrument'),
                      group=Group.objects.get(name='Coastal - Endurance'),
                      status=AssetStatus.objects.get(name='Ok'),
                      deployment=deployment, erddap_id='test-asset',
                      platform_id=self.assetPlatform.id)

        self.asset.save()

        self.variable = Variable(name='voltage', asset=self.asset,
                                 erddap_id='voltage')
        self.variable2 = Variable(name='int_temp', asset=self.asset,
                                 erddap_id='int_temp')
        self.variable.save()
        # mock response for the CSV
        self.csv_mock = '''time,voltage,int_temp
UTC,V,dgC
2017-03-20T00:00:00Z,14.01,10
2017-03-20T00:01:00Z,14.0,
2017-03-20T00:02:00Z,12.7,
2017-03-20T00:03:00Z,14.0,12
2017-03-20T00:04:00Z,13.99998,19
2017-03-20T00:05:00Z,13.99998,24
2017-03-20T00:06:00Z,13.9543,22'''

        self.dud_ds_url = urljoin(test_url,
                                              'tabledap/dud-asset.csv?time,dud&time%3E=now-2days')

        self.dud_asset = Asset(name='Dud Asset', code='dud-asset',
                               type=AssetType.objects.get(name='Instrument'),
                               group=Group.objects.get(name='Coastal - Endurance'),
                               status=AssetStatus.objects.get(name='Ok'),
                               deployment=deployment, erddap_id='dud-asset',
                               platform_id=self.assetPlatform.id)
        self.dud_asset.save()

        self.dud_variable = Variable(name='dud', asset=self.dud_asset,
                                 erddap_id='dud')
        self.dud_variable.save()

        self.csv_mock_dud = '''HTTP Status 500 - Your query produced no matching results (nRows = 0)'''
        self.csv_mock_dud2 = 'Bad Gateway'

    def test_rate_of_change_time(self):
        """Test rate of change"""
        with requests_mock.mock() as m:
            m.get(self.ds_url, text=self.csv_mock)

            df, _ = Erddap().fetch_data(OrderedDict([('voltage', self.variable),
                                                  ('int_temp', self.variable2)]),
                                     None, 'dud', relative_to='max_time',
                                     change_interval=pd.Timedelta('10 seconds'))


            # 14.00 - 14.01 / (60 / 10) ~= -0.0016666...
            npt.assert_almost_equal(df.voltage.iloc[0], -0.01 / 6)
            # first entry is followed by NaN, so the rate of change is NaN
            # between the points
            self.assertTrue(np.isnan(df.int_temp.iloc[0]))

    def test_fetch_empty(self):
        """Tests fetching an empty ERDDAP result"""
        with requests_mock.mock() as m:
            m.get(self.dud_ds_url, text=self.csv_mock_dud, status_code=500)
            df, messages = Erddap().fetch_data({'dud': self.dud_variable},
                                                   None, 'dud')
            self.assertTrue(messages[0].startswith('No ERDDAP data in range'))


    def test_fetch_misc_error(self):
        """
        Tests fetching a result with a miscellaneous error message.
        Catchall for non-empty data errors
        """
        with requests_mock.mock() as m:
            m.get(self.dud_ds_url, text=self.csv_mock_dud2, status_code=502)
            df, _ = Erddap().fetch_data({'dud': self.dud_variable},
                                                   None, 'dud')
            self.assertTrue(df._empty_reasons[0].startswith(
                             "HTTP Status 502: Bad Gateway"))
