from django.test import TestCase
from numpy import dtype
from ..helpers import validate_expression


class TestHelpers(TestCase):

    def test_expression_validation(self):
        """
        Test that the expression validation returns the proper response.
        Check that valid expressions return True as the first element, or
        False as the first element if the expression is not valid.
        """
        variables = ['int_temp', 'voltage']

        ex_dtype = dtype('bool')
        self.assertTrue(validate_expression('voltage < 13 & int_temp < 40',
                                            variables, ex_dtype)[0])
        # should fail because "q" and "v" are not defined
        test_expr = 'q + v > 5'
        self.assertFalse(validate_expression(test_expr, variables, ex_dtype)[0])
        # non-boolean result when dtype is set to boolean, should fail
        test_expr = 'int_temp + voltage'
        self.assertFalse(validate_expression(test_expr, variables, ex_dtype)[0])
        # when dtype is unset, the expression should pass
        self.assertTrue(validate_expression(test_expr, variables, None)[0])
        # invalid operator should fail
        test_expr = 'int_temp @@@ voltage > 5'
        self.assertFalse(validate_expression(test_expr, variables, ex_dtype)[0])
