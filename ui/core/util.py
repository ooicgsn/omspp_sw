import re
from functools import wraps
from enum import Enum

from django.shortcuts import redirect
from django.http import JsonResponse
from django.urls import resolve, Resolver404
from django.conf import settings


class HttpStatusCode(Enum):
    """ 
        Pre-defined http status codes; these do not appear else where in Django, except in additional modules 
        that are not being used
    """
    OK = 200
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403


def enum_choices(values):
    """ Converts an emum into a list of key/value pairs that can be used as the limit_to_choices filed on models """
    return tuple([(e.value, e.name) for e in values])


def apply_classifications(assets):
    from core.models.asset import AssetClassification
    classifications = AssetClassification.objects.all()

    for asset in assets:
        found = next((classification for classification in classifications if re.match(classification.pattern, asset.code, re.IGNORECASE) is not None), None)
        if not found:
            continue

        asset.classification = found
        asset.save()


def DataTablesResponse(data):
    """ 
        Applies JSON wrapper needed for data to work as a DataTables ajax source. Naming style is used to mimic 
        other responses like JsonResponse
    """
    if len(data) == 0:
        return JsonResponse({'data': []})

    # DataTables expects a list of list, but a database call will return a list of tuples; fix the data if needed
    if isinstance(data[0], tuple):
        data = [list(row) for row in data]

    return JsonResponse({ 'data': data })


def route_name(url):
    """ Safely tries to resolve a URL based on the name, returning an empty string if it wasn't found"""
    try:
        return resolve(url).url_name

    except Resolver404:
        return ''


def require_perm(perm):
    """
        Decorator that can be used on views to restrict access to those with the proper permissions. Instead of
        using the built-in Django decorator, this one defaults to redirecting the user back to the predefined
        dashboard URL. It also handles not prefixing the permission with the app name if needed
    """
    def decorator(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if not check_perm(request, perm):
                return redirect(settings.DASHBOARD_URL)

            return func(request, *args, **kwargs)
        return wrapper
    return decorator


def check_perm(request, perm):
    """
        A simple wrapper to check if the current user has permission to do something. It will prepend our
        default app 'core' if one isn't specified in the permission name
    """
    if not request.user:
        return False

    if '.' not in perm:
        perm = 'core.' + perm

    return request.user.has_perm(perm)


def parse_querystring_list(qs, default_value):
    """
        Attempts to parse a comma separated list from a querystring. Allows for a default value if no data is
        supplied for the field, as well as a special value of 0 to indicate an empty list (not the default value)
    """
    qs = qs or ''

    if qs == '':
        return default_value

    if str(qs) == '0':
        return []

    return [int(value) for value in qs.split(',')]


def parse_deployment_number(deployment_code):
    """
        Attemps to parse a deployment number, which contains letters and numbers in order to produce an integer
        value that can be used for sorting. Returns 0 if a number cannot be found (rather than throwing an exception)
    """
    try:
        return int(re.sub('[^0-9]', '', deployment_code))
    except:
        return 0


def parse_deployment_code_leading_zeros(deployment_code):
    """
        Attemps to parse a deployment string, which contains letters and numbers in order to produce an integer
        value that can be used for sorting. Returns 0 if a number cannot be found (rather than throwing an exception)
    """
    try:
        return re.sub('[^0-9]', '', deployment_code)
    except:
        return 0


def print_success(cmd, msg):
    """
        Helper command to print status messages to the console with color coding
    """
    if cmd.stdout:
        cmd.stdout.write(cmd.style.SUCCESS(msg))
        cmd.stdout.flush()
    else:
        print(msg)


def print_error(cmd, msg):
    """
        Helper command to print status messages to the console with color coding
    """
    if cmd.stdout:
        cmd.stdout.write(cmd.style.ERROR(msg))
        cmd.stdout.flush()
    else:
        print(msg)


def print_info(cmd, msg):
    """
        Helper command to print status messages to the console with color coding
    """
    if cmd.stdout:
        cmd.stdout.write(msg)
        cmd.stdout.flush()
    else:
        print(msg)
