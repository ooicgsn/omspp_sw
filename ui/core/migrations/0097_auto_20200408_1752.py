# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2020-04-08 17:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0096_auto_20200408_1754'),
    ]

    operations = [
        migrations.RunSQL("DROP FUNCTION IF EXISTS public.get_custom_variables();"),
        migrations.RunSQL("""
            create or replace function get_custom_variables(disp_list varchar)
                returns TABLE(
                    id integer, 
                    name character varying, 
                    units character varying, 
                    asset_name character varying, 
                    created timestamp with time zone, 
                    asset_id integer, 
                    platform_id integer, 
                    deployment_name character varying, 
                    is_global boolean, 
                    user_id integer, 
                    user_name character varying, 
                    first_name character varying, 
                    last_name character varying, 
                    num_triggers bigint, 
                    num_plots bigint)
                language sql
            as
            $$
                SELECT      variable.id,
                            variable.name,
                            variable.units,
                            asset.name,
                            variable.created,
                            variable.asset_id,
                            variable.platform_id,
                            deployment.name,
                            variable.is_global,
                            variable.user_id,
                            auth_user.username,
                            auth_user.first_name,
                            auth_user.last_name,
                            (
                                SELECT      COUNT(DISTINCT atv.trigger_id)
                                FROM        alert_trigger_variable atv
                                WHERE       atv.variable_id = variable.id
                            ) AS num_triggers,
                            (
                                SELECT      COUNT(DISTINCT pv.plot_id)
                                FROM        plot_variable pv
                                WHERE       pv.variable_id = variable.id
                            ) AS num_plots
                FROM        variable
                INNER JOIN  asset
                            ON asset.id = variable.asset_id
                LEFT JOIN   auth_user
                            ON auth_user.id = variable.user_id
                INNER JOIN  deployment
                            ON deployment.id = asset.deployment_id
                INNER JOIN  asset platform
                            ON platform.id = variable.platform_id
                WHERE       variable.is_custom = TRUE 
                            AND (
                                $1 is null
                                or asset.disposition_id::text = ANY(string_to_array($1,','))
                            )
                ORDER BY    variable.name
            $$;
        """)
    ]
