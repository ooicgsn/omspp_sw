# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-12-24 19:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0047_auto_20171220_2024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='is_active',
            field=models.NullBooleanField(default=True),
        ),
    ]
