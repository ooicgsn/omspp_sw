# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0099_auto_20200528_1646'),
    ]

    operations = [
        migrations.RunSQL("DROP FUNCTION IF EXISTS get_plots(platform_id integer, disp_list varchar);"),
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.get_plots(platform_id integer, disp_list varchar)
              RETURNS TABLE(id integer, name character varying, is_global character varying, platform_id integer, platform_name character varying, asset_id integer, asset_name character varying, deployment_code character varying, deployment_id integer, created timestamp with time zone, "user" character varying, user_id integer) AS
            $BODY$

                        SELECT		plot.id AS id,
                                plot.name as "name",
                                CASE WHEN plot.is_global = TRUE THEN 'Yes' ELSE 'No' END AS is_global,
                                COALESCE(platform.id, asset.id) AS platform_id,
                                COALESCE(platform.name, asset.name) AS platform_name,
                                asset.id AS asset_id,
                                asset.name AS asset_name,
                                deployment.code AS deployment_code,
                                deployment.id AS deployment_id,
                                plot.created,
                                COALESCE(CONCAT(auth_user.first_name, ' ', auth_user.last_name), username) AS "user",
                                auth_user.id AS user_id
                        FROM		plot
                        INNER JOIN	asset asset
                                ON asset.id = plot.asset_id
                        INNER JOIN	deployment
                                ON asset.deployment_id = deployment.id
                        LEFT JOIN	asset platform
                                ON asset.platform_id = platform.id
                        LEFT JOIN	auth_user
                                ON plot.user_id = auth_user.id
                        WHERE		(
                                    $1 IS NULL
                                    OR asset.platform_id = $1
                                ) and (
                                        $2 is null
                                        or platform.disposition_id::text = ANY(string_to_array($2,','))
                                )

                        $BODY$
              LANGUAGE sql VOLATILE
              COST 100
              ROWS 1000;
        """),
        migrations.RunSQL("DROP FUNCTION IF EXISTS get_custom_variables(disp_list varchar)"),
        migrations.RunSQL("""
            create or replace function get_custom_variables(disp_list varchar)
                returns TABLE(
                    id integer,
                    name character varying,
                    units character varying,
                    asset_name character varying,
                    created timestamp with time zone,
                    asset_id integer,
                    platform_id integer,
                    deployment_name character varying,
                    is_global boolean,
                    user_id integer,
                    user_name character varying,
                    first_name character varying,
                    last_name character varying,
                    num_triggers bigint,
                    num_plots bigint,
                    platform_name character varying)
                language sql
            as
            $$
                SELECT      variable.id,
                            variable.name,
                            variable.units,
                            asset.name,
                            variable.created,
                            variable.asset_id,
                            variable.platform_id,
                            deployment.name,
                            variable.is_global,
                            variable.user_id,
                            auth_user.username,
                            auth_user.first_name,
                            auth_user.last_name,
                            (
                                SELECT      COUNT(DISTINCT atv.trigger_id)
                                FROM        alert_trigger_variable atv
                                WHERE       atv.variable_id = variable.id
                            ) AS num_triggers,
                            (
                                SELECT      COUNT(DISTINCT pv.plot_id)
                                FROM        plot_variable pv
                                WHERE       pv.variable_id = variable.id
                            ) AS num_plots,
                            platform.name
                FROM        variable
                INNER JOIN  asset
                            ON asset.id = variable.asset_id
                LEFT JOIN   auth_user
                            ON auth_user.id = variable.user_id
                INNER JOIN  deployment
                            ON deployment.id = asset.deployment_id
                INNER JOIN  asset platform
                            ON platform.id = variable.platform_id
                WHERE       variable.is_custom = TRUE
                            AND (
                                $1 is null
                                or platform.disposition_id::text = ANY(string_to_array($1,','))
                            )
                ORDER BY    variable.name
            $$;
        """)
    ]
