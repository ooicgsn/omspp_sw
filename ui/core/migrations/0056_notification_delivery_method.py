# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-01-24 16:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0055_auto_20180112_2000'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='delivery_method',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.DeliveryMethod'),
        ),
    ]
