# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-10-12 17:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_alert_mandatory'),
    ]

    operations = [
        migrations.AddField(
            model_name='alert',
            name='roll_down',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='alert',
            name='roll_up',
            field=models.BooleanField(default=False),
        ),
    ]
