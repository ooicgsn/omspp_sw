# Generated by Django 4.2.4 on 2023-12-15 04:01

from django.db import migrations


def apply_migration(apps, schema_editor):
    db_alias = schema_editor.connection.alias

    TriggerType = apps.get_model('core', 'TriggerType')
    TriggerType.objects.using(db_alias).bulk_create([
        TriggerType(id=1, name='Expression'),
        TriggerType(id=2, name='Last Update')
    ])


def revert_migration(apps, schema_editor):
    TriggerType = apps.get_model('core', 'TriggerType')
    TriggerType.objects.filter(name='Expression').delete()
    TriggerType.objects.filter(name='Last Update').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0116_triggertype_trigger_trigger_type'),
    ]

    operations = [
        migrations.RunPython(apply_migration, revert_migration)
    ]
