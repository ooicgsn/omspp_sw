# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-11-24 18:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0044_remove_asset_current_deployment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assetdeployment',
            name='asset',
        ),
        migrations.RemoveField(
            model_name='assetdeployment',
            name='deployment',
        ),
        migrations.RemoveField(
            model_name='deployment',
            name='assets',
        ),
        migrations.DeleteModel(
            name='AssetDeployment',
        ),
        migrations.RunSQL("DROP FUNCTION public.get_sidebar_assets();"),
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.get_sidebar_assets()
              RETURNS TABLE(asset_id integer, group_id integer, group_name text, group_is_glider boolean, name text, code text, deployment text, status_id integer) AS
            $BODY$
                                                            
            WITH assets AS (
                SELECT      asset.id AS asset_id,
                    group_.id AS group_id,
                    group_.name AS group_name,
                    group_.is_glider AS group_is_glider,
                    asset.name AS name,
                    asset.code AS code,
                    deployment.code AS deployment,
                    asset.status_id AS status_id,
                    ROW_NUMBER() OVER (
                        PARTITION BY asset.code
                        ORDER BY substring(deployment.code from '[0-9]+') DESC
                    )
                FROM        asset_group group_
                LEFT JOIN   asset
                    ON asset.group_id = group_.id
                    AND asset.type_id = 1 -- Mooring
                LEFT JOIN   deployment
                    ON deployment_id = deployment.id
            )
            SELECT 		asset_id, group_id, group_name, group_is_glider, name, code, deployment, status_id 
            FROM 		assets
            WHERE 		row_number = 1
            ORDER BY    	group_name, code
                                                            
                                                                        $BODY$
              LANGUAGE sql VOLATILE
              COST 100
              ROWS 1000;
        """),
        migrations.RunSQL("DROP FUNCTION public.get_hotlist_alerts(integer);"),
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.get_hotlist_alerts(IN user_id integer)
              RETURNS TABLE(alert_notification_id integer, alert_id integer, trigger_id integer, asset_id integer, asset_code text, deployment_code text, trigger_name text, severity_name text, details text, alert_date timestamp with time zone, platform_id integer, platform_code text, is_manual boolean, name character varying, last_occurrence timestamp with time zone, severity_id integer, has_acknowledged boolean) AS
            $BODY$
                                                                                    
                                                                                            SELECT      alert_notification.id,
                                                                                                        alert.id,
                                                                                                        alert_trigger.id,
                                                                                                       asset.id,
                                                                                                        asset.code,
                                                                                                        deployment.code,
                                                                                                        alert_trigger.name,
                                                                                                        alert_severity.name,
                                                                                                        alert.details,
                                                                                                        alert.created,
                                                                                                        COALESCE(platform.id, asset.id) AS platform_id,
                                                                                                        COALESCE(platform.code, asset.code) AS platform_code,
                                                                                                        alert.is_manual,
                                                                                                        alert.name,
                                                                                                        alert.last_occurrence,
                                                                                                        alert_severity.id,
                                                                                                        alert_notification.has_acknowledged
                                                                                            FROM        alert_notification
                                                                                            INNER JOIN  alert
                                                                            ON alert_id = alert.id
                                                                                            INNER JOIN  asset
                                                                                                        ON asset_id = asset.id
                                                                                            INNER JOIN  deployment
                                                                                                        ON asset.deployment_id = deployment.id
                                                                                            LEFT JOIN  alert_trigger
                                                                                                        ON trigger_id = alert_trigger.id
                                                                                            INNER JOIN  alert_severity
                                                                                                        ON alert.severity_id = alert_severity.id
                                                                                            LEFT JOIN asset platform
                                                                                    ON platform.id = asset.platform_id
                                                                                        WHERE          alert_notification.user_id = $1
                                                                                            AND           alert.status_id <> 4 -- Resolved
                                                                                        ORDER BY     alert.created DESC
                                                                        
                                                                                            $BODY$
              LANGUAGE sql VOLATILE
              COST 100
            ROWS 1000;
        """)

    ]
