# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-01-02 19:07
from __future__ import unicode_literals

from django.db import migrations

def create_carriers(apps, schema_editor):
    MobileCarrier = apps.get_model('core', 'MobileCarrier')
    MobileCarrier.objects.bulk_create([
        MobileCarrier(pk=5, name='Project Fi'),
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0050_auto_20171228_1607'),
    ]

    operations = [
        migrations.RunPython(create_carriers)
    ]
