# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-04-23 14:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion

def create_plot_time_ranges(apps, schema_editor):
    PlotTimeRange = apps.get_model('core', 'PlotTimeRange')
    PlotTimeRange.objects.bulk_create([
        PlotTimeRange(pk=1, days=-1, name='Last 24 Hours'),
        PlotTimeRange(pk=2, days=-2, name='Last 2 Days'),
        PlotTimeRange(pk=3, days=-3, name='Last 3 Days'),
        PlotTimeRange(pk=4, days=-7, name='Last 7 Days'),
        PlotTimeRange(pk=5, days=-30, name='Last 30 Days'),
        PlotTimeRange(pk=6, days=-182, name='Last 6 Months'),
        PlotTimeRange(pk=7, days=-365, name='Last Year'),
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0076_time_rate_change_interval'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlotTimeRange',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('days', models.IntegerField(blank=True, null=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'plot_time_range',
            },
        ),
        migrations.RunPython(create_plot_time_ranges),
        migrations.AddField(
            model_name='plot',
            name='plot_time_range',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.PlotTimeRange'),
        ),
        migrations.RunSQL("UPDATE plot SET plot_time_range_id = 1"),
    ]
