# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-03-29 06:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0069_auto_20180328_0601'),
    ]

    operations = [
        migrations.CreateModel(
            name='Summary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'summary',
            },
        ),
        migrations.CreateModel(
            name='SummaryAlertCount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_alerts', models.IntegerField(default=0)),
                ('platform', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Asset')),
                ('severity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Severity')),
                ('summary', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Summary')),
            ],
            options={
                'db_table': 'summary_alert_count',
            },
        ),
        migrations.CreateModel(
            name='SummaryGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status_percent', models.DecimalField(blank=True, decimal_places=2, default=1.0, max_digits=3, null=True)),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Group')),
                ('summary', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Summary')),
            ],
            options={
                'db_table': 'summary_group',
            },
        ),
        migrations.CreateModel(
            name='SummaryPlatform',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('telemetry_percent', models.DecimalField(blank=True, decimal_places=2, default=1.0, max_digits=3, null=True)),
                ('data_percent', models.DecimalField(blank=True, decimal_places=2, default=1.0, max_digits=3, null=True)),
                ('power_percent', models.DecimalField(blank=True, decimal_places=2, default=1.0, max_digits=3, null=True)),
                ('functional_percent', models.DecimalField(blank=True, decimal_places=2, default=1.0, max_digits=3, null=True)),
                ('platform', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Asset')),
                ('summary', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Summary')),
            ],
            options={
                'db_table': 'summary_platform',
            },
        ),
    ]
