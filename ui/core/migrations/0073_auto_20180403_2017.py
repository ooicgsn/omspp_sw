# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-04-03 20:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_auto_20180402_1755'),
    ]

    operations = [
        migrations.RunSQL("DROP FUNCTION public.get_group_platforms(IN group_id integer);"),
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.get_group_platforms(IN group_id integer)
              RETURNS TABLE(platform_id integer, platform_code character varying, deployment_code character varying, disposition_id integer, alerts bigint) AS
            $BODY$
                                                
            SELECT  asset.id,
                    asset.code,
                    deployment.code,
                    asset.disposition_id,
                    (
                        SELECT      COUNT(*)
                        FROM        asset_link
                        INNER JOIN  alert
                                    ON alert.asset_id = linked_asset_id
                                    AND alert.status_id = 1
                        WHERE       asset_link.asset_id = asset.id
                    )
            FROM            asset
            INNER JOIN      deployment
                            ON asset.deployment_id = deployment.id
            WHERE           group_id = $1
                            AND type_id = 1 -- Platform
            
            $BODY$
              LANGUAGE sql VOLATILE
              COST 100
              ROWS 1000;
        """)
    ]
