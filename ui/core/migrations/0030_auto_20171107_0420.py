# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-11-07 04:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_status_change_updates'),
    ]

    operations = [
        migrations.CreateModel(
            name='MobileCarrier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'mobile_carrier',
            },
        ),
        migrations.AddField(
            model_name='profile',
            name='sms_number',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='sms_carrier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.MobileCarrier'),
        ),
    ]
