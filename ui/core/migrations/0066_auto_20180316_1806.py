# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-03-16 18:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0065_auto_20180315_1455'),
    ]

    operations = [
        migrations.RunSQL("DROP FUNCTION public.get_asset_alerts(integer, boolean);"),
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.get_asset_alerts(
                IN asset_id integer,
                IN include_all boolean)
              RETURNS TABLE(alert_id integer, created timestamp with time zone, trigger_name character varying, status character varying, asset character varying, severity character varying, severity_id integer, last_occurrence timestamp with time zone) AS
            $BODY$
                                                BEGIN
                                                
                                                RETURN QUERY
                                                SELECT  alert.id AS alert_id,
                                                        alert.created,
                                                        CASE WHEN alert_trigger.id IS NULL THEN alert.name ELSE alert_trigger.name END AS trigger_name,
                                                        alert_status.name AS status,
                                                        asset.name AS asset,
                                                        alert_severity.name AS severity,
                                                        alert.severity_id AS severity_id,
                                                        alert.last_occurrence
                                                FROM	asset_link
                                                INNER JOIN	alert
                                                        ON alert.asset_id = linked_asset_id
                                                INNER JOIN	asset
                                                        ON alert.asset_id = asset.id
                                                LEFT JOIN	alert_trigger
                                                        ON alert_trigger.id = trigger_id
                                                INNER JOIN	alert_status
                                                        ON alert.status_id = alert_status.id
                                                        AND (
                                                            $2 = TRUE
                                                            OR alert.status_id = 1 -- Open
                                                        )
                                                INNER JOIN	alert_severity
                                                        ON alert.severity_id = alert_severity.id
                                                WHERE		asset_link.asset_id = $1
                                                        AND is_child = TRUE;
                                                
                                                END
                                                $BODY$
              LANGUAGE plpgsql VOLATILE
              COST 100
              ROWS 1000;
        """)
    ]
