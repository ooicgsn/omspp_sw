import re

from django.core.management.base import BaseCommand

from core.models.asset import Asset

from core.util import apply_classifications


class Command(BaseCommand):
    help = 'Applies asset classifications to all assets'

    def handle(self, *args, **options):
        assets = Asset.objects.filter(classification=None)
        apply_classifications(assets)


