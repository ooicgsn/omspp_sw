import os, sys, traceback

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.db import connection

# TODO: livesettings no longer exists (but rebuildign the database should not be used anyway...)
from livesettings.functions import ConfigurationSettings


class Command(BaseCommand):
    help = 'Drops and recreates the OMS++ database, runs migrations, and loads initial data'

    def add_arguments(self, parser):
        parser.add_argument('--nodrop', dest='nodrop', action='store_true', help='Do not drop and recreate the database (which is the default')
        parser.add_argument('-es', dest='erddap_server', help='ERDDAP server URL')
        parser.add_argument('-eu', dest='erddap_username', help='Username for HTTP authentication to ERDDAP server (if required)')
        parser.add_argument('-ep', dest='erddap_password', help='Password for HTTP authentication to ERDDAP server (if required)')

    def handle(self, *args, **options):
        db_name = connection.settings_dict['NAME']
        nodrop = options['nodrop']
        erddap_server = options['erddap_server']
        erddap_username = options['erddap_username']
        erddap_password = options['erddap_password']

        try:
            if not nodrop:
                print("[+] Dropping and creating database")
                if os.system("sudo -u postgres psql -c 'DROP DATABASE {};'".format(db_name)) != 0:
                    raise Exception("Failed to drop database. Make sure any connections are closed, including a running web server or database GUI tool")

                if os.system("sudo -u postgres psql -c 'CREATE DATABASE {};'".format(db_name)) != 0:
                    raise Exception("Failed to create new database")

            print("[+] Running migrations")
            call_command('migrate', interactive=False)

            print('[+] Updating settings from any command line arguments')
            if erddap_username or erddap_password or erddap_server:
                settings = ConfigurationSettings()
                settings.get_config('ERDDAP', 'USERNAME').update(erddap_username)
                settings.get_config('ERDDAP', 'PASSWORD').update(erddap_password)
                settings.get_config('ERDDAP', 'SERVER_URL').update(erddap_server)

            print("[+] Loading initial data")
            call_command('load_initial_data')

            print("[+] Updating permissions")
            call_command('update_permissions', interactive=False)

            print("[+] Updating asset last_update times")
            call_command('get_latest_updates')

            print("[+] Updating Redmine configuration")
            call_command('update_redmine')

        except:
            traceback.print_exc(file=sys.stdout)
            self.stdout.write(self.style.ERROR("Failed to rebuild database"))
            return

        self.stdout.write(self.style.SUCCESS("Database has been rebuilt successfully"))

