import os

from django.conf import settings
from django.core.management.base import BaseCommand

from core.models.asset import Asset, Deployment

class Command(BaseCommand):
    help = 'Helper script to transition existing production assets to the new load scripts'

    CONFIG_LOGS = os.path.join(settings.BASE_DIR, 'external_scripts/ConfigLogs')

    def handle(self, *args, **options):
        for filename in os.listdir(self.CONFIG_LOGS):
            full_path = os.path.join(self.CONFIG_LOGS, filename)

            data = None
            with open(full_path) as file:
                data = file.readlines()

            if not data:
                return

            platform_name = filename.split('.')[0].upper()
            deployment_code = None
            for line in data:
                if line.startswith('Deploy.id'):
                    deployment_code = line.split('=')[1].strip()

            if not deployment_code:
                return

            platform = Asset.objects.get(name=platform_name)
            assets = Asset.objects.filter(platform=platform)

            print("Creating deployment {} for {}".format(deployment_code, platform_name))

            deployment = Deployment()
            deployment.code = deployment_code
            deployment.name = deployment_code
            deployment.save()

            for asset in assets:
                asset.deployment = deployment
                asset.save()
