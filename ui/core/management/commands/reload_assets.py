from django.core.management.base import BaseCommand
from core.models.asset import Asset
from core.importer import AssetImporter


class Command(BaseCommand):
    help = 'Reloads the configuration for some or all moorings'

    def add_arguments(self, parser):
        parser.add_argument('--platform', type=str, default=None, help='Platform code, e.g. CP01CNSM')
        parser.add_argument('--deployment', type=str, default=None, help='Platform code, e.g. D0016')

    def handle(self, *args, **options):
        assets = Asset.objects\
            .filter(parent=None)\
            .exclude(group__is_glider=True)\
            .values('code', 'deployment__code')

        platform = options.get('platform')
        if platform:
            print("Limit to platform: " + platform)
            assets = assets.filter(code=platform)

        deployment = options.get('deployment')
        if deployment:
            print("Limit to deployment: " + deployment)
            assets = assets.filter(deployment__code=deployment)

        if len(assets) == 0:
            print("No matching assets found.")
            return

        assets = sorted(assets, key=lambda x: (x['code'], x['deployment__code']))
        for asset in assets:
            platform_code = asset['code']
            deployment_code = asset['deployment__code']

            print('\n===== Reloading configuration for {} ({}) ====='.format(
                platform_code, deployment_code
            ))

            config_path = Asset.get_config_path(platform_code, deployment_code)
            if not config_path:
                print("No platform config found.")
                continue

            print('Config: {}'.format(config_path))

            AssetImporter().import_asset(
                config_path,
                form_platform_code=platform_code,
                form_deployment_code=deployment_code)

