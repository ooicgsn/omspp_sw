from django.core.management.base import BaseCommand

from core.redmine import Redmine

class Command(BaseCommand):
    help = 'Retrieves the list of available projects (and the respective IDs) from Redmine'

    def add_arguments(self, parser):
        parser.add_argument('api_key', type=str, help='Redmine API Key')

    def handle(self, *args, **options):
        api_key = options.get('api_key')

        projects = Redmine(api_key).get_projects()
        for id,name in projects.items():
            print("{0: > 4}: {1}".format(id, name))

