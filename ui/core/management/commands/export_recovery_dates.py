import requests
import pandas as pd
from io import BytesIO
import csv
from django.conf import settings
from django.core.management.base import BaseCommand
from core.models.asset import Asset


class Command(BaseCommand):
    help = """
    """

    def handle(self, *args, **options):

        platforms = Asset.objects \
            .filter(parent_id__isnull=True, group__is_glider=False, recovery_date__isnull=False) \
            .select_related('deployment') \
            .select_related('group')

        filename = 'recovery_dates.csv'
        with open(filename, 'w') as f:
            writer = csv.writer(f)
            for platform in platforms:
                writer.writerow([platform.code, platform.deployment.code, platform.recovery_date, platform.deployment_date])

            print("Wrote {} rows to {}".format(len(platforms), filename))
