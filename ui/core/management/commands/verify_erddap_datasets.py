import requests
import pandas as pd
from io import BytesIO
from django.conf import settings
from django.core.management.base import BaseCommand
from core.models.asset import Asset


class Command(BaseCommand):
    help = """
        Matches dataset id's against the dataset (ERDDAP) id on each asset. Compares each dataset within ERDDAP to
        each deployment for the matching mooring
    """

    def add_arguments(self, parser):
        parser.add_argument('--match', default=None, action='store_true',
                            help='Only include assets that match the dataset id.')
        parser.add_argument('--no-match', dest='match', action='store_false',
                            help='Only include assets that do not match the dataset id.')

    def handle(self, *args, **options):
        match_filter = options['match']
        user = settings.ERDDAP_USERNAME
        password = settings.ERDDAP_PASSWORD
        auth = requests.auth.HTTPBasicAuth(user, password)
        base_url = settings.ERDDAP_URL
        url = '{}/tabledap/allDatasets.csv?datasetID,title'.format(base_url)

        print('Checking ERDDAP dataset IDs from: {}'.format(url))
        if match_filter is not None:
            print(' Include only {} assets.'.format('FOUND' if match_filter else 'NOT FOUND'))

        try:
            req = requests.get(url, auth=auth)
        except Exception as e:
            print('Failed to connect to ERDDAP: {}'.format(str(e)))
            return

        columns = ['datasetID']
        content = pd.read_csv(BytesIO(req.content), header=0, usecols=columns)
        rows = content[content.notnull()]

        last_code = ''
        for index, row in rows.iterrows():
            dataset_id = str(row['datasetID'])
            platform_code = dataset_id.split('-')[0]

            if last_code and last_code != platform_code:
                print("\n\n" + platform_code)

            last_code = platform_code

            if dataset_id.endswith('allDatasets') or dataset_id == 'nan':
                continue

            print("  {} on {}:".format(dataset_id, platform_code))
            platforms = Asset.objects\
                .filter(code=platform_code, parent_id__isnull=True)\
                .select_related('deployment')

            for platform in platforms:
                assets = list(Asset.objects.filter(
                    platform=platform,
                    deployment=platform.deployment,
                    erddap_id=dataset_id
                ))

                names = []
                for asset in assets:
                    names.append('{} ({})'.format(asset.code, asset.id))

                show = options['match'] is None or \
                       (not match_filter and len(assets) == 0) or \
                       (match_filter and len(assets) > 0)

                if show:
                    status = ', '.join(names) if len(assets) > 0 else '** No Match **'
                    print('    {} ({}): {}'.format(platform.code, platform.deployment.code, status))
