import requests
import pandas as pd
from io import BytesIO
from django.conf import settings
from django.core.management.base import BaseCommand
from core.models.asset import Asset


class Command(BaseCommand):
    help = """
        Matches dataset id's against the dataset (ERDDAP) id on each asset. Compares each dataset within ERDDAP to
        each deployment for the matching mooring, and counts (and then summarizes) the results into CSV format
    """
    def handle(self, *args, **options):
        user = settings.ERDDAP_USERNAME
        password = settings.ERDDAP_PASSWORD
        auth = requests.auth.HTTPBasicAuth(user, password)
        base_url = settings.ERDDAP_URL
        url = '{}/tabledap/allDatasets.csv?datasetID,title'.format(base_url)

        try:
            req = requests.get(url, auth=auth)
        except Exception as e:
            print('Failed to connect to ERDDAP: {}'.format(str(e)))
            return

        columns = ['datasetID', 'title']
        content = pd.read_csv(BytesIO(req.content), header=0, usecols=columns)
        rows = content[content.notnull()]

        print('Mooring,Dataset ID,Title,Deployments Matched,Assets Matched')
        for index, row in rows.iterrows():
            dataset_id = str(row['datasetID'])
            title = str(row['title'])
            platform_code = dataset_id.split('-')[0]

            if dataset_id.endswith('allDatasets') or dataset_id == 'nan':
                continue

            platforms = Asset.objects\
                .filter(code=platform_code, parent_id__isnull=True)\
                .select_related('deployment')

            platforms_matched = 0
            assets_matched = 0
            for platform in platforms:
                assets = list(Asset.objects.filter(
                    platform=platform,
                    deployment=platform.deployment,
                    erddap_id=dataset_id
                ))

                platforms_matched += 1 if len(assets) > 0 else 0
                assets_matched += len(assets)

            print('{},{},{},{},{}'.format(platform_code, dataset_id, title, platforms_matched, assets_matched))