import os
import json
from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.conf import settings

class Command(BaseCommand):
    help = 'Updates groups and permissions based on a pre-defined configuration file'

    def handle(self, *args, **options):
        print("Synchronizing permissions...")

        print("[+] Reading configuration file")
        with open(os.path.join(settings.CONFIG_DIR, "permissions.json")) as permissions_file:
            data = json.load(permissions_file)

        # TODO(mchagnon): Handled delete groups (should we even allow this? Flag as deleted? At least confirm delete)
        print("[+] Updating groups")
        groups = {}
        for name in data['groups']:
            groups[name] = Group.objects.get_or_create(name=name)[0]

        print('[+] Updating permissions')
        for perm in data['permissions']:
            ct = ContentType.objects.get(model=perm['model'])

            permission = Permission.objects.get_or_create(
                codename=perm['code'],
                content_type=ct
            )[0]

            permission.name = perm['name']

            # Add missing groups
            for name in perm['groups']:
                if not permission.group_set.filter(name=name).exists():
                    permission.group_set.add(groups[name])

            # Removes permissions from groups it is no longer set on
            for group_permission in permission.group_set.all():
                if not group_permission.name in perm['groups']:
                    permission.group_set.remove(group_permission)

        print('[+] Removing stale permissions')
        config_perms = [perm['code'] for perm in data['permissions']]
        Permission.objects.exclude(codename__in=config_perms).delete()
