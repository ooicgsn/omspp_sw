import os, json, shutil

from django.conf import settings
from django.core.management.base import BaseCommand

CONFIG_DIR = os.path.join(settings.BASE_DIR, 'external_scripts/ConfigLogs')

class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        print("Creating Raw Data Folder Structure...")

        # Create the data directory if it does not already exist
        os.makedirs(settings.RAW_DATA_DIR, exist_ok=True)

        print("[+] Reading configuration file")
        with open(os.path.join(settings.CONFIG_DIR, "rawdata.json")) as permissions_file:
            items = json.load(permissions_file)

            for item in items:
                src_path = os.path.join(CONFIG_DIR, item['file'])
                dest_path = os.path.join(settings.RAW_DATA_DIR, item['path'])
                dest_file = os.path.join(dest_path, "platform.cfg")



                os.makedirs(dest_path, exist_ok=True)

                if item['file']:
                    shutil.copyfile(src_path, dest_file)
                    print("{} => {}".format(item['file'], dest_file))
                else:
                    print("(empty dir) => {}".format(dest_path))

