import os

from django.core.management.base import BaseCommand

from core.models.asset import Asset
from core.erddap import Erddap
from warnings import warn
import traceback

class Command(BaseCommand):
    help = "Processes"
    def add_arguments(self, parser):
        parser.add_argument('output_dir', type=str,
                            help='Directory which to output CSV files')
        parser.add_argument('-c', '--check_vars', action='store_true',
                            help='Run checks against all associated variables '
                                 'of an asset')
        parser.add_argument('-e', '--erddap_only', action='store_true',
                            help='Only check assets which have defined ERDDAP '
                                 'ids in the database')

    def handle(self, *args, **options):
        out_dir = options['output_dir']
        try:
            os.mkdir('/tmp/omspp_reports')
        except FileExistsError:
            pass
        if options['erddap_only']:
            a_queryset = Asset.objects.exclude(erddap_id__isnull=True)
        else:
            a_queryset = Asset.objects.all()
        # (badams): Consider making process_asset async to improve throughput
        #           fetching from ERDDAP
        for a in a_queryset:
            self.process_asset(a, out_dir, options['check_vars'])

    def process_asset(self, a, out_dir, process_vars):
        try:
            erd = Erddap()
            res = erd.get_last_update_times(a, process_vars)
            out_dir = '/tmp/omspp_reports'
            res['platform'] = a.platform
            res['deployment'] = getattr(getattr(a,
                                                'deployment', None),
                                        'code', None)
            res['asset_code'] = a.code
            res['asset_erddap_id'] = a.erddap_id

            var_str = 'vars-' if process_vars else ''
            res.to_csv(os.path.join(out_dir,
                                    "{}-{}-{}{}.csv".format(a.erddap_id or
                                                            a.get_asset_name(),
                                                            a.deployment.code,
                                                            var_str, a.id)),
                       index=False)
        except:
            warn("Exception hit on asset id {}: {}".format(a.id,
                                                        traceback.format_exc()))
