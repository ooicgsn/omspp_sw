import sys, os, json

from django.core.management.base import BaseCommand
from django.conf import settings
from core.models.asset import Asset, Group, AssetStatuses, AssetTypes, Deployment


# TODO: This script currently does not check to see if the link to ERDDAP for this asset is actually valid
class Command(BaseCommand):
    help = 'Load gliders from configuration file'

    def handle(self, *args, **options):
        print("Loading gliders...")

        print("[+] Reading configuration file")
        with open(os.path.join(settings.CONFIG_DIR, "gliders.json")) as permissions_file:
            data = json.load(permissions_file)

        # Get the group the assets are going to be assigned to and mark it as a glider group
        group = Group.objects.get(name='Gliders')
        group.is_glider = True
        group.save()

        for glider_cfg in data:
            deployment_code = glider_cfg['deployment']
            platform_code = glider_cfg['name']
            erddap_id = glider_cfg['erddap-id']

            try:
                group_id = glider_cfg['group_id']
            except:
                group_id = None

            print("[+] Loading glider {} ({})".format(platform_code, deployment_code))

            # Determine if the glider already exists
            try:
                glider = Asset.objects.get(code=platform_code, deployment__code=deployment_code)

                if glider.erddap_id != erddap_id or glider.glider_group_id != group_id:
                    glider.glider_group_id = group_id
                    glider.erddap_id = erddap_id
                    glider.save()

                    print("[.] Updated ERDDAP ID")
                else:
                    print("[.] No changes")
                continue
            except:
                # Proceed to creating a new asset and deployment
                pass


            # Create a deployment
            deployment = Deployment()
            deployment.name = glider_cfg['deployment']
            deployment.code = glider_cfg['deployment']
            deployment.save()

            # Create the glider
            glider = Asset()
            glider.name = glider_cfg['name']
            glider.code = glider_cfg['name']
            glider.erddap_id = glider_cfg['erddap-id']
            glider.status_is_manually_set = True
            glider.deployment = deployment
            glider.group = group
            glider.type_id = AssetTypes.PLATFORM.value
            glider.status_id = AssetStatuses.OK.value
            glider.glider_group_id = group_id
            glider.save()

            # Update the platform ID on the asset since it is it's own platform (top level)
            glider.platform_id = glider.id
            glider.save()

            print("[.] Created new asset and deployment")

        # Update the asset groups for flag which ones now have gliders
        group_ids = list(Asset.objects\
                      .order_by()\
                      .exclude(glider_group_id=None)\
                      .values_list('glider_group_id', flat=True)\
                      .distinct())

        for grp in Group.objects.all():
            grp.has_gliders = (grp.id in group_ids)
            grp.save()




