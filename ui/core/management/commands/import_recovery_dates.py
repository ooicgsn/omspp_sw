import requests
import pandas as pd
from io import BytesIO
import csv
from django.conf import settings
from django.core.management.base import BaseCommand
from core.models.asset import Asset


class Command(BaseCommand):
    help = """
    """

    def handle(self, *args, **options):
        filename = 'recovery_dates.csv'
        row_count = 0
        with open(filename) as f:
            reader = csv.reader(f)

            for row in reader:
                platform_code = row[0]
                deployment_code = row[1]
                recovery_date = row[2]
                deployment_date = row[3]

                try:

                    platform = Asset.objects \
                        .filter(parent_id__isnull=True, group__is_glider=False) \
                        .filter(code=platform_code, deployment__code=deployment_code) \
                        .select_related('deployment') \
                        .select_related('group') \
                        .first()

                    if recovery_date:
                        print("Updating recovery date on {} ({}) to {} (from {})".format(
                            platform_code, deployment_code, recovery_date, platform.recovery_date
                        ))

                        platform.recovery_date = recovery_date

                    if deployment_date:
                        print("Updating deployment date on {} ({}) to {} (from {})".format(
                            platform_code, deployment_code, deployment_date, platform.deployment_date
                        ))

                        platform.deployment_date = deployment_date

                    if recovery_date or deployment_date:
                        platform.save()

                except Exception as e:
                    print(" - Failed to update {} ({}) - {}".format(platform_code, deployment_code, str(e)))

                row_count += 1

            print("Read {} rows from {}".format(row_count, filename))

