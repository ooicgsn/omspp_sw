import os
import json
from django.core.management.base import BaseCommand
from django.conf import settings

from core.models.redmine import Project, Tracker, Location, Organization, Platform, \
    Condition, TrackerField, CustomField, Priority


class Command(BaseCommand):
    help = 'Updates Redmine data based on a pre-defined configuration file'

    def handle(self, *args, **options):
        print("Updating Redmine settings...")

        config_file = 'redmine.prod.json' if settings.REDMINE_USE_LIVE_PROJECTS else 'redmine.dev.json'
        print("[-] Configuration file: " + config_file)

        print("[+] Reading configuration file")
        with open(os.path.join(settings.CONFIG_DIR, config_file)) as redmine_file:
            data = json.load(redmine_file)

        print("[+] Reading locations")
        for location_cfg in data['locations']:
            Location.objects.get_or_create(name=location_cfg)

        print("[+] Reading organizations")
        for organization_cfg in data['organizations']:
            Organization.objects.get_or_create(name=organization_cfg)

        print("[+] Reading conditions")
        for condition_cfg in data['conditions']:
            Condition.objects.get_or_create(name=condition_cfg)

        print("[+] Reading platforms")
        for platform_cfg in data['platforms']:
            Platform.objects.get_or_create(name=platform_cfg)

        print("[+] Reading priorities")
        for priority_cfg in data['priority']:
            Priority.objects.get_or_create(
                name=priority_cfg['name'],
                external_id=priority_cfg['external-id'],
                is_default=('default' in priority_cfg)
            )

        print("[+] Reading custom fields")
        for field_cfg in data['custom_fields']:
            try:
                field = CustomField.objects.get(name=field_cfg['name'])
            except:
                field = CustomField()
                field.name = field_cfg['name']

                field.external_id = field_cfg['external-id']
            field.save()

        print("[+] Reading projects & trackers")
        active_projects_ids = []

        for project_cfg in data['projects']:
            active_projects_ids.append(project_cfg['external-id'])

            project, _ = Project.objects.get_or_create(
                name=project_cfg['name'],
                external_id=project_cfg['external-id']
            )

            for tracker_cfg in project_cfg['trackers']:
                tracker, _ = Tracker.objects.get_or_create(
                    project=project,
                    name=tracker_cfg['name'],
                    external_id=tracker_cfg['external-id']
                )

                fields = tracker_cfg['fields']

                TrackerField.objects.filter(tracker=tracker).exclude(name__in=fields).delete()
                for field in fields:
                    TrackerField.objects.get_or_create(tracker=tracker, name=field)

        print("[-] Removing non-active projects and trackers")
        Tracker.objects.all().exclude(project__external_id__in=active_projects_ids).delete()
        Project.objects.exclude(external_id__in=active_projects_ids).delete()

