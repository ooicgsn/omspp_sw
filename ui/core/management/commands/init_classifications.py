import os
import re
import csv

from django.conf import settings
from django.core.management.base import BaseCommand

from core.models.asset import Asset, AssetClassification


class Command(BaseCommand):
    help = 'Iniitializes the asset classifications list with a set of defaults'

    def handle(self, *args, **options):
        classifications = AssetClassification.objects.all()

        for classification in self.get_classifications():
            if classifications.filter(abbreviation=classification.abbreviation).exists():
                print(f'Classification for "{classification.abbreviation} already exists.')
                continue

            print(f'Adding {classification.abbreviation} ({classification.name})')
            classification.save()

    def get_classifications(self):
        path = os.path.join(settings.CONFIG_DIR, "initial-classifications.csv")
        classifications = []

        with open(path, 'r') as csvfile:
            datareader = csv.reader(csvfile)

            # Skip header
            next(datareader)

            for row in datareader:
                classification = AssetClassification(
                    name=row[0],
                    abbreviation=row[1],
                    description=row[2],
                    is_virtual=row[3].lower() == 'yes',
                    pattern=row[4]
                )

                classifications.append(classification)

        return classifications


