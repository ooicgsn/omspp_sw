import os
import json

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth.models import User

from core.models.variable import Variable
from core.models.alert_trigger import Trigger, TriggerVariable

class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        print("Loading default alerts")

        print("[+] Reading configuration file")
        with open(os.path.join(settings.CONFIG_DIR, "alerts.json")) as permissions_file:
            triggers = json.load(permissions_file)

        # TODO(mchagnon): Probably need a safer way to determine the admin account for globals
        admin = User.objects.get(pk=1)

        for trigger_data in triggers:
            print("[+] Adding " + trigger_data['name'])

            # TODO(mchagnon): Check for existing trigger

            variable = Variable.objects\
                .select_related('asset')\
                .filter(asset__erddap_id=trigger_data['asset_erddap_id'])\
                .filter(erddap_id=trigger_data['variable_erddap_id'])\
                .first()

            if not variable:
                # TODO(mchagnon): Handle
                print("[!] Can't locate variable")
                continue

            # Create the trigger
            trigger = Trigger()
            trigger.name = trigger_data["name"]
            trigger.severity_id = trigger_data["severity"]
            trigger.asset = variable.asset
            trigger.duration = trigger_data["duration"]
            trigger.duration_code_id = trigger_data["duration_code"]
            trigger.is_global = True
            trigger.expression = trigger_data["expression"]
            trigger.created_by = admin
            trigger.roll_down = False
            trigger.roll_up = False
            trigger.save()

            # Create a variable for the parameter
            trigger_var = TriggerVariable()
            trigger_var.trigger = trigger
            trigger_var.variable = variable
            trigger_var.name = trigger_data["variable_erddap_id"]
            trigger_var.save()

