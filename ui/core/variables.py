import sys, os
from .assets import strip_asset_name
from .models.asset import AssetTypes
from .models.variable import Variable
from .models.alert_trigger import TriggerVariable
from .models.plot import PlotVariable


# Retrieves the first equivalent variable that matches for the list of linked assets
# search_only_by_errddap used by 
# code/webapp/core/views/variable.py:clone_variable_for_asset::145 to prevent searchin with stripped_asset_name
def get_linked_variable(variable, linked_assets, deployment_id, search_only_by_erddap=False):
    try:
        if variable.asset.type_id is AssetTypes.PLATFORM.value or search_only_by_erddap:
            # Platform names don't follow a set pattern so don't filter based on it. There should only be one anyways.
            new_trigger_variable = Variable.objects.filter(erddap_id=variable.erddap_id,
                                                           deployment_id=deployment_id,
                                                           is_custom=False,
                                                           asset__type_id=variable.asset.type_id,
                                                           asset__id__in=linked_assets.values_list('id', flat=True))[0]
        else:
            # Match only asset variables that match with the same asset name prefix, prioritizes an exact match
            stripped_asset_name = strip_asset_name(variable.asset.name)
            new_trigger_variables = Variable.objects.filter(erddap_id=variable.erddap_id,
                                                            deployment_id=deployment_id,
                                                            is_custom=False,
                                                            asset__type_id=variable.asset.type_id,
                                                            asset__id__in=linked_assets.values_list('id', flat=True),
                                                            asset__name__startswith=stripped_asset_name)

            # Start by assigning the first variable with a similar asset name
            new_trigger_variable = new_trigger_variables[0]

            # If there is more than one match, try and find an exact match.
            # Otherwise keep the first match.
            if len(new_trigger_variables) > 1:
                try:
                    exact_match_variable = new_trigger_variables.filter(asset__name=variable.asset.name)[0]
                    if exact_match_variable is not None:
                        new_trigger_variable = exact_match_variable
                except:
                    # Lazy way instead of trapping for filter not found or index error
                    pass

        return new_trigger_variable

    except Exception as e:
        # If there is an error it's because a matching variable asset was not found so return None
        # exc_type, exc_obj, exc_tb = sys.exc_info()
        # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        # print(e)
        # print(exc_type, fname, exc_tb.tb_lineno)
        return None


def get_related_triggers(variable_id):
    return [tv.trigger for tv in TriggerVariable.objects.filter(variable_id=variable_id)]


def get_related_plots(variable_id):
    return [pv.plot for pv in PlotVariable.objects.filter(variable_id=variable_id)]