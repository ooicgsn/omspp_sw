import re
from typing import List
from .schemas import CpmSnapshot, DclSnapshot, CpmTelemetrySchedule, PpsStatus, DclPort, CpmHotel, CvtStatus, \
    BatteryStatus, MpeaCvt, CpmPowerSchedule
from .models.asset import AssetCategories, AssetSubcategories

UnknownValue = '???'
TelemetryPrefix = 'Sched.telem.'
PowerSchedulePrefix = 'Sched.dcl'
MaxPorts = 8
PortPrefix = 'DCL.port.'
InstrumentPrefix = 'DLOGP'
PortSchedulePrefix = 'Sched.port'
CpmHotelPrefix = 'MPIC.hotel'
AlarmPrefix = 'ALARM.'
WindTurbinePrefix = 'Pwrsys.wt'
SolarPrefix = 'Pwrsys.pv'
BatteryPrefix = 'Pwrsys.b'
LastErrorPrefix = 'STATUS.last_err.'
MpeaCvtPrefix = 'MPEA.cvt'
MaxWindSolarBatteryCounts = 10


def load_cpm_snapshot(asset, platform) -> CpmSnapshot:
    data = _parse_file(asset.status_file_path)

    snapshot = CpmSnapshot()

    snapshot.platform_time = _find_value('Platform.time', data)
    snapshot.platform_utime = _find_value('Platform.utime', data)

    snapshot.cpu_uptime = _find_value('CPU.uptime', data)
    snapshot.cpu_load = _find_value('CPU.load', data)

    snapshot.mpic_last_update = _find_value('MPIC.last_update', data)
    snapshot.mpic_eflag = _find_value('MPIC.eflag', data)
    snapshot.mpic_temp = _find_value('MPIC.temp', data)
    snapshot.mpic_humid = _find_value('MPIC.humid', data)
    snapshot.mpic_press = _find_value('MPIC.press', data)
    snapshot.mpic_ground_fault = _find_value('MPIC.gflt', data)
    snapshot.mpic_leak_detect = _find_value('MPIC.ldet', data)
    snapshot.mpic_ground_fault_enabled = _find_value('MPIC.gf_ena', data)
    snapshot.mpic_leak_detect_enabled = _find_value('MPIC.ld_ena', data)
    snapshot.mpic_main = _parse_main(data)
    snapshot.mpic_heartbeat = _find_value('MPIC.cpm_hb', data)
    snapshot.mpic_wake_cpm = _find_value('MPIC.wake_cpm', data)

    snapshot.loadshed_status = _find_value('LoadShed.status', data)
    snapshot.autopwr_status = _find_value('AutoPwr.status', data)
    snapshot.wake_schedule = _find_value('Sched.cpm.wake', data)

    snapshot.timing_last_update = _find_value('LoadShed.last_update', data)
    snapshot.timing_refid = _find_value('NTP.refid', data)
    snapshot.timing_offset = _find_value('NTP.offset', data)
    snapshot.timing_jitter = _find_value('NTP.jitter', data)
    snapshot.timing_pps_status = _parse_pps_status(_find_value('PPS.status', data))

    snapshot.gps_timestamp = _find_value('GPS.timestamp', data)
    snapshot.gps_last_update = _find_value('GPS.last_update', data)
    snapshot.gps_spd = _find_value('GPS.spd', data)
    snapshot.gps_cog = _find_value('GPS.cog', data)
    snapshot.gps_lat = _find_value('GPS.lat', data)
    snapshot.gps_lon = _find_value('GPS.lon', data)
    snapshot.gps_alt = _find_value('GPS.alt', data)
    snapshot.gps_fix_quality = _find_value('GPS.fix_q', data)
    snapshot.gps_nsat = _find_value('GPS.nsat', data)
    snapshot.gps_hdop = _find_value('GPS.hdop', data)

    snapshot.pwr_last_update =_find_value('Pwrsys.last_update', data)
    snapshot.pwr_current = _find_value('Pwrsys.main_c', data)
    snapshot.pwr_voltage = _find_value('Pwrsys.main_v', data)

    try:
        snapshot.pwr_wattage = float(snapshot.pwr_voltage) * float(snapshot.pwr_current) / 1000
    except:
        snapshot.pwr_wattage = ""

    snapshot.pwr_charge = _find_value('Pwrsys.b_chg', data)
    snapshot.pwr_temp = _find_value('Pwrsys.temp', data)
    snapshot.pwr_cvt = _parse_cvt(data)
    snapshot.pwr_override = _find_value('Pwrsys.override', data)
    snapshot.pwr_error_flag1 = _find_value('Pwrsys.eflag1', data)
    snapshot.pwr_error_flag2 = _find_value('Pwrsys.eflag2', data)
    snapshot.pwr_error_flag3 = _find_value('Pwrsys.eflag3', data)
    snapshot.pwr_wind_turbine_currents = _parse_wind_turbine_currents(data)
    snapshot.pwr_solar_currents = _parse_solar_currents(data)
    snapshot.pwr_battery_status = _parse_battery(data)

    # Power System Controllers are only on available on high and standard power surface moorings. They'll always be
    #   located on the CPM1
    snapshot.show_psc = \
        asset.code.lower() == 'cpm1' and \
        platform.category and platform.category.id == AssetCategories.SURFACE_MOORING.value and \
        platform.subcategory and (
                platform.subcategory.id == AssetSubcategories.STANDARD_POWER.value or
                platform.subcategory.id == AssetSubcategories.HIGH_POWER.value
        )

    snapshot.mpea_last_update = _find_value('MPEA.last_update', data)
    snapshot.mpea_voltage =_find_value('MPEA.main_v', data)
    snapshot.mpea_current =_find_value('MPEA.main_c', data)
    snapshot.mpea_error_flag1 = _find_value('MPEA.eflag1', data)
    snapshot.mpea_error_flag2 = _find_value('MPEA.eflag2', data)
    snapshot.mpea_cvts = _parse_mpea_cvts(data)
    snapshot.mpea_aux = _parse_mpea_aux(data)
    snapshot.mpea_hotel = _parse_mpea_hotel(data)

    # MPEA's are only on CPM3 instruments; only show if there are values for the MPEA found (e.g, mpea_last_update)
    snapshot.show_mpea = asset.code.lower() == 'cpm3' and snapshot.mpea_last_update is not None

    try:
        snapshot.mpea_wattage = float(snapshot.mpea_voltage) * float(snapshot.mpea_current) / 1000
    except:
        snapshot.mpea_wattage = ""

    telemetry_rows = [row for row in data if TelemetryPrefix in row]
    snapshot.telemetry = [
        _parse_telemetry(row)
        for row in telemetry_rows
    ]

    power_schedule_rows = [row for row in data if PowerSchedulePrefix in row]
    snapshot.power_schedule = [
        _parse_power_schedule(row)
        for row in power_schedule_rows
    ]

    snapshot.hotels = _parse_cpm_hotels(data)
    snapshot.alarms = _parse_alarms(data)
    snapshot.error_counts = _find_value('STATUS.err_cnts', data)
    snapshot.last_errors = _parse_last_errors(data)

    return snapshot


def load_dcl_snapshot(filename: str) -> DclSnapshot:
    data = _parse_file(filename)

    snapshot = DclSnapshot()

    snapshot.platform_time = _find_value('Platform.time', data)
    snapshot.platform_utime = _find_value('Platform.utime', data)

    snapshot.cpu_uptime = _find_value('CPU.uptime', data)
    snapshot.cpu_load = _find_value('CPU.load', data)

    snapshot.mpic_last_update = _find_value('MPIC.last_update', data)
    snapshot.mpic_eflag = _find_value('MPIC.eflag', data)
    snapshot.mpic_stc_eflag2 = _find_value('MPIC.stc_eflag2', data)
    snapshot.mpic_temp = _find_value('MPIC.temp', data)
    snapshot.mpic_humid = _find_value('MPIC.humid', data)
    snapshot.mpic_press = _find_value('MPIC.press', data)
    snapshot.mpic_ground_fault = _find_value('MPIC.gflt', data)
    snapshot.mpic_leak_detect = _find_value('MPIC.ldet', data)
    snapshot.mpic_ground_fault_enabled = _find_value('MPIC.gf_ena', data)
    snapshot.mpic_leak_detect_enabled = _find_value('MPIC.ld_ena', data)
    snapshot.mpic_wake_cpm = _find_value('MPIC.wake_cpm', data)
    snapshot.mpic_main = _parse_main(data)
    snapshot.mpic_hotel = _find_value('MPIC.hotel', data)

    snapshot.timing_last_update = _find_value('LoadShed.last_update', data)
    snapshot.timing_refid = _find_value('NTP.refid', data)
    snapshot.timing_offset = _find_value('NTP.offset', data)
    snapshot.timing_jitter = _find_value('NTP.jitter', data)
    snapshot.timing_pps_status = _parse_pps_status(_find_value('PPS.status', data))

    snapshot.gps_timestamp = _find_value('GPS.timestamp', data)
    snapshot.gps_last_update = _find_value('GPS.last_update', data)
    snapshot.gps_spd = _find_value('GPS.spd', data)
    snapshot.gps_cog = _find_value('GPS.cog', data)
    snapshot.gps_lat = _find_value('GPS.lat', data)
    snapshot.gps_lon = _find_value('GPS.lon', data)
    snapshot.gps_alt = _find_value('GPS.alt', data)
    snapshot.gps_fix_quality = _find_value('GPS.fix_q', data)
    snapshot.gps_nsat = _find_value('GPS.nsat', data)
    snapshot.gps_hdop = _find_value('GPS.hdop', data)

    snapshot.ports = _parse_dcl_ports(data)
    snapshot.manager_status = _find_value('DMGR.status', data)
    snapshot.alarms = _parse_alarms(data)
    snapshot.error_counts = _find_value('STATUS.err_cnts', data)
    snapshot.last_errors = _parse_last_errors(data)

    return snapshot


def _parse_file(filename: str) -> List[str]:
    file = open(filename, mode='r')
    content = file.readlines()
    data = [line.rstrip() for line in content]
    file.close()

    return data


def _find_value(name, data):
    value = ''

    for row in data:
        if row.startswith(name):
            value = row
            break

    try:
        return value.split('=', 1)[1]
    except:
        return ''


def _find_rows(prefix, data):
    rows = []

    for row in data:
        if not row.startswith(prefix):
            continue

        try:
            parts = row.split('=', 1)
            rows.append((parts[0], parts[1],))
        except:
            continue

    return rows


def _parse_battery(data) -> BatteryStatus:
    battery_status = BatteryStatus(temperatures=[], voltages=[], currents=[])

    # Attempt to find all matching rows, which contain the prefix and a numerical index
    for i in range(MaxWindSolarBatteryCounts):
        row = _find_value(f'{BatteryPrefix}{i}', data)
        if not row:
            continue

        parts = row.split(' ')
        temperature = float(parts[0]) if len(parts) > 0 else None
        voltage = float(parts[1]) if len(parts) > 1 else None
        current = float(parts[2]) if len(parts) > 2 else None

        battery_status.temperatures.append(temperature)
        battery_status.voltages.append(voltage)
        battery_status.currents.append(current)

    battery_status.net_current = sum(battery_status.currents)

    return battery_status


def _parse_wind_turbine_currents(data) -> List[float]:
    currents = []

    # Attempt to find all matching rows, which contain the prefix and a numerical index
    for i in range(MaxWindSolarBatteryCounts):
        row = _find_value(f'{WindTurbinePrefix}{i}', data)
        if not row:
            continue

        parts = row.split(' ')
        if len(parts) > 2:
            currents.append(float(parts[2]))

    return currents


def _parse_solar_currents(data) -> List[float]:
    currents = []

    # Attempt to find all matching rows, which contain the prefix and a numerical index
    for i in range(MaxWindSolarBatteryCounts):
        row = _find_value(f'{SolarPrefix}{i}', data)
        if not row:
            continue

        parts = row.split(' ')
        if len(parts) > 2:
            currents.append(float(parts[2]))

    return currents


def _parse_main(data) -> str:
    voltage = _find_value('MPIC.main_v', data)
    voltage_text = f'{voltage} v' if voltage else ''

    current = _find_value('MPIC.main_c', data)
    current_text = f'{current} ma ' if current else ''

    try:
        wattage = float(voltage) * float(current) / 1000
        wattage_text = f'{wattage} w'
    except:
        wattage_text = ""

    return f'{voltage_text} {current_text} {wattage_text}'.strip()


def _parse_cvt(data) -> CvtStatus:
    cvt_status = _find_value('Pwrsys.cvt', data)
    result = CvtStatus()

    try:
        parts = cvt_status.split(' ')


        result.enabled = parts[0] if len(parts) > 0 else False
        result.voltage = float(parts[1]) if len(parts) > 1 else False
        result.current = float(parts[2]) if len(parts) > 2 else False
        result.interlock = parts[3] if len(parts) > 3 else False
        result.temperature = parts[4] if len(parts) > 4 else False
        result.wattage = float(result.voltage) * float(result.current) / 1000.0

        return result
    except:
        return result


def _parse_telemetry(row: str) -> CpmTelemetrySchedule:
    try:
        # Example format: Sched.telem.dcls2irid=stopped 1:0-23:05:1  Start_in: 3536  sec
        #   Name: text before the equals sign, minus the telemetry prefix
        #   State: first word after the equals sign
        #   Schedule: second "word" after the equals sign
        #   Status: Everything after schedule
        key, value = tuple(row.split('='))
        state, schedule, status = tuple(re.split(r' +', value, 2))

        return CpmTelemetrySchedule(
            name=key.replace(TelemetryPrefix, ''),
            state=state,
            schedule=schedule,
            status=status
        )
    except:
        return CpmTelemetrySchedule()


def _parse_power_schedule(row: str) -> CpmPowerSchedule:
    try:
        # Example format: Sched.dcl6.pwr=started 1:0-23:27:11      Remaining: 44 sec
        #   Name: text before the equals sign, minus the telemetry prefix
        #   State: first word after the equals sign
        #   Schedule: second "word" after the equals sign
        #   Status: Everything after schedule
        key, value = tuple(row.split('='))
        state, schedule, status = tuple(re.split(r' +', value, 2))

        # Locate the instrument within the key
        instrument = re.search(r'(?i)sched\.(.*?)\.pwr', key)
        if instrument:
            key = instrument.groups()[0]

        return CpmPowerSchedule(
            name=key.replace(TelemetryPrefix, ''),
            state=state,
            schedule=schedule,
            status=status
        )
    except:
        return CpmPowerSchedule()


def _parse_pps_status(row: str) -> PpsStatus:
    try:
        match = re.search(r'nmea_lock:\s*(\w+)', row, re.IGNORECASE)
        nmea_lock = match.group(1) if match and len(match.group()) > 1 else ''

        match = re.search(r'badpulses:\s*(\w+)', row, re.IGNORECASE)
        bad_pulses = match.group(1) if match and len(match.group()) > 1 else ''

        match = re.search(r'(?m)ts:\s*(.*?)$', row, re.IGNORECASE)
        timestamp = match.group(1) if match and len(match.group()) > 1 else ''

        return PpsStatus(
            nmea_lock=nmea_lock,
            bad_pulses=bad_pulses,
            timestamp=timestamp
        )
    except:
        return PpsStatus()


def _parse_cpm_hotels(data) -> List[CpmHotel]:
    hotels = []

    rows = _find_rows(CpmHotelPrefix, data)
    for key, value in rows:
        number = int(key.replace(CpmHotelPrefix, '') or 1)

        hotels.append(CpmHotel(
            number=number,
            description=value
        ))

    return hotels


def _parse_alarms(data) -> List[str]:
    alarms = []

    rows = _find_rows(AlarmPrefix, data)
    for key, value in rows:
        key = key.replace(AlarmPrefix, '')
        alarms.append(f'{key}: {value}')

    return alarms


def _parse_last_errors(data) -> List[str]:
    errors = []

    rows = _find_rows(LastErrorPrefix, data)
    for key, value in rows:
        key = key.replace(LastErrorPrefix, '')
        errors.append(f'{key}: {value}')

    return errors


def _parse_dcl_ports(data) -> List[DclPort]:
    ports = [DclPort(number=x+1) for x in range(MaxPorts)]

    states = _find_rows(PortPrefix, data)
    for key, value in states:
        try:
            index = int(key.replace(PortPrefix, '')) - 1
            match = re.search(r'\s*(\d+)\s*([\.\d]+)\s*([\.\d]+)\s*(\d+)\s*vsel:', value)
            groups = match.groups() if match else []

            ports[index].enabled = int(groups[0]) == 1 if len(groups) > 0 else None
            ports[index].voltage = groups[1] if len(groups) > 1 else None
            ports[index].ma = groups[2] if len(groups) > 2 else None
            ports[index].err = groups[3] if len(groups) > 3 else None

        except Exception as e:
            print('Error parsing DCL ports: ' + str(e))
            continue

    instruments = _find_rows(InstrumentPrefix, data)
    for key, value in instruments:
        index = int(key.replace(InstrumentPrefix, '')) - 1
        match = re.search(r'\s*(\w+)\s*(\w+)\s*tx:.*?bad:\s*([\d+\.])\s*.*?ld:\s+([-\d\.]+)\s*.*?lu:\s*([-\d\.]+)', value)
        groups = match.groups() if match else []

        ports[index].name = groups[0] if len(groups) > 0 else None
        ports[index].status = groups[1] if len(groups) > 1 else None
        ports[index].bad = groups[2] if len(groups) > 2 else None
        ports[index].leak_detect = groups[3] if len(groups) > 3 else None
        ports[index].last_update = groups[4] if len(groups) > 4 else None

    schedules = _find_rows(PortSchedulePrefix, data)
    for key, value in schedules:
        key = key.replace(PortSchedulePrefix, '')
        number, type_name = key.split('.')
        index = int(number) - 1

        match = re.search(r'(?m)\s*(\w+)\s+(.*?)\s{2,}(.*?)$', value, re.MULTILINE)
        groups = match.groups() if match else []

        ports[index].schedule_type = type_name
        ports[index].schedule_state = groups[0] if len(groups) > 0 else None
        ports[index].schedule_description = groups[1] if len(groups) > 2 else None
        ports[index].schedule_status = groups[2] if len(groups) > 2 else None

    return ports


def _parse_mpea_aux(data):
    try:
        parts =  _find_value('MPEA.aux', data).split(' ')
        state = parts[0]
        current = parts[1] + ' ma' if len(parts) > 0 else ''
        interlock = parts[2] if len(parts) > 1 else ''

        return f'State: {state} InterLock: {interlock} {current}'
    except:
        return ''



def _parse_mpea_hotel(data):
    try:
        parts = _find_value('MPEA.htl', data).split(' ')
        voltage = parts[0] + ' v'
        current = parts[1] + ' ma'
        temperature = parts[2] + ' degC'

        return f'{voltage} {current} {temperature}'
    except:
        return ''


def _parse_mpea_cvts(data):
    cvts = []

    rows = _find_rows(MpeaCvtPrefix, data)
    for key, value in rows:
        number = int(key.replace(MpeaCvtPrefix, '') or 1)
        parts = value.split(' ')

        cvts.append(MpeaCvt(
            index=number,
            enabled=bool(parts[0]),
            voltage=parts[1],
            current=parts[2]
        ))

    return cvts