from pydantic import BaseModel
from typing import Optional


class PpsStatus(BaseModel):
    nmea_lock: Optional[str]
    bad_pulses: Optional[str]
    timestamp: Optional[str]
