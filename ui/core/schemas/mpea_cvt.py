from pydantic import BaseModel
from typing import List, Optional


class MpeaCvt(BaseModel):
    index: int
    enabled: bool
    voltage: float
    current: float
