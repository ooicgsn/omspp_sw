from pydantic import BaseModel
from typing import Optional, List
from .pps_status import PpsStatus
from .cvt_status import CvtStatus
from .battery_status import BatteryStatus
from .mpea_cvt import MpeaCvt


class CpmTelemetrySchedule(BaseModel):
    name: Optional[str]
    state: Optional[str]
    schedule: Optional[str]
    status: Optional[str]


class CpmPowerSchedule(BaseModel):
    name: Optional[str]
    state: Optional[str]
    schedule: Optional[str]
    status: Optional[str]


class CpmHotel(BaseModel):
    number: int
    description: str


class CpmSnapshot(BaseModel):
    name: Optional[str]

    platform_time: Optional[str]
    platform_utime: Optional[str]

    cpu_uptime: Optional[str]
    cpu_load: Optional[str]

    mpic_last_update: Optional[str]
    mpic_eflag: Optional[str]
    mpic_temp: Optional[str]
    mpic_humid: Optional[str]
    mpic_press: Optional[str]
    mpic_ground_fault: Optional[str]
    mpic_leak_detect: Optional[str]
    mpic_ground_fault_enabled: Optional[str]
    mpic_leak_detect_enabled: Optional[str]
    mpic_heartbeat: Optional[str]
    mpic_wake_cpm: Optional[str]
    mpic_main: Optional[str]

    loadshed_status: Optional[str]
    autopwr_status: Optional[str]

    timing_last_update: Optional[str]
    timing_refid: Optional[str]
    timing_offset: Optional[str]
    timing_jitter: Optional[str]
    timing_pps_status: Optional[PpsStatus]

    gps_last_update: Optional[str]
    gps_timestamp: Optional[str]
    gps_spd: Optional[str]
    gps_cog: Optional[str]
    gps_lat: Optional[str]
    gps_lon: Optional[str]
    gps_alt: Optional[str]
    gps_fix_quality: Optional[str]
    gps_nsat: Optional[str]
    gps_hdop: Optional[str]

    pwr_last_update: Optional[str]
    pwr_voltage: Optional[str]
    pwr_wattage: Optional[str]
    pwr_temp: Optional[str]
    pwr_current: Optional[str]
    pwr_charge: Optional[str]
    pwr_override: Optional[str]
    pwr_error_flag1: Optional[str]
    pwr_error_flag2: Optional[str]
    pwr_error_flag3: Optional[str]
    pwr_cvt: Optional[CvtStatus]
    pwr_wind_turbine_currents: Optional[List[float]]
    pwr_solar_currents: Optional[List[float]]
    pwr_battery_status: Optional[BatteryStatus]

    mpea_last_update: Optional[str]
    mpea_voltage: Optional[str]
    mpea_wattage: Optional[str]
    mpea_current: Optional[str]
    mpea_error_flag1: Optional[str]
    mpea_error_flag2: Optional[str]
    mpea_cvts: Optional[List[MpeaCvt]]
    mpea_aux: Optional[str]
    mpea_hotel: Optional[str]

    wake_schedule: Optional[str]
    telemetry: Optional[List[CpmTelemetrySchedule]]
    power_schedule: Optional[List[CpmPowerSchedule]]
    hotels: Optional[List[CpmHotel]]
    alarms: Optional[List[str]]
    error_counts: Optional[str]
    last_errors: Optional[List[str]]

    show_mpea: Optional[bool] = False
    show_psc: Optional[bool] = False







