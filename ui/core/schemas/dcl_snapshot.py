from typing import Optional, List
from pydantic import BaseModel
from .pps_status import PpsStatus


class DclPort(BaseModel):
    number: int

    enabled: Optional[bool]
    voltage: Optional[float]
    ma: Optional[float]
    err: Optional[int]

    name: Optional[str]
    status: Optional[str]
    bad: Optional[float]
    leak_detect: Optional[float]
    last_update: Optional[float]

    schedule_type: Optional[str]
    schedule_state: Optional[str]
    schedule_description: Optional[str]
    schedule_status: Optional[str]


class DclSnapshot(BaseModel):
    name: Optional[str]

    platform_time: Optional[str]
    platform_utime: Optional[str]

    cpu_uptime: Optional[str]
    cpu_load: Optional[str]

    mpic_last_update: Optional[str]
    mpic_eflag: Optional[str]
    mpic_stc_eflag2: Optional[str]
    mpic_temp: Optional[str]
    mpic_humid: Optional[str]
    mpic_press: Optional[str]
    mpic_ground_fault: Optional[str]
    mpic_leak_detect: Optional[str]
    mpic_ground_fault_enabled: Optional[str]
    mpic_leak_detect_enabled: Optional[str]
    mpic_wake_cpm: Optional[str]
    mpic_main: Optional[str]
    mpic_hotel: Optional[str]

    ntp_refid: Optional[str]
    ntp_offset: Optional[str]
    ntp_jitter: Optional[str]
    timing_last_update: Optional[str]
    timing_refid: Optional[str]
    timing_offset: Optional[str]
    timing_jitter: Optional[str]
    timing_pps_status: Optional[PpsStatus]

    gps_last_update: Optional[str]
    gps_timestamp: Optional[str]
    gps_spd: Optional[str]
    gps_cog: Optional[str]
    gps_lat: Optional[str]
    gps_lon: Optional[str]
    gps_alt: Optional[str]
    gps_fix_quality: Optional[str]
    gps_nsat: Optional[str]
    gps_hdop: Optional[str]

    ports: Optional[List[DclPort]]
    manager_status: Optional[str]
    alarms: Optional[List[str]]
    error_counts: Optional[str]
    last_errors: Optional[List[str]]

