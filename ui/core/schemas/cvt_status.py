from pydantic import BaseModel
from typing import Optional


class CvtStatus(BaseModel):
    enabled: bool = False
    voltage: Optional[float]
    current: Optional[float]
    wattage: Optional[float]
    interlock: Optional[str]
    temperature: Optional[int]
