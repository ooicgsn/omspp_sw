from pydantic import BaseModel
from typing import Optional


class SbdLogEntry(BaseModel):
    momsn: Optional[int] = None
    timestamp: Optional[str] = None
    mo: Optional[str] = None
    transfer_status: Optional[str] = None
    transfer_bytes: Optional[int] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    cep_radius: Optional[int] = None
    data: Optional[str] = None
