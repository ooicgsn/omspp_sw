from pydantic import BaseModel
from typing import List, Optional


class BatteryStatus(BaseModel):
    temperatures: Optional[List[float]]
    voltages: Optional[List[float]]
    currents: Optional[List[float]]
    net_current: Optional[float]