import json
import os
import datetime
from django import forms
import numpy as np
import collections
from django.db.models import Q
from django.utils.text import slugify
from django.conf import settings
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import MultipleObjectsReturned
from django.http import JsonResponse
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..models.variable import Variable, VariableComponent
from ..forms.l3_variable import L3VariableForm
from ..forms.plot import PlotForm
from ..forms.upload_file import UploadFileForm
from ..models.asset import Asset, AssetDisposition, AssetDispositions, AssetLink
from ..util import DataTablesResponse, check_perm
from ..variables import get_linked_variable
from ..db import exec_dict
from core.helpers import validate_expression
from core.erddap import Erddap
from core.plotting import Plotting
from ..models.static import L3Duration, IntervalTimes
from ..models.account import User
from webapp.settings import DASHBOARD_URL
from ..variables import get_related_triggers, get_related_plots
from ..models.plot import PlatformDefaultPlot

EXPORT_APP_NAME = "OMS"
EXPORT_APP_VERSION = "1"
EXPORT_APP_PLOT = "plot"
EXPORT_APP_VAR = "variable"
EXPORT_APP_TRIGGER = "trigger"
KEY_EXPORT_APP_NAME = "app_name"
KEY_EXPORT_APP_VERSION = "app_version"
KEY_EXPORT_DATA_TYPE= "data_type"
KEY_EXPORT_CONTENT= "content"

import logging
logger = logging.getLogger('django')

class PreviewVariable:
    """Simple class to mock a variable for use with previewing"""
    def __init__(self, erddap_id, units):
        self.erddap_id = erddap_id
        self.units = units


def manage_l3_variables(request):
    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value,]

    platforms = Asset.objects.filter(parent_id=None).select_related('platform').select_related('deployment')
    platforms = sorted(platforms, key=lambda a: a.name + str(a.deployment.sortable_code))

    return render(request, 'variable/manage_l3_variables.html', {
        'platforms': platforms,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions
    })


def delete_l3(request):
    variable_id = request.POST.get("variable_id")
    variable = get_object_or_404(Variable, pk=variable_id)

    if variable.is_global and not check_perm(request, "delete_shared_items"):
        return JsonResponse({'success': False})

    if not variable.is_global and variable.user != request.user and not check_perm(request, "delete_other_items"):
        return JsonResponse({'success': False})

    # Prevent deleting an L3 that's still tied to other triggers/plots
    triggers = get_related_triggers(variable_id)
    plots = get_related_plots(variable_id)
    if len(triggers) > 0 or len(plots) > 0:
        return JsonResponse({'success': False})

    variable.delete()

    return JsonResponse({'success': True})


def clone_l3_variable(request, variable_id=''):
    variable = Variable.objects.get(pk=variable_id)
    can_clone_shared_items = check_perm(request, "clone_shared_items")
    can_clone_other_items = check_perm(request, "clone_other_items")

    if variable.is_global and not can_clone_shared_items:
        return redirect(DASHBOARD_URL)

    if not variable.is_global and variable.user != request.user and not can_clone_other_items:
        return redirect(DASHBOARD_URL)

    try:
        asset = Asset.objects.get(pk=variable.asset_id)
        variables = Variable.objects.filter(pk__in=VariableComponent.objects.filter(
            parent_id=variable_id).values_list('child_id', flat=True), is_custom=False)
        target_assets = Asset().get_clone_variable_assets(variable_id)
        dispositions = AssetDisposition.objects.all()
        shown_dispositions = [AssetDispositions.DEPLOYED.value, ]

    except ObjectDoesNotExist:
        pass

    return render(request, 'variable/clone_l3_variable.html', {
        'variable': variable,
        'variables': variables,
        'asset': asset,
        'target_assets': target_assets,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions,
    })


def execute_clone_variable(request):
    clone_without_owning = check_perm(request, "clone_without_owning")
    success, successful_assets, failed_assets = \
        clone_variable(request.POST['variable_id'],
                   Asset.objects.filter(pk__in=request.POST['target_asset_ids'].split(',')),
                   request.user.id, clone_without_owning)

    return JsonResponse({'success': success, 'successful_assets': successful_assets, 'failed_assets': failed_assets})

def clone_variable(variable_id, target_assets, user_id, clone_without_owning):
    success = True
    successful_assets = []
    failed_assets = []
    try:
        # trigger = get_object_or_404(Trigger, pk=trigger_id)
        # trigger_variables = TriggerVariable.objects.filter(trigger_id=trigger.id, variable__is_custom=False)
        # trigger_l3_vars = TriggerVariable.objects.filter(trigger_id=trigger.id, variable__is_custom=True)

        l3_var = Variable.objects.filter(pk=variable_id).first()

        # Loop through each target asset and clone the trigger for it
        for target_asset in target_assets:
            returned_successful_assets, returned_failed_assets = clone_variable_for_asset(
                l3_var, target_asset, user_id, clone_without_owning
            )
            successful_assets += returned_successful_assets
            failed_assets += returned_failed_assets
    except:
        success = False

    if len(successful_assets) == 0:
        success = False

    return success, successful_assets, failed_assets


def clone_variable_for_asset(l3_var, target_asset, user_id, clone_without_owning):
    successful_assets = []
    failed_assets = []
    try:
        mismatched_vars = False
        mismatched_l3_vars = False

        # Get list of child assets to traverse through to find variable assets
        linked_assets = Asset.objects.filter(pk__in=AssetLink.objects.filter(
            is_child=True, asset_id=target_asset.id).values_list('linked_asset_id', flat=True))

        # Loop through and find variables to clone
        new_l3_vars = []
        new_l3_var_components = []
        new_l3_var_variables = []
        try:
            current_l3_var_component = []
            current_l3_var_variables = []
            l3_var_variables = Variable.objects.filter(childVariable__parent_id=l3_var.id)
            for l3_var_variable in l3_var_variables:
                new_l3_variable = get_linked_variable(l3_var_variable, linked_assets,
                                                        target_asset.deployment_id, True)
                if new_l3_variable is not None:
                    current_l3_var_component.append(
                        VariableComponent.objects.filter(parent_id=l3_var.id,
                                                            child_id=l3_var_variable.id)[0])
                    current_l3_var_variables.append(new_l3_variable)

            if len(current_l3_var_variables) is len(l3_var_variables):
                new_l3_var_components.append(current_l3_var_component)
                new_l3_vars.append(l3_var)
                new_l3_var_variables.append(current_l3_var_variables)
            else:
                mismatched_l3_vars = True
        except:
            mismatched_l3_vars = True

        # Save new L3 variables for the cloned asset
        var_index = 0
        for l3_var in new_l3_vars:
            # Save parent variable
            new_l3_var = Variable(name=l3_var.name,
                                    is_custom=True,
                                    erddap_id=l3_var.erddap_id,
                                    units=l3_var.units,
                                    is_plottable=l3_var.is_plottable,
                                    expression=l3_var.expression,
                                    asset=target_asset,
                                    deployment=target_asset.deployment,
                                    platform=target_asset.platform,
                                    is_global=l3_var.is_global,
                                    user_id=user_id if not l3_var.is_global else None)
            new_l3_var.save()

            # Save child variables
            current_l3_var_components = new_l3_var_components[var_index]
            current_l3_var_variables = new_l3_var_variables[var_index]
            var_l3_index = 0
            for current_l3_var_variable in current_l3_var_variables:
                new_var_com = VariableComponent(name=current_l3_var_components[var_l3_index].name,
                                                child=current_l3_var_variable,
                                                parent=new_l3_var)
                new_var_com.save()
                var_l3_index += 1

            var_index += 1

            successful_assets.append({'AssetID': target_asset.id,
                                    'L3VarID': new_l3_var.id,
                                    'AssetName': '{} ({}) > {}'.format(target_asset.platform.name, target_asset.deployment.code, target_asset.name),
                                    'L3Name': l3_var.name,
                                    'DeploymentCode': target_asset.deployment.code})

        if mismatched_l3_vars:
            failed_assets.append({'AssetName': '{} ({}) > {}'.format(target_asset.platform.name, target_asset.deployment.code, target_asset.name),
                                    'L3Name': l3_var.name,
                                    'Message': '{} ({}) > {}: L3 Variables do not match up.'.format(
                                        target_asset.platform.name, target_asset.deployment.code, target_asset.name)})

    except:
        failed_assets.append({'AssetName': '{} ({}) > {}'.format(target_asset.platform.name, target_asset.deployment.code, target_asset.name),
                              'L3Name': l3_var.name,
                              'Message': '{} ({}) > {}: Unexpected error.'.format(
                                  target_asset.platform.name, target_asset.deployment.code, target_asset.name)})

    return successful_assets, failed_assets


def save_uploaded_file(file_ref):
    file_name = str(datetime.datetime.now()) + '.json'
    
    try:
        with open(settings.IMPORT_JSON_URL + file_name, 'wb+') as destination:
            for chunk in file_ref.chunks():
                destination.write(chunk)
            destination.close()
    except FileNotFoundError:
        os.mkdir(settings.IMPORT_JSON_URL)
        return save_uploaded_file(file_ref)

    return file_name


def load_uploaded_file(filename):
    file_handle = open(settings.IMPORT_JSON_URL + filename, 'r')
    return file_handle


def dict_to_orm(obj_dict):
    raw_json = json.dumps([obj_dict])
    obj_generator = serializers.deserialize("json", raw_json)
    return next(obj_generator).object


def orm_to_dict(obj_orm):
    raw_json = serializers.serialize('json', [obj_orm])
    return json.loads(raw_json)[0]


recursion_limiter = 0
def variable_to_importable_dict_wrapper(var_orm):
    global recursion_limiter
    recursion_limiter = 0
    return variable_to_importable_dict(var_orm)


def variable_to_importable_dict(var_orm):
    global recursion_limiter
    var_dict = orm_to_dict(var_orm)

    if var_dict['fields']['asset']:            
        var_dict['fields']['asset_name'] = var_orm.asset.name
        del var_dict['fields']['asset']
    if var_dict['fields']['platform']:
        var_dict['fields']['platform_name'] = var_orm.asset.platform.name
        var_dict['fields']['deployment_name'] = var_orm.asset.platform.deployment.name
        del var_dict['fields']['platform']
        del var_dict['fields']['deployment']

    if var_dict['fields']['user']:
        var_dict['fields']['user_email'] = var_orm.user.email
        del var_dict['fields']['user']

    if var_orm.is_custom and recursion_limiter<1:
        recursion_limiter+=1
        component_list = []
        for component in var_orm.components.all():
            component_dict = variable_to_importable_dict(component.child)
            #todo: limit recursion here and determine how many levels to represent.
            component_dict["relation_name"] = component.name
            component_list.append(component_dict)
        var_dict['component_list'] = component_list
        
    return var_dict


class AssetError(Exception):
    pass


def build_asset_key(platform_code, deployment_code, instrument_code=''):
    # Platforms aren't really an instrument
    if platform_code == instrument_code:
        instrument_code = ''

    return "{}|{}|{}".format(platform_code, deployment_code, instrument_code)


def build_asset_dict():
    assets = Asset.objects.select_related('deployment')

    asset_dict = {}
    for asset in assets:
        if not asset.platform:
            continue
        key = build_asset_key(asset.platform.name, asset.deployment.code, asset.name)
        asset_dict[key] = asset

    return asset_dict


def importable_dict_to_variable(var_dict, asset_dict=None):
    if not asset_dict:
        asset_dict = build_asset_dict()

    try:
        platform_key = build_asset_key(var_dict['fields']['platform_name'], var_dict['fields']['deployment_name'])
        platform = asset_dict[platform_key]

    except KeyError:
        raise AssetError("[{}] Could not find platform for: {}({})".format(var_dict['fields']['name'],
                                                                           var_dict['fields']['platform_name'],
                                                                           var_dict['fields']['deployment_name']))

    except ObjectDoesNotExist:
        raise AssetError("[{}] Could not find platform for: {}({})".format(var_dict['fields']['name'], var_dict['fields']['platform_name'], var_dict['fields']['deployment_name']))

    except MultipleObjectsReturned:  
        raise AssetError("[{}] More than one platform found for: {}({})".format(var_dict['fields']['name'], var_dict['fields']['platform_name'], var_dict['fields']['deployment_name']))

    var_dict['fields']['platform'] = platform.id
    var_dict['fields']['deployment'] = platform.deployment.id
   
    try:
        asset_key = build_asset_key(
            var_dict['fields']['platform_name'],
            var_dict['fields']['deployment_name'],
            var_dict['fields']['asset_name']
        )
        asset = asset_dict[asset_key]

    except KeyError:
        raise AssetError("[{}] Could not find asset for: {}({}): {}".format(var_dict['fields']['name'],
                                                                            var_dict['fields']['platform_name'],
                                                                            var_dict['fields']['deployment_name'],
                                                                            var_dict['fields']['asset_name']))

    except ObjectDoesNotExist:
        raise AssetError("[{}] Could not find asset for: {}({}): {}".format(var_dict['fields']['name'], var_dict['fields']['platform_name'], var_dict['fields']['deployment_name'], var_dict['fields']['asset_name']))

    except MultipleObjectsReturned:
        raise AssetError("[{}] More than one asset found for: {}({}): {}".format(var_dict['fields']['name'], var_dict['fields']['platform_name'], var_dict['fields']['deployment_name'], var_dict['fields']['asset_name'])) 

    var_dict['fields']['asset'] = asset.id

    del var_dict['fields']['asset_name']
    del var_dict['fields']['deployment_name']
    del var_dict['fields']['platform_name']

    if 'user_email' in var_dict['fields']:
        user = User.objects.filter(email=var_dict['fields']['user_email']).first()
        if user:
            var_dict['fields']['user'] = user.id
            del var_dict['fields']['user_email']
        else:
            raise forms.ValidationError("User account with email address {} not found. Variable Name: {}".format(var_dict['fields']['user_email'], var_dict['fields']['name']))
    
    variable = dict_to_orm(var_dict)

    return variable


def find_variable(var_dict, asset_dict=None):
    if not asset_dict:
        asset_dict = build_asset_dict()

    try:
        platform_key = build_asset_key(var_dict['fields']['platform_name'], var_dict['fields']['deployment_name'])
        platform = asset_dict[platform_key]

    except KeyError:
        raise AssetError("[{}] Could not find platform for: {}({})".format(var_dict['fields']['name'],
                                                                           var_dict['fields']['platform_name'],
                                                                           var_dict['fields']['deployment_name']))

    except ObjectDoesNotExist:
        raise AssetError("[{}] Could not find platform for: {}({})".format(var_dict['fields']['name'],
                                                                           var_dict['fields']['platform_name'],
                                                                           var_dict['fields']['deployment_name']))

    except MultipleObjectsReturned:
        raise AssetError("[{}] More than one platform found for: {}({})".format(var_dict['fields']['name'],
                                                                                var_dict['fields']['platform_name'],
                                                                                var_dict['fields']['deployment_name']))

    var_dict['fields']['platform'] = platform.id
    var_dict['fields']['deployment'] = platform.deployment.id

    try:
        asset_key = build_asset_key(
            var_dict['fields']['platform_name'],
            var_dict['fields']['deployment_name'],
            var_dict['fields']['asset_name']
        )
        asset = asset_dict[asset_key]

    except KeyError:
        raise AssetError("[{}] Could not find asset for: {}({}): {}".format(var_dict['fields']['name'],
                                                                            var_dict['fields']['platform_name'],
                                                                            var_dict['fields']['deployment_name'],
                                                                            var_dict['fields']['asset_name']))

    except ObjectDoesNotExist:
        raise AssetError("[{}] Could not find asset for: {}({}): {}".format(var_dict['fields']['name'],
                                                                            var_dict['fields']['platform_name'],
                                                                            var_dict['fields']['deployment_name'],
                                                                            var_dict['fields']['asset_name']))

    except MultipleObjectsReturned:
        raise AssetError("[{}] More than one asset found for: {}({}): {}".format(var_dict['fields']['name'],
                                                                                 var_dict['fields']['platform_name'],
                                                                                 var_dict['fields']['deployment_name'],
                                                                                 var_dict['fields']['asset_name']))

    var_dict['fields']['asset'] = asset.id

    if var_dict['fields']['is_custom']:
        variables = Variable.objects.filter(asset_id=asset.id, is_custom=True).prefetch_related("components__child")
        for test_variable in variables:
            test_exp = test_variable.expression
            components = test_variable.components.all()
            for component in components:
                test_exp.replace(component.name, component.child.erddap_id)
            
            dict_exp = var_dict['fields']['expression']
            for dict_component in var_dict['component_list']:
                dict_exp.replace(dict_component["relation_name"], dict_component["fields"]["erddap_id"])


            if(test_exp == dict_exp):
                variable = test_variable
                break
        if not variable:
            import copy
            source_dict = copy.deepcopy(var_dict)
            variable = importable_dict_to_variable(source_dict, asset_dict)
    
            # First make sure required component variables can be found
            components = find_components_for_variable(source_dict, variable, asset_dict)

            for component in components:
                component.parent = variable
            
    else:
        variable = Variable.objects.filter(asset_id=asset.id, erddap_id=var_dict['fields']['erddap_id']).prefetch_related('components').first()
        components = variable.components.all()

    if not variable:
        raise AssetError("[{}] Could not find variable {} ({}) : {}({}): {}".format(
            var_dict['fields']['name'],
            var_dict['fields']['name'],
            var_dict['fields']['erddap_id'],
            var_dict['fields']['platform_name'],
            var_dict['fields']['deployment_name'],
            var_dict['fields']['asset_name']))

    return variable, components


def find_components_for_variable(parent_var, parent_var_orm, asset_dict=None):
    components = []

    for component in parent_var['component_list']:
        if component['fields']['is_custom']:
            raise Exception('Importing of L3 variables comprised of other L3 variables is not supported.')

        child_variable = Variable.objects.filter(
            erddap_id=component['fields']['erddap_id'],
            asset_id=parent_var_orm.asset.id,
        ).first()

        if not child_variable:
            raise Exception("Unable to located required variable {} for L3.".format(component['fields']['erddap_id']))

        new_component = VariableComponent()
        new_component.child = child_variable
        new_component.name = component['relation_name']
        components.append(new_component)

    return components


def export_l3_variables(request):
    variable_ids = request.POST.getlist('target_entity_id')

    variables = Variable.objects.select_related('asset__platform__deployment').prefetch_related('components__child__asset__platform__deployment').filter(pk__in=variable_ids)

    var_list = []
    for variable in variables:
        var_dict = variable_to_importable_dict_wrapper(variable)
        var_list.append(var_dict)
    base_obj = {}
    base_obj[KEY_EXPORT_APP_NAME] = EXPORT_APP_NAME
    base_obj[KEY_EXPORT_DATA_TYPE] = EXPORT_APP_VAR
    base_obj[KEY_EXPORT_APP_VERSION] = EXPORT_APP_VERSION
    base_obj['content'] = var_list

    variable_json = json.dumps(base_obj, cls=DjangoJSONEncoder)
    
    file_name = "export_l3_variables"
    if variables.count()==1:
        file_name = slugify(variables.first().name)
    time_stamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    file_str = "attachment; filename={file_name}_{time_stamp}.json".format(file_name=file_name, time_stamp=time_stamp)

    response = HttpResponse(variable_json, content_type='application/json')
    response['Content-Disposition'] = file_str

    return response


def import_l3_variables(request):
    return render(request, 'variable/import_l3_variables.html', {
        'form': UploadFileForm(),
    })



def parse_variables_from_json(var_list, obj_import_ids=[], name_changes={}):
    l3s_not_imported = []
    l3s_imported = []
    error_objs = []

    asset_dict = build_asset_dict()

    for var in var_list:
        try:
            var_orm, components_orm = find_variable(var, asset_dict)
            if not str(var_orm.pk) in obj_import_ids:
                l3s_not_imported.append(var_orm)
                continue

            if str(var_orm.pk) in name_changes:
                var_orm.name = name_changes[str(var_orm.pk)]
            
            var_orm.save()
            for component in components_orm:
                component.save()

            l3s_imported.append(var_orm)

        except AssetError as e:
            error_objs.append(e)

        except forms.ValidationError as e:
            error_objs.append(e)

    return l3s_not_imported, l3s_imported, error_objs


def check_app_version_type(json_obj, data_type):
    if json_obj[KEY_EXPORT_APP_NAME] != EXPORT_APP_NAME:
        raise ValueError("File Format Error: Invalid {}: provided: {} required: {}".format(KEY_EXPORT_APP_NAME, json_obj[KEY_EXPORT_APP_NAME], EXPORT_APP_NAME))

    if json_obj[KEY_EXPORT_APP_VERSION] != EXPORT_APP_VERSION:
        raise ValueError("File Format Error: Invalid {}: provided: {} required: {}".format(KEY_EXPORT_APP_VERSION, json_obj[KEY_EXPORT_APP_VERSION], EXPORT_APP_VERSION))

    if json_obj[KEY_EXPORT_DATA_TYPE] != data_type:
        raise ValueError("File Format Error: Invalid {}: provided: {} required: {}".format(KEY_EXPORT_DATA_TYPE, json_obj[KEY_EXPORT_DATA_TYPE], data_type))
    


@require_http_methods(["POST"])
def confirm_import_l3_variables(request):
    form = UploadFileForm(request.POST, request.FILES)
    filename = ""
    l3s_not_imported = []
    error_objs = []
    if form.is_valid():
        filename = save_uploaded_file(request.FILES['file'])
        raw_json = load_uploaded_file(filename)
        json_obj = json.loads(raw_json.read())
        try:
            check_app_version_type(json_obj, EXPORT_APP_VAR)
        except ValueError as e:
            return render(request, 'variable/import_l3_variables.html', {
                'form': form,
                'error_msg': e
            })

        l3s_not_imported, l3s_imported, error_objs = parse_variables_from_json(json_obj[KEY_EXPORT_CONTENT])
    else:
        return render(request, 'variable/import_l3_variables.html', {
            'form': form,
        })

        
    perms_form = L3VariableForm(True)
    perms_form.fields['owner'].widget.attrs = {'class':'owner'}

    return render(request, 'variable/confirm_import_l3_variables.html', {
        'perms_form': perms_form, 
        'filename': filename,
        'matched_l3s': l3s_not_imported,
        'error_objs': error_objs
    })


def do_import_l3_variables(request):
    filename = request.POST.get("filename")
    obj_import_ids = request.POST.getlist("obj_import_id")
    
    raw_json = load_uploaded_file(filename)
    json_obj = json.loads(raw_json.read())

    name_changes = {}
    for import_id in obj_import_ids:
        name_changes[import_id] = request.POST.get("obj_import_name_{}".format(import_id))
        
    l3s_not_imported, l3s_imported, error_objs = parse_variables_from_json(json_obj[KEY_EXPORT_CONTENT], obj_import_ids, name_changes)

    can_edit_shared_items = check_perm(request, "edit_shared_items")

    if not can_edit_shared_items:
        is_global = False
        set_user = request.user
    else:
        is_global = request.POST.get("is_global") == "on"
        if request.POST.get("owner"):
            set_user = User.objects.filter(pk=request.POST.get("owner")).first()
        else:
            set_user = request.user

    for var in l3s_imported:
        if request.POST.get("override_global") == "on":
            var.is_global = is_global
            var.user = set_user
        var.save()


    return render(request, 'variable/do_import_l3_variables.html', {
        'filename': filename,
        'l3s_imported': l3s_imported,
        'l3s_not_imported': l3s_not_imported,
        'error_objs': error_objs
    })


def plot_to_importable_dict(plot_orm):
    plot_dict = orm_to_dict(plot_orm)

    if plot_dict['fields']['asset']:
        plot_dict['fields']['asset_name'] = plot_orm.asset.name
        plot_dict['fields']['platform_name'] = plot_orm.asset.platform.name
        plot_dict['fields']['deployment_name'] = plot_orm.asset.platform.deployment.name
        del plot_dict['fields']['asset']
        
    if plot_dict['fields']['user']:
        plot_dict['fields']['user_email'] = plot_orm.user.email
        del plot_dict['fields']['user']
    
    return plot_dict


def importable_dict_to_plot(plot_dict, asset_dict = None):
    if not asset_dict:
        asset_dict = build_asset_dict()

    if plot_dict['fields']['asset_name']:
        try:
            asset_key = build_asset_key(
                plot_dict['fields']['platform_name'],
                plot_dict['fields']['deployment_name'],
                plot_dict['fields']['asset_name']
            )
            asset = asset_dict[asset_key]

        except KeyError:
            raise AssetError("[{}] Could not find asset for: {}({}): {}".format(plot_dict['fields']['name'],
                                                                                plot_dict['fields']['platform_name'],
                                                                                plot_dict['fields']['deployment_name'],
                                                                                plot_dict['fields']['asset_name']))

        except ObjectDoesNotExist:
            raise AssetError("[{}] Could not find asset for: {}({}): {}".format(plot_dict['fields']['name'], plot_dict['fields']['platform_name'], plot_dict['fields']['deployment_name'], plot_dict['fields']['asset_name']))

        except MultipleObjectsReturned:
            raise AssetError("[{}] More than one asset found for: {}({}): {}".format(plot_dict['fields']['name'], plot_dict['fields']['platform_name'], plot_dict['fields']['deployment_name'], plot_dict['fields']['asset_name'])) 

        plot_dict['fields']['asset'] = asset.id

    del plot_dict['fields']['asset_name']
    del plot_dict['fields']['deployment_name']
    del plot_dict['fields']['platform_name']

    if 'user_email' in plot_dict['fields']:
        user = User.objects.filter(email=plot_dict['fields']['user_email']).first()
        if user:
            plot_dict['fields']['user'] = user.id
            del plot_dict['fields']['user_email']
        else:
            raise forms.ValidationError("User account with email address {} not found. Plot Name: {}".format(plot_dict['fields']['user_email'], plot_dict['fields']['name']))
        
    plot = dict_to_orm(plot_dict)

    return plot


def export_plots(request):
    plot_ids = request.POST.getlist('target_entity_id')
    from ..models.plot import Plot, PlotVariable, PlatformDefaultPlot
    plots = Plot.objects.select_related('asset__platform__deployment').prefetch_related('plotvariable__variable__asset__platform__deployment').filter(pk__in=plot_ids)
    
    plot_list = []
    for plot in plots:
        raw_plot = plot_to_importable_dict(plot)

        var_list = []

        for pv in plot.plotvariable.all():
            raw_json = serializers.serialize('json', [pv])
            raw_pv = json.loads(raw_json)[0]
            
            raw_var = variable_to_importable_dict_wrapper(pv.variable)

            raw_pv["variable"] = raw_var
            var_list.append(raw_pv)

        raw_plot["plot_variables"] = var_list
        
        default_on_platforms = PlatformDefaultPlot.objects.filter(plot=plot)
        platform_list = []
        for default_platform in default_on_platforms:
            platform_list.append({'platform_name': default_platform.platform_name})

        raw_plot["default_on_platforms"] = platform_list

        plot_list.append(raw_plot)

    base_obj = {}
    base_obj[KEY_EXPORT_APP_NAME] = EXPORT_APP_NAME
    base_obj[KEY_EXPORT_DATA_TYPE] = EXPORT_APP_PLOT
    base_obj[KEY_EXPORT_APP_VERSION] = EXPORT_APP_VERSION
    base_obj['content'] = plot_list
    plot_json = json.dumps(base_obj, cls=DjangoJSONEncoder)

    file_name = "export_plots"
    if plots.count()==1:
        file_name = slugify(plots.first().name)
    time_stamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    file_str = "attachment; filename={file_name}_{time_stamp}.json".format(file_name=file_name, time_stamp=time_stamp)
    response = HttpResponse(plot_json, content_type='application/json')
    response['Content-Disposition'] = file_str

    return response


def import_plots(request):
    return render(request, 'plots/import_plots.html', {
        'form': UploadFileForm(),
    })


def parse_plots_from_json(plot_list, obj_import_ids=[], name_changes={}):
    plots_not_imported = []
    plots_imported = []
    error_objs = []

    asset_dict = build_asset_dict()

    for plot_dict in plot_list:
        try:
            plot_orm = importable_dict_to_plot(plot_dict, asset_dict)
            if not str(plot_orm.pk) in obj_import_ids:
                plots_not_imported.append(plot_orm)
                continue

            # Make sure required variables exist
            variables = []
            errors = []
            for plot_var_dict in plot_dict['plot_variables']:
                var_dict = plot_var_dict['variable']

                try:
                    var_orm, components_orm = find_variable(var_dict, asset_dict)
                    var_orm.save()
                    for component in components_orm:
                        component.save()
                except AssetError as e:
                    errors.append(e)
                    continue

                variables.append({
                    'dict': plot_var_dict,
                    'variable': var_orm
                })

            if len(plot_dict['plot_variables']) != len(variables):
                error_objs.append(AssetError(
                    "Could not import plot {} because some variables could not be found.".format(
                        plot_orm.name)))
                for error in errors:
                    error_objs.append(error)

                continue

            plot_orm.name = name_changes[str(plot_orm.pk)]
            plot_orm.pk = None
            plot_orm.save()

            default_on_platforms = plot_dict['default_on_platforms']

            for default_platform in default_on_platforms:
                platform_default_plot = PlatformDefaultPlot(
                    platform_name=default_platform['platform_name'],
                    plot=plot_orm
                )

                platform_default_plot.save()

            for variable in variables:
                plot_var_orm = dict_to_orm(variable['dict'])
                plot_var_orm.pk = None
                plot_var_orm.plot = plot_orm
                plot_var_orm.variable = variable['variable']
                plot_var_orm.save()

            plots_imported.append(plot_orm)

        except AssetError as e:
            error_objs.append(e)

        except forms.ValidationError as e:
            error_objs.append(e)

    return plots_not_imported, plots_imported, error_objs


@require_http_methods(["POST"])
def confirm_import_plots(request):
    form = UploadFileForm(request.POST, request.FILES)
    filename = ""
    l3s_not_imported = []
    error_objs = []
    if form.is_valid():
        filename = save_uploaded_file(request.FILES['file'])
        raw_json = load_uploaded_file(filename)
        json_obj = json.loads(raw_json.read())
        try:
            check_app_version_type(json_obj, EXPORT_APP_PLOT)
        except ValueError as e:
            return render(request, 'plots/import_plots.html', {
                'form': form,
                'error_msg': e
            })

        plots_not_imported, plots_imported, error_objs = parse_plots_from_json(json_obj[KEY_EXPORT_CONTENT])
        
        perms_form = PlotForm(initial={'is_global': True})
        perms_form.fields['is_global'].widget = forms.CheckboxInput()

    else:
        return render(request, 'plots/import_plots.html', {
            'form': form,
        })
    return render(request, 'plots/confirm_import_plots.html', {
        'perms_form':perms_form,
        'filename': filename,
        'matched_plots': plots_not_imported,
        'error_objs': error_objs
    })


@require_http_methods(["POST"])
def do_import_plots(request):
    filename = request.POST.get("filename")
    obj_import_ids = request.POST.getlist("obj_import_id")
    raw_json = load_uploaded_file(filename)
    json_obj = json.loads(raw_json.read())
    name_changes = {}
    for import_id in obj_import_ids:
        name_changes[import_id] = request.POST.get("obj_import_name_{}".format(import_id))
  

    plots_not_imported, plots_imported, error_objs = parse_plots_from_json(json_obj[KEY_EXPORT_CONTENT], obj_import_ids, name_changes)


    can_edit_shared_items = check_perm(request, "edit_shared_items")

    if not can_edit_shared_items:
        is_global = False
        set_user = request.user
    else:
        is_global = request.POST.get("is_global") == "on"
        if request.POST.get("owner"):
            set_user = User.objects.filter(pk=request.POST.get("owner")).first()
        else:
            set_user = request.user

    for plot in plots_imported:
        if request.POST.get("override_global") == "on":
            plot.is_global = is_global
            plot.user = set_user
        if request.POST.get("override_timeframes") == "on":
            plot.plot_time_range_id = request.POST.get("timeframe")
        plot.save()

    return render(request, 'plots/do_import_plots.html', {
        'filename': filename,
        'plots_imported': plots_imported,
        'plots_not_imported': plots_not_imported,
        'error_objs': error_objs
    })


def edit_l3_variable(request, variable_id=''):
    is_readonly = False
    can_edit_shared_items = check_perm(request, "edit_shared_items")
    can_edit_other_items = check_perm(request, "edit_other_items")

    try:
        variable = Variable.objects.get(pk=variable_id)

        # Non custom (L3) variables cannot be edited at all
        if not variable.is_custom:
            return redirect('variable.manage_l3_variables')

        # User's cannot modify non-owner, non-global variables
        if not variable.is_global and variable.user_id != request.user.id and not can_edit_other_items:
            is_readonly = True

        # See if the user can edit shared (global) variables
        if variable.is_global and not can_edit_shared_items:
            is_readonly = True
    except:
        variable = Variable()

    assets = Asset.objects.all()
    duration = ''

    if request.method == 'POST':
        initial_user = request.user if not can_edit_shared_items else None
        initial_user_id = initial_user.id if initial_user else None

        form = L3VariableForm(
            can_edit_shared_items,
            request.POST,
            instance=variable,
            initial={'user': initial_user, 'owner': initial_user_id}
        )

        if 'deletionComponentIds' in request.POST:
            deletion_ids_obj = request.POST.get('deletionComponentIds')
            if deletion_ids_obj:
                deletion_id_list = json.loads(deletion_ids_obj)
                VariableComponent.objects.filter(id__in=deletion_id_list[0].values()).delete()

        interval = variable.interpolation_amount
        if interval:
            seconds = interval.total_seconds()
            if seconds < IntervalTimes.SECONDS.value:
                duration_code = L3Duration.SECONDS.value
                duration = seconds
            if IntervalTimes.SECONDS.value <= seconds < IntervalTimes.MINUTES.value:
                duration_code = L3Duration.MINUTES.value
                duration = seconds / IntervalTimes.SECONDS.value
            if seconds >= IntervalTimes.MINUTES.value:
                duration_code = L3Duration.HOURS.value
                duration = seconds / IntervalTimes.MINUTES.value

        if form.is_valid():
            variable = form.save(commit=False)
            variable.is_custom = True
            asset = Asset.objects.get(pk=request.POST.get('selectAsset_assetpicker'))
            platform_asset = Asset.objects.get(pk=asset.platform.id)
            variable.asset = asset
            variable.platform = platform_asset

            if not can_edit_shared_items:
                variable.is_global = False
                variable.user = request.user
            else:
                if not variable.is_global:
                    variable.user = User.objects.get(pk=form.cleaned_data['owner'])

            if request.POST['duration'] and request.POST['duration_code']:
                duration_units = ''
                duration_value = request.POST.get('duration_code')
                if duration_value == str(L3Duration.SECONDS.value):
                    duration_units = 'Seconds'
                if duration_value == str(L3Duration.MINUTES.value):
                    duration_units = 'Minutes'
                if duration_value == str(L3Duration.HOURS.value):
                    duration_units = 'Hours'
                duration_string = str(request.POST.get('duration')) + ' ' + str(duration_units)
                variable.interpolation_amount = duration_string
            else:
                variable.interpolation_amount = None

            variable.save()

            components_obj = request.POST.get('saveComponents')
            components_obj_list = json.loads(components_obj)
            for component_obj in components_obj_list:
                component_variable = Variable.objects.get(pk=component_obj['childId'])

                # Do not allow L3 variables of other L3s. They will already be filtered out of the UI
                if component_variable.is_custom:
                    continue

                if component_obj['componentId'] > 0:
                    new_component = VariableComponent.objects.get(pk=component_obj['componentId'])
                else:
                    new_component = VariableComponent()

                new_component.child = component_variable
                new_component.parent = variable
                new_component.name = component_obj['name']
                new_component.save()

            return redirect('variable.manage_l3_variables')
        else:
            comp_obj = str(json.loads(request.POST.get('saveComponents'))).replace("'", '"')

            # Prevents null asset error on re-post since asset picker is cleared on post-back
            variable.asset_id = request.POST.get('selectAsset_assetpicker')
            if request.POST['duration'] and duration == '':
                duration = request.POST['duration']

            return render(request, 'variable/edit_l3_variable.html', {
                'form': form,
                'variable': variable,
                'assets': assets,
                'duration': duration,
                'saved_components': comp_obj,
                "is_readonly": is_readonly,
            })

    else:
        duration_code = ''
        interval = variable.interpolation_amount
        if interval:
            seconds = interval.total_seconds()
            if seconds < IntervalTimes.SECONDS.value:
                duration_code = L3Duration.SECONDS.value
                duration = seconds
            if IntervalTimes.SECONDS.value <= seconds < IntervalTimes.MINUTES.value:
                duration_code = L3Duration.MINUTES.value
                duration = seconds / IntervalTimes.SECONDS.value
            if seconds >= IntervalTimes.MINUTES.value:
                duration_code = L3Duration.HOURS.value
                duration = seconds / IntervalTimes.MINUTES.value

        # Set some defaults if this is someone who cannot edit shared variables
        if not can_edit_shared_items and not variable.pk:
            variable.is_global = False
            variable.user = request.user

        user_id = variable.user.id if variable.user else None
        form = L3VariableForm(
            can_edit_shared_items,
            instance=variable,
            initial={'duration': duration, 'duration_code': duration_code, 'user': variable.user, 'owner': user_id}
        )

    return render(request, 'variable/edit_l3_variable.html', {
        'form': form,
        'variable': variable,
        'assets': assets,
        'duration': duration,
        #'can_edit_global': can_edit_global
        "is_readonly": is_readonly,
    })


def saved_component_list(request):
    components_obj = str((request.POST.get('components'))).replace('&quot;', '"')
    vars = json.loads(components_obj)
    list = []

    for var in vars:
        variable = Variable.objects.get(pk=var['childId'])

        # The names here must match the output from the component_list method
        list.append({
            'id': var['componentId'],
            'child__id': var['childId'],
            'name': var['name'],
            'child__name': variable.name,
            'child__asset__id': variable.asset.id,
            'child__asset__name': variable.asset.name,
            'child__platform__id': variable.platform.id,
            'child__platform__name': variable.platform.name,
            'child__deployment__id': variable.deployment.id,
            'child__deployment__name': variable.deployment.code
        })

    return DataTablesResponse(list)


def component_list(request, variable_id):
    components = VariableComponent.objects\
        .filter(parent_id=variable_id)\
        .values('id', 'child__id', 'name', 'child__name', 'child__asset__id', 'child__asset__name',
                'child__platform__id', 'child__platform__name', 'child__deployment__id', 'child__deployment__name')

    return DataTablesResponse(list(components))


def l3_variable_list(request):
    asset_id = request.POST['asset_id']
    variable_disposition = request.POST['variable_disposition']

    if asset_id != '' and variable_disposition != '':
        asset = get_object_or_404(Asset, pk=asset_id)
        platform_id = asset.platform_id or asset.id
        variables = exec_dict('get_custom_variables', (platform_id, variable_disposition))

    elif asset_id == '' and variable_disposition == '':
        variables = exec_dict('get_custom_variables', (None, ''))

    elif asset_id != '' and variable_disposition == '':
        asset = get_object_or_404(Asset, pk=asset_id)
        platform_id = asset.platform_id or asset.id
        variables = exec_dict('get_custom_variables', (platform_id, ''))
    else:
        variables = exec_dict('get_custom_variables', (None, variable_disposition))

    return DataTablesResponse(list(variables))


def get_related_triggers_plots(request):
    variable_id = request.POST.get("variable_id")
    triggers = get_related_triggers(variable_id)
    plots = get_related_plots(variable_id)

    return JsonResponse({
        "triggers": [t.name for t in triggers],
        "plots": [p.name for p in plots]
    })


def variable_list_by_asset(request, asset_id):
    variables = Variable.objects.filter(asset_id=asset_id) \
        .values_list(
            'id', 'name', 'asset__id', 'asset__name', 'platform__id', 'platform__name',
            'deployment__id', 'deployment__name', 'units', 'erddap_id',
        ).order_by('name')

    exclude_custom = request.GET.get('exclude_custom', False)
    if exclude_custom=='True':
        variables = variables.filter(is_custom=False)

    return JsonResponse({'variables': list(variables)})


def validate(request):
    variable_alias = request.POST.getlist('variable_alias[]')
    expression = request.POST['expression']
    valid = validate_expression(expression, variable_alias, None)

    return JsonResponse({'success': valid})


def l3_plot_data(request):
    present_asset_id = request.POST['asset']
    expression = request.POST['expression']
    variable_ids = request.POST.getlist('variable_ids[]')
    variable_alias = request.POST.getlist('variable_alias[]')
    title = request.POST['title']
    y_var = request.POST['y_var']
    units = request.POST['units']

    asset = get_object_or_404(Asset, pk=present_asset_id)

    if len(variable_ids) != len(variable_alias):
        raise IndexError("variable_ids must be the same length as variable_alias")
    var_objs = []
    for v_id in variable_ids:
        # TODO: add exception handling
        var_objs.append(Variable.objects.get(id=v_id))
    variable_mapping = collections.OrderedDict((alias, var) for alias, var in
                                                zip(variable_alias, var_objs))
    asset_ids = []
    deployment_ids = []
    plot_data_dict = collections.defaultdict(dict)
    for v in range(0, len(variable_ids)):
        var = Variable.objects.get(pk=variable_ids[v])
        asset_ids.append(var.asset_id)
        deployment_ids.append(var.deployment_id)
    l3_data = Erddap()

    if l3_data.check_erddap_connection():
        l3_data_frame, _ = l3_data.fetch_data(variable_mapping, expression,
                                           'l3_preview',
                                           interp_interval=var.interpolation_amount).to_frame()

        # TODO (badams): pass in L3 variable name and units here
        # a mock L3 variable that exposes some of the same fields as
        # the variable model
        l3_variable_mock = PreviewVariable('Test Expression', 'Some units')
        # simulated response from ERDDAP
        table_struct = l3_data.transform_dataframe([l3_variable_mock],
                                                   l3_data_frame)

    # TODO (badams) add exception case for when ERDDAP is unreachable
    if table_struct:
        plot_data = Plotting().build_l3_plot(table_struct, plot_type='time_series', title=title, y_var=y_var,
                                             units=units, deployment_code=asset.deployment.code, custom_variable=True,
                                             parent_asset=asset.platform.name, present_asset=asset.name)
        plot_data_dict['0000'] = plot_data
    else:
        msg = 'Could not load ERDDAP plot data for custom L3 Variable'
        plot_data_dict['0000'] = {'error': msg}

    return JsonResponse({
        'asset_ids': asset_ids,
        'deployment_ids': deployment_ids,
        'l3_data': plot_data_dict
    })
