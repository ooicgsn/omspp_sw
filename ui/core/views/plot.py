from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.core.mail import EmailMessage
from django.urls import reverse
from django.conf import settings
from django.template.loader import get_template
from django.template import Context
from django.db.models import Q
import uuid
import re
from ..models.asset import Asset
from ..models.plot import Plot, PlotVariable, PlotPage, PlotType, PlotPagePlot, PlotClass, PlotTimeRange
from ..models.account import Profile, User
from ..models.static import AssetPages, YAxisChoices, PlotTypes, PlotRelativeToChoices


def plot_list(request, asset_id):
    asset_overview = request.POST.get("asset_overview") == "True"
    plot_page_id = request.POST.get("plot_page_id")
    page_name = request.POST.get("page_name") or ""
    default_plots_ids = request.POST.get("default_plots")

    page_title = "Edit Plot Page" if plot_page_id else "Create Plot Page"
    selected_plots = ''
    default_plots = []

    plot_class_dict = {}
    for cls in PlotClass.objects.all():
        plot_class_dict.setdefault(cls.name, cls.id)

    if plot_page_id:
        selected_plots = PlotPagePlot.objects.filter(page_id=plot_page_id)
    else:
       plot_page_id = 0; 

    asset = Asset.objects.get(pk=asset_id)
    assets = Asset.objects.filter(platform_id=asset.platform_id)
    plots = []
    for item in assets:
        plots.extend(Plot.objects.filter(asset_id=item.id))

    if default_plots_ids:
        default_plots = Plot.objects.filter(id__in=default_plots_ids.split(","))

        for p in default_plots:
            y = PlotVariable.objects.filter(plot=p, axis='y').values_list('variable', flat=True)[::1] or ['']
            x = PlotVariable.objects.filter(plot=p, axis='x').values_list('variable', flat=True)[::1] or ['']
            p.x_var = x[0]
            p.y_var = y[0]
    else:
        for p in selected_plots:
            y = PlotVariable.objects.filter(plot=p.plot, axis='y').values_list('variable', flat=True)[::1] or ['']
            x = PlotVariable.objects.filter(plot=p.plot, axis='x').values_list('variable', flat=True)[::1] or ['']
            p.plot.x_var = x[0]
            p.plot.y_var = y[0]

    plot_pages = PlotPage.objects.filter(user_id=request.user.id, asset_id=asset.platform.id, is_asset_page=False)

    return render(request, 'plots/plot_page/plot_create_page.html', {
        'plots': plots,
        'asset': asset,
        'deployment': asset.deployment,
        'page_name': page_name,
        'selected_plots': selected_plots,
        'plot_page_id': plot_page_id,
        'plot_pages': plot_pages,
        'plot_class_dict': plot_class_dict,
        'current_asset_page': AssetPages.CREATE_PLOT_PAGE.value,
        'title': page_title,
        'asset_overview': asset_overview,
        'default_plots': default_plots
    })


def save_plot_page(request):
    plot_ids = [int(x) for x in request.POST.getlist('plot_ids[]')]
    plot_page_id = int(request.POST['plot_page_id'])
    asset_overview = request.POST['asset_overview']

    asset_id = request.POST['asset_id']
    asset = get_object_or_404(Asset, pk=asset_id)

    plot_page = get_object_or_404(PlotPage, pk=plot_page_id) if plot_page_id != 0 else PlotPage()
    plot_page.name = request.POST['plot_page_name']
    plot_page.user = Profile.objects.get(pk=request.user.id).user

    if asset_overview.lower() == 'true':
        plot_page.asset = asset
        plot_page.is_asset_page = True
    else:
        plot_page.asset = Asset.objects.get(pk=asset.platform_id)

    plot_page.save()

    plots = PlotPagePlot.objects.filter(page_id=plot_page.id)
    existing_plot_ids = []
    next_order = 0

    for item in plots:
        # Delete any plots that have been removed
        if item.plot_id not in plot_ids:
            PlotPagePlot.objects.get(page_id=plot_page_id, plot_id=item.plot_id).delete()
            continue

        existing_plot_ids.append(item.plot_id)
        next_order = max(next_order, item.order + 1)

    for plot_id in plot_ids:
        # Skip any existing plots
        if plot_id in existing_plot_ids:
            continue

        plot = Plot.objects.get(pk=plot_id)
        plot_page_plot = PlotPagePlot()
        plot_page_plot.page = plot_page
        plot_page_plot.plot = plot
        plot_page_plot.order = next_order
        plot_page_plot.save()

        next_order += 1

    return JsonResponse({
        'success': True,
        'plot_page_id': plot_page.pk
    })


def plot_page(request, plot_page_id=None, share_code=None):
    plotclass = PlotClass.objects.all()
    plot_class_dict = {}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    time_choices = PlotTimeRange.objects.order_by('-days')
    time_choices_dict = {}
    for i in time_choices:
        time_choices_dict.setdefault(i.id, {"name": i.name, "days": i.days})

    plots = []
    try:
        current_plot_page = PlotPage.objects.get(pk=plot_page_id)
        plot_page_plots = PlotPagePlot.objects.filter(page_id=plot_page_id).select_related('plot').order_by(
            'order')
        plot_page_id = current_plot_page.id
    except:
        current_plot_page = PlotPage.objects.get(share_code=share_code)
        plot_page_plots = PlotPagePlot.objects.filter(page_id=current_plot_page.id).select_related('plot').order_by(
            'order')
        plot_page_id = current_plot_page.id

    shared = (current_plot_page.user != request.user)
    code = current_plot_page.share_code
    p_asset = None

    for item in plot_page_plots:
        platform_plot = Plot.objects.get(pk=item.plot_id)
        asset = Asset.objects.get(pk=platform_plot.asset_id)
        p_asset = asset.platform
        platform_name = p_asset.name
        plots.append(platform_plot)

    if not p_asset:
        return redirect(settings.DASHBOARD_URL)

    for plot in plots:
        if plot.plot_class is None:
            plot.plot_class = plot_class_dict['Timeseries']

        if plot.plot_time_range is None:
            plot.plot_time_range = time_choices.get(pk=1) # default to last 24 hours

        plot.x_var = PlotVariable.objects.filter(plot=plot, axis='x').values_list('variable', flat=True)[::1] or ''
        plot.y_var = PlotVariable.objects.filter(plot=plot, axis='y').values_list('variable', flat=True)[::1] or ''

        y_direction = PlotVariable.objects.filter(plot=plot, axis='y').first().reverse_axis
        plot.yaxis_dir = y_direction

    deployment = current_plot_page.asset.deployment
    plot_pages = PlotPage.objects.filter(user_id=request.user.id, asset_id=p_asset.id, is_asset_page=False)
    is_asset_page = current_plot_page.is_asset_page

    plot_type_choices = [(e.value, e.name) for e in PlotTypes]
    yaxis_choices = [(e.value, e.name.title()) for e in YAxisChoices]

    plot_relative_to_choices = [( e.name, e.value) for e in PlotRelativeToChoices]

    if request.user.id != current_plot_page.user_id and not current_plot_page.share_code:
        return redirect('asset.overview', asset_id=p_asset.id, deployment_code=deployment.code)
    else:
        return render(request, 'plots/plot_page/plot_page.html', {
            'plots': plots,
            'time_choices_dict': time_choices_dict,
            'plot_type_choices': plot_type_choices,
            'plot_relative_to_choices': plot_relative_to_choices,
            'plot_class_dict': plot_class_dict,
            'yaxis_choices': yaxis_choices,
            'name': current_plot_page.name,
            'platform_name': platform_name,
            'asset': p_asset,
            'deployment': deployment,
            'plot_page_id': plot_page_id,
            'current_asset_page': AssetPages.PLOT_PAGE.value,
            'plot_pages': plot_pages,
            'shared': shared,
            'share_code': code,
            'asset_overview': is_asset_page,
            'current_asset_page': AssetPages.PLOT_PAGE.value
        })


def share_plot_page(request):
    plot_page_id = request.POST['plot_page_id']
    if request.user.first_name and request.user.last_name:
        subject = request.user.first_name + ' has shared an OMS++ plot page with you'
        name = request.user.first_name + ' ' + request.user.last_name
    else:
        subject = request.user.username + ' has shared an OMS++ plot page with you'
        name = request.user.username
    email_recip = request.POST['email_recip']
    regex = re.compile('[;,\s]+')
    email_recip_list = regex.split(email_recip)
    uuid_code = uuid.uuid4()
    plot_page = PlotPage.objects.get(pk=plot_page_id)
    if plot_page.share_code == None:
        plot_page.share_code = uuid_code
        plot_page.save()
    else:
        uuid_code = plot_page.share_code
    url = reverse('plot.shared_plot_page', kwargs={'share_code': uuid_code})
    url = settings.SITE_URL + url
    body = get_template('plots/emails/shared_plot_page_email.html').render(
        {
            'name': name,
            'url': url
        }
    )
    for email in email_recip_list:
        msg = EmailMessage(subject, body, settings.DEFAULT_FROM_EMAIL, [email])
        msg.content_subtype = 'html'
        msg.send()

    return JsonResponse({'success': True})


def import_plot_page(request):
    plot_page = PlotPage.objects.get(pk=request.POST['plot_page_id'])
    imported_page = PlotPage()
    imported_page.user = request.user
    imported_page.is_asset_page = plot_page.is_asset_page
    imported_page.name = plot_page.name
    imported_page.asset = plot_page.asset
    imported_page.save()

    plotclass = PlotClass.objects.all()
    plot_class_dict = {}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    time_choices = PlotTimeRange.objects.order_by('-days')
    time_choices_dict = {}
    for i in time_choices:
        time_choices_dict.setdefault(i.id, {"name": i.name, "days": i.days})

    plot_page_plots = PlotPagePlot.objects.filter(page_id=plot_page.id).select_related('plot').order_by('plot__name')
    plots = []
    for plot in plot_page_plots:
        new_plot = PlotPagePlot()
        new_plot.page = imported_page
        new_plot.plot = plot.plot
        new_plot.save()
        asset = Asset.objects.get(pk=new_plot.plot.asset_id)
        platform_asset = Asset.objects.get(pk=asset.platform_id)
        platform_name = platform_asset.name
        plots.append(new_plot.plot)

    for plot in plots:
        if plot.plot_class is None:
            plot.plot_class = plot_class_dict['Timeseries']

        if plot.plot_time_range is None:
            plot.plot_time_range = time_choices.get(pk=1) # default to last 24 hours

        plot.x_var = PlotVariable.objects.filter(plot=plot, axis='x').values_list('variable', flat=True)[::1] or ''
        plot.y_var = PlotVariable.objects.filter(plot=plot, axis='y').values_list('variable', flat=True)[::1] or ''


    deployment = platform_asset.deployments.filter(pk=asset.deployment_id).first()
    plot_pages = PlotPage.objects.filter(user_id=request.user.id, asset_id=platform_asset.id, is_asset_page=False)

    return render(request, 'plots/plot_page/plot_page.html', {
        'plots': plots,
        'name': imported_page.name,
        'platform_name': platform_name,
        'asset': platform_asset,
        'deployment': deployment,
        'plot_page_id': imported_page.id,
        'plot_pages': plot_pages

    })


def disable_sharing(request):
    plot_page = PlotPage.objects.get(id=request.POST['plot_page_id'])
    plot_page.share_code = None
    plot_page.save()

    return JsonResponse({'success': True})


def delete_plot_page(request):
    plot_page = get_object_or_404(PlotPage, pk=request.POST['plot_page_id'])
    if request.user != plot_page.user:
        return JsonResponse({'success': False})

    plot_page.delete()
    return JsonResponse({'success': True})


def customize_plot_page(request, plot_page_id=None):
    current_user = Profile.objects.get(pk=request.user.id).user
    num_columns = request.POST.get('num_columns')
    asset_id = request.POST.get('asset_id')
    plot_ids = [int(x) for x in request.POST.getlist('plot_ids[]')]

    index = 0
    plot_page = None
    if plot_page_id == '0':
        plot_page = PlotPage.objects\
            .filter(asset_id=asset_id, user_id=request.user.id, is_asset_page=True)\
            .first()


        if plot_page is None:
            plot_page = PlotPage()
            plot_page.name = "System Monitoring Plots"
            plot_page.user = current_user
            plot_page.asset_id = asset_id
            plot_page.is_asset_page = True
            plot_page.save()

        plot_page_id = plot_page.pk

        plots = Plot.objects.filter(pk__in=plot_ids)
        for plot in plots:
            plot_page_plot = PlotPagePlot()
            plot_page_plot.page = plot_page
            plot_page_plot.plot = plot
            plot_page_plot.order = plot_ids.index(plot.pk)
            plot_page_plot.save()
            index += 1
    else:
        plot_page_plots = PlotPagePlot.objects.filter(page_id=plot_page_id, plot_id__in=plot_ids)
        for plot_page_plot in plot_page_plots:
            plot_page_plot.order = plot_ids.index(plot_page_plot.plot_id)
            plot_page_plot.save()
            index += 1

    after_order = PlotPagePlot.objects\
        .filter(page_id=plot_page_id, plot_id__in=plot_ids)\
        .order_by("order")\
        .values_list('plot_id', flat=True)
    
    if num_columns:
        if not plot_page:
            plot_page = PlotPage.objects.get(pk=plot_page_id)
        plot_page.num_columns = num_columns
        plot_page.save()

    return JsonResponse({
        'success': True, 
        'plot_page_id': plot_page_id,
        'after_order': list(after_order),
    })	


def plot_list_by_asset(request):
    asset_id = request.POST['asset_id']
    plots = Plot.objects.filter(asset_id=asset_id).filter(Q( user_id=request.user.id)| Q(is_global=True))
    plot_list = []

    plotclass = PlotClass.objects.all()
    plot_class_dict = {}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    for plot in plots:
        plot.x_var = PlotVariable.objects.filter(plot=plot, axis='x').values_list('variable', flat=True)[::1] or ''
        plot.y_var = PlotVariable.objects.filter(plot=plot, axis='y').values_list('variable', flat=True)[::1] or ''

        try:
            plot_type = plot.plot_type.name
        except:
            plot_type = PlotTypes['Line'].name

        try:
            plot_class = plot.plot_class.id
        except:
            plot_class = plot_class_dict['Timeseries']

        y_direction = PlotVariable.objects.filter(plot=plot, axis='y').first().reverse_axis

        plot_list.append([plot.id, plot.name, plot.asset_id, plot.x_var, plot.y_var, plot_type, plot_class, y_direction])

    return JsonResponse({'plots': list(plot_list)})
