from pprint import pprint
import os
import glob
import re
from pathlib import Path
import collections, time
import numpy as np
import pandas as pd
import re
import datetime
import dateutil.parser
import json
import urllib.parse
from enum import Flag

from django.conf import settings
from django.db.models import Q, Case, When, Count
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.http import JsonResponse, HttpResponseNotFound, HttpResponseForbidden, HttpResponseBadRequest, \
    HttpResponseRedirect, HttpRequest
from django.views.decorators.cache import never_cache

from ..models.asset import Asset, Group, AssetLink, AssetTypes, AssetGroups, AssetSubcategory, AssetDisposition, AssetDispositions, AssetLocation
from django.forms.models import model_to_dict
from ..models.asset import Asset, Group, AssetLink, Deployment, AssetTypes, AssetGroups, AssetDispositions, \
    AssetClassification, \
    SNAPSHOT_CPM_MPIC_ERROR_FLAGS, SNAPSHOT_DCL_MPIC_ERROR_FLAGS, \
    SNAPSHOT_PSC_ERROR_FLAGS_1, SNAPSHOT_PSC_ERROR_FLAGS_2, SNAPSHOT_PSC_ERROR_FLAGS_3, \
    SNAPSHOT_MPEA_ERROR_FLAGS_1, SNAPSHOT_MPEA_ERROR_FLAGS_2, \
    SBD_MO_STATUSES, SNAPSHOT_PSC_OVERRIDE_FLAGS, WfpProfileStatuses, SNAPSHOT_WAKE_CODES

from ..models.alert_trigger import Trigger
from ..models.variable import Variable
from ..models.plot import Plot, PlotVariable, PlotPage, PlotPagePlot, PlotClass, PlotTimeRange
from ..models.account import User
from ..models.static import AssetPages, TimeChoices, YAxisChoices, PlotClasses, PlotTypes, PlotRelativeToChoices
from ..forms.asset import PlatformForm, ImportPlatformForm, ClassificationForm, EditAssetForm
from ..views.alert import clone_trigger
from ..views.plots import clone_plot
from ..assets import strip_asset_name
from ..alerts import purge_alerts_for_deployment
from ..snapshot import load_cpm_snapshot, load_dcl_snapshot
from ..erddap import Erddap
from ..importer import AssetImporter
from ..plotting import Plotting
from ..util import DataTablesResponse, check_perm, parse_deployment_number
from ..schemas.sbd_log_entry import SbdLogEntry
from ..config_parser import ConfigParser

from collections import OrderedDict

import logging
logger = logging.getLogger('django')

READONLY_ERDDAP_IDS = ["time", "deploy_id", "latitude", "lat", "longitude", "lon", "platform", "profile_id"]


def group_overview(request, group_id):
    group = get_object_or_404(Group, pk=group_id)
    platforms = Asset().get_group_platforms(group_id)
    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value, ]

    return render(request, 'asset/group_overview.html', {
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions,
        'group': group,
        'platforms': platforms,
        'bookmark': True
    })


def group_mooring_locations(request, group_id):
    group = get_object_or_404(Group, pk=group_id)
    all_platforms = Asset().get_group_platforms(group_id)

    # cache entire asset_location table for performance boost
    asset_locations = list(AssetLocation.objects.all())
    asset_locations_dict = collections.defaultdict(list)

    for position in asset_locations:
        asset_locations_dict[position.asset_id].append((position.location_date,
                                                        float(position.latitude),
                                                        float(position.longitude)))
    # check query params
    specific_platform_id = request.GET.get('platform_id', '')
    if specific_platform_id:
        platforms = [platform for platform in all_platforms if platform['platform_id'] == int(specific_platform_id)]
        try:
            specific_platform_code = platforms[0]['platform_code']
        except:
            specific_platform_code = ''

    else:
        platforms = all_platforms
        specific_platform_code = ''

    epoch = datetime.datetime.utcfromtimestamp(0).replace(tzinfo=datetime.timezone.utc)
    one_hour_interval = datetime.timedelta(seconds=(60 * 60 * 1))

    output_mooring_data = []
    for mooring in platforms:
        platform_id = mooring['platform_id']
        platform_deployed = mooring['disposition_id'] == AssetDispositions.DEPLOYED.value
        if platform_id in asset_locations_dict and platform_deployed:
            tp_array = []
            mooring_data = asset_locations_dict[platform_id]
            sorted_mooring_data = sorted(mooring_data, key=lambda tup: tup[0], reverse=False)

            time_array = np.array([elem[0] for elem in sorted_mooring_data])
            max_time = np.max(time_array)

            max_time_indx = np.where(time_array == max_time)[0][0]
            js_time = (sorted_mooring_data[max_time_indx][0] - epoch).total_seconds() * 1000

            tp_array.append([js_time, sorted_mooring_data[max_time_indx][1], sorted_mooring_data[max_time_indx][2]])

            previous_time = max_time
            while previous_time > (max_time - datetime.timedelta(days=7)):
                previous_time -= one_hour_interval
                data_exists = np.where(time_array <= previous_time)[0].size > 0
                if data_exists:
                    previous_time_indx = np.where(time_array <= previous_time)[0][-1]
                    js_time = (sorted_mooring_data[previous_time_indx][0] - epoch).total_seconds() * 1000

                    tp_array.append(
                        [js_time, sorted_mooring_data[previous_time_indx][1],
                         sorted_mooring_data[previous_time_indx][2]])

            mooring['time_position_array'] = tp_array

            # pull the platform deployment location and watch circle radius values
            platform = get_object_or_404(Asset, pk=platform_id)
            if platform.lat:
                lat = platform.lat
            else:
                lat = np.mean(tp_array, axis=0)[1]

            if platform.lon:
                lon = platform.lon
            else:
                lon = np.mean(tp_array, axis=0)[2]

            if platform.watch_circle_radius:
                circle = platform.watch_circle_radius
            else:
                circle = 300

            mooring['watch_circle_data'] = [lat, lon, circle]
            output_mooring_data.append(mooring)

        else:
            continue
    try:
        # use for map initialization in JS
        fallback_position = [mooring_data[0][1], mooring_data[0][2]]
    except:
        # WHOI coordinates
        fallback_position = [41.5242, -70.6712]

    return render(request, 'asset/group_mooring_locations.html', {
        'group': group,
        'output_mooring_data': output_mooring_data,
        'specific_platform_code': specific_platform_code,
        'fallback_position': fallback_position,
        'bookmark': True
    })

def _load_snapshots(asset):
    items = []

    dcl_ids = []

    cpms = Asset.objects.filter(platform=asset, type_id=AssetTypes.CPM.value)
    for cpm in cpms:
        cpm_snapshot = load_cpm_snapshot(cpm, asset) if cpm.status_file_path else None
        item = {
            'is_stc': False,
            'has_cpm': True,
            'position': cpm.parent.code,
            'name': cpm.code,
            'snapshot': cpm_snapshot,
            'dcls': [],
            'sort_order': cpm.parent.type_id,
        }

        dcls = Asset.objects.filter(platform=asset, type_id=AssetTypes.DCL.value, parent=cpm)
        for dcl in dcls:
            dcl_ids.append(dcl.id)
            dcl_snapshot = load_dcl_snapshot(dcl.status_file_path) if dcl.status_file_path else None
            item['dcls'].append({
                'name': dcl.code,
                'snapshot': dcl_snapshot
            })

        items.append(item)

    # Find DCLs that have no direct CPM parent and STCs (that also have no CPM parent)
    dcls = Asset.objects\
        .filter(platform=asset, type_id__in=[AssetTypes.DCL.value, AssetTypes.STC.value, ])\
        .exclude(id__in=dcl_ids)
    for dcl in dcls:
        matching_items = [item for item in items if item['position'] == dcl.parent.code]
        if len(matching_items) > 0:
            item = items[0]
        else:
            item = {
                'is_stc': dcl.type_id == AssetTypes.STC.value,
                'has_cpm': False,
                'position': dcl.parent.code,
                'name': '',
                'snapshot': None,
                'dcls': [],
                'sort_order': dcl.parent.type_id
            }
            items.append(item)

        dcl_ids.append(dcl.id)
        dcl_snapshot = load_dcl_snapshot(dcl.status_file_path) if dcl.status_file_path else None
        item['dcls'].append({
            'name': dcl.code,
            'snapshot': dcl_snapshot
        })

    # Sort the top level items in the following order: Buoy, NSIF, MFN, STC, None. This assumes that's the order of
    #   the IDs in the asset type model
    items.sort(key=lambda x: x['sort_order'])

    return items


def parsed_data(request, asset_id):
    asset = get_object_or_404(Asset, pk=asset_id)
    platform = get_object_or_404(Asset, pk=asset.platform_id)

    paths = []

    # First, check for files based on an explicitly set pattern on the asset
    if asset.parsed_data_glob:
        # The root folder is needed for glob to locate the files, but then for processing, it needs to be removed
        root = settings.PARSED_DATA_DIR + '/'

        try:
            paths = [
                Path(str(file.replace(root, '')))
                for file in glob.glob(root + asset.parsed_data_glob)
            ]
        except:
            paths = []

    if not paths:
        physical_path = os.path.join(settings.PARSED_DATA_DIR, asset.parsed_data_dir)

        try:
            paths = Path(physical_path).iterdir()
        except Exception as e:
            paths = []

    # Remove all but .json files
    paths = [path for path in paths if str(path).lower().endswith('.json')]
    paths = sorted(paths, key=lambda x: os.path.basename(x), reverse=True)

    return render(request, 'asset/parsed_data.html', {
        'asset': asset,
        'platform': platform,
        'deployment': asset.deployment,
        'files': [
            {
                'path': x.parent,
                'filename': os.path.basename(x),
            }
            for x in paths
        ],
    })


def _load_parsed_data(asset, path, sub=''):
    filename = os.path.basename(path)

    # Used to determine if, when the data is in a nested element, to merge in the time from the tope level. This
    #   is used for ADCP instruments where the primary data is in a "variable" object, but time is at the root level
    use_root_time = False

    # For most examples of parsed data, the key/column for time is just "time". But in some cases, this can be
    #   different. Specifically, the prawler data uses "epoch_time"
    time_key = 'time'

    # Some instruments, like the VELPT, store the parsed data in a different JSON structure
    if 'velpt' in filename.lower():
        sub = 'velocity'

    if 'vel3d' in filename.lower():
        sub = 'system'

    if 'ctdmo' in filename.lower() or ('ctdbp' in filename.lower() and 'imm' in path.lower()):
        sub = 'ctd'

    # Check for ADCP instruments, excluding the ADCP's on an IMM!
    if 'adcp' in filename.lower() and 'imm' not in path.lower():
        use_root_time = True
        sub = 'variable'

    # Prawler data has a different format and uses "scidata" and "engdata" for subsections, and "epoch_time" for time
    if asset.does_instrument_have_nested_data and 'prkt' in filename.lower():
        time_key = 'epoch_time'
        if sub == '':
            sub = 'scidata'

    # Set the default subsection for WFP instruments
    if asset.does_instrument_have_nested_data and sub == '':
        sub = 'edata'

    try:
        with open(os.path.join(settings.PARSED_DATA_DIR, path)) as f:
            if sub == '':
                return {}, pd.DataFrame(json.load(f))

            data = json.load(f)

            if asset.does_instrument_have_nested_data:
                if 'profile' in data:
                    common = data['profile']
                    common['type'] = 'profile'

                if 'summarydata' in data:
                    common = data['summarydata']
                    common['type'] = 'summarydata'
            else:
                common = {}

            df = pd.DataFrame(data[sub])
            if df.empty:
                return {}, df

            if use_root_time:
                df.insert(0, 'time', data['time'])

            if time_key in data:
                df[time_key] = pd.to_datetime(df[time_key], unit='s')
                df.index = df[time_key]

            for col in df.columns:
                if df[col].dtype == np.int64:
                    df[col] = df[col].astype(np.int32)

            try:
                profile_status = int(common['profile_status'])
                common['pretty_profile_status'] = WfpProfileStatuses(profile_status).name
            except:
                common['pretty_profile_status'] = ''

            return common, df

    except Exception as e:
        print("JSON data file {} was empty, returning empty data frame: {}".format(filename, str(e)))
        return {}, None


def parsed_data_for_plot(request, asset_id):
    asset = get_object_or_404(Asset, pk=asset_id)
    path = request.POST.get('path')
    sub = request.POST.get('sub', '')
    variables = request.POST.getlist('variables[]')

    # Some data files are empty, exit early if so.
    common, df = _load_parsed_data(asset, path, sub)

    if df is None:
        return JsonResponse({
            'success': False,
            'error': 'Could not parse data file',
        })

    if df.empty:
        return JsonResponse({
            'success': False,
            'error': 'Data file was empty',
        })

    # TODO: Code is duplicated in parsed_data_for_file - clean up
    # Replace NaN with None, otherwise the resulting JSON is invalid and will not parse correctly in the browser
    df = df.replace({np.NaN: None})

    columns = [str(c) for c in df.columns]

    # The time column is either called "time" or "epoch_time"
    if 'time' in columns:
        time_key = 'time'
    elif 'epoch_time' in columns:
        time_key = 'epoch_time'
    else:
        return JsonResponse({
            'success': False,
            'error': 'Variables can not be plotted because there is no variable for time',
        })

    data = {}
    for variable in variables:
        if variable not in columns:
            return JsonResponse({
                'success': False,
                'error': f'Unable to plot variable {variable}',
            })

        data[variable] = df[variable].values.tolist()

    return JsonResponse({
        'success': True,
        'x_axis': [_prepare_timestamp(x) for x in df[time_key].values.tolist()],
        'y_axis': data,
    })


# Timestamps are of varying precision. In order to work properly with plots, they must all me reduced down if the
#   precision is too high. This is done by checking the length of the number and dividing as needed. This might be
#   able to be improved/cleaned up once hte logic is confirmed further.
def _prepare_timestamp(value):
    if not value:
        return value

    # Assuming that this timestamp is in nanoseconds (1 billionth of a second)
    if len(str(value)) == 19:
        return value / 1000000000

    # Assuming that this timestamp is in microseconds (1/1,000,000 second)
    if len(str(value)) == 16:
        return value / 1000000

    # Assuming that this timestamp is in milliseconds
    if len(str(value)) == 13:
        return value / 1000

    return value

def parsed_data_for_file(request, asset_id):
    asset = get_object_or_404(Asset, pk=asset_id)
    path = request.POST.get('path')
    sub = request.POST.get('sub', '')

    # Some data files are empty, exit early if so.
    common, df = _load_parsed_data(asset, path, sub)

    if df is None:
        columns = []
        return JsonResponse({
            'common': common,
            'parsed': False,
            'parse_error': 'Could not parse data file',
            'columns': columns,
            'data': []
        })


    if df.empty:
        columns = [str(c) for c in df.columns]
        print("JSON data file {0} was empty, returning table".format(path))
        return JsonResponse({
            'common': common,
            'parsed': False,
            'parse_error': 'Data file was empty',
            'columns': columns,
            'data': []
        })

    # Replace NaN with None, otherwise the resulting JSON is invalid and will not parse correctly in the browser
    df = df.replace({np.NaN: None})

    data = df.to_records(index=False).tolist()
    columns = [str(c) for c in df.columns]

    # If there is a time column in the data, attempt to format it into a human-readable date (vs. a unix timestamp)
    if 'time' in columns:
        index = columns.index('time')

        # The data is a list of tuples coming out of pandas - each row must be converted to a list in order to update
        #   any of the columns
        data = [list(row) for row in data]

        for row in data:
            value = row[index]

            if not value or not re.match(r'\d+', str(value)):
                continue

            try:
                row[index] = pd.Timestamp(value, unit='s').strftime("%m/%d/%Y %H:%M:%S")
            except:
                # Do nothing - leave the value as-is
                pass

    return JsonResponse({
        'common': common,
        'parsed': True,
        'columns': columns,
        'data': data
    })


@never_cache
def overview(request, asset_id, deployment_code):
    asset = get_object_or_404(Asset, pk=asset_id)
    platform = asset.platform or asset
    group_id = asset.group.id

    deployment = platform.deployment
    variables = Variable().get_asset_variables(asset_id, request.user.id)

    user_id = request.user.id
    # TODO(mchagnon): check what the SQL for this call looks like
    links = AssetLink.objects.filter(asset=asset).filter(is_child=True).select_related('asset')

    # TODO(mchagnon): Needs query cleanup so we're not making tons of database calls
    # only grab plots that pertain to a certain asset and user_id

    plotclass = PlotClass.objects.all()
    plot_class_dict={}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    time_choices = PlotTimeRange.objects.order_by('-days')
    time_choices_dict = {}
    for i in time_choices:
        time_choices_dict.setdefault(i.id, {"name": i.name, "days": i.days})

    plot_page_id = 0
    plot_page_name = 'System Monitoring Plots'
    plot_page = None
    try:
        plot_page = PlotPage.objects.get(asset_id=asset_id, user_id=request.user.id, is_asset_page=True) 
        plot_page_plots = PlotPagePlot.objects.filter(page_id=plot_page.id).order_by("order").select_related("plot")
        plot_page_id = plot_page.id
        plot_page_name = plot_page.name
        plots = []
        for plot in plot_page_plots:
            plots.append(plot.plot)
    except:
        plots = Plot.objects.filter(asset_id=asset_id).filter(Q(is_global=True) | Q(user=request.user))

    for plot in plots:
        # this adds support for plots with multiple variables...
        if plot.plot_class is None:
            plot.plot_class = plot_class_dict['Timeseries']

        plot.x_var = PlotVariable.objects.filter(plot=plot, axis='x').values_list('variable', flat=True)[::1] or ''
        plot.y_var = PlotVariable.objects.filter(plot=plot, axis='y').values_list('variable', flat=True)[::1] or ''

        first_variable = PlotVariable.objects.filter(plot=plot, axis='y').first()
        y_direction = first_variable.reverse_axis if first_variable else False

        plot.yaxis_dir = y_direction

        if plot.plot_time_range is None:
            plot.plot_time_range = time_choices.get(pk=1) # default to last 24 hours

    plot_type_choices = [(e.value, e.name) for e in PlotTypes]

    if plot_page is not None:
        plot_page_num_columns = plot_page.num_columns
    else:
        plot_page_num_columns=2

    plot_relative_to_choices = [( e.name, e.value) for e in PlotRelativeToChoices]
    
    yaxis_choices = [(e.value, e.name.title()) for e in YAxisChoices]

    status_history = Asset().get_asset_status_history(asset_id)
    status_history = [history for history in status_history
                      if (history['previous_status'] != '' and history['reason'] != 'Initial Asset Load')]

    user = User.objects.get(pk=request.user.id)
    plot_pages = PlotPage.objects.filter(user_id=user.id, asset_id=platform.id, is_asset_page=False)

    instruments = _load_snapshots(asset.platform)

    sbd_logs = _build_xeos_sbd_log_file_list(asset)

    return render(request, 'asset/overview.html', {
        'asset': asset,
        'group_id': group_id,
        'deployment': deployment,
        'platform': platform,
        'plots': plots,
        'time_choices_dict': time_choices_dict,
        'plot_type_choices': plot_type_choices,
        'plot_relative_to_choices': plot_relative_to_choices,
        'plot_class_dict': plot_class_dict,
        'yaxis_choices': yaxis_choices,
        'status_history': status_history,
        'links': links,
        'last_updated_time': asset.last_update,
        'plot_pages': plot_pages,
        'current_asset_page': AssetPages.ASSET_OVERVIEW.value,
        'plot_page_id': plot_page_id,
        'plot_page_num_columns': plot_page_num_columns,
        'plot_page_name': plot_page_name,
        'bookmark': True,
        'instruments': instruments,
        'sbd_logs': sbd_logs,
    })


def find_xeos_or_sbd_log_folder(asset):
    # Current logic only supports XEOS or SBD instruments
    folder = 'xeos_sbd' if 'xeos' in asset.code.lower() else 'irid_sbd'

    platform_code = asset.platform.code.lower()
    deployment = asset.deployment.code

    path = f'{platform_code}/{deployment}/{folder}/'
    if Path(f'{settings.RAW_DATA_DIR}/{path}').exists():
        return path

    path = f'{platform_code}/{deployment}/cg_data/{folder}/'
    if Path(f'{settings.RAW_DATA_DIR}/{path}').exists():
        return path

    return ''

def _build_xeos_sbd_log_file_list(asset):
    # Current logic only supports XEOS or SBD instruments
    is_xeos = 'xeos' in asset.code.lower()

    sbd_logs = []

    # Locate log files based on the "common" locations based only on the type (code) of asset
    folder = find_xeos_or_sbd_log_folder(asset)
    if folder:
        absolute_path = os.path.join(settings.RAW_DATA_DIR, folder)

        for item in Path(absolute_path).iterdir():
            if not item.name.endswith('.log'):
                continue

            # Exclude files that start with a year, which are duplicates
            if re.match(r'(?m)^2\d{3}_', item.stem):
                continue

            sbd_logs.append({
                'name': 'XEOS' if is_xeos else 'SBD',
                'path': os.path.join(folder, item.name),
                'filename': item.stem,
            })

    pattern = 'xeos' if is_xeos else 'irid_sbd'
    assets = Asset.objects. \
        filter(platform=asset.platform). \
        filter(code__icontains=pattern). \
        exclude(raw_data_glob__isnull=True). \
        order_by('code')

    for asset in assets:
        absolute_path = os.path.join(settings.RAW_DATA_DIR, asset.raw_data_glob)
        for item in glob.glob(absolute_path):
            if not item.lower().endswith('.log'):
                continue

            sbd_logs.append({
                'name': asset.code,
                'path': os.path.relpath(item, settings.RAW_DATA_DIR),
                'filename': Path(item).stem,
            })

    return sbd_logs


# TODO: (badams) move to ERDDAP module instead?  Refactor ERDDAP module to not
#                require so much passing parameter passing
#   possibly make fetch_data methods return
def transform_plot_data(variables,   
                        time_window,
                        deployment_field='deploy_id',
                        deployment_id=None, return_none=True, x_var=None,
                        interp_interval=None,
                        change_interval=None,
                        start_date=None, end_date=None,
                        plot_relative_to="last_reported_data"
                        ):

    relative_to = plot_relative_to
    df, messages, erddap_url = Erddap().fetch_data(OrderedDict([(v.erddap_id, v) for v in variables]), None, None,
                    time_window,
                    deployment_field=deployment_field,
                    deployment_id=deployment_id,
                    x_var=x_var,
                    interp_interval=interp_interval,
                    change_interval=change_interval,
                    start_date=start_date, end_date=end_date,
                    relative_to=relative_to)
   
    df = df.where(df.notnull(), None)
    if isinstance(df.index, pd.DatetimeIndex):
        conv_data = [
            list(tup) for tup in df
                .set_index(df.index.view(np.int64) / 10 ** 9)
                .to_records()
                .tolist()
        ]
    else:
        conv_data = [list(tup) for tup in df.to_records().tolist()]
    if x_var is None:
        # use name if erddap_id isn't defined (i.e. L3 variables)
        col_names = ['time'] + [v.erddap_id or v.name for v in variables]
        col_units = ['UTC'] + [v.units for v in variables]
    else:
        col_names = [x_var.erddap_id] + [v.erddap_id for v in variables]
        col_units = [x_var.units] + [v.units for v in variables]

    interp_values = {v.erddap_id or v.name:
         str(v.interpolation_amount)
         if v.interpolation_amount is not None
         else None
     for v in variables}

    structure = {
        'table': {
            'columnNames': col_names,
            'columnUnits': col_units,
            'rows': conv_data
        },
        'errors': messages,
        'interp_values': interp_values,
    }

    return structure, erddap_url

def plot_data(request):

    # TODO(mchagnon): Need to define what options for time window we are allowing (plus error checking)
    # (badams) default to -1 day time window
    start_date = request.POST.get('start_date', None)
    end_date = request.POST.get('end_date', None)
    asset_id = request.POST['asset_id']

    time_window = request.POST.get('time_window', -1)

    plot_relative_to = request.POST.get('plot_relative_to', PlotRelativeToChoices.last_reported_data.name)

    plot_type = request.POST.get('plot_type', 'timeseries')
    x_var = request.POST['x_var']
    y_var = request.POST['y_var']

    asset = Asset.objects.get(id=int(asset_id))
    deployment_code = asset.platform.deployment.code

    if plot_relative_to == PlotRelativeToChoices.last_reported_data.name:

        if asset.platform.disposition_id != AssetDispositions.DEPLOYED.value:
            if asset.platform.recovery_date:
                end_date = asset.platform.recovery_date.strftime('%Y-%m-%d')
                start_date_obj = asset.platform.recovery_date + datetime.timedelta(int(time_window))
                start_date = start_date_obj.strftime('%Y-%m-%d')
                plot_relative_to = PlotRelativeToChoices.custom_time_frame.name


    # each variable gets its on erddap_data dict object
    plot_data_dict=collections.defaultdict(dict)
    if plot_type == 'Timeseries':
        # could be looking at multiple deployments in one plot
        for pk in y_var.split(','):
            # error checking the variable.. output custom object instead of 404 message
            try:
                variable = get_object_or_404(Variable, pk=int(pk))
                if variable.deployment is None:
                    var_deployment_code = None
                else:
                    var_deployment_code = variable.deployment.code
            except:
                msg = 'Could not get variable with primary key => {variableID}'.format(variableID = pk)
                plot_data = {'error': msg}
                plot_data_dict[pk] = plot_data
                continue
            erddap_data, erddap_url = transform_plot_data([variable],
                                            time_window,
                                            'deploy_id', var_deployment_code,
                                            interp_interval=variable.interpolation_amount,
                                            change_interval=variable.change_interval,
                                            start_date=start_date, 
                                            end_date=end_date,
                                            plot_relative_to=plot_relative_to
                                            )
            
            if erddap_data:
                # feed deployment code so it's returned in the result
                plot_data = Plotting().build_plot_data(variable.asset,
                                                       var_deployment_code,
                                                       variable, x_var, y_var,
                                                       plot_type, erddap_data, erddap_url)
                plot_data_dict[pk] = plot_data
            else:
                msg = 'Could not load ERDDAP plot data for {asset} => {variable}'.format(asset=variable.asset,
                                                                                         variable=variable)
                plot_data = {'error': msg}
                plot_data_dict[pk] = plot_data

    # logic for comparison plots (non-timeseries)
    if plot_type == 'Comparison':
        variable_id_array = [int(x_var), int(y_var)]

        # generate a list of erddap ids
        data_identifier=(x_var + '-' + y_var).replace(' ','')

        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(variable_id_array)])
        vars = Variable.objects.filter(pk__in=variable_id_array).order_by(preserved)

        variable_asset= vars[0].asset # should be on the same asset so doesn't matter which one we pick

        # check that we are dealing with the same asset.. if not then return an error of some sort
        asset_names = [v.asset.name for v in vars]
        asset_erddap_ids = [v.erddap_id for v in vars]
        same_asset = asset_names[0] == asset_names[1]

        if same_asset:
            # time can't appear more than once in the ERDDAP query, so handle it separately if it occurs
            # in the selected x variable
            if 'time' in asset_erddap_ids:
                erddap_data, erddap_url = transform_plot_data([vars[1]], time_window,
                                                  'deploy_id' ,deployment_code,
                                                  True, vars[0],
                                                  interp_interval=vars[1].interpolation_amount,
                                                  change_interval=vars[1].change_interval,
                                                  start_date=start_date,
                                                  end_date=end_date,
                                                  plot_relative_to=plot_relative_to)
            else:
                erddap_data, erddap_url = transform_plot_data(vars, time_window, 'deploy_id',
                                                  deployment_code,
                                                  interp_interval=vars[1].interpolation_amount,
                                                  change_interval=vars[1].change_interval,
                                                  start_date=start_date,
                                                  end_date=end_date,
                                                  plot_relative_to=plot_relative_to)

            plot_data = Plotting().build_plot_data(variable_asset, deployment_code, [v for v in vars], x_var, y_var,
                                                   plot_type, erddap_data, erddap_url)
            plot_data_dict[data_identifier] = plot_data
        else:
            plot_data_dict[data_identifier] = {'errors': ['Variables are from different assets'],'data':[]}

    return JsonResponse(plot_data_dict)


def ajax_alert_history_data(request):
    asset_id = request.POST['asset_id']
    include_all = request.POST['include_all']

    return Asset().get_alerts(asset_id, include_all)


def ajax_status_history_data(request, asset_id):
    status_history = Asset().get_asset_status_history(asset_id)
    status_history = [history for history in status_history
                      if (history['previous_status'] != '' and history['reason'] != 'Initial Asset Load')]

    return DataTablesResponse(status_history)


def status_grid(request, platform_id):
    platform = Asset.objects.get(pk=platform_id)

    return render(request, "asset/status_grid.html", {
        'platform': platform,
        'callback': request.GET.get('callback'),
    })


def picker(request, asset_id, name):
    all_platforms = Asset.objects.filter(type_id=AssetTypes.PLATFORM.value)
    deployed_platforms = Asset.objects.filter(type_id=AssetTypes.PLATFORM.value, disposition_id=AssetDispositions.DEPLOYED.value)

    # Exclude gliders since there we do not have their entire hierarchy in place yet
    all_platforms = all_platforms.exclude(group_id=AssetGroups.GLIDERS.value)
    deployed_platforms = deployed_platforms.exclude(group_id=AssetGroups.GLIDERS.value)

    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value, ]

    platforms = sorted(all_platforms, key=lambda x: ("{}-{}-{}".format(x.name, x.deployment.sortable_code, x.deployment.code)))
    deployed_platforms = sorted(deployed_platforms, key=lambda x: ("{}-{}-{}".format(x.name, x.deployment.sortable_code, x.deployment.code)))

    if asset_id != '0':
        asset = Asset.objects.get(pk=asset_id)

        return render(request, "asset/picker.html", {
            'platforms': platforms,
            'selected_asset': asset,
            'name': name,
            'dispositions': dispositions,
            'deployed_platforms': deployed_platforms,
            'shown_dispositions': shown_dispositions
        })
    else:
        return render(request, "asset/picker.html", {
            'platforms': platforms,
            'name': name,
            'dispositions': dispositions,
            'deployed_platforms': deployed_platforms,
            'shown_dispositions': shown_dispositions
        })

def compute_latest_time(glider_array):

    all_times=np.array([])
    for glider in glider_array:
        last_update_time = glider.last_update or datetime.datetime.min
        all_times=np.append(all_times,last_update_time)

    latest_time = np.max(all_times)
    if latest_time == datetime.datetime.min:
        latest_time = ''

    return latest_time

def glider_overview(request, group_id):
    group = get_object_or_404(Group, pk=group_id)
    platforms = Asset.objects.filter(glider_group=group).order_by('code')
    platforms = sorted(list(platforms), key=lambda platform: (platform.code, parse_deployment_number(platform.deployment.code)))

    plat_dict = collections.OrderedDict()
    final_dict = {}
    for p in platforms:
        if p.name in plat_dict:
            assets = []
            for asset in plat_dict[p.name]:
                assets.append(asset)
            assets.append(p)
            plat_dict[p.name] = assets
        else:
            plat_dict[p.name] = [p]

    pointer = 1
    index = 1
    assets = []
    for asset in plat_dict:
        if pointer == 4:
            final_dict[index] = assets
            pointer = 1
            index += 1
            assets = []
        assets.append({asset: {'glider_deployments': plat_dict[asset], 'latest_time': compute_latest_time(plat_dict[asset])}})
        pointer += 1

    final_dict[index] = assets

    return render(request, 'asset/glider_overview.html', {
        'plat_dict': final_dict,
        'group': group
    })


def manage_platforms(request):

    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value, ]

    return render(request, 'asset/manage_platforms.html', {
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions
    })


def platform_list(request):

    platforms = Asset.objects\
        .filter(type_id=AssetTypes.PLATFORM.value)\
        .exclude(group__is_glider=True)\
        .select_related('asset_disposition')\
        .select_related('deployment')\
        .values('id', 'name', 'deployment__code', 'category__name', 'subcategory__name', 'disposition__name')
    
    asset_disposition = request.POST['asset_disposition']
    if asset_disposition!='':
        platforms = platforms.filter(disposition_id__in=asset_disposition.split(","))

    platforms = sorted(list(platforms), key=lambda platform: (platform["name"], parse_deployment_number(platform["deployment__code"])))

    return DataTablesResponse(platforms)


def manage_classifications(request):
    if not check_perm(request, 'manage_classifications'):
        return redirect(settings.DASHBOARD_URL)

    return render(request, 'asset/manage_classifications.html')


def classification_list(request):
    if not check_perm(request, 'manage_classifications'):
        return HttpResponseForbidden()

    assets = Asset.objects.\
        exclude(classification=None).\
        values('classification_id').\
        annotate(count=Count('classification_id'))
    asset_counts = {asset['classification_id']:asset['count'] for asset in assets}

    classifications = AssetClassification.objects.all()\
        .values('id', 'name', 'description', 'abbreviation', 'pattern', 'is_virtual')\
        .order_by('name')
    classifications = list(classifications)

    for classification in classifications:
        classification_id = classification['id']
        classification['asset_count'] = asset_counts[classification_id] if classification_id in asset_counts else 0

    return DataTablesResponse(list(classifications))


def edit_classification(request, classification_id=None):
    if not check_perm(request, 'manage_classifications'):
        return redirect(settings.DASHBOARD_URL)

    classification = get_object_or_404(AssetClassification, pk=classification_id) if classification_id else AssetClassification()

    if request.method == 'POST':
        form = ClassificationForm(request.POST, instance=classification)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('asset.manage_classifications'))
    else:
        form = ClassificationForm(instance=classification)

    return render(request, 'asset/edit_classification.html', {
        'classification': classification,
        'form': form,
        'is_new': classification_id is not None,
    })


def delete_classification(request):
    if not check_perm(request, 'manage_classifications'):
        return HttpResponseForbidden()

    classification_id = request.POST.get('classification_id')
    classification = get_object_or_404(AssetClassification, pk=classification_id)

    num_assets = Asset.objects.filter(classification=classification).count()
    if num_assets > 0:
        return JsonResponse({
            'success': False,
            'error': 'The classification cannot be deleted because there are associated assets.'
        })

    classification.delete()

    return JsonResponse({
        'success': True,
    })


def purge_asset(request, asset_id, deployment_code):
    if not request.user or not request.user.is_superuser:
        return HttpResponseForbidden()

    asset = get_object_or_404(Asset, pk=asset_id)

    return render(request, "asset/purge_asset.html", {
        "asset": asset,
    })


def execute_purge_asset(request):
    if not request.user or not request.user.is_superuser:
        return JsonResponse({'success': False, 'message':"Error: Only superusers can purge assets"})

    try:
        asset_id = request.POST.get('asset_id')
        if Variable.objects.filter(is_custom=True, asset_id=asset_id ).exists():
            return JsonResponse({'success': False, 'message':"Error: Asset has related L3 vars"})
        if Trigger.objects.filter(asset_id=asset_id ).exists():
            return JsonResponse({'success': False, 'message':"Error: Asset has related Trigger"})
        if Plot.objects.filter(asset_id=asset_id ).exists():
            return JsonResponse({'success': False, 'message':"Error: Asset has related Plots"})
        if Asset.objects.filter(parent_id=asset_id).exists():
            return JsonResponse({'success': False, 'message':"Error: Asset has child assets"})

        asset = Asset.objects.get(pk=asset_id)

        if asset.type is AssetTypes.PLATFORM:
            return JsonResponse({'success': False, 'message':"Error: Cannot Purge Platform"})

        asset_platform = asset.platform
        asset.delete()

        assets = Asset.objects.filter(platform=asset_platform)
        for asset in assets:
            Asset().rebuild_asset_links(asset.id)

       
    except Exception as e:
        return JsonResponse({'success': False, 'message':"Error: Unknown Error: " + e})

    return JsonResponse({'success':True, 'message':"Asset Successfully Purged" })

def get_platform_details(request, platform_id):
    if not check_perm(request, 'edit_platforms'):
        return redirect(reverse("asset.manage_platforms"))

    platform = get_object_or_404(Asset, pk=platform_id)

    deployment_date = platform.deployment_date.strftime("%m/%d/%y %I:%M%p").lower() if platform.deployment_date else ''
    recovery_date = platform.recovery_date.strftime("%m/%d/%y %I:%M%p").lower() if platform.recovery_date else ''

    return JsonResponse({
        'disposition': platform.disposition.id,
        'category': platform.category.id,
        'subcategory': platform.subcategory.id,
        'deployment_date': deployment_date,
        'recovery_date': recovery_date,
        'lat': platform.lat,
        'lon': platform.lon,
        'depth': platform.depth,
        'watch_circle_radius': platform.watch_circle_radius,
        'deployment_cruise': platform.deployment_cruise,
        'recovery_cruise': platform.recovery_cruise,
    })

def edit_platform(request, platform_id=None):
    if not check_perm(request, 'edit_platforms'):
        return redirect(reverse("asset.manage_platforms"))

    platform = get_object_or_404(Asset, pk=platform_id)
    update_successful = False

    related_assets = Asset.objects\
        .filter(name=platform.name, platform__name=platform.name)\
        .select_related('deployment')\
        .select_related('disposition')\
        .exclude(id=platform_id)

    if request.method == 'POST':
        form = PlatformForm(request.POST)
        if form.is_valid():
            platform.disposition_id = form.cleaned_data['disposition']
            
            if form.cleaned_data['deployment_date'] != '':
                platform.deployment_date = dateutil.parser.parse(form.cleaned_data['deployment_date'])
            else:
                platform.deployment_date = None

            if form.cleaned_data['recovery_date'] != '':
                platform.recovery_date =  dateutil.parser.parse(form.cleaned_data['recovery_date'])
            else:
                platform.recovery_date = None

            platform.lat = form.cleaned_data['lat']
            platform.lon = form.cleaned_data['lon']
            platform.depth = form.cleaned_data['depth']
            platform.watch_circle_radius = form.cleaned_data['watch_circle_radius']

            if form.cleaned_data['category'] != '0':
                platform.category_id = form.cleaned_data['category']
            else:
                platform.category_id = None

            if form.cleaned_data['subcategory'] != '0':
                platform.subcategory_id = form.cleaned_data['subcategory']
            else:
                platform.subcategory = None

            platform.deployment_cruise = form.cleaned_data['deployment_cruise']
            platform.recovery_cruise = form.cleaned_data['recovery_cruise']

            platform.save()

            update_successful = True
    else:
        form = PlatformForm()
        if platform.disposition:
            form.fields['disposition'].initial = platform.disposition.id
        if platform.category:
            form.fields['category'].initial = platform.category.id
        if platform.subcategory:
            form.fields['subcategory'].initial = platform.subcategory.id
        if platform.deployment_date:
            form.fields['deployment_date'].initial = platform.deployment_date
        if platform.recovery_date:
            form.fields['recovery_date'].initial = platform.recovery_date
        if platform.lat:
            form.fields['lat'].initial = platform.lat
        if platform.lon:
            form.fields['lon'].initial = platform.lon
        if platform.depth:
            form.fields['depth'].initial = platform.depth
        if platform.watch_circle_radius:
            form.fields['watch_circle_radius'].initial = platform.watch_circle_radius
        form.fields['deployment_cruise'].initial = platform.deployment_cruise
        form.fields['recovery_cruise'].initial = platform.recovery_cruise


    return render(request, 'asset/edit_platform.html', {
        'platform': platform,
        'related_assets': related_assets,
        'form': form,
        'groups': Group.objects.all(),
        'update_successful': update_successful,
    })


def subcategories(request):
    category_id = request.POST.get('id')

    subcategories = AssetSubcategory.objects.filter(category_id=category_id).values("id", "name")
    subcategories = [{"id": 0, "name": "Select a Subcategory"}] + list(subcategories)

    return JsonResponse({'data': subcategories})


def clone_asset(request, asset_id):
    asset = Asset.objects.get(pk=asset_id)
    linked_assets = Asset.objects.filter(pk__in=AssetLink.objects.filter(
        is_child=True, asset_id=asset_id).values_list('linked_asset_id', flat=True))
    if check_perm(request, "edit_shared_items"):
        triggers = Trigger.objects.filter(asset_id__in=linked_assets.values_list('id', flat=True)).order_by('asset__name', 'name')
        plots = Plot.objects.filter(asset_id__in=linked_assets.values_list('id', flat=True)).order_by('asset__name', 'name')
    else:
        triggers = Trigger.objects.filter(asset_id__in=linked_assets.values_list('id', flat=True))\
                                  .filter(Q(is_global=True) |
                                          Q(is_global=False, created_by=request.user))\
                                  .order_by('asset__name')
        plots = Plot.objects.filter(asset_id__in=linked_assets.values_list('id', flat=True))\
                            .filter(Q(is_global=True) |
                                    Q(is_global=False, user=request.user))\
                            .order_by('asset__name')

    if asset.type_id == AssetTypes.PLATFORM.value:
        target_assets = Asset.objects.filter(type=asset.type).filter(~Q(id=asset_id)).order_by('name', 'deployment__code').values()
    else:
        # For non-mooring type assets we want to only bring back assets with the same prefix
        stripped_asset_name = strip_asset_name(asset.name)
        target_assets = Asset.objects.filter(type=asset.type, name__startswith=stripped_asset_name).filter(~Q(id=asset_id)).order_by('name', 'deployment__code').values()

    return render(request, 'asset/clone_asset.html', {
        'asset': asset,
        'linked_assets': linked_assets,
        'triggers': triggers,
        'plots': plots,
        'target_assets': target_assets
    })


def execute_clone_asset(request):
    success = False
    successful_cloned_assets = []
    failed_cloned_assets = []
    
    try:
        asset_id = request.POST['asset_id']
        clone_without_owning = check_perm(request, "clone_without_owning")
        target_assets = Asset.objects.filter(pk__in=request.POST['target_asset_ids'].split(','))
        if len(request.POST['clone_trigger_ids']) > 0:
            triggers_to_clone = Trigger.objects.filter(pk__in=request.POST['clone_trigger_ids'].split(',')).order_by('asset__name')
        else:
            triggers_to_clone = None
        if len(request.POST['clone_plot_ids']) > 0:
            plots_to_clone = Plot.objects.filter(pk__in=request.POST['clone_plot_ids'].split(',')).order_by('asset__name')
        else:
            plots_to_clone = None

        # Loop through each target asset and clone the trigger and plots for it
        for target_asset in target_assets:
            successful_triggers = []
            failed_triggers = []
            successful_plots = []
            failed_plots = []

            # Start with the selected triggers
            if triggers_to_clone is not None:
                for trigger in triggers_to_clone:
                    # If the asset isn't an exact match then find a child asset that is
                    if target_asset.type is trigger.asset.type and target_asset.name is trigger.asset.name:
                        asset_to_clone_to = target_asset
                    else:
                        asset_to_clone_to = find_child_asset(target_asset.id, trigger.asset.type.id, trigger.asset.name)

                    if asset_to_clone_to is not None:
                        # Check for a same named trigger
                        try:
                            duplicate_trigger = Trigger.objects.filter(asset_id=asset_to_clone_to.id, name=trigger.name)
                        except:
                            pass

                        # If there is already a trigger with the same name then skip cloning it
                        if len(duplicate_trigger) == 0:
                            # Clone the trigger
                            returned_success, returned_successful_assets, returned_failed_assets = clone_trigger(trigger.id, [asset_to_clone_to], request.user.id, clone_without_owning)
                            successful_triggers += returned_successful_assets
                            failed_triggers += returned_failed_assets
                        else:
                            failed_triggers.append({'AssetName': '{} > {}'.format(asset_to_clone_to.platform.name, asset_to_clone_to.name),
                                                    'TriggerName': trigger.name,
                                                    'Message': '{} > {}: There is already a trigger with the same name for this asset.'.format(
                                                        asset_to_clone_to.platform.name, asset_to_clone_to.name)})
                    else:
                        failed_triggers.append({'AssetName': '{} > {}'.format(target_asset.platform.name, trigger.asset.name),
                                                'TriggerName': trigger.name,
                                                'Message': '{} > {}: No matching asset found.'.format(
                                                    target_asset.platform.name, trigger.asset.name)})

            # Next clone the selected plots
            if plots_to_clone is not None:
                for plot in plots_to_clone:
                    # If the asset isn't an exact match then find a child asset that is
                    if target_asset.type is plot.asset.type and target_asset.name is plot.asset.name:
                        asset_to_clone_to = target_asset
                    else:
                        asset_to_clone_to = find_child_asset(target_asset.id, plot.asset.type.id, plot.asset.name)

                    if asset_to_clone_to is not None:
                        # Check for a same named plot
                        try:
                            duplicate_plot = Plot.objects.filter(asset_id=asset_to_clone_to.id, name=plot.name)
                        except:
                            pass

                        # If there is already a plot with the same name then skip cloning it
                        if len(duplicate_plot) == 0:
                            # Clone the plot
                            returned_success, returned_successful_assets, returned_failed_assets = clone_plot(plot.id, [asset_to_clone_to], request.user.id, clone_without_owning)
                            successful_plots += returned_successful_assets
                            failed_plots += returned_failed_assets
                        else:
                            failed_plots.append({'AssetName': '{} > {}'.format(asset_to_clone_to.platform.name, asset_to_clone_to.name),
                                                 'PlotName': plot.name,
                                                 'Message': '{} > {}: There is already a plot with the same name for this asset.'.format(
                                                     asset_to_clone_to.platform.name, asset_to_clone_to.name)})
                    else:
                        failed_plots.append({'AssetName': '{} > {}'.format(target_asset.platform.name, plot.asset.name),
                                             'PlotName': plot.name,
                                             'Message': '{} > {}: No matching asset found.'.format(
                                                 target_asset.platform.name, plot.asset.name + '')})

            # If at least one item succeed we have a successful cloning process
            if len(successful_plots) > 0 or len(successful_triggers) > 0:
                success = True

                # Build data to be sent back to client side for summary display
                successful_cloned_assets.append({'TargetID': target_asset.id,
                                                 'TargetName': '{} ({})'.format(target_asset.name, target_asset.deployment.code)
                                                               if target_asset.name == target_asset.platform.name
                                                               else '{} on {} ({})'.format(target_asset.name, target_asset.platform.name, target_asset.deployment.code),
                                                 'SuccessfulTriggers': successful_triggers,
                                                 'FailedTriggers': failed_triggers,
                                                 'SuccessfulPlots': successful_plots,
                                                 'FailedPlots': failed_plots})
            else:
                # Build data to be sent back to client side for summary display
                failed_cloned_assets.append({'TargetID': target_asset.id,
                                             'TargetName': '{} ({})'.format(target_asset.name, target_asset.deployment.code)
                                                           if target_asset.name == target_asset.platform.name
                                                           else '{} on {} ({})'.format(target_asset.name, target_asset.platform.name, target_asset.deployment.code),
                                             'SuccessfulTriggers': successful_triggers,
                                             'FailedTriggers': failed_triggers,
                                             'SuccessfulPlots': successful_plots,
                                             'FailedPlots': failed_plots})
    except Exception as e:
        print(str(e))
        success = False

    return JsonResponse({
        'success': success,
        'successful_cloned_assets': successful_cloned_assets,
        'failed_cloned_assets': failed_cloned_assets
    })

def find_child_asset(asset_id, target_type_id, target_name):
    try:
        child_asset = Asset.objects.filter(
            linked_asset__asset_id=asset_id,
            linked_asset__is_child=True,
            linked_asset__linked_asset__type_id=target_type_id,
            linked_asset__linked_asset__name=target_name).first()
        
        if child_asset:
            return child_asset

        prefix = re.sub(r'\d+$', '', target_name)
        child_asset = Asset.objects.filter(
            linked_asset__asset_id=asset_id,
            linked_asset__is_child=True,
            linked_asset__linked_asset__type_id=target_type_id,
            linked_asset__linked_asset__name__istartswith=prefix).first()
    except Exception as e:
        child_asset = None

    return child_asset


def import_platform(request):
    if not check_perm(request, 'edit_platforms'):
        return redirect(reverse("asset.manage_platforms"))

    config_url = ""
    config_path = ""
    data_url = ""
    verified = False
    ready_for_import = False
    existing_asset = None
    new_asset = None
    missing_config = False
    import_failed = False
    import_succeeded = False
    import_message = ""

    if request.method == 'POST':
        form = ImportPlatformForm(request.POST, initial={'disposition': '3'})
        confirmed = request.POST.get('confirmed')

        if form.is_valid():
            # Gather user inputted data
            platform_code = form.cleaned_data['platform_code'].upper()
            deployment_code = form.cleaned_data['deployment_code'].upper()
            category = request.POST.get('category')
            subcategory = request.POST.get('subcategory')
            disposition = request.POST.get('disposition')
            group_id = request.POST.get('group')

            # Locate files needed to do the input
            config_url = Asset.get_config_url(platform_code, deployment_code)
            config_path = Asset.get_config_path(platform_code, deployment_code)
            data_url = Asset.get_data_url(platform_code, deployment_code)
            verified = True

            # Check for an existing asset
            existing_asset = Asset.objects.filter(code__iexact=platform_code,
                                                  deployment__code__iexact=deployment_code).first()

            if confirmed:
                status, msg = AssetImporter().import_asset(
                    config_path,
                    form_platform_code=platform_code,
                    form_deployment_code=deployment_code,
                    override_group_id=group_id
                )

                if status:
                    import_succeeded = True

                    # If this is an existing asset, use the disposition of the existing asset, rather than the
                    #   default (which isn't being passed in anyway)
                    if existing_asset:
                        disposition = existing_asset.disposition

                    # Get the new data and save the values the user specified on this page
                    new_asset = Asset.objects.filter(code__iexact=platform_code, deployment__code__iexact=deployment_code).first()
                    new_asset.category_id = category if category and category != "0" else None
                    new_asset.subcategory_id = subcategory if subcategory and subcategory != "0" else None
                    new_asset.disposition_id = disposition
                    new_asset.save()
                else:
                    import_failed = True
                    import_message = msg

            else:
                ready_for_import = True

                # Make sure the platform config file can be found
                if not config_path:
                    missing_config = True
                    ready_for_import = False

                # Make sure this mooring/deployment hasn't already been imported
                if existing_asset:
                    ready_for_import = False

    else:
        form = ImportPlatformForm(initial={'disposition': '3'})

    return render(request, 'asset/import_platform.html', {
        'form': form,
        'verified': verified,
        'config_url': config_url,
        'config_path': config_path,
        'data_url': data_url,
        'existing_asset': existing_asset,
        'new_asset': new_asset,
        'missing_config': missing_config,
        'ready_for_import': ready_for_import,
        'import_failed': import_failed,
        'import_succeeded': import_succeeded,
        'import_message': import_message,
        'default_disposition': AssetDispositions.INACTIVE.value,
    })


def reload_configuration(request):
    if not check_perm(request, 'edit_platforms'):
        return redirect(reverse("asset.manage_platforms"))

    platform_code = request.POST.get('platform_code')
    deployment_code = request.POST.get('deployment_code')
    source = request.POST.get('source')

    if source == 'cfg':
        config_path = Asset.get_config_path(platform_code, deployment_code)

        status, msg = AssetImporter().import_asset(
            config_path,
            form_platform_code=platform_code,
            form_deployment_code=deployment_code)

        return JsonResponse({
            'success': status,
            'platform_code': platform_code,
            'deployment_code': deployment_code,
            'config_path': config_path,
            'message': msg,
        })

    # Make sure file was uploaded
    file = request.FILES.get('file')
    if not file:
        return JsonResponse({
            'success': False,
            'message': 'A file is required'
        })

    # Check for a yaml extension
    filename, extension = os.path.splitext(file.name.lower())
    if extension not in ['.yml', '.yaml']:
        return JsonResponse({
            'success': False,
            'message': 'A YAML (.yml) file is required'
        })

    file_data: bytes = None
    try:
        file_data = file.read()
    except Exception as e:
        return JsonResponse({
            'success': False,
            'message': 'Could not read uploaded file: ' + str(e)
        })

    platform_config = ConfigParser().parse_bytes(file_data)
    if not platform_config.is_valid:
        return JsonResponse({
            'success': False,
            'message': platform_config.error.replace("\n", "<br />")
        })

    platform = Asset.objects.get(code=platform_code, deployment__code=deployment_code)
    platform.category_id = platform_config.category.id if platform_config.category else None
    platform.subcategory_id = platform_config.subcategory.id if platform_config.subcategory else None
    platform.deployment_date = platform_config.deployed_date
    platform.recovery_date = platform_config.recovered_date
    platform.lat = platform_config.latitude
    platform.lon = platform_config.longitude
    platform.depth = platform_config.depth
    platform.watch_circle_radius = platform_config.watch_circle
    platform.deployment_cruise = platform_config.deployed_cruise
    platform.recovery_cruise = platform_config.recovered_cruise
    platform.save()

    return JsonResponse({
        'success': True,
    })


def purge_alerts(request):
    platform_id = request.POST.get("platform_id")
    deployment_code = request.POST.get("deployment_code")

    if not request.user or not request.user.is_superuser:
        return HttpResponseForbidden(['POST'])

    try:
        asset = Asset.objects.get(pk=platform_id, deployment__code=deployment_code)
    except:
        return HttpResponseNotFound()

    amount, failed = purge_alerts_for_deployment(asset.deployment)

    return JsonResponse({
        "success": True,
        "amount": amount,
        "failed": failed,
    })


def edit_dataset(request, asset_id, deployment_code):
    if not check_perm(request, "manage_erddap_id"):
        return HttpResponseForbidden()

    asset = get_object_or_404(Asset, pk=asset_id)
    variables = Variable.objects.filter(asset=asset, is_custom=False)

    return render(request, "asset/edit_dataset.html", {
        "asset": asset,
        'last_updated_time': asset.last_update,
        "deployment": asset.deployment,
        "variables": variables,
    })


def asset_variables(request):
    asset_id = request.POST.get("asset_id")
    asset = get_object_or_404(Asset, pk=asset_id)
    erddap_id = request.POST.get("erddap_id").strip()
    erddap = Erddap()

    existing = Variable.objects.filter(asset=asset, is_custom=False)
    imported = erddap.get_variables(erddap_id)

    # Initialize the list with existing variables
    variables = []
    for var in existing:
        variables.append({
            "id": var.id,
            "name": var.name,
            "erddap_id": var.erddap_id,
            "units": var.units,
            "matched": False,
            "new_name": "",
            "new_erddap_id": "",
            "new_units": "",
        })

    # Go through each variable found in ERDDAP with the specified ID and try to match it up
    for imported_var in imported:

        found = False
        for var in variables:
            if var["erddap_id"] == imported_var.erddap_id and erddap_id == asset.erddap_id:
                var["matched"] = True
                var["new_name"] = imported_var.name
                var["new_erddap_id"] = imported_var.erddap_id
                var["new_units"] = str(imported_var.units)
                found = True
                break

        if not found:
            variables.append({
                "id": 0,
                "name": "",
                "erddap_id": "",
                "units": "",
                "matched": False,
                "new_name": imported_var.name,
                "new_erddap_id": imported_var.erddap_id,
                "new_units": str(imported_var.units),
            })

    # Give each variable a row # so it can be referenced if there are errors with the user's data
    for i in range(0, len(variables)):
        variables[i]["row"] = i + 1
        variables[i]["readonly"] = variables[i]["erddap_id"].lower() in READONLY_ERDDAP_IDS

    return DataTablesResponse(variables)


def update_erddap(request):
    asset_id = request.POST.get("asset_id")
    asset = get_object_or_404(Asset, pk=asset_id)
    asset_erddap_id = request.POST.get("asset_erddap_id").strip()

    ids = request.POST.getlist("variable_id")
    names = request.POST.getlist("name")
    erddap_ids = request.POST.getlist("erddap_id")
    units = request.POST.getlist("units")
    new_names = request.POST.getlist("new_name")
    new_erddap_ids = request.POST.getlist("new_erddap_id")
    new_units = request.POST.getlist("new_units")
    confirmed = request.POST.get("confirmed", "") == "true"
    remove = request.POST.get("remove")

    errors = []
    changes = []
    num_added = 0
    num_deleted = 0
    num_updated = 0
    has_blockers = False

    if not asset_erddap_id and not remove:
        errors.append("A dataset ID for the asset is missing")
        return JsonResponse({"errors": errors})

    # For removing the dataset, clone and blank out all the expected fields so everything gets tagged as deleted
    if remove:
        new_names = [""] * len(names)
        new_erddap_ids = [""] * len(erddap_ids)
        new_units = [""] * len(units)
        asset_erddap_id = None

    # First pass is to validate all the information and to group variables into update/new/delete
    for i in range(0, len(ids)):
        # If the ERDDAP ID is readonly, do nothing
        if ids[i] in READONLY_ERDDAP_IDS:
            continue

        if not ids[i] and new_names[i] and new_erddap_ids[i] and new_units[i]:
            changes.append("Add <strong>{} ({})</strong>".format(new_names[i], new_erddap_ids[i]))
            if confirmed:
                variable = Variable()
                variable.platform = asset.platform
                variable.asset = asset
                variable.deployment = asset.deployment
                variable.units = new_units[i]
                variable.name = new_names[i]
                variable.erddap_id = new_erddap_ids[i]
                variable.save()

            continue

        if ids[i] and not new_names[i] and not new_erddap_ids[i] and not new_units[i]:
            msg = "Delete <strong>{} ({})</strong>".format(names[i], erddap_ids[i])

            variable = Variable.objects.get(pk=ids[i])
            blockers = variable.get_usage()

            if len(blockers) > 0:
                has_blockers = True
                issue = "This variable cannot be deleted because it is used in: "
                issue += ", ".join(blockers)
                msg += "<br /><span class='text-danger'>{}</span>".format(issue)

            changes.append(msg)

            if confirmed:
                try:
                    variable.delete()
                except Exception as e:
                    errors.append("Row {}: Failed to delete variable - {}".format(i + 1, str(e)))

            continue

        if ids[i] and new_names[i] and new_erddap_ids[i]:
            # If everything is the same, there's nothing to do
            if names[i] == new_names[i] and erddap_ids[i] == new_erddap_ids[i] and units[i] == new_units[i]:
                continue

            changes.append("Update <strong>{} ({}) [{}]</strong> to <strong>{} ({}) [{}]</strong>".format(names[i], erddap_ids[i], units[i], new_names[i], new_erddap_ids[i], new_units[i]))
            if confirmed:
                try:
                    variable = Variable.objects.get(pk=ids[i])
                    variable.name = new_names[i]
                    variable.erddap_id = new_erddap_ids[i]
                    variable.units = new_units[i]
                    variable.save()

                    num_updated += 1
                except Exception as e:
                    errors.append("Row {}: Failed to update variable - {}".format(i + 1, str(e)))
                    pass

            continue

        # If there is no id, and the fields are blank, we can skip this one. Either the user is removing the entire
        #   dataset, or choosing not to add some of the found variables
        if not ids[i] and not new_names[i] and not new_erddap_ids[i] and not new_units[i]:
            continue

        # Handle errors or updates we cannot make
        errors.append("Row {}: All data variable fields are required".format(i + 1))

    if confirmed:
        asset.erddap_id = asset_erddap_id
        asset.save()

    return JsonResponse({
        "confirmed": confirmed,
        "changes": changes,
        "num_added": num_added,
        "num_deleted": num_deleted,
        "num_updated": num_updated,
        "errors": errors,
        "has_blockers": has_blockers,
        "remove": remove,
    })


def get_cpm_mpic_error_flags(request):
    flags = request.GET.get('flags')
    if not flags:
        return HttpResponseBadRequest()

    errors = _get_bit_flag_descriptions(flags, SNAPSHOT_CPM_MPIC_ERROR_FLAGS)

    return JsonResponse({
        'errors': errors,
    })


def get_dcl_mpic_error_flags(request):
    flags = request.GET.get('flags')
    if not flags:
        return HttpResponseBadRequest()

    errors = _get_bit_flag_descriptions(flags, SNAPSHOT_DCL_MPIC_ERROR_FLAGS)

    return JsonResponse({
        'errors': errors,
    })


def get_psc_error_flags(request):
    # This method expects a single flags query parameter that has 3 separate bitmasks, separated by commas
    # E.g: 00000000,00000000,08001080
    flags = request.GET.get('flags')
    if not flags:
        return HttpResponseBadRequest()

    masks = flags.split(',')
    if len(masks) != 3:
        return HttpResponseBadRequest()

    errors1 = _get_bit_flag_descriptions(masks[0].strip(), SNAPSHOT_PSC_ERROR_FLAGS_1)
    errors2 = _get_bit_flag_descriptions(masks[1].strip(), SNAPSHOT_PSC_ERROR_FLAGS_2)
    errors3 = _get_bit_flag_descriptions(masks[2].strip(), SNAPSHOT_PSC_ERROR_FLAGS_3)

    return JsonResponse({
        'errors1': errors1,
        'errors2': errors2,
        'errors3': errors3,
    })


def get_mpea_error_flags(request):
    # This method expects a single flags query parameter that has 2 separate bitmasks, separated by commas
    # E.g: 00000000,00000000,08001080
    flags = request.GET.get('flags')
    if not flags:
        return HttpResponseBadRequest()

    masks = flags.split(',')
    if len(masks) != 2:
        return HttpResponseBadRequest()

    errors1 = _get_bit_flag_descriptions(masks[0].strip(), SNAPSHOT_MPEA_ERROR_FLAGS_1)
    errors2 = _get_bit_flag_descriptions(masks[1].strip(), SNAPSHOT_MPEA_ERROR_FLAGS_2)

    return JsonResponse({
        'errors1': errors1,
        'errors2': errors2,
    })


def get_psc_override_flags(request):
    flags = request.GET.get('flags')
    if not flags:
        return HttpResponseBadRequest()

    errors = _get_bit_flag_descriptions(flags, SNAPSHOT_PSC_OVERRIDE_FLAGS)

    return JsonResponse({
        'errors': errors,
    })

def get_wake_codes(request):
    return JsonResponse({
        'codes': SNAPSHOT_WAKE_CODES,
    })

def get_sbd_mo_description(request):
    try:
        value = int(request.GET.get('value'))

        for option in SBD_MO_STATUSES:
            if option[0] == value:
                return JsonResponse({
                    'description': option[1],
                })
    except:
        pass

    return JsonResponse({
        'description': 'Unknown Value',
    })


def _parse_sbd_log_file(path):
    try:
        with open(path) as file:
            lines = [line.strip() for line in file]
    except Exception as e:
        print(f"Failed to parse log file at {path}: {e}")

        return []

    if len(lines) == 0:
        return []

    pattern = re.compile(
        r'momsn=(?P<momsn>\d+), (?P<timestamp>.*?), ' +\
        r'(?P<mo>\d+) - transfer (?P<transfer_status>[\w ]+), bytes=(?P<transfer_bytes>\d+),' +\
        r'(?: lat: (?P<latitude>[\d\.\-]+) lon: (?P<longitude>[\d\.\-]+))? +' +\
        r'cepradius: (?P<cepradius>\d+)?, (?P<data>.*)',
        re.RegexFlag.IGNORECASE
    )
    entries = []

    for line in lines:
        if not line.strip():
            continue

        try:
            match = pattern.match(line)

            # If there was no match, but there was content, assume it belongs to the prior log entry. That means adding
            #   it to the data field of the prior item
            if match is None:
                if len(entries) > 0:
                    entries[-1].data += '<br />' + line
                continue

            groups = match.groupdict()

            entry = SbdLogEntry(
                momsn=groups.get('momsn'),
                timestamp=groups.get('timestamp'),
                mo=groups.get('mo'),
                transfer_status=groups.get('transfer_status'),
                transfer_bytes=groups.get('transfer_bytes'),
                latitude=groups.get('latitude'),
                longitude=groups.get('longitude'),
                cep_radius=groups.get('cepradius'),
                data=groups.get('data'),
            )

        except Exception as e:
            # If the line cannot be parsed, dump the entire thing into the "data" field as a catchall. This way it is
            #   still visible, and since it will essentially be indented, it will be viewable by an end user and
            #   reported to get the parsing updated.
            logger.error(f'Cannot parse SBD line: --{line}--')
            # logger.error(str(e))

            entry = SbdLogEntry(data=line)

        entries.append(entry)

    return entries


def get_sbd_log_data(request):
    log = request.GET.get('log')
    if not log:
        return JsonResponse({
            'data': []
        })

    # Prevent path traversal issues by making sure the final url stays within the raw data directory
    try:
        Path(settings.RAW_DATA_DIR).joinpath(log).resolve().relative_to(settings.RAW_DATA_DIR)
    except Exception as e:
        return HttpResponseBadRequest()

    try:
        absolute_path = os.path.join(settings.RAW_DATA_DIR, log)
        entries = _parse_sbd_log_file(absolute_path)
    except:
        return JsonResponse({
            'data': []
        })

    return JsonResponse({
        'data': [entry.dict() for entry in entries],
    })


def edit_asset(request: HttpRequest, asset_id: int):
    if not check_perm(request, "edit_asset"):
        return HttpResponseForbidden()

    asset = get_object_or_404(Asset, pk=asset_id)

    if request.POST:
        form = EditAssetForm(request.POST, instance=asset)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(f'/asset/overview/{asset.id}/{asset.deployment.code}')
    else:
        form = EditAssetForm(instance=asset)

    return render(request, 'asset/edit_asset.html', {
        'asset': asset,
        'deployment': asset.deployment,
        'form': form,
    })


#region " Helpers "

def _ensure_valid_bit_flag(flags):
    if not flags or len(flags) < 2:
        return "0x00"

    # Append a leading 0x if it's not there already so Python can convert it to a base 16 integer
    prefix = '0x' if flags[:2] != '0x' else ''

    return f'{prefix}{flags}'


def _get_bit_flag_descriptions(flags, options):
    descriptions = []
    flag_value = int(flags, 16)

    for option in options:
        value = int(option[0], 16)

        # Short circuit return if both the option is 0 and the value is 0 (which means no error)
        if value == 0 and flag_value == 0:
            return [option, ]
        elif value == 0:
            continue

        if value & flag_value == value:
            descriptions.append(option)

    return descriptions


#endregion