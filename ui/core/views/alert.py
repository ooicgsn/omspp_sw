import json
from django.forms.forms import NON_FIELD_ERRORS
from django.shortcuts import render, get_object_or_404, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.urls import reverse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Q, Count
from django.utils.text import slugify
from django.utils import timezone
from core.db import exec_dict
from ..models.alert import Alert, Notification, Note, Occurrence
from ..models.alert_trigger import Trigger, Rule, TriggerUser, TriggerVariable, PlatformDefaultTrigger
from ..models.asset import Asset, Deployment, AssetTypes, AssetLink, AssetDispositions, AssetDisposition
from ..models.static import DeliveryMethods, Statuses, Page, AssetPages, L3Duration, IntervalTimes, TriggerType
from ..models.redmine import Tracker, TrackerField
from ..forms.alert import ManualAlertForm, EditTriggerForm
from ..forms.redmine import RedmineIssueForm
from ..alerts import recalculate_asset_status
from ..variables import get_linked_variable
from ..redmine import Redmine
from ..util import DataTablesResponse
from ..ui import get_alert_bar_css_class
from ..models.variable import Variable, VariableComponent
from ..models.account import User
from ..alerts import send_user_alert_notification
from ..util import check_perm
from django.views.decorators.http import require_http_methods
from ..views.variable import orm_to_dict, dict_to_orm, variable_to_importable_dict_wrapper, importable_dict_to_variable, save_uploaded_file, load_uploaded_file, check_app_version_type, build_asset_dict, build_asset_key, AssetError, find_variable
from ..views.variable import KEY_EXPORT_APP_NAME, EXPORT_APP_NAME, EXPORT_APP_TRIGGER, KEY_EXPORT_DATA_TYPE, KEY_EXPORT_APP_VERSION, EXPORT_APP_VERSION, KEY_EXPORT_CONTENT
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
import datetime
from ..forms.upload_file import UploadFileForm


def resolve_alert_view(request, alert_id):
    alert = get_object_or_404(Alert, pk=alert_id)

    return render(request, 'alert/resolve_alert.html', {
        'alert': alert,
    })


def view_trigger(request, trigger_id):
    trigger = get_object_or_404(Trigger, pk=trigger_id)

    return render(request, 'alert/view_trigger.html', {
        'trigger': trigger,
    })


def get_trigger_by_id(request, trigger_id):
    trigger = get_object_or_404(Trigger, pk=trigger_id)

    return JsonResponse({'severity_id': trigger.severity_id})


def manage_triggers(request, status=''):
    users = User.objects.all()
    dispositions = AssetDisposition.objects.all()
    platforms = Asset.objects.filter(parent_id=None).select_related('platform').select_related('deployment')
    platforms = sorted(platforms, key=lambda a: a.name + str(a.deployment.sortable_code))
    shown_dispositions = [AssetDispositions.DEPLOYED.value,]

    return render(request, 'alert/manage_triggers.html', {
        'users': users,
        'platforms': platforms,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions
    })


def trig_to_importable_dict(trig_orm):
    trig_dict = orm_to_dict(trig_orm)

    if trig_dict['fields']['asset']:
        trig_dict['fields']['asset_name'] = trig_orm.asset.name
        trig_dict['fields']['platform_name'] = trig_orm.asset.platform.name
        trig_dict['fields']['deployment_name'] = trig_orm.asset.platform.deployment.name
        del trig_dict['fields']['asset']

    if trig_dict['fields']['created_by']:
        trig_dict['fields']['created_by_email'] = trig_orm.created_by.email
        del trig_dict['fields']['created_by']
        
    return trig_dict


def importable_dict_to_trig(trig_dict, asset_dict = None):
    if not asset_dict:
        asset_dict = build_asset_dict()

    if trig_dict['fields']['asset_name']:
        try:
            asset_key = build_asset_key(
                trig_dict['fields']['platform_name'],
                trig_dict['fields']['deployment_name'],
                trig_dict['fields']['asset_name']
            )
            asset = asset_dict[asset_key]

        except KeyError:
            raise AssetError("[{}] Could not find asset for: {}({})".format(trig_dict['fields']['name'],
                                                                                trig_dict['fields']['platform_name'],
                                                                                trig_dict['fields']['deployment_name']))                                      
        except ObjectDoesNotExist:
            raise AssetError("[{}] Could not find asset for: {}({}): {}".format(trig_dict['fields']['name'], trig_dict['fields']['platform_name'], trig_dict['fields']['deployment_name'], trig_dict['fields']['asset_name']))
        except MultipleObjectsReturned:  
            raise AssetError("[{}] More than one asset found for: {}({}): {}".format(trig_dict['fields']['name'], trig_dict['fields']['platform_name'], trig_dict['fields']['deployment_name'], trig_dict['fields']['asset_name'])) 
        trig_dict['fields']['asset'] = asset.id

    del trig_dict['fields']['asset_name']
    del trig_dict['fields']['deployment_name']
    del trig_dict['fields']['platform_name']

    if 'created_by_email' in trig_dict['fields']:
        user = User.objects.filter(email=trig_dict['fields']['created_by_email']).first()
        if user:
            trig_dict['fields']['created_by'] = user.id
            del trig_dict['fields']['created_by_email']
        else:
            raise forms.ValidationError("User account with email address {} not found. Trigger Name: {}".format(trig_dict['fields']['created_by_email'], trig_dict['fields']['name']))

    trig = dict_to_orm(trig_dict)
        
    return trig


def export_triggers(request):
    trig_ids = request.POST.getlist('target_entity_id')

    trigs = Trigger.objects.select_related('asset__platform__deployment').prefetch_related('variables__variable__asset__platform__deployment').filter(pk__in=trig_ids)
    trig_list = []
    for trig in trigs:
        raw_trig = trig_to_importable_dict(trig)

        var_list = []
        for tv in trig.variables.all():
            raw_json = serializers.serialize('json', [tv])
            raw_tv = json.loads(raw_json)[0]
            
            raw_var = variable_to_importable_dict_wrapper(tv.variable)

            raw_tv["variable"] = raw_var
            var_list.append(raw_tv)

        raw_trig["trig_variables"] = var_list

        default_on_platforms = PlatformDefaultTrigger.objects.filter(trigger=trig)
        platform_list = []
        for default_platform in default_on_platforms:
            platform_list.append({'platform_name': default_platform.platform_name})

        raw_trig["default_on_platforms"] = platform_list

        trig_list.append(raw_trig)

    base_obj = {}
    base_obj[KEY_EXPORT_APP_NAME] = EXPORT_APP_NAME
    base_obj[KEY_EXPORT_DATA_TYPE] = EXPORT_APP_TRIGGER
    base_obj[KEY_EXPORT_APP_VERSION] = EXPORT_APP_VERSION
    base_obj['content'] = trig_list
    trig_json = json.dumps(base_obj, cls=DjangoJSONEncoder)

    file_name = "export_triggers"
    if trigs.count()==1:
        file_name = slugify(trigs.first().name)
    time_stamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    file_str = "attachment; filename={file_name}_{time_stamp}.json".format(file_name=file_name, time_stamp=time_stamp)
    response = HttpResponse(trig_json, content_type='application/json')
    response['Content-Disposition'] = file_str

    return response


def import_triggers(request):
    return render(request, 'alert/import_triggers.html', {
        'form': UploadFileForm(),
    })


def parse_trigs_from_json(trig_list, obj_import_ids=[], name_changes={}):
    trigs_not_imported = []
    trigs_imported = []
    error_objs = []

    asset_dict = build_asset_dict()

    for trig_dict in trig_list:
        try:
            trig_orm = importable_dict_to_trig(trig_dict, asset_dict)
            if not str(trig_orm.pk) in obj_import_ids:
                trigs_not_imported.append(trig_orm)
                continue

            # Make sure required variables exist
            variables = []
            errors = []
            for trig_var_dict in trig_dict['trig_variables']:
                var_dict = trig_var_dict['variable']

                try:
                    var_orm, components_orm = find_variable(var_dict, asset_dict)            
                    var_orm.save()
                    for component in components_orm:
                        component.save()
                except AssetError as e:
                    errors.append(e)
                    continue

                variables.append({
                    'dict': trig_var_dict,
                    'variable': var_orm
                })

            if len(trig_dict['trig_variables']) != len(variables):
                error_objs.append(AssetError(
                    "Could not import trigger {} because some variables could not be found.".format(
                        trig_orm.name)))
                for error in errors:
                    error_objs.append(error)

                continue

            trig_orm.name = name_changes[str(trig_orm.pk)]
            trig_orm.pk = None
            trig_orm.save()

            default_on_platforms = trig_dict['default_on_platforms']
            for default_platform in default_on_platforms:
                platform_default_trig = PlatformDefaultTrigger(
                    platform_name=default_platform['platform_name'],
                    trigger=trig_orm
                )

                platform_default_trig.save()

            for variable in variables:
                trig_var_orm = dict_to_orm(variable['dict'])
                trig_var_orm.pk = None
                trig_var_orm.trigger = trig_orm
                trig_var_orm.variable = variable['variable']
                trig_var_orm.save()

            trigs_imported.append(trig_orm)
        except AssetError as e:
            error_objs.append(e)
            
        except forms.ValidationError as e:
            error_objs.append(e)

    return trigs_not_imported, trigs_imported, error_objs


@require_http_methods(["POST"])
def confirm_import_triggers(request):
    form = UploadFileForm(request.POST, request.FILES)
    filename = ""
    l3s_not_imported = []
    error_objs = []
    if form.is_valid():
        filename = save_uploaded_file(request.FILES['file'])
        raw_json = load_uploaded_file(filename)
        json_obj = json.loads(raw_json.read())
        try:
            check_app_version_type(json_obj, EXPORT_APP_TRIGGER)
        except ValueError as e:
            return render(request, 'alert/import_triggers.html', {
                'form': form,
                'error_msg': e
            })

        trigs_not_imported, trigs_imported, error_objs = parse_trigs_from_json(json_obj[KEY_EXPORT_CONTENT])
        perms_form = EditTriggerForm(False, initial={'is_global': True})
        perms_form.fields['owner'].widget.attrs = {'class':'owner'}
    else:
        return render(request, 'alert/import_triggers.html', {
            'form': form,
        })
    return render(request, 'alert/confirm_import_triggers.html', {
        'perms_form':perms_form,
        'filename': filename,
        'matched_trigs': trigs_not_imported,
        'error_objs': error_objs
    })


def do_import_triggers(request):
    filename = request.POST.get("filename")
    obj_import_ids = request.POST.getlist("obj_import_id")
    raw_json = load_uploaded_file(filename)
    json_obj = json.loads(raw_json.read())

    name_changes = {}

    for import_id in obj_import_ids:
        name_changes[import_id] = request.POST.get("obj_import_name_{}".format(import_id))

    trigs_not_imported, trigs_imported, error_objs = parse_trigs_from_json(json_obj[KEY_EXPORT_CONTENT], obj_import_ids, name_changes)


    can_edit_shared_items = check_perm(request, "edit_shared_items")
    if not can_edit_shared_items:
        is_global = False
        set_user = request.user
    else:
        is_global = request.POST.get("is_global") == "on"
        if request.POST.get("owner"):
            set_user = User.objects.filter(pk=request.POST.get("owner")).first()
        else:
            set_user = request.user

    for trig in trigs_imported:

        if request.POST.get("override_global") == "on":
            trig.is_global = is_global
            trig.created_by = set_user
        trig.save()


    return render(request, 'alert/do_import_triggers.html', {
        'filename': filename,
        'trigs_imported': trigs_imported,
        'trigs_not_imported': trigs_not_imported,
        'error_objs': error_objs
    })

    
def edit_trigger(request, trigger_id, asset_id=None):
    is_readonly = False
    can_edit_shared_items = check_perm(request, "edit_shared_items")
    can_edit_other_items = check_perm(request, "edit_other_items")

    try:
        trigger = Trigger.objects.get(pk=trigger_id)

        if trigger.is_global and not can_edit_shared_items:
            is_readonly = True

        if not trigger.is_global and trigger.created_by != request.user and not can_edit_other_items:
            is_readonly = True

    except:
        trigger = Trigger()

    if asset_id:
        trigger.asset = Asset.objects.get(pk=asset_id)

    assets = Asset.objects.filter(type=AssetTypes.PLATFORM.value)


    email_selected = TriggerUser.objects.filter(trigger=trigger, delivery_method_id=DeliveryMethods.EMAIL.value).values_list('user_id', flat=True)
    text_selected = TriggerUser.objects.filter(trigger=trigger, delivery_method_id=DeliveryMethods.TEXT.value).values_list('user_id', flat=True)
    summary_selected = TriggerUser.objects.filter(trigger=trigger, delivery_method_id=DeliveryMethods.SUMMARY.value).values_list('user_id', flat=True)

    if request.method == 'POST':
        form = EditTriggerForm(check_perm(request, "edit_shared_items"), request.POST, instance=trigger)

        if 'deletionVariableIds' in request.POST:
            deletion_ids_obj = request.POST.get('deletionVariableIds')
            if deletion_ids_obj:
                deletion_id_list = json.loads(deletion_ids_obj)
                TriggerVariable.objects.filter(id__in=deletion_id_list[0].values()).delete()

        if form.is_valid():
            trigger = form.save(commit=False)
            trigger.created_by = request.user
            trigger.severity_id = form.cleaned_data['severity']
            trigger.duration_code_id = form.cleaned_data['duration_code']
            trigger.asset_id = request.POST.get('selectAsset_assetpicker')

            default_on_platforms = form.cleaned_data['default_on_platforms']

            if not can_edit_shared_items:
                trigger.is_global = False
                trigger.created_by = request.user
            else:
                if not trigger.is_global:
                    trigger.created_by = User.objects.get(pk=form.cleaned_data['owner'])

            if form.cleaned_data['category']:
                trigger.category_id = form.cleaned_data['category']

            if request.POST['interp_duration'] and request.POST['interpolation_code']:
                duration_units = ''
                duration_value = request.POST.get('interpolation_code')
                if duration_value == str(L3Duration.SECONDS.value):
                    duration_units = 'Seconds'
                if duration_value == str(L3Duration.MINUTES.value):
                    duration_units = 'Minutes'
                if duration_value == str(L3Duration.HOURS.value):
                    duration_units = 'Hours'
                duration_string = str(request.POST.get('interp_duration')) + ' ' + str(duration_units)
                trigger.interpolation_amount = duration_string
            else:
                trigger.interpolation_amount = None

            trigger.save()

            variables_obj = request.POST.get('saveVariables')
            variables_obj_list = json.loads(variables_obj)
            for variable_obj in variables_obj_list:
                trigger_variable_variable = Variable.objects.get(pk=variable_obj['childId'])
                if variable_obj['triggerVariableId'] > 0:
                    new_trigger_variable = TriggerVariable.objects.get(pk=variable_obj['triggerVariableId'])
                else:
                    new_trigger_variable = TriggerVariable()

                new_trigger_variable.variable = trigger_variable_variable
                new_trigger_variable.trigger = trigger
                new_trigger_variable.name = variable_obj['name']
                new_trigger_variable.save()

            email_immediately = request.POST.get('email_immediate')
            email_immediately_list = email_immediately.split(',')

            text_immediately = request.POST.get('text_immediate')
            text_immediately_list = text_immediately.split(',')

            email_daily = request.POST.get('email_daily')
            email_daily_list = email_daily.split(',')


            # loop through requested variables and save to trig_variable table if they don't already exist
            PlatformDefaultTrigger.objects.filter(trigger=trigger).delete()
            for platform_d in default_on_platforms:
                defaulttrigger = PlatformDefaultTrigger()
                defaulttrigger.trigger = trigger
                defaulttrigger.platform_name = platform_d
                defaulttrigger.save()
                

            if email_immediately:
                for user in email_immediately_list:
                    if int(user) not in list(email_selected):
                        t_user_email = TriggerUser()
                        t_user_email.delivery_method_id = DeliveryMethods.EMAIL.value
                        t_user_email.trigger = trigger
                        t_user_email.user_id = user
                        t_user_email.save()
            if text_immediately:
                for user in text_immediately_list:
                    if int(user) not in list(text_selected):
                        t_user_text = TriggerUser()
                        t_user_text.delivery_method_id = DeliveryMethods.TEXT.value
                        t_user_text.trigger = trigger
                        t_user_text.user_id = user
                        t_user_text.save()
            if email_daily:
                for user in email_daily_list:
                    if int(user) not in list(summary_selected):
                        t_user_daily = TriggerUser()
                        t_user_daily.delivery_method_id = DeliveryMethods.SUMMARY.value
                        t_user_daily.trigger = trigger
                        t_user_daily.user_id = user
                        t_user_daily.save()

            for user in email_selected:
                if str(user) not in email_immediately_list:
                    TriggerUser.objects.get(trigger=trigger, delivery_method_id=DeliveryMethods.EMAIL.value, user_id=user).delete()

            for user in text_selected:
                if str(user) not in text_immediately_list:
                    TriggerUser.objects.get(trigger=trigger, delivery_method_id=DeliveryMethods.TEXT.value, user_id=user).delete()

            for user in summary_selected:
                if str(user) not in email_daily_list:
                    TriggerUser.objects.get(trigger=trigger, delivery_method_id=DeliveryMethods.SUMMARY.value, user_id=user).delete()

            return redirect('alert.manage_triggers')
        else:
            variable_obj = str(json.loads(request.POST.get('saveVariables'))).replace("'", '"')
            try:
                alert_asset = get_object_or_404(Asset, pk=request.POST.get('selectAsset_assetpicker'))
            except:
                alert_asset = None
            if request.POST['interp_duration']:
                interp_duration = request.POST['interp_duration']
            else:
                interp_duration = ''
            return render(request, 'alert/edit_trigger.html', {
                'trigger': trigger,
                'form': form,
                'assets': assets,
                'alert_asset': alert_asset,
                'saved_vars': variable_obj,
                'interp_duration': interp_duration,
                "is_readonly": is_readonly,
            })
    else:
        interpolation_code = ''
        interp_duration = ''
        interval = trigger.interpolation_amount
        if interval:
            seconds = interval.total_seconds()
            if seconds < IntervalTimes.SECONDS.value:
                interpolation_code = L3Duration.SECONDS.value
                interp_duration = seconds
            if IntervalTimes.SECONDS.value <= seconds < IntervalTimes.MINUTES.value:
                interpolation_code = L3Duration.MINUTES.value
                interp_duration = seconds / IntervalTimes.SECONDS.value
            if seconds >= IntervalTimes.MINUTES.value:
                interpolation_code = L3Duration.HOURS.value
                interp_duration = seconds / IntervalTimes.MINUTES.value
        
        default_on_platforms = list(PlatformDefaultTrigger.objects.filter(trigger=trigger).values_list("platform_name", flat=True))
        form = EditTriggerForm(check_perm(request, "edit_shared_items"),
                               instance=trigger, initial={'severity': trigger.severity_id, 'duration_code': trigger.duration_code_id,
                                                          'default_on_platforms': default_on_platforms, 'category': trigger.category_id, 
                                                          'owner': trigger.created_by_id, 'interpolation_code': interpolation_code})

    return render(request, 'alert/edit_trigger.html', {
        'trigger': trigger,
        'form': form,
        'assets': assets,
        'email_selected': list(email_selected),
        'text_selected': list(text_selected),
        'summary_selected': list(summary_selected),
        'interp_duration': interp_duration,
        "is_readonly": is_readonly,
    })


def alert_notification_list(request, alert_id):
    notifications = Notification.objects\
        .filter(alert=alert_id)\
        .select_related('user')\
        .values_list('created', 'user__first_name', 'user__last_name', 'has_acknowledged', 'id')

    return DataTablesResponse(notifications)


def hotlist_alert_list(request, user_id):
    if request.GET['is_view_acknowledged'] == 'false':
        response = [x for x in list(exec_dict('get_hotlist_alerts', (user_id,))) if x["has_acknowledged"] is False]
    else:
        response = list(exec_dict('get_hotlist_alerts', (user_id,)))

    response = [dict(row, css_class=get_alert_bar_css_class(row['severity_id'])) for row in response]

    return DataTablesResponse(response)


def clone_trigger_page(request, trigger_id):
    trigger = Trigger.objects.get(pk=trigger_id)
    can_clone_shared_items = check_perm(request, "clone_shared_items")
    can_clone_other_items = check_perm(request, "clone_other_items")

    if trigger.is_global and not can_clone_shared_items:
        return redirect(settings.DASHBOARD_URL)

    if not trigger.is_global and trigger.created_by != request.user and not can_clone_other_items:
        return redirect(settings.DASHBOARD_URL)

    try:
        asset = Asset.objects.get(pk=trigger.asset_id)
        variables = Variable.objects.filter(pk__in=TriggerVariable.objects.filter(
            trigger_id=trigger_id).values_list('variable_id', flat=True), is_custom=False)
        l3_vars = Variable.objects.filter(pk__in=TriggerVariable.objects.filter(
            trigger_id=trigger_id).values_list('variable_id', flat=True), is_custom=True)
        target_assets = Asset().get_clone_trigger_assets(trigger_id)
        dispositions = AssetDisposition.objects.all()
        shown_dispositions = [AssetDispositions.DEPLOYED.value, ]

    except ObjectDoesNotExist:
        pass

    return render(request, 'alert/clone_trigger.html', {
        'trigger': trigger,
        'asset': asset,
        'variables': variables,
        'l3_vars': l3_vars,
        'target_assets': target_assets,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions,
    })


def execute_clone_trigger(request):
    clone_without_owning = check_perm(request, "clone_without_owning")
    success, successful_assets, failed_assets = \
        clone_trigger(request.POST['trigger_id'],
                      Asset.objects.filter(pk__in=request.POST['target_asset_ids'].split(',')),
                      request.user.id, clone_without_owning)

    return JsonResponse({'success': success, 'successful_assets': successful_assets, 'failed_assets': failed_assets})


def clone_trigger(trigger_id, target_assets, user_id, clone_without_owning):
    success = True
    successful_assets = []
    failed_assets = []
    try:
        trigger = get_object_or_404(Trigger, pk=trigger_id)
        trigger_variables = TriggerVariable.objects.filter(trigger_id=trigger.id, variable__is_custom=False)
        trigger_l3_vars = TriggerVariable.objects.filter(trigger_id=trigger.id, variable__is_custom=True)
        trigger_users = TriggerUser.objects.filter(trigger_id=trigger.id)

        # Loop through each target asset and clone the trigger for it
        for target_asset in target_assets:
            returned_successful_assets, returned_failed_assets = clone_trigger_for_asset(
                trigger, trigger_variables, trigger_l3_vars, trigger_users, target_asset, user_id, clone_without_owning
            )
            successful_assets += returned_successful_assets
            failed_assets += returned_failed_assets
    except:
        success = False

    if len(successful_assets) is 0:
        success = False

    return success, successful_assets, failed_assets


def clone_trigger_for_asset(trigger, trigger_variables, trigger_l3_vars, trigger_users, target_asset, user_id, clone_without_owning):
    successful_assets = []
    failed_assets = []
    platform_name = target_asset.platform.name if target_asset.platform else target_asset.name
    try:
        mismatched_vars = False
        mismatched_l3_vars = False

        # Get list of child assets to traverse through to find variable assets
        linked_assets = Asset.objects.filter(pk__in=AssetLink.objects.filter(
            is_child=True, asset_id=target_asset.id).values_list('linked_asset_id', flat=True))

        # Loop through trigger variables and find matching assets/triggers on the target asset
        new_trigger_variables = []
        for trigger_variable in trigger_variables:
            try:
                new_trigger_variable = get_linked_variable(trigger_variable.variable, linked_assets,
                                                           target_asset.deployment_id)
                if new_trigger_variable is not None:
                    new_trigger_variables.append(new_trigger_variable)
                else:
                    mismatched_vars = True
            except:
                mismatched_vars = True

        # Loop through and find trigger L3 variables to clone
        new_l3_vars = []
        new_l3_var_components = []
        new_l3_var_variables = []
        for trigger_l3_var in trigger_l3_vars:
            try:
                current_l3_var_component = []
                current_l3_var_variables = []
                trigger_l3_var_variables = Variable.objects.filter(childVariable__parent_id=trigger_l3_var.variable_id)
                for trigger_l3_var_variable in trigger_l3_var_variables:
                    new_l3_variable = get_linked_variable(trigger_l3_var_variable, linked_assets,
                                                          target_asset.deployment_id)
                    if new_l3_variable is not None:
                        current_l3_var_component.append(
                            VariableComponent.objects.filter(parent_id=trigger_l3_var.variable_id,
                                                             child_id=trigger_l3_var_variable.id)[0])
                        current_l3_var_variables.append(new_l3_variable)

                if len(current_l3_var_variables) is len(trigger_l3_var_variables):
                    new_l3_var_components.append(current_l3_var_component)
                    new_l3_vars.append(trigger_l3_var)
                    new_l3_var_variables.append(current_l3_var_variables)
                else:
                    mismatched_l3_vars = True
            except:
                mismatched_l3_vars = True

        # Only clone the trigger if all the variables match
        if len(new_trigger_variables) is len(trigger_variables) and len(new_l3_vars) is len(trigger_l3_vars):
            # Create the new trigger
            new_trigger = Trigger(name=trigger.name,
                                  is_global=trigger.is_global,
                                  duration=trigger.duration,
                                  asset=target_asset,
                                  created_by=trigger.created_by,
                                  duration_code=trigger.duration_code,
                                  severity=trigger.severity,
                                  category=trigger.category,
                                  expression=trigger.expression,
                                  roll_up=trigger.roll_up,
                                  roll_down=trigger.roll_down,
                                  mandatory=trigger.mandatory)

            # If the user cannot clone the plot as is, make sure that the clone is owned by this user and not shared
            if not clone_without_owning and not trigger.is_global:
                new_trigger.created_by_id = user_id
                new_trigger.is_global = False

            new_trigger.save()

            # Save new trigger variables for the cloned asset
            var_index = 0
            for variable in new_trigger_variables:
                var_name = trigger_variables[var_index].name
                new_trigger_variable = TriggerVariable(name=var_name,
                                                       trigger=new_trigger,
                                                       variable=variable)
                new_trigger_variable.save()
                var_index += 1

            # Save new L3 variables for the cloned asset
            var_index = 0
            for l3_var in new_l3_vars:
                # Save parent variable
                new_l3_var = Variable(name=l3_var.variable.name,
                                      is_custom=True,
                                      erddap_id=l3_var.variable.erddap_id,
                                      units=l3_var.variable.units,
                                      is_plottable=l3_var.variable.is_plottable,
                                      expression=l3_var.variable.expression,
                                      asset=target_asset,
                                      deployment=target_asset.deployment,
                                      platform=target_asset.platform,
                                      is_global=l3_var.variable.is_global,
                                      user_id=user_id if not l3_var.variable.is_global else None)
                new_l3_var.save()

                # Save child variables
                current_l3_var_components = new_l3_var_components[var_index]
                current_l3_var_variables = new_l3_var_variables[var_index]
                var_l3_index = 0
                for current_l3_var_variable in current_l3_var_variables:
                    new_var_com = VariableComponent(name=current_l3_var_components[var_l3_index].name,
                                                    child=current_l3_var_variable,
                                                    parent=new_l3_var)
                    new_var_com.save()
                    var_l3_index += 1

                # Save the actual trigger variable for the L3
                new_trigger_variable = TriggerVariable(name=l3_var.name,
                                                       trigger=new_trigger,
                                                       variable=new_l3_var)
                new_trigger_variable.save()
                var_index += 1

            # Save new trigger users for the cloned asset
            for trigger_user in trigger_users:
                new_trigger_user = TriggerUser(delivery_method=trigger_user.delivery_method,
                                               trigger=new_trigger,
                                               user=trigger_user.user)
                new_trigger_user.save()

            successful_assets.append({'TriggerID': new_trigger.id,
                                      'TriggerName': trigger.name,
                                      'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name)})
        else:
            if mismatched_vars and mismatched_l3_vars:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'TriggerName': trigger.name,
                                      'Message': '{} ({}) > {}: Trigger and L3 Variables do not match up.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
            elif mismatched_vars:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'TriggerName': trigger.name,
                                      'Message': '{} ({}) > {}: Trigger Variables do not match up.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
            elif mismatched_l3_vars:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'TriggerName': trigger.name,
                                      'Message': '{} ({}) > {}: L3 Variables do not match up.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
            else:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'TriggerName': trigger.name,
                                      'Message': '{} ({}) > {}: Unexpected error.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
    except:
        failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                              'TriggerName': trigger.name,
                              'Message': '{} ({}) > {}: Unexpected error.'.format(
                                  platform_name, target_asset.deployment.code, target_asset.name)})

    return successful_assets, failed_assets


def trigger_list(request):
    asset_id = request.POST['asset_id']
    trigger_disposition = request.POST['trigger_disposition']

    if trigger_disposition != '':
        disp_array = trigger_disposition.split(',')
    else:
        disp_array = ''

    if asset_id:
        triggers = Trigger.objects.filter(asset__platform__disposition__in=disp_array, asset_id=asset_id)
    else:
        triggers = Trigger.objects.filter(asset__platform__disposition__in=disp_array)
        
    triggers = triggers.annotate(
        num_alerts = Count("alert")
    )

    values = triggers.values(
        'id', 'name', 'severity__name', 'is_global', 'duration', 'duration_code__name', 'asset__id', 'asset__name',
        'asset__platform__id', 'asset__platform__name', 'created', 'created_by__first_name', 'created_by__last_name',
        'asset__deployment__code', 'category__name', 'created_by__username', 'num_alerts', 'created_by_id'
    )

    return DataTablesResponse(list(values))


def trigger_variable_list(request, trigger_id):
    variables = TriggerVariable.objects\
        .filter(trigger_id=trigger_id)\
        .values('id', 'variable_id', 'name', 'variable__name', 'variable__asset__name',
                'variable__platform__name', 'variable__deployment__name',
                'variable__asset__id', 'variable__platform__id', 'variable__deployment__id')

    return DataTablesResponse(list(variables))


def saved_variables_list(request):
    variable_obj = str((request.POST.get('Variables'))).replace('&quot;', '"')
    vars = json.loads(variable_obj)
    list = []

    for var in vars:
        variable = Variable.objects.get(pk=var['childId'])

        # The names here must match the output from the trigger variable list method
        list.append({
            "id": var['triggerVariableId'],
            "variable_id": var['childId'],
            "name": var['name'],
            "variable__name": variable.name,
            "variable__asset__name": variable.asset.name,
            "variable__platform__name": variable.asset.platform.name,
            "variable__deployment__name": variable.asset.deployment.code,
            "variable__asset__id": variable.asset.id,
            "variable__platform__id": variable.asset.platform.id,
            "variable__deployment__id": variable.asset.deployment.id
        })

    return DataTablesResponse(list)


def delete_trigger(request):
    trigger_id = request.POST.get("trigger_id")
    trigger = get_object_or_404(Trigger, pk=trigger_id)

    # Make sure the user has permission to delete the trigger
    if trigger.is_global and not check_perm(request, "delete_shared_items"):
        return JsonResponse({'success': False})

    if not trigger.is_global and trigger.created_by != request.user and not check_perm(request, "delete_other_items"):
        return JsonResponse({'success': False})

    # Some roles can only delete shared triggers if there are no alerts
    alerts = Alert.objects.filter(trigger_id=trigger_id)
    if trigger.is_global and not check_perm(request, 'delete_share_items_with_alerts') and alerts.count() > 0:
        return JsonResponse({"success": False})

    # Delete an associate alerts
    for alert in alerts:
        Alert.delete(alert.id)

    # Delete the trigger
    Trigger.delete(trigger_id)

    return JsonResponse({'success': True})


def purge_trigger(request):
    trigger = get_object_or_404(Trigger, pk=request.POST.get("trigger_id"))
    alerts = Alert.objects.filter(trigger=trigger)
    failed = 0
    amount = 0

    if not request.user or not request.user.is_superuser:
        return HttpResponseForbidden(['POST'])

    if len(alerts) > 0:
        for alert in alerts:
            try:
                Alert.delete(alert.id)
                amount += 1
            except:
                failed += 1

    return JsonResponse({
        "success": "true",
        "amount": amount,
        "failed": failed,
    })


def acknowledge(request):
    # TODO(pduffy): Needs error checking
    if 'alert_id' in request.POST:
        notification = get_object_or_404(Notification, user_id=request.POST['user_id'], alert_id=request.POST['alert_id'])
    else:
        notification = get_object_or_404(Notification, pk=request.POST['alert_notification_id'])

    notification.has_acknowledged = True
    notification.save()

    return JsonResponse({'success':'true'})


def update_status(request):
    # TODO(pduffy): Needs error checking
    alert_id = request.POST['alert_id']
    alert = get_object_or_404(Alert, pk=alert_id)

    alert.status_id = request.POST['status_id']
    alert.save()

    # Create a new note with regarding the status change
    note = Note()
    note.alert_id = alert_id
    note.details = request.POST['reason']
    note.user = request.user
    note.save()

    recalculate_asset_status(alert.asset_id, request.POST['reason'], request.user, alert.is_manual, alert_id)

    return JsonResponse({'success':'true', 'alert_status': alert.status.name, 'asset_status': alert.asset.sidebar_css_class})


def detail(request, alert_id, is_resolve=False):
    alert = get_object_or_404(Alert, pk=alert_id)
    rules = Rule.objects.filter(trigger=alert.trigger)
    notifications = Notification.objects.filter(alert=alert).select_related('user')
    notes = Note.objects.filter(alert=alert).select_related('user')

    laptop_mode = settings.LAPTOP_MODE

    # TODO(mchagnon): Times on template need to account for local time

    # TODO(mchagnon): since the full list is no longer needed, this could be changed to min()/max() calls in SQL
    occurrences = Occurrence.objects.filter(alert=alert).order_by('-created')

    is_acknowledged = False
    for notes in notifications:
        if notes.user == request.user:
            is_acknowledged = notes.has_acknowledged

    return render(request, 'alert/detail.html', {
        'alert': alert,
        'asset': alert.asset,
        'resolved_status': Statuses.RESOLVED.value,
        'trigger': alert.trigger,
        'rules': rules,
        'notifications': notifications,
        'notes': notes,
        'occurrences': occurrences,
        'first_occurrence': occurrences.reverse()[0] if occurrences else None,
        'last_occurrence': occurrences.first(),
        'is_resolve': is_resolve,
        'redmine_url': settings.REDMINE_URL,
        'current_user': request.user,
        'is_acknowledged': is_acknowledged,
        'laptop_mode': laptop_mode
    })


def save_note(request, alert_id):
    # TODO(mchagnon): error handling on data

    note = Note()
    note.alert_id = alert_id
    note.details = request.POST['details']
    note.user = request.user
    note.save()

    return JsonResponse({'success':True})


def get_notes(request, alert_id):
    notes = Note.objects.filter(alert_id=alert_id).select_related('user')\
        .values('id', 'created', 'details', 'user__first_name', 'user__last_name')

    return DataTablesResponse(list(notes))


def get_occurrences(request, alert_id):
    occurrences = Occurrence.objects.filter(alert_id=alert_id).order_by('-created')\
        .values('id', 'created', 'triggered_value')

    return DataTablesResponse(list(occurrences))


def create_manual(request, asset_id, deployment_code):
    asset = get_object_or_404(Asset, pk=asset_id)

    if request.POST:
        form = ManualAlertForm(request.POST, asset=asset, deployment=asset.deployment)
        if form.is_valid():
            alert = form.save(commit=True)
            email_immediately = request.POST.get('email_immediate')
            email_immediately_list = email_immediately.split(',')

            text_immediately = request.POST.get('text_immediate')
            text_immediately_list = text_immediately.split(',')

            email_daily = request.POST.get('email_daily')
            email_daily_list = email_daily.split(',')

            if email_immediately:
                for user in email_immediately_list:
                    send_user_alert_notification(User.objects.get(pk=user), alert, DeliveryMethods.EMAIL.value)
            if text_immediately:
                for user in text_immediately_list:
                    send_user_alert_notification(User.objects.get(pk=user), alert, DeliveryMethods.TEXT.value)
            if email_daily:
                for user in email_daily_list:
                    send_user_alert_notification(User.objects.get(pk=user), alert, DeliveryMethods.SUMMARY.value)

            alert.add_alert_notifications(alert.id)

            return HttpResponseRedirect(reverse('alert.detail', args=[alert.id]))

    else:
        form = ManualAlertForm(asset=asset, deployment=asset.deployment)

    return render(request, 'alert/create_manual.html', {
        'asset': asset,
        'deployment': asset.deployment,
        'form': form,
        'current_asset_page': AssetPages.CREATE_MANUAL_ALERT.value
    })


def manage_subscriptions(request, asset_id):
    asset = get_object_or_404(Asset, pk=asset_id)
    save_confirmed = False
    triggers = Trigger.objects.filter(asset__platform__id=asset.platform.id).order_by('name')
    trigger_ids = list(triggers.values_list('id', flat=True))
    subscriptions = TriggerUser.objects.filter(user=request.user)

    email_ids = list(subscriptions.filter(delivery_method_id=DeliveryMethods.EMAIL.value).values_list('trigger_id', flat=True))
    text_ids = list(subscriptions.filter(delivery_method_id=DeliveryMethods.TEXT.value).values_list('trigger_id', flat=True))
    summary_ids = list(subscriptions.filter(delivery_method_id=DeliveryMethods.SUMMARY.value).values_list('trigger_id', flat=True))

    if request.method == 'POST':
        _update_subscriptions(request.user, DeliveryMethods.EMAIL.value, request.POST.getlist('email'))
        _update_subscriptions(request.user, DeliveryMethods.TEXT.value, request.POST.getlist('text'))
        _update_subscriptions(request.user, DeliveryMethods.SUMMARY.value, request.POST.getlist('summary'))
        save_confirmed = True

    return render(request, 'alert/manage_subscriptions.html', {
        'all_emails_selected': len(trigger_ids) == len(email_ids),
        'email_ids': email_ids,
        'all_texts_selected': len(trigger_ids) == len(email_ids),
        'text_ids': text_ids,
        'all_summaries_selected': len(trigger_ids) == len(email_ids),
        'summary_ids': summary_ids,
        'asset': asset,
        'deployment': asset.deployment,
        'triggers': triggers,
        'save_confirmed': save_confirmed,
    })


def _update_subscriptions(user, delivery_method_id, trigger_ids):
    for trigger_id in trigger_ids:
        TriggerUser.objects.get_or_create(
            trigger_id=trigger_id,
            user=user,
            delivery_method_id=delivery_method_id
        )

    TriggerUser.objects\
        .filter(user=user)\
        .filter(delivery_method_id=delivery_method_id)\
        .exclude(trigger_id__in=trigger_ids)\
        .delete()


def create_redmine_issue(request, alert_id):
    if not check_perm(request, 'manage_redmine'):
        return redirect(settings.DASHBOARD_URL)

    alert = ''
    if alert_id != '0':
        alert = get_object_or_404(Alert, pk=alert_id)

    if request.POST:
        form = RedmineIssueForm(request.POST)
        if form.is_valid():
            issue_id = form.save(request.user, alert)
            if issue_id:
                if alert:
                    return HttpResponseRedirect(reverse('alert.confirm_redmine_issue', args=[alert.id]))
                else:
                    return HttpResponseRedirect(reverse('redmine.confirm_redmine', args=[issue_id]))

    else:
        form = RedmineIssueForm()

    return render(request, 'alert/create_redmine_issue.html', {
        'alert': alert,
        'form': form,
    })


def confirm_redmine_issue(request, alert_id):
    alert = get_object_or_404(Alert, pk=alert_id)

    return render(request, 'alert/confirm_redmine_issue.html', {
        'alert': alert,
        'redmine_url': settings.REDMINE_URL
    })

def confirm_redmine(request, issue_id):
    return render(request, 'alert/confirm_redmine_issue.html', {
        'issue_id': issue_id,
        'redmine_url': settings.REDMINE_URL,
    })


def redmine_trackers(request, project_id):
    trackers = Tracker.objects.filter(project_id=project_id)

    return JsonResponse({t.id: t.name for t in trackers})


def redmine_tracker_details(request, tracker_id):
    fields = TrackerField.objects.filter(tracker__id=tracker_id).values_list('name', flat=True)

    return JsonResponse(list(fields), safe=False)


def refresh_notifications(request):
    notifications = Notification.objects.\
        filter(alert_id=request.POST['alert_id'])\
        .select_related('user')\
        .values("created", "user__first_name", "user__last_name", "has_acknowledged")

    return DataTablesResponse(list(notifications))
