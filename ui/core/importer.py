import sys, os, datetime, re, json
import pandas as pd
import numpy as np
from time import sleep

from collections import defaultdict
from urllib.parse import urljoin
import requests
import dateutil.parser
from io import BytesIO
from django.conf import settings
from django.utils import timezone
import pytz
from django.core.management.base import BaseCommand

from core.models.asset import Asset, Group, Deployment, AssetTypes, AssetStatuses, AssetDispositions, AssetLink,\
    GroupValues, GroupPrefixes, AssetClassification
from core.models.variable import Variable
from core.models.plot import Plot
from core.models.plot import PlatformDefaultPlot
from core.models.alert_trigger import PlatformDefaultTrigger
from core.models.alert_trigger import Trigger
from core.erddap import Erddap

from core.views.plots import clone_plot
from core.views.alert import clone_trigger

from core.util import apply_classifications

class AssetImporter():
    SECTIONS = {
        'Platform Deployment Info Section': 'deployment',
        'Platform Configuration Section': 'platform',
        'Power Configuration Section': 'power',
        'Telem Configuration Section': 'telemetry',
        'DCL Port Config  Section': 'dcl',
        'STC Port Config': 'stc',
    }


    def _find_child_asset(self, asset_id, target_type_id, target_name):
        try:
            child_asset = Asset.objects.filter(
                linked_asset__asset_id=asset_id, linked_asset__is_child=True,
                linked_asset__linked_asset__type_id=target_type_id, linked_asset__linked_asset__name=target_name).first()
        except:
            child_asset = None

        return child_asset

    
    def import_asset(self, filename, stdout=None, form_platform_code=None, form_deployment_code=None, override_group_id=None):
        self.stdout = stdout
        self.platform = None
        self.deployment = None
        self.group = None
        self.default_parent = None

        self.platform_cfg = [] #pcs
        self.deployment_cfg = [] #pdis
        self.power_cfg = [] #pocs
        self.telemetry_cfg = [] #tcs
        self.dcl_cfg = [] #dpcs
        self.stc_cfg = [] #stcpc

        # Array to hold the names of the assets processed so we can remove existing ones that weren't found
        self.found_assets = []

        # Whether or not the structure of the platform has changed
        self.has_structure_changed = False

        # Names of assets that should not be imported because they do not require monitoring
        self.blacklisted_assets = []

        print("Loading " + filename)

        data = None
        try:
            with open(filename, errors='ignore') as file:
                data = file.readlines()
        except FileNotFoundError:
            return (False, "The platform configuration file does not exist")
        except:
            return (False, "Error opening file")

        if not data:
            return (False, "Error opening file")

        if not self._parse_file(data):
            return (False, "Error parsing file")

        if override_group_id:
            self.group = Group.objects.get(pk=override_group_id)
        elif not self._get_group(self.deployment_cfg['Deploy.location']):
            return (False, "Could not load asset group: " + self.deployment_cfg['Deploy.location'])

        # Load asset blacklist (and compile the regexs)
        try:
            with open(os.path.join(settings.CONFIG_DIR, "blacklisted_assets.json")) as blacklist_file:
                blacklist = json.load(blacklist_file)

                self.blacklisted_assets = [re.compile(item) for item in blacklist]
        except Exception as e:
            print("[!] Failed to load asset blacklist file")
            self.blacklisted_assets = []

        # Attempt to find an existing platform/deployment
        platform_code = self.platform_cfg['Platform.id'].upper()
        deployment_code = self.deployment_cfg['Deploy.id'].upper()
        
        if form_platform_code is not None and platform_code!=form_platform_code:
            message = "Platform Code entered ({}) does not match Platform Code in platform config ({}).".format(form_platform_code, platform_code)
            return (False, message)

        if form_deployment_code is not None and deployment_code!=form_deployment_code:
            message = "Deployment Code entered ({}) does not match Deployment Code in platform config ({}).".format(form_deployment_code, deployment_code)
            return (False, message)

        # Always add the platform to the list of found assets
        self.found_assets.append(platform_code)

        try:
            self.platform = Asset.objects.select_related('deployment').get(code=platform_code, deployment__code=deployment_code)
            self.deployment = self.platform.deployment
            print("[+] Using existing asset/deployment")
        except:
            self.deployment = Deployment()
            self.deployment.code = deployment_code
            self.deployment.name = deployment_code
            self.deployment.deployment_date = self._parse_date(self.deployment_cfg['Deploy.sdate'])
            self.deployment.end_date = self._parse_date(self.deployment_cfg['Deploy.edate'])
            self.deployment.save()

            self.platform = self._get_or_create_asset(platform_code, AssetTypes.PLATFORM.value, None)

            # Set the parent to itself (needed for some queries)
            self.platform.platform_id = self.platform.id

            # Newly imported moorings should start in Inactive status
            self.platform.disposition_id = AssetDispositions.INACTIVE.value

            self.platform.save()

            print("[+] Creating new asset/deployment")

        buoy = None
        wfp = None
        imm = None
        nsif = None
        mfn = None

        # Buoy
        if self._has_value(self.platform_cfg, 'Platform.buoy.inst_cfg'):
            buoy = self._get_or_create_asset("BUOY", AssetTypes.BUOY.value, self.platform)

            # Xeos
            for inst in [inst for inst in self.telemetry_cfg if 'xeos' in inst]:
                name = inst[6:11].upper()
                self._get_or_create_asset(name, AssetTypes.INSTRUMENT.value, buoy)

        # NSIF
        if self._has_value(self.platform_cfg, 'Platform.nsif.inst_cfg'):
            nsif = self._get_or_create_asset("NSIF", AssetTypes.NSIF.value, self.platform)

        # MFN
        if self._has_value(self.platform_cfg, 'Platform.mfn.inst_cfg'):
            mfn = self._get_or_create_asset("MFN", AssetTypes.MFN.value, self.platform)

        # WFP
        if self._has_value(self.platform_cfg, 'Platform.wfp.inst_cfg'):
            wfp = self._get_or_create_asset("WFP", AssetTypes.MFN.value, self.platform)

        # IMM
        if self._has_value(self.platform_cfg, 'Platform.imm.inst_cfg'):
            imm = self._get_or_create_asset("IMM", AssetTypes.INSTRUMENT.value, self.platform)

        # BUOY Instruments
        if self._has_value(self.platform_cfg, 'Platform.buoy.inst_cfg'):
            asset = buoy
            cpm = None

            instruments = self.platform_cfg['Platform.buoy.hotel_cfg'].upper().split(',')
            for inst in filter(None, instruments):
                if 'CPM' in inst:
                    cpm = self._get_or_create_asset(inst, AssetTypes.CPM.value, asset)
                    if not self.default_parent:
                        self.default_parent = cpm
                elif 'DCL' in inst:
                    dcl = self._get_or_create_asset(inst, AssetTypes.DCL.value, cpm)

                    for dcl_inst in self.dcl_cfg:
                        dcl_inst = dcl_inst.upper()

                        if inst in dcl_inst and 'INST' in  dcl_inst and '.INST.' not in dcl_inst:
                            port_name = dcl_inst[6:-5]
                            port_num = dcl_inst[10:-5]
                            inst_name = self.dcl_cfg[dcl_inst.lower()].upper()

                            port = self._get_or_create_asset(port_name, AssetTypes.PORT.value, dcl, port_num, is_port=True)
                            if inst_name == 'IMM':
                                imm.parent = port
                                imm.port = port_num
                                imm.save()
                            else:
                                instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, port)

                elif 'STC' in inst:
                    stc = self._get_or_create_asset(inst, AssetTypes.STC.value, asset)
                    if not self.default_parent:
                        self.default_parent = stc

                    for stc_inst in self.stc_cfg:
                        stc_inst = stc_inst.upper()

                        if inst in stc_inst and 'INST' in  stc_inst and '.INST.' not in stc_inst:
                            port_name = stc_inst[5:-5]
                            port_num = stc_inst[9:-5]
                            inst_name = self.stc_cfg[stc_inst.lower()].upper()

                            port = self._get_or_create_asset(port_name, AssetTypes.PORT.value, stc, port_num, is_port=True)
                            if inst_name == 'IMM':
                                imm.parent = port
                                imm.port = port_num
                                imm.save()
                            else:
                                instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, port)

                elif 'ISU' in inst:
                    isu = self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, cpm)

                    for telem_inst in self.telemetry_cfg:
                        if 'ISU' in telem_inst.upper():
                            inst_name = telem_inst[6:10].upper()

                            instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, isu)

                elif 'SBD' in inst:
                    sbd = self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, cpm)

                    for telem_inst in self.telemetry_cfg:
                        if 'SBD' in telem_inst.upper():
                            parts = telem_inst.split('.')
                            if len(parts) > 1:
                                inst_name = parts[1].upper()
                            else:
                                inst_name = telem_inst[6:10].upper()

                            instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, sbd)

                elif 'FB' in inst:
                    fb = self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, cpm)

                    for telem_inst in self.telemetry_cfg:
                        if 'FB' in telem_inst.upper():
                            inst_name = telem_inst[6:9].upper()

                            instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, fb)

                else:
                    instrument = self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, cpm)


        # NSIF instruments
        if self._has_value(self.platform_cfg, 'Platform.nsif.inst_cfg'):
            asset = nsif
            cpm = None

            instruments = self.platform_cfg['Platform.nsif.hotel_cfg'].upper().split(',')
            for inst in filter(None, instruments):
                if 'CPM' in inst:
                    cpm = self._get_or_create_asset(inst, AssetTypes.CPM.value, asset)
                elif 'DCL' in inst:
                    dcl = self._get_or_create_asset(inst, AssetTypes.DCL.value, cpm or nsif)

                    for dcl_inst in self.dcl_cfg:
                        dcl_inst = dcl_inst.upper()

                        if inst in dcl_inst and 'INST' in  dcl_inst and '.INST.' not in dcl_inst:
                            port_name = dcl_inst[6:-5]
                            port_num = dcl_inst[10:-5]
                            inst_name = self.dcl_cfg[dcl_inst.lower()].upper()

                            port = self._get_or_create_asset(port_name, AssetTypes.PORT.value, dcl, port_num, is_port=True)
                            instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, port)
                else:
                    instrument = self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, cpm)

        # MFN instruments
        if self._has_value(self.platform_cfg, 'Platform.mfn.inst_cfg'):
            asset = mfn
            cpm = None

            instruments = self.platform_cfg['Platform.mfn.hotel_cfg'].upper().split(',')
            for inst in filter(None, instruments):
                if 'CPM' in inst:
                    cpm = self._get_or_create_asset(inst, AssetTypes.CPM.value, asset)
                elif 'DCL' in inst:
                    dcl = self._get_or_create_asset(inst, AssetTypes.DCL.value, cpm)

                    for dcl_inst in self.dcl_cfg:
                        dcl_inst = dcl_inst.upper()

                        if inst in dcl_inst and 'INST' in  dcl_inst and '.INST.' not in dcl_inst:
                            port_name = dcl_inst[6:-5]
                            port_num = dcl_inst[10:-5]
                            inst_name = self.dcl_cfg[dcl_inst.lower()].upper()

                            port = self._get_or_create_asset(port_name, AssetTypes.PORT.value, dcl, port_num, is_port=True)
                            instrument = self._get_or_create_asset(inst_name, AssetTypes.INSTRUMENT.value, port)
                else:
                    instrument = self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, cpm)

        # WFP instruments
        if self._has_value(self.platform_cfg, 'Platform.wfp.inst_cfg'):
            asset = wfp
            cpm = None

            instruments = self.platform_cfg['Platform.wfp.inst_cfg'].upper().split(',')
            for inst in filter(None, instruments):
                self._get_or_create_asset(inst, AssetTypes.INSTRUMENT.value, wfp)

        # IMM instruments
        if self._has_value(self.platform_cfg, 'Platform.imm.inst_cfg'):
            asset = imm
            cpm = None

            instruments = self.platform_cfg['Platform.imm.inst_cfg'].upper().split(',')
            for inst in filter(None, instruments):
                name = inst.split(':')[1]

                self._get_or_create_asset(name, AssetTypes.INSTRUMENT.value, imm)


        # Add variables
        self._add_variables()

        # TODO(mchagnon): Add asset history record

        # Remove (flag) old assets that are no longer in the config file
        removed_assets = Asset.objects.filter(platform=self.platform).exclude(name__in=self.found_assets)
        for removed_asset in removed_assets:
            print("Removing unused asset: " + str(removed_asset))
            removed_asset.is_deleted = True
            removed_asset.save()

        # Rebuild asset links
        if self.has_structure_changed:
            print("Structure has changed - rebuilding asset links")
            assets = Asset.objects.filter(platform=self.platform)
            for asset in assets:
                Asset().rebuild_asset_links(asset.id)

        # Update asset statuses
        if self.has_structure_changed:
            # TODO(mchagnon): Implement
            pass
        total_successful_assets_plot = []
        total_failed_assets_plot = []
        plots_attempted = 0


        # apply asset classifications
        assets = Asset.objects.filter(platform=self.platform)
        apply_classifications(assets)

        plots_on_platforms = PlatformDefaultPlot.objects.filter(platform_name=self.platform.name)
        for pp in plots_on_platforms:
            plot = pp.plot
            target_asset = self.platform
            if target_asset.type is plot.asset.type and target_asset.name is plot.asset.name:
                asset_to_clone_to = target_asset
            else:
                asset_to_clone_to = self._find_child_asset(target_asset.id, plot.asset.type.id, plot.asset.name)

            if not Plot.objects.filter(asset=asset_to_clone_to, name=plot.name).exists():
                success, successful_assets, failed_assets = \
                    clone_plot(pp.plot_id, [asset_to_clone_to], 1, True)

                total_successful_assets_plot += successful_assets
                total_failed_assets_plot += failed_assets

        # if plots_attempted is not 0:
        #     success = len(total_successful_assets_plot) is not 0
        #     if len(plots_on_platforms) is not 0:
        #         print("success: {}, attempted plot names: {}, success_assets_default_plots: {}, failed_assets_default_plots: {}".format(success, list(plots_on_platforms.values_list('plot__name', flat=True)), total_successful_assets_plot, total_failed_assets_plot))
        #         print("success: total_plots: {}, total_successful_assets_plot: {}, total_failed_assets_plot: {}".format(len(plots_on_platforms), len(total_successful_assets_plot), len(total_failed_assets_plot)))

        total_successful_assets_trigger = []
        total_failed_assets_trigger = []
        triggers_attempted = 0
        triggers_on_platforms = PlatformDefaultTrigger.objects.filter(platform_name=self.platform.name)
        for tp in triggers_on_platforms:
            trigger = tp.trigger
            target_asset = self.platform
            if target_asset.type is trigger.asset.type and target_asset.name is trigger.asset.name:
                asset_to_clone_to = target_asset
            else:
                asset_to_clone_to = self._find_child_asset(target_asset.id, trigger.asset.type.id, trigger.asset.name)
            if not Trigger.objects.filter(asset=asset_to_clone_to, name=trigger.name).exists():
                success, successful_assets, failed_assets = \
                    clone_trigger(tp.trigger_id, [asset_to_clone_to], 1, True)
                total_successful_assets_trigger += successful_assets
                total_failed_assets_trigger += failed_assets
                triggers_attempted += 1

        if triggers_attempted != 0:
            success = len(total_successful_assets_trigger) != 0
            print("success: {}, attempted trigger names: {}, success_assets_default_triggers: {}, failed_assets_default_triggers: {}".format(success, list(triggers_on_platforms.values_list('trigger__name', flat=True)), total_successful_assets_trigger, total_failed_assets_trigger))
            print("success: total_triggers: {}, total_successful_assets_trigger: {}, total_failed_assets_trigger: {}".format(len(triggers_on_platforms), len(total_successful_assets_trigger), len(total_failed_assets_trigger)))

        print("Platform: {}, Deployment: {}, Group: {}".format(self.platform.name, self.deployment.name, self.group.name))
        return (True, "")

    def _parse_file(self, data):
        section = ""
        all_sections = {}
        values = {}

        for line in data:
            # Check for comments or data we don't have to process
            if not line.strip() or line in ['\n', '\r\n']:
                continue

            # See if we're changing sections
            # TODO: Could be cleaned up
            last_section = section
            if '#-' in line and 'Section' in line:
                for name,idx in self.SECTIONS.items():
                    if name in line:
                        section = idx
                        break
                else:
                    section = ''

            if section == '':
                continue

            if last_section != section:
                values = {}

                continue

            # Ignore comments; must be done after section check because that's based on comments
            if (line[:1] == "#") or (line in ['\n', '\r\n']) or ("######" in line):
                continue

            if not section in all_sections:
                values = {}

            line = line.replace("=", "|").replace('\n', '').split("#", 1)[0]
            keyval = line.split('|')
            key = keyval[0].strip()
            val = keyval[1].strip() if len(keyval) > 1 else ''

            values.update({key: val})
            all_sections.update({section: values})

        # Fill in member variables with section data
        self.deployment_cfg = all_sections['deployment'] if 'deployment' in all_sections else {}
        self.platform_cfg = all_sections['platform'] if 'platform' in all_sections else {}
        self.power_cfg = all_sections['power'] if 'power' in all_sections else {}
        self.telemetry_cfg = all_sections['telemetry'] if 'telemetry' in all_sections else {}
        self.dcl_cfg = all_sections['dcl'] if 'dcl' in all_sections else {}
        self.stc_cfg = all_sections['stc'] if 'stc' in all_sections else {}

        return True

    def _get_group(self, location):
        # The 'Test' group is intentionally omitted here as because there are no keywords in the platform config files
        #   that would flag the mooring as a test. Instead, the test group is intended to be set when the end user is
        #   importing the mooring within the UI, where they can now select the group to override the logic below.
        try:
            if GroupPrefixes.GLOBAL.value in location:
                self.group = Group.objects.get(name=GroupValues.GLOBAL.value)
            elif GroupPrefixes.MAB.value in location:
                self.group = Group.objects.get(name=GroupValues.MAB.value)
            elif GroupPrefixes.PIONEER.value in location:
                self.group = Group.objects.get(name=GroupValues.PIONEER.value)
            elif GroupPrefixes.ENDURANCE.value in location:
                self.group = Group.objects.get(name=GroupValues.ENDURANCE.value)
            elif GroupPrefixes.AUV.value in location:
                self.group = Group.objects.get(name=GroupValues.AUV.value)
            elif GroupPrefixes.GLIDERS.value in location:
                self.group = Group.objects.get(name=GroupValues.GLIDERS.value)
            else:
                return False
        except Exception as e:
            return False

        return True

    def _parse_date(self, value):
        if "dd" in value or "yy" in value or "mm" in value:
            return None

        try:
            dte = datetime.datetime.strptime(value, '%Y/%m/%d')
            return timezone.make_aware(dte, timezone.utc)
        except:
            return None

    def _has_value(self, array, name):
        if not name in array:
            return False

        value = array[name].strip()
        return (value not in ['', 'N/A'])

    def _get_or_create_asset(self, name, type_id, parent, port_num=None, is_port=False):
        self.found_assets.append(name)

        # Do not import certain assets that do not require any monitoring. This check is intentionally after
        #   adding the asset ot the list of found assets to make sure we do not remove it if it had already been
        #   imported (which could cause issues with existing plots/triggers/L3s
        if any(item.match(name) for item in self.blacklisted_assets):
            print("[-] Ignoring blacklisted asset/instrument: " + name)
            return None

        if not parent:
            parent = self.default_parent

        try:
            assets = Asset.objects.filter(name=name, platform=self.platform)

            # If this is a port, we're going to have duplicates across the asset. Make sure its under the correct one
            if is_port:
                assets = assets.filter(parent=parent)

            if len(assets) > 0:
                asset = assets.first()

                # If the asset exists but has a different parent (dcl/port) or port #, move it
                if asset.type_id == AssetTypes.INSTRUMENT.value and parent.type_id == AssetTypes.PORT.value:
                    if asset.parent != parent or asset.port != port_num:
                        asset.parent = parent
                        asset.port = port_num
                        asset.save()

                        self.has_structure_changed = True

                return asset
        except:
            # An exception gets treated as if there were no matching assets
            pass

        asset = Asset()
        asset.code = name
        asset.name = name
        asset.status_is_manually_set = False
        asset.deployment = self.deployment
        asset.group = self.group
        asset.type_id = type_id
        asset.status_id = AssetStatuses.OK.value
        asset.platform = self.platform
        asset.parent = parent
        asset.port = port_num
        asset.disposition_id = AssetDispositions.DEPLOYED.value
        asset.is_deleted = False
        asset.save()

        self.has_structure_changed = True

        return asset

    def _add_variables(self):
        """Get ERDDAP user info from database"""
        user = settings.ERDDAP_USERNAME
        password = settings.ERDDAP_PASSWORD

        # authenticate creds for ERDDAP
        auth = requests.auth.HTTPBasicAuth(user, password)

        # get ERDDAP url from Database
        base_url = settings.ERDDAP_URL

        # ERDDAP calls sometimes fail for no reason with "Name or service not know". Since there
        #   isn't much that can be done, pause and try again. if it fails a second time, move on
        try:
            req = requests.get(urljoin(base_url + '/', 'info/index.csv'), auth=auth)
        except:
            sleep(30)
            try:
                req = requests.get(urljoin(base_url + '/', 'info/index.csv'), auth=auth)
            except:
                self._print_error("Failed to process ERDDAP request (a second time")
                return

        # choose fields to load csv
        fields = ['Title', 'tabledap']
        tables_all = pd.read_csv(BytesIO(req.content), header=0, usecols=fields)
        tables = tables_all[tables_all.notnull()]

        print("Processing variables for {} ({})".format(self.platform.code, self.platform.id))

        for index, row in tables.iterrows():
            # Some of these rows are gridded datasets, which we are not using for alerts and alarms
            if isinstance(row['tabledap'], float):
                continue

            if row['tabledap'].endswith('allDatasets'):
                continue

            # Make sure this dataset applies to the platform that's being loaded
            ref_des = row['tabledap'].split('/')[-1]
            if not ref_des.startswith(self.platform.name):
                continue

            url_target = urljoin(base_url + '/', 'info/{}/index.csv'.format(ref_des))

            # Location path example: CP02PMUO IMM CTDPFK
            # First word is mooring/platform code
            # Second word is the parent asset needed to locate some instruments
            # Third word is the instrument name
            location_path = row['Title'].split(' ')
            parent_name = location_path[-2].upper()
            inst_name = location_path[-1].upper()

            # If the instrument title ends in "raw", then we need to drop that before parsing
            if inst_name.lower() == 'raw':
                parent_name = location_path[-3].upper()
                inst_name = location_path[-2].upper()

            print("Importing variable {} ({})...".format(ref_des, inst_name), end='')

            # The power system on the buoy is tied to the root platform asset
            power_systems = [
                self.platform.name + '-BUOY-001-PWRSYS',
                self.platform.name + '-BUOY-PWRSYS-01-1'
            ]
            if ref_des in power_systems:
                self._import_variables(self.platform, ref_des, url_target, auth)
                self._print_success("OK")
                continue

            # The METBK instrument is sometimes named as the DCL, with the instrument name in the reference designator
            if inst_name.startswith("DCL") and ref_des.endswith("METBK"):
                parent = Asset.objects.filter(name=inst_name, platform=self.platform).first()
                assets = AssetLink.objects.filter(asset=parent, linked_asset__code__istartswith="METBK")

                if assets.count() == 1:
                    self._import_variables(assets.first().linked_asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue
                else:
                    self._print_error("FAILED")
                    print("[!] Multiple instruments match and there was no unique asset based on the parent")
                    continue

            # Check for an exact match on the instrument name
            asset = Asset.objects.filter(name=inst_name, platform=self.platform).first()
            if asset:
                self._import_variables(asset, ref_des, url_target, auth)
                self._print_success("OK")
                continue

            # Sometimes the dataset will end in "K", so try it without that character
            if inst_name.endswith("K"):
                asset = Asset.objects.filter(name=inst_name[0:-1], platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue

            # WFP could be called a few different options
            if inst_name.startswith("WFP"):
                asset = Asset.objects.filter(name__in=["WFP", "WFPENG"], platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue

            # The Iridium maps to a separate asset name entirely
            if inst_name == "IRID" or inst_name == "IRIDIUM":
                asset = Asset.objects.filter(name="IRID_ISU", platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue

            # Sometimes HYD in OMS++ is expanded to HYDGN in the dataset name
            if inst_name.startswith("HYD"):
                asset = Asset.objects.filter(name=inst_name.replace("HYDGN", "HYD"), platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue
                else:
                    # If no exact match is found, update the instrument name so that we can try to find by it's
                    # parent in the next filter
                    inst_name = inst_name.replace("HYDGN", "HYD")

            # SUNA is being renamed in new databases to NUTNR
            if inst_name == 'SUNA':
                asset = Asset.objects.filter(name="NUTNR", platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue
                else:
                    # If no exact match is found, update the instrument name so that we can try to find by the
                    # parent in the next filter
                    inst_name = "NUTNR"

            # FB250s appear in ERDDAP as -FB-
            if '-FB-' in ref_des:
                asset = Asset.objects.filter(name="FB250", platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue

            # On globals, CTDBP instruments that are number have a leading zero. E.g., CTDBP01 vs CTDBP1
            alt_ctdbp_name = inst_name.replace('CTDBP', 'CTDBP0')
            asset = Asset.objects.filter(name=alt_ctdbp_name, platform=self.platform).first()
            if asset:
                self._import_variables(asset, ref_des, url_target, auth)
                self._print_success("OK")
                continue

            # ACDPs sometimes have a numeric suffix, and/or are called ADCPT
            if inst_name.startswith('ADCP'):
                asset = Asset.objects.filter(name__in=["ADCP", "ADCPT"], platform=self.platform).first()
                if asset:
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue
                else:
                    # If no exact match is found, update the instrument name so that we can try to find by the
                    # parent in the next filter
                    inst_name = "ADCPT" if inst_name.startswith('ADCPT') else 'ADCP'

            # Handle datasets that have a trailing one (1) in the name, even though the name from the platform.cfg
            #   file does not contain a trailing number. This check only works if there is a single asset with a
            #   matching name - otherwise there would be unclear which asset matched.
            if inst_name.endswith('1'):
                matches = Asset.objects.filter(name=inst_name[:-1])
                if len(matches) == 1:
                    asset = matches.first()
                    self._import_variables(asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue

            # OMS++ names assets with a trailing number to distinguish them, but the platform config files don't.
            #  In this case the parent needs to be used to determine which OMS++ asset matches the dataset
            matches = Asset.objects.filter(name__startswith=inst_name, platform=self.platform)
            if matches.count() > 1:
                parent = Asset.objects.filter(name=parent_name, platform=self.platform).first()
                if not parent:
                    self._print_error("FAILED")
                    print("[!] Multiple instruments match but parent asset ({}) could not be determined".format(parent_name))
                    print("[!] Matching instruments: ", ".".join([asset.name for asset in matches]))
                    continue

                assets = AssetLink.objects.filter(asset=parent, linked_asset__in=matches)
                if assets.count() == 1:
                    self._import_variables(assets.first().linked_asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue
                elif inst_name == 'RDA':
                    # Eventually RDA assets will not have data associated with them, but for the time being, we
                    #   need to tie all matching RDA instruments to the same data stream
                    for rda_asset in assets:
                        self._import_variables(rda_asset.linked_asset, ref_des, url_target, auth)
                    self._print_success("OK")
                    continue
                else:
                    self._print_error("FAILED")
                    print("[!] Multiple instruments match and there was no unique asset based on the parent")
                    continue

            self._print_error("FAILED")
            print("[!] Could not locate asset")

    def _import_variables(self, asset, ref_des, url_target, auth):
        asset.erddap_id = ref_des
        asset.save()

        # get units
        try:
            req = requests.get(url_target, auth=auth)
        except:
            # For some reason, calls to ERDDAP will fail randomly with the following error:
            #   Name or service not known
            # Since there's nothing that can be done abut this, wait a few seconds and try again
            self._print_error("Call to ERDDAP failed, retrying after 30 seconds...")
            sleep(30)
            try:
                req = requests.get(url_target, auth=auth)
            except:
                self._print_error("Call to ERDDAP failed a second time; moving one")
                return

        tabledap_info = pd.read_csv(BytesIO(req.content), header=0)
        valid_vars = self._get_valid_variables(tabledap_info)

        for var_name, var_attrs in valid_vars.iterrows():
            try:
                var = Variable.objects.filter(asset=asset, erddap_id=var_name).first()
            except Exception as e:
                print("ERROR: " + str(e))

            if not var:
                var = Variable()

            # ERDDAP may not always contain a long name. If not, use the ERDDAP ID, since that's the only
            #   name that's going to be similar
            var.name = str(var_attrs.get('long_name'))
            if not var.name or var.name.lower() == "nan" or var.name.lower() == "none":
                var.name = var_name

            var.asset = asset
            var.erddap_id = var_name
            var.units = var_attrs.get('units')
            var.is_custom = False
            var.asset = asset
            var.is_plottable = True
            var.platform = self.platform
            var.deployment = self.deployment
            var.save()

    def _filter_bad_vars(self, varname):
        """Boolean check to determine if any invalid variable names are present"""
        var_blacklist = ['^NC_GLOBAL$', '^deploy_id$', '^http.*$', '.*feature.*']
        return not any(re.search(pat, varname) for pat in var_blacklist)

    def _get_valid_variables(self, info_df):
        """
        Takes an ERDDAP info dataframe and outputs a pivoted dataframe with only
        valid variables and associated attributes
        """
        # exclude global attributes so that only variable related rows remain
        variable_df = info_df[info_df['Variable Name'].apply(self._filter_bad_vars)]

        # pivot on vars for index, attrs for columns
        pivot_vars = variable_df.pivot('Variable Name', 'Attribute Name', 'Value')

        # axis info should be present in most datasets
        if 'axis' not in pivot_vars:
            pivot_vars['axis'] = np.NaN
        # We can't reliably establish variable dimension from TableDAP datasets,
        # so platform is used as an arbitrary proxy to determine which variables
        # have dimensions.

        if 'platform' in pivot_vars:
            return pivot_vars[pivot_vars['platform'].notnull() |
                              pivot_vars['axis'].notnull()]
        # if it's not there, anything else goes until there's a reliable way of
        # determining dimension info.
        else:
            return pivot_vars

    def _print_success(self, msg):
        if self.stdout:
            self.stdout.write(self.style.SUCCESS(msg))
            self.stdout.flush()
        else:
            print(msg)

    def _print_error(self, msg):
        if self.stdout:
            self.stdout.write(self.style.ERROR(msg))
            self.stdout.flush()
        else:
            print(msg)