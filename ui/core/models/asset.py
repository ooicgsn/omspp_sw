import itertools
import re, os
from enum import Enum
import datetime
import pytz
from pathlib import Path

from django.db import models
from django.db.models import Avg, Q
from django.contrib.auth.models import User
from django.conf import settings
from collections import deque

from ..db import exec_dict, exec_void, exec_raw
from ..util import enum_choices, DataTablesResponse, parse_deployment_number, parse_deployment_code_leading_zeros
from ..ui import get_alert_bar_css_class

import pandas as pd

import collections

class AssetGroups(Enum):
    GLOBAL = 1
    PIONEER = 2
    ENDURANCE = 3
    AUV = 4
    GLIDERS = 5


class AssetStatuses(Enum):
    OK = 1
    WARNING = 2
    ERROR = 3


class AssetTypes(Enum):
    PLATFORM = 1
    CPM = 2
    DCL = 3
    INSTRUMENT = 4
    BUOY = 7
    NSIF = 8
    MFN = 9
    PORT = 10
    STC = 11
    MAX = 12  # Pseudo value used for some sorting routines


class AssetDispositions(Enum):
    BURN_IN = 1
    DEPLOYED = 2
    RECOVERED = 3
    INACTIVE = 4


class AssetCategories(Enum):
    SURFACE_MOORING = 1
    SUBSURFACE_MOORING = 2
    PROFILER_MOORING = 3


class AssetSubcategories(Enum):
    FLANKING = 1
    HYBRID_PROFILER = 2
    HIGH_POWER = 3
    STANDARD_POWER = 4
    LOW_POWER = 5


class GroupValues(Enum):
    GLOBAL = 'Global'
    MAB = 'MAB'
    PIONEER = 'Coastal - Pioneer'
    ENDURANCE = 'Coastal - Endurance'
    AUV = 'AUV'
    GLIDERS = 'Gliders'
    TEST = 'Test'
    TESTBED = 'Testbed'


# Text strings used to help parse Deployment.Location from platform.cfg files. Almost identical to GroupValues, but
#   with a few shorter names to account for name variants that may or may not be in the configuration files.
class GroupPrefixes(Enum):
    GLOBAL = 'Global'
    MAB = 'MAB'
    PIONEER = 'Pioneer'
    ENDURANCE = 'Endurance'
    AUV = 'AUV'
    GLIDERS = 'Gliders'
    TEST = 'Test'
    TESTBED = 'Testbed'


class WfpProfileStatuses(Enum):
    SMOOTH_RUNNING = 0
    MISSION_COMPLETE = 1
    OPERATOR_CTRL_C = 2
    TT8_COMM_FAILURE = 3
    CTD_COMM_FAILURE = 4
    ACM_COMM_FAILURE = 5
    TIMER_EXPIRED = 6
    MIN_BATTERY = 7
    AVG_MOTOR_CURRENT = 8
    MAX_MOTOR_CURRENT = 9
    SINGLE_PRESSURE = 10
    AVG_PRESSURE = 11
    AVG_TEMPERATURE = 12
    TOP_PRESSURE = 13
    BOTTOM_PRESSURE = 14
    PRESSURE_RATE_ZERO = 15
    STOP_NULL = 16
    FLASH_CARD_FULL = 17
    FILE_SYSTEM_FULL = 18
    TOO_MANY_OPEN_FILES = 19
    AANDERAA_COMM_FAILURE = 20


class Group(models.Model):
    name = models.CharField(max_length=50)
    display_name = models.CharField(max_length=50, default='')
    sort_order = models.SmallIntegerField()

    # Glider is both a type of mooring and a top level group. This flag is the group that all gliders are attached to
    is_glider = models.BooleanField(default=False)
    has_gliders = models.BooleanField(default=False)

    # Determine whether there are any gliders on this group. For example, Pioneer has gliders and would get this flag,
    #   even though the gliders themselves are attached to the "gliders" asset group
    has_gliders = models.BooleanField(default=False)

    class Meta:
        db_table = 'asset_group'

    def __str__(self):
        return self.name


class AssetStatus(models.Model):
    name = models.CharField(max_length=50)
    min_threshold = models.DecimalField(decimal_places=0, max_digits=3, default=0)
    max_threshold = models.DecimalField(decimal_places=0, max_digits=3, default=0)

    class Meta:
        db_table = 'asset_status'


class AssetType(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'asset_type'


class AssetClassification(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    description = models.TextField(blank=False, null=True)
    abbreviation = models.CharField(max_length=10, blank=False, null=False)
    is_virtual = models.BooleanField(blank=False, null=False, default=False)
    pattern = models.CharField(max_length=100, blank=False, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'asset_classification'


class AssetDisposition(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'asset_disposition'


class AssetCategory(models.Model):
    name = models.CharField(max_length=100)
    sidebar_column = models.IntegerField(default=1, null=False, blank=False)

    class Meta:
        db_table = 'asset_category'


class AssetSubcategory(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(AssetCategory, on_delete=models.CASCADE)

    class Meta:
        db_table = 'asset_subcategory'


class Deployment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=30)
    deployment_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    withdraw_date = models.DateTimeField(blank=True, null=True)
    withdraw_reason = models.TextField(blank=True, null=True)

    # Returns the deployment number as an integer so that it can be used for sorting
    @property
    def sortable_code(self):
        return parse_deployment_code_leading_zeros(self.code)

    class Meta:
        db_table = 'deployment'


class Asset(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    last_update = models.DateTimeField(null=True, blank=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    platform = models.ForeignKey('Asset', null=True, blank=True, related_name='+', on_delete=models.CASCADE)
    parent = models.ForeignKey('Asset', null=True, blank=True, related_name='+', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=30)
    type = models.ForeignKey(AssetType, limit_choices_to=enum_choices(AssetTypes), on_delete=models.CASCADE)
    status = models.ForeignKey(AssetStatus, limit_choices_to=enum_choices(AssetStatuses), on_delete=models.CASCADE)
    status_reason = models.TextField(blank=True, null=True)
    status_reported_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    status_is_manually_set = models.BooleanField(default=False)
    erddap_id = models.CharField(max_length=50, null=True, blank=True)
    deployment = models.ForeignKey(Deployment, null=True, blank=True, on_delete=models.CASCADE)
    deployment_date = models.DateTimeField(blank=True, null=True)
    recovery_date = models.DateTimeField(blank=True, null=True)
    lat = models.FloatField(null=True)
    lon = models.FloatField(null=True)
    depth = models.FloatField(null=True)
    watch_circle_radius = models.FloatField(null=True)
    classification = models.ForeignKey(AssetClassification, null=True, blank=True, on_delete=models.CASCADE)

    # Decimal based percentage from 0 to 1
    status_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)

    # Port number, which is needed for the instrument level of an asset (under a DCL)
    port = models.PositiveSmallIntegerField(null=True, blank=True)

    photo = models.CharField(max_length=255, null=True, blank=True)

    # Technically this is a status on the deployment, but it makes more sense to keep it on the asset since
    #   it will make database calls easier
    disposition = models.ForeignKey(AssetDisposition, null=False, blank=False, default=AssetDispositions.DEPLOYED.value, on_delete=models.CASCADE)

    category = models.ForeignKey(AssetCategory, null=True, blank=True, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(AssetSubcategory, null=True, blank=True, on_delete=models.CASCADE)

    # Flag set when an instrument is removed from mooring during its deployment
    is_deleted = models.BooleanField(default=False, null=False, blank=False)

    # This is a forced constraint on the moorings
    NUM_PORTS_PER_DCL = 8

    # Store a specific ID of the GPs instrument on the asset (mainly for the top level platform)
    gps = models.ForeignKey('Asset', null=True, blank=True, related_name='+', on_delete=models.CASCADE)

    # Even though gliders are all assigned to a top level group of "glider", we still need to determine which array
    #   this asset is part of
    glider_group = models.ForeignKey(Group, null=True, blank=True, related_name='+', on_delete=models.CASCADE)

    parsed_data_glob = models.CharField(max_length=255, null=True, blank=True)
    raw_data_glob = models.CharField(max_length=255, null=True, blank=True)

    reference_designator = models.CharField(max_length=50, null=True, blank=True)
    serial_number = models.CharField(max_length=100, null=True, blank=True)

    deployment_cruise = models.CharField(max_length=100, blank=True, null=True)
    recovery_cruise = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'asset'

    def __str__(self):
        return self.name

    def get_sidebar_assets(self):
        return exec_dict('get_sidebar_assets', None)

    def get_asset_status_grid(self, asset_id):
        return exec_dict('get_asset_status_grid', (asset_id,))

    def get_asset_status_history(self, asset_id):
        return exec_dict('get_asset_status_history', (asset_id,))

    def get_asset_name(self):
        last_id = self.id
        name_list = deque([self.code])
        # platforms are their own platform ids
        cur_asset = self.platform

        while last_id != cur_asset.id or cur_asset is None:
            name_list.appendleft(cur_asset.code)
            last_id = cur_asset.id
            cur_asset = cur_asset.platform

        return '-'.join(name_list)

    def rebuild_asset_links(self, asset_id):
        exec_void('rebuild_asset_links', (asset_id,))

    def get_alerts(self, asset_id, include_all):
        alerts = []

        for alert in exec_dict('get_asset_alerts', (asset_id, include_all)):
            alert["row_css_class"] = get_alert_bar_css_class(alert["severity_id"])
            alerts.append(alert)

        return DataTablesResponse(alerts)

    def get_asset_alerts(self, asset_id, include_all):
        # TODO(mchagnon): This needs to be cleaned up; remove hard coded 6 for the position of severity ID
        data = exec_raw('get_asset_alerts', (asset_id, include_all))
        data = [row + (get_alert_bar_css_class(row[6]),) for row in data]
        return DataTablesResponse(data)

    def get_assets_for_status_grid(self, platform_id):
        return exec_dict('get_assets_for_status_grid', (platform_id,))

    def get_group_platforms(self, group_id):
        return exec_dict('get_group_platforms', (group_id,))

    def get_assets_for_pulling_last_update(self):
        return exec_dict('get_assets_for_pulling_last_update', None)

    def get_clone_trigger_assets(self, trigger_id):
        return exec_dict('get_clone_trigger_assets', (trigger_id,))

    def get_clone_variable_assets(self, variable_id):
        return exec_dict('get_clone_variable_assets', (variable_id,))

    def get_clone_plot_assets(self, plot_id):
        return exec_dict('get_clone_plot_assets', (plot_id,))

    def get_asset_severity_multipliers(self, asset_id):
        return [float(row[0]) for row in exec_raw('get_asset_severity_multipliers', (asset_id,))]

    @property
    def status_grid_css_class(self):
        return Asset.get_status_grid_class(self.status_id, self.type_id)

    @staticmethod
    def get_status_grid_class(status_id, type_id):
        css_class = 'status'

        if status_id == AssetStatuses.OK.value:
            css_class += ' status status-ok'
        elif status_id == AssetStatuses.WARNING.value:
            css_class += ' status-warning'
        elif status_id == AssetStatuses.ERROR.value:
            css_class += ' status-error'

        if type_id in (AssetTypes.CPM.value, AssetTypes.BUOY.value, AssetTypes.NSIF.value, AssetTypes.MFN.value):
            css_class += ' status-grid-cpm'
        elif type_id in (AssetTypes.DCL.value, AssetTypes.STC.value):
            css_class += ' status-grid-dcl'

        return css_class

    @property
    def is_data_recent(self):
        # use 24 hour threshold
        present_utc_time = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
        asset_updated_time = self.last_update or datetime.datetime.min.replace(tzinfo=pytz.UTC)
        time_dif = (present_utc_time - asset_updated_time).total_seconds() < 86400
        return time_dif

    @property
    def sidebar_css_class(self):
        if self.status_id == AssetStatuses.OK.value:
            return 'circle-status-ok'
        elif self.status_id == AssetStatuses.WARNING.value:
            return 'circle-status-warning'
        elif self.status_id == AssetStatuses.ERROR.value:
            return 'circle-status-error'

        return 'status'

    @property
    def photo_url(self):
        platform = self.platform or self
        filename = f'{platform.code}.png'

        path = Path(os.path.join(settings.MEDIA_ROOT, settings.DRAWINGS_FOLDER, filename))
        if not path.is_file():
            return ''

        return os.path.join(settings.MEDIA_URL, settings.DRAWINGS_FOLDER, filename)

    @property
    def erddap_data_url(self):
        return "{}/tabledap/{}.html".format(settings.ERDDAP_URL, self.erddap_id)

    @property
    def erddap_graph_url(self):
        # This method was only used for gliders, which have since been deprecated

        # erddap_url = config_value('ERDDAP', 'SERVER_URL')
        # return "{}/tabledap/{}.graph".format(erddap_url, self.erddap_id)

        return ''

    # TODO(mchagnon): This could be cleaned up to prevent lost of database calls, since this is called within a loop
    @property
    def average_platform_status(self):
        assets = Asset.objects.filter(platform=self.platform)
        average_status = assets.aggregate(Avg('status_percent'))['status_percent__avg']

        if assets.count() == 0:
            return 0

        total = float(0.0)
        for asset in assets:
            total += float(100.0) if asset.status_percent is None else float(asset.status_percent) * 100.0

        return total / assets.count()

    @property
    def raw_data_dir(self):
        return Asset.get_data_url_asset(self)

    @property
    def parsed_data_dir(self):
        code = self.code.lower()
        deployment_code = self.deployment.code.upper()
        platform_code = self.platform.code.lower()

        try:
            # The parent folder is either going to be one of the three top level items, BOUY, NSIF, MFN, OR it will be
            #   the IMM. This logic gets the list of those instruments on this particular platform
            parent_types = [AssetTypes.BUOY.value, AssetTypes.NSIF.value, AssetTypes.MFN.value]
            parents = Asset.objects\
                .filter(platform=self.platform)\
                .filter(Q(type_id__in=parent_types) | Q(code__istartswith='imm'))

            # From the list of possible parents, find only those that are linked to the asset in question. This is to
            #   make sure that something like a FLORT2 located on the NSIF does not get tied to the BUOY, which could
            #   house the FLORT1. This is necessary because some of the naming options will remove the numerical suffix,
            #   so it will not  be so clear where that instrument is located on the mooring.
            parent_codes = [
                asset.linked_asset.code.lower()
                for asset in AssetLink.objects.filter(asset=self, linked_asset__in=parents)
            ]
        except:
            print('Could not locate the asset.')
            return ''

        # The 'imm' folder might sometimes be called 'mmp', so add it to the list of possible parent paths
        if 'imm' in parent_codes:
            parent_codes.append('mmp')

        # Default the list of codes to just the instrument code
        codes = [code]

        # Handle folder mapping variant for 3dmgx3 -> mopak
        if code == '3dmgx3':
            codes.append('mopak')

        # Handle different name and parent folder for ADCP
        if code == 'adcpt' or code == 'adcps':
            codes.append('adcp')

        # Handle different name for IRID
        if code == 'irid_isu':
            codes.append('irid')

        if code == 'wfp':
            codes.append('prkt')
            parent_codes.append('imm')

        # Parsed data for CPMs and DCLs is in a "superv" folder - add that to the list of variants
        if code.startswith('cpm') or code.startswith('dcl'):
            codes.append(os.path.join('superv', code))

        # If the code ends in a number add a variant of the code without the suffix to the list of possible locations
        code_without_suffix = re.sub(r'\d+$', '', code)
        if code != code_without_suffix:
            codes.append(code_without_suffix)

        # Add variants (old style) where instruments always have a dash and then a number
        # E.g., "cpm1" becomes "cpm-1", "wavss" becomes "wavss-1", and "hyd2" becomes "hyd-2"
        (alternate_code, matches) = re.subn(r'(\d+)$', r'-\1', code)
        codes.append(f'{code}-1' if matches == 0 else alternate_code)

        # Handle different name for HYD. This needs to be done after the variants (numerical suffices) are added. This
        #   is done in two passes to make sure there are no infinite loops (because 'hyd' is also a prefix of 'hydgn')
        hydrogen_items = [x for x in codes if x.lower().startswith('hyd')]
        for hydrogen_item in hydrogen_items:
            codes.append(hydrogen_item.replace('hyd', 'hydgn'))

        # Build a list of each possible combination of parent_code and code
        paths = [
            os.path.join(item[0], item[1])
            for item in itertools.product(parent_codes, codes)
        ]

        for path in paths:
            relative_path = Path(os.path.join(platform_code, deployment_code, path))
            absolute_path = Path(os.path.join(settings.PARSED_DATA_DIR, relative_path))
            exists = absolute_path.exists()

            status = "found" if exists else "not found"
            print(f'Trying "{absolute_path}" - {status}')

            if exists:
                return relative_path

        print(f'Could not locate a parsed data folder for the {self.code} instrument')
        return ''

        # Deprecated 2023-07-23 Replaced with newer mapping logic
        # code = self.code.lower()
        # deployment_code = self.deployment.code.upper()
        # platform_code = self.platform.code.lower()
        # use_numbered = settings.USE_NUMBERED_PARSED_DATA_FOLDERS
        # use_imm_prefixed = settings.USE_IMM_PREFIXED_PARSED_DATA_FOLDERS
        #
        # try:
        #     parent_types = [AssetTypes.BUOY.value, AssetTypes.NSIF.value, AssetTypes.MFN.value]
        #     parent_asset = Asset.objects.filter(parent=self.platform, type_id__in=parent_types)
        #
        #     linked_asset = AssetLink.objects\
        #         .filter(asset=self, linked_asset__in=parent_asset)\
        #         .first()\
        #         .linked_asset
        #
        #     linked_code = linked_asset.code.lower()
        # except:
        #     return ""
        #
        # # Handle folder mapping variant for 3dmgx3 -> mopak
        # if code == '3dmgx3':
        #     code = 'mopak'
        #
        # # Handle different name and parent folder for ADCP
        # if code == 'adcpt':
        #     code = 'imm-adcp' if use_imm_prefixed else 'adcp'
        #     linked_code = 'imm'
        #
        # # Handle different paths for WFP. This instrument does not have the suffixed number, regardless of the
        # #   USE_NUMBERED_PARSED_DATA_FOLDERS setting
        # if code == 'wfp':
        #     code = 'wfp'
        #     linked_code = 'imm'
        #     use_numbered = False
        #
        # if code == 'irid_isu':
        #     code = 'irid'
        #
        # # Handle mapping for instruments located on the IMM, which are in a separate parent 'imm' path (instead of
        # #   buoy, nsif, mfn)
        # if code.startswith('phsen') or code.startswith('ctdbp') or code.startswith('ctdmo') or code.startswith('pco2w'):
        #     linked_code = 'imm'
        #     code = 'imm-' + code if use_imm_prefixed else code
        #
        # try:
        #     (numbered_code, matches) = re.subn(r'(\d+)$', r'-\1', code)
        #     if use_numbered:
        #         # If the match was not found, just add '-1'
        #         if matches == 0:
        #             numbered_code = code + '-1'
        #
        #         # If the code does not end in a 1, check to see if the path exists. If not, faill back to '-1', which
        #         #   is more likely to catch non-standard paths
        #         if not numbered_code.endswith('-1'):
        #             path = Path(f'{platform_code}/{deployment_code}/{linked_code}/{numbered_code}')
        #             full_path = Path(os.path.join(settings.PARSED_DATA_DIR, str(path)))
        #
        #             if not full_path.exists():
        #                 (numbered_code, _) = re.subn(r'(\d+)$', '-1', code)
        #     else:
        #         numbered_code = code if matches == 0 else code[:5]
        #
        #         # The first attempt is with the number removed, e.g., "phsen". If that cannot be located, try with the
        #         #   original number from the instrument, e.g., "phsen1" or "ctdmo01"
        #         path = Path(f'{platform_code}/{deployment_code}/{linked_code}/{numbered_code}')
        #         full_path = Path(os.path.join(settings.PARSED_DATA_DIR, str(path)))
        #         if not full_path.exists():
        #             numbered_code = code
        #
        # except Exception as e:
        #     print(f'Exception locating path: {e}')
        #
        #     # If the above fails it means there is no number on the instrument. If that's the case, assume it's the
        #     #   first and add "-1"
        #     numbered_code = code + '-1'
        #
        # return f'{platform_code}/{deployment_code}/{linked_code}/{numbered_code}'

    @staticmethod
    def get_data_url(platform_code, deployment_code):
        location = Asset._get_data_location(platform_code, deployment_code)
        return os.path.join(settings.DATA_DIRECTORY, location) if location else ""

        
        def datesort(e):
            return e['sortdate']
        file_list.sort(key=datesort, reverse=True)

        return file_list

    @staticmethod
    def get_data_url_asset(asset):
        local_base_path = settings.RAW_DATA_DIR + '/'
        remote_base_path = settings.DATA_DIRECTORY
        
        final_path = ''
        attempt_path = os.path.join(local_base_path, asset.platform.code.lower())

        if os.path.exists(attempt_path):
            final_path = attempt_path
            attempt_path = os.path.join(attempt_path, asset.platform.deployment.code.upper())
            if os.path.exists(attempt_path):
                final_path = attempt_path
                
                attempt_path_with_cg_data = os.path.join(attempt_path, 'cg_data')
                if os.path.exists(attempt_path_with_cg_data):
                    final_path = attempt_path = attempt_path_with_cg_data

                path_parts = []
                cur_asset = asset
                db_structure_path = ''
                while cur_asset.parent:
                    if(cur_asset.type.id in [AssetTypes.DCL.value, AssetTypes.INSTRUMENT.value, AssetTypes.CPM.value]):
                        asset_name = cur_asset.name.lower()
                        path_parts.append(asset_name)
                        db_structure_path = os.path.join(asset_name, db_structure_path) #for debug only - this is what the db structure thinks the file path should look like
                    cur_asset = cur_asset.parent

                path_parts.reverse()
                for part in path_parts:
                    attempt_path = os.path.join(attempt_path, part)
                    if(os.path.exists(attempt_path)):
                        final_path = attempt_path
                    else:
                        break

        return final_path.replace(local_base_path, remote_base_path)

 
    @staticmethod
    def get_data_path(platform_code, deployment_code):
        location = Asset._get_data_location(platform_code, deployment_code)
        return os.path.join(settings.RAW_DATA_DIR, location) if location else ""

    @staticmethod
    def get_config_url(platform_code, deployment_code):
        location = Asset._get_config_location(platform_code, deployment_code)
        return os.path.join(settings.DATA_DIRECTORY, location) if location else ""

    @staticmethod
    def get_config_path(platform_code, deployment_code):
        location = Asset._get_config_location(platform_code, deployment_code)
        return os.path.join(settings.RAW_DATA_DIR, location) if location else ""

    @staticmethod
    def _get_data_location(platform_code, deployment_code):
        local_path = os.path.join(settings.RAW_DATA_DIR, platform_code.lower(), deployment_code.upper())
        if not os.path.exists(local_path):
            return ""

        return os.path.join(platform_code.lower(), deployment_code.upper())

    @staticmethod
    def _get_config_location(platform_code, deployment_code):
        location = Asset._get_data_location(platform_code, deployment_code)
        if not location:
            return ""

        # There are two potential locations for a config file:
        #   cfg_files/platform.cfg
        #   cg_data/cfg_files/platform.cfg
        config_location = os.path.join(location, "cfg_files", "platform.cfg")
        if os.path.isfile(os.path.join(settings.RAW_DATA_DIR, config_location)):
            return config_location

        config_location = os.path.join(location, "cg_data", "cfg_files", "platform.cfg")
        if os.path.isfile(os.path.join(settings.RAW_DATA_DIR, config_location)):
            return config_location

        return ""

    @property
    def status_file_path(self):
        if self.type_id not in [AssetTypes.CPM.value, AssetTypes.DCL.value, AssetTypes.STC.value]:
            return ''

        platform = self.platform.code.lower()
        code = self.code.lower()
        deployment = self.deployment.code
        prefix = 'cpm' if self.type_id == AssetTypes.CPM.value else 'dcl'

        path = Path(f'{settings.RAW_DATA_DIR}/{platform}/{deployment}/cg_data/{code}/syslog/{prefix}_status.txt')
        if path.exists():
            return str(path)

        # For CPM1, the status file is in the root level syslog folder for the mooring
        if self.type_id == AssetTypes.CPM.value and code == 'cpm1':
            path = Path(f'{settings.RAW_DATA_DIR}/{platform}/{deployment}/cg_data/syslog/{prefix}_status.txt')
            if path.exists():
                return str(path)

        # For STCs, the file is in a root level syslog folder for the mooring
        if self.type_id == AssetTypes.STC.value:
            path = Path(f'{settings.RAW_DATA_DIR}/{platform}/{deployment}/syslog/stc_status.txt')
            if path.exists():
                return str(path)

        return ''

    @property
    def does_instrument_have_nested_data(self):
        return self.code.lower().startswith('wfp') or self.code.lower().startswith('mmp')
    


class AssetLink(models.Model):
    asset = models.ForeignKey(Asset, related_name='links', on_delete=models.CASCADE)
    linked_asset = models.ForeignKey(Asset, related_name='linked_asset', on_delete=models.CASCADE)

    # Due to the way this model is used, assets will be linked to themselves as both the
    #   parent and child. This is to allow calls to get parents for status changes to include
    #   the asset, as well as allowing calls to get children for variables to plot to also
    #   include them
    is_parent = models.BooleanField(null=False, blank=False, default=True)
    is_child = models.BooleanField(null=False, blank=False, default=False)

    class Meta:
        db_table = 'asset_link'


class AssetProperty(models.Model):
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = 'asset_property'


class Cruise(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    notes = models.TextField(blank=True, null=True)
    log_notes = models.TextField(blank=True, null=True)
    ship_name = models.CharField(max_length=100, blank=True, null=True)

    # TODO(mchagnon): Use 'date' prefix in variable name, or use "started" and "ended"
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cruise'


class CruiseDeployment(models.Model):
    cruise = models.ForeignKey(Cruise, on_delete=models.CASCADE)
    deployment = models.ForeignKey(Deployment, on_delete=models.CASCADE)

    class Meta:
        db_table = 'cruise_deployment'


class CruiseDocument(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    cruise = models.ForeignKey(Cruise, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notes = models.TextField()

    # TODO(mchagnon): one more field is needed for either the document contents or the file name
    # content = models.BinaryField()
    # filename = models.CharField(max_length=255)

    class Meta:
        db_table = 'cruise_document'


class UserAsset(models.Model):
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_asset'


class StatusHistory(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    status_id = models.IntegerField(choices=enum_choices(AssetStatuses))
    prev_status_id = models.IntegerField(choices=enum_choices(AssetStatuses), blank=True, null=True)
    reason = models.TextField()

    class Meta:
        db_table = 'asset_history'

# TODO(mchagnon): According to the initial database diagram, we have a table for coefficient data. Further discussion
# TODO(mchagnon): needed to see if we do need this table and/or which Django app it goes in

class AssetLocation(models.Model):
    asset = models.ForeignKey(Asset, blank=False, null=False, on_delete=models.CASCADE)
    location_date = models.DateTimeField()
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    class Meta:
        db_table = 'asset_location'

#region " SBD Statuses "

SBD_MO_STATUSES = [
    (0, 'MO message, if any, transferred successfully.'),
    (1, 'MO message, if any, transferred successfully, but the MT message in the queue was too big to be transferred.'),
    (2, 'MO message, if any, transferred successfully, but the requested Location Update was not accepted.'),
    (3, 'Reserved, but indicate MO session success if used.'),
    (4, 'Reserved, but indicate MO session success if used.'),
    (5, 'Reserved, but indicate MO session failure if used.'),
    (6, 'Reserved, but indicate MO session failure if used.'),
    (7, 'Reserved, but indicate MO session failure if used.'),
    (8, 'Reserved, but indicate MO session failure if used.'),
    (10, 'Gateway reported that the call did not complete in the allowed time.'),
    (11, 'MO message queue at the Gateway is full.'),
    (12, 'MO message has too many segments.'),
    (13, 'Gateway reported that the session did not complete.'),
    (14, 'Invalid segment size.'),
    (15, 'Access is denied. Transceiver-reported values'),
    (16, 'Transceiver has been locked and may not make SBD calls (see +CULK command).'),
    (17, 'Gateway not responding (local session timeout).'),
    (18, 'Connection lost (RF drop).'),
    (19, 'Reserved, but indicate MO session failure if used.'),
    (20, 'Reserved, but indicate MO session failure if used.'),
    (21, 'Reserved, but indicate MO session failure if used.'),
    (22, 'Reserved, but indicate MO session failure if used.'),
    (23, 'Reserved, but indicate MO session failure if used.'),
    (24, 'Reserved, but indicate MO session failure if used.'),
    (25, 'Reserved, but indicate MO session failure if used.'),
    (26, 'Reserved, but indicate MO session failure if used.'),
    (27, 'Reserved, but indicate MO session failure if used.'),
    (28, 'Reserved, but indicate MO session failure if used.'),
    (29, 'Reserved, but indicate MO session failure if used.'),
    (30, 'Reserved, but indicate MO session failure if used.'),
    (31, 'Reserved, but indicate MO session failure if used.'),
    (32, 'No network service, unable to initiate call. '),
    (33, 'Antenna fault, unable to initiate call.'),
    (34, 'Radio is disabled, unable to initiate call (see *Rn command).'),
    (35, 'Transceiver is busy, unable to initiate call (typically performing auto-registration).'),
    (36, 'Reserved, but indicate failure if used.')
]

#endregion

#region " Snapshot Flags "

SNAPSHOT_CPM_MPIC_ERROR_FLAGS = [
    ('0x00000000', 'No errors'),
    ('0x00000001', 'SBD Hardware Failure'),
    ('0x00000002', 'SBD Antenna Fault'),
    ('0x00000004', 'SBD No Comms'),
    ('0x00000008', 'SBD Timeout Exceeded'),
    ('0x00000010', 'SBD Bad Message Received'),
    ('0x00000020', 'Main_V Out of Range'),
    ('0x00000040', 'Main_C Out of Range'),
    ('0x00000080', 'BBatt_V Out of Range'),
    ('0x00000100', 'BBatt_C Out of Range'),
    ('0x00000200', 'Seascan PPS Fault'),
    ('0x00000400', 'GPS PPS Fault'),
    ('0x00000800', 'Wake from Unknown Source'),
    ('0x00001000', 'No PSC Data'),
    ('0x00002000', 'PSC Main_V and Main_V disagree'),
    ('0x00004000', 'PSC Main_C and Main_C disagree'),
    ('0x00008000', 'No CPM Heartbeat'),
    ('0x00010000', 'Heartbeat Threshold Exceeded, power-cycling CPM'),
    ('0x00020000', 'Iseawater Gflt_SBD_Pos out of allowable range'),
    ('0x00040000', 'Iseawater Gflt_SBD_Gnd out of allowable range'),
    ('0x00080000', 'Iseawater Gflt_GPS_Pos out of allowable range'),
    ('0x00100000', 'Iseawater Gflt_GPS_Gnd out of allowable range'),
    ('0x00200000', 'Iseawater Gflt_Main_Pos out of allowable range'),
    ('0x00400000', 'Iseawater Gflt_Main_Gnd out of allowable range'),
    ('0x00800000', 'Iseawater Gflt_9522_fw_Pos out of allowable range'),
    ('0x01000000', 'Iseawater Gflt_9522_fw_Gnd out of allowable range'),
    ('0x02000000', 'Leak Det1 Exceeded Limit'),
    ('0x04000000', 'Leak Det2 Exceeded Limit'),
    ('0x08000000', 'I2C Communication Error'),
    ('0x10000000', 'UART Communication Error'),
    ('0x20000000', 'CPM dead - recommend switchover...'),
    ('0x40000000', 'Channel PIC over current'),
    ('0x80000000', 'MPIC Brown-out Reset'),
]

SNAPSHOT_DCL_MPIC_ERROR_FLAGS = [
    ('0x00000000', 'no errors'),
    ('0x00000001', 'Vmain out of normal range'),
    ('0x00000002', 'Imain out of normal range'),
    ('0x00000004', 'Iseawater Gflt_iso3v3_Pos out of allowable range'),
    ('0x00000008', 'Iseawater Gflt_iso3v3_Gnd out of allowable range'),
    ('0x00000010', 'Iseawater Gflt_vmain_Pos out of allowable range'),
    ('0x00000020', 'Iseawater Gflt_vmain_Gnd out of allowawble range'),
    ('0x00000040', 'Iseawater Gflt_inst_Pos out of allowable range'),
    ('0x00000080', 'Iseawater Gflt_inst_Gnd out of allowable range'),
    ('0x00000100', 'Iseawater out of allowable range for volt. src 4 +V'),
    ('0x00000200', 'Iseawater out of allowable range for volt. src 4 gnd'),
    ('0x00000400', 'Leak 0 voltage exceeded limit'),
    ('0x00000800', 'Leak 1 voltage exceeded limit'),
    ('0x00001000', 'CHPIC overcurrent fault exists'),
    ('0x00002000', 'CHPIC (addr 1) not responding'),
    ('0x00004000', 'CHPIC (addr 2) not responding'),
    ('0x00008000', 'CHPIC (addr 3) not responding'),
    ('0x00010000', 'CHPIC (addr 4) not responding'),
    ('0x00020000', 'CHPIC (addr 5) not responding'),
    ('0x00040000', 'CHPIC (addr 6) not responding'),
    ('0x00080000', 'CHPIC (addr 7) not responding'),
    ('0x00100000', 'CHPIC (addr 8) not responding'),
    ('0x00200000', 'I2C error'),
    ('0x00400000', 'UART error'),
    ('0x80000000', 'MPIC Brown-out Reset'),
]

SNAPSHOT_MPEA_ERROR_FLAGS_1 = [
    ('0x00000000', 'No Error'),
    ('0x00000001', 'High Voltage Input Undervoltage (<270VDC)'),
    ('0x00000002', 'High Voltage Input Overvoltage  (>390VDC)'),
    ('0x00000004', 'High Voltage Input Power Sensor Fault'),
    ('0x00000008', 'MPM Internal Over Temp (>85degC)'),
    ('0x00000010', 'MPEA Hotel Power Coverter Over Temp (>100 degC)'),
    ('0x00000020', '5V Hotel Power Undervoltage (<3VDC)'),
    ('0x00000040', '5V Hotel Power Overvoltage  (>7VDC)'),
    ('0x00000080', 'Microcontroller Core Undervoltage (<3VDC)'),
    ('0x00000100', 'Microcontroller Core Overvoltage (>4.5VDC)'),
    ('0x00000200', 'Hotel Power Status Sensor Fault'),
    ('0x00000400', 'MPEA Reset Flag'),
    ('0x00000800', 'Converter 1 input Overcurrent (>500mA)'),
    ('0x00001000', 'Converter 1 output Overvoltage (>32VDC)'),
    ('0x00002000', 'Converter 1 output Undervoltage (<18VDC)'),
    ('0x00004000', 'Converter 1 output Overcurent (>4.5A)'),
    ('0x00008000', 'Converter 1 DC-DC Converter Fault'),
    ('0x00010000', 'Converter 1 input sensor fault'),
    ('0x00020000', 'Converter 1 output sensor fault'),
    ('0x00040000', 'Converter 2 input Overcurrent (>500Ma)'),
    ('0x00080000', 'Converter 2 output voltage too high (>32VDC)'),
    ('0x00100000', 'Converter 2 output voltage too low (<18VDC)'),
    ('0x00200000', 'Converter 2 output Overcurrent (>4.5A)'),
    ('0x00400000', 'Converter 2 DC-DC Converter Fault'),
    ('0x00800000', 'Converter 2 input sensor fault'),
    ('0x01000000', 'Converter 2 output sensor fault'),
    ('0x02000000', 'Converter 3 input Overcurrent (>500mA)'),
    ('0x04000000', 'Converter 3 output voltage too high (>32VDC)'),
    ('0x08000000', 'Converter 3 output voltage too low (<18VDC)'),
    ('0x10000000', 'Converter 3 output Overcurrent (>4.5A)'),
    ('0x20000000', 'Converter 3 DC-DC Converter Fault'),
    ('0x40000000', 'Converter 3 input sensor fault'),
    ('0x80000000', 'Converter 3 output sensor fault'),
]

SNAPSHOT_MPEA_ERROR_FLAGS_2 = [
    ('0x00000000', 'No Error'),
    ('0x00000001', 'Converter 4 input Overcurrent (>500mA)'),
    ('0x00000002', 'Converter 4 output voltage too high (>32VDC)'),
    ('0x00000004', 'Converter 4 output voltage too low (<18VDC)'),
    ('0x00000008', 'Converter 4 output Overcurrent (>4.5A)'),
    ('0x00000010', 'Converter 4 DC-DC Converter Fault'),
    ('0x00000020', 'Converter 4 input sensor fault'),
    ('0x00000040', 'Converter 4 output sensor fault'),
    ('0x00000080', 'Not Used'),
    ('0x00000100', 'Not Used'),
    ('0x00000200', 'Not Used'),
    ('0x00000400', 'Not Used'),
    ('0x00000800', 'Not Used'),
    ('0x00001000', 'Not Used'),
    ('0x00002000', 'Not Used'),
    ('0x00004000', 'Not Used'),
    ('0x00008000', 'Not Used'),
    ('0x00010000', 'Not Used'),
    ('0x00020000', 'Not Used'),
    ('0x00040000', 'Not Used'),
    ('0x00080000', 'Not Used'),
    ('0x00100000', 'Not Used'),
    ('0x00200000', 'Not Used'),
    ('0x00400000', 'Not Used'),
]

SNAPSHOT_PSC_ERROR_FLAGS_1 = [
    ('0x00000000', 'No Error'),
    ('0x00000001', 'Battery1 of String1 Overtemp'),
    ('0x00000002', 'Battery2 of String1 Overtemp'),
    ('0x00000004', 'Battery1 of String2 Overtemp'),
    ('0x00000008', 'Battery2 of String2 Overtemp'),
    ('0x00000010', 'Battery1 of String3 Overtemp'),
    ('0x00000020', 'Battery2 of String3 Overtemp'),
    ('0x00000040', 'Battery1 of String4 Overtemp'),
    ('0x00000080', 'Battery2 of String4 Overtemp'),
    ('0x00000100', 'Battery String 1 Fuse Blown'),
    ('0x00000200', 'Battery String 2 Fuse Blown'),
    ('0x00000400', 'Battery String 3 Fuse Blown'),
    ('0x00000800', 'Battery String 4 Fuse Blown'),
    ('0x00001000', 'Battery String 1 Charging Sensor Fault'),
    ('0x00002000', 'Battery String 1 Discharging Sensor Fault'),
    ('0x00004000', 'Battery String 2 Charging Sensor Fault'),
    ('0x00008000', 'Battery String 2 Discharging Sensor Fault'),
    ('0x00010000', 'Battery String 3 Charging Sensor Fault'),
    ('0x00020000', 'Battery String 3 Discharging Sensor Fault'),
    ('0x00040000', 'Battery String 4 Charging Sensor Fault'),
    ('0x00080000', 'Battery String 4 Discharging Sensor Fault'),
    ('0x00100000', 'PV1 Sensor Fault'),
    ('0x00200000', 'PV2 Sensor Fault'),
    ('0x00400000', 'PV3 Sensor Fault'),
    ('0x00800000', 'PV4 Sensor Fault'),
    ('0x01000000', 'WT1 Sensor Fault'),
    ('0x02000000', 'WT2 Sensor Fault'),
    ('0x04000000', 'EEPROM Access Fault'),
    ('0x08000000', 'RTCLK Access Fault'),
    ('0x10000000', 'External Power Sensor Fault'),
    ('0x20000000', 'PSC Hotel Power Sensor Fault'),
    ('0x40000000', 'PSC Internal Oververtemp Fault'),
    ('0x80000000', '24V-300V DC-DC Converter Fuse Blown'),
]

SNAPSHOT_PSC_ERROR_FLAGS_2 = [
    ('0x00000000', 'No Error'),
    ('0x00000001', '24V Buoy Power Sensor Fault'),
    ('0x00000002', '24V Buoy Power Over-voltage Fault'),
    ('0x00000004', '24V Buoy Power Under-voltage Fault'),
    ('0x00000008', '5V Fuse Blown (non-critical)'),
    ('0x00000010', 'WT1 Control Relay Fault'),
    ('0x00000020', 'WT2 Control Relay Fault'),
    ('0x00000040', 'PV1 Control Relay Fault'),
    ('0x00000080', 'PV2 Control Relay Fault'),
    ('0x00000100', 'PV3 Control Relay Fault'),
    ('0x00000200', 'PV4 Control Relay Fault'),
    ('0x00000400', 'FC1 Control Relay Fault'),
    ('0x00000800', 'FC2 Control Relay Fault'),
    ('0x00001000', 'CVT SWG Fault'),
    ('0x00002000', 'CVT General Fault'),
    ('0x00004000', 'PSC Hard Reset Flag'),
    ('0x00008000', 'PSC Power On Reset Flag'),
    ('0x00010000', 'WT1 Fuse Blown'),
    ('0x00020000', 'WT2 Fuse Blown'),
    ('0x00040000', 'PV1 Fuse Blown'),
    ('0x00080000', 'PV2 Fuse Blown'),
    ('0x00100000', 'PV3 Fuse Blown'),
    ('0x00200000', 'PV4 Fuse Blown'),
    ('0x00400000', 'CVT Shut Down Due to Low Input voltage '),
]

SNAPSHOT_PSC_ERROR_FLAGS_3 = [
    ('0x00000000', 'No Error'),
    ('0x00000001', 'CVTR Board Overtemp (>100 C)'),
    ('0x00000002', 'Interlock Output Supply Fuse Blown'),
    ('0x00000004', 'Interlock Status 1 Supply Fuse Blown'),
    ('0x00000008', 'Interlock Status 2 Supply Fuse Blown'),
    ('0x00000010', 'Input 1 Fuse Blown'),
    ('0x00000020', 'Input 2 Fuse Blown'),
    ('0x00000040', 'Input 3 Fuse Blown'),
    ('0x00000080', 'Input 4 Fuse Blown'),
    ('0x00000100', '+5V Over-voltage (>5.5V)'),
    ('0x00000200', '+5V Under-voltage (<4.5V)'),
    ('0x00000400', 'Output Sensor Circuit Power Over-voltage (>9.45V)'),
    ('0x00000800', 'Output Sensor Circuit Power Under-voltage (<4.5V)'),
    ('0x00001000', 'P_SWGF Sensor Circuit Power Over-voltage (>9.45V)'),
    ('0x00002000', 'P_SWGF Sensor Circuit Power Under-voltage (<8.64V)'),
    ('0x00004000', 'N_SWGF Sensor Circuit Power Over-voltage (>9.45V)'),
    ('0x00008000', 'N_SWGF Sensor Circuit Power Under-voltage (<8.64V)'),
    ('0x00010000', 'RAW_24V Input Power Sensor Fault'),
    ('0x00020000', 'CVTR_24V Hotel Power Sensor Fault'),
    ('0x00040000', 'Interlock Supply Output Sensor Fault'),
    ('0x00080000', 'Interlock Status 1 Sensor Fault'),
    ('0x00100000', 'Interlock Status 2 Sensor Fault'),
    ('0x00200000', 'Interlock Input Sensor Fault'),
    ('0x00400000', 'P_SWGF Occured'),
    ('0x00800000', 'N_SWGF Occured'),
    ('0x01000000', 'Input 1 Sensor Fault'),
    ('0x02000000', 'Input 2 Sensor Fault'),
    ('0x04000000', 'Input 3 Sensor Fault'),
    ('0x08000000', 'Input 4 Sensor Fault'),
    ('0x10000000', 'High Voltage Output Current Sensor Fault'),
    ('0x20000000', 'High Voltage Output Voltage Sensor Fault'),
    ('0x40000000', 'P_SWGF Sensor Fault'),
    ('0x80000000', 'N_SWGF Sensor Fault'),
]

SNAPSHOT_PSC_OVERRIDE_FLAGS = [
    ('0x00000000', 'Override Off'),                 # override_off
    ('0x00000001', 'Wind Turbine 1 Connected'),     # wt1_connected
    ('0x00000002', 'Wind Turbine 2 Connected'),     # wt2_connected
    ('0x00000004', 'Solar Panel 1 Connected'),      # sp1_connected
    ('0x00000008', 'Solar Panel 2 Connected'),      # sp2_connected
    ('0x00000010', 'Solar Panel 3 Connected'),      # sp3_connected
    ('0x00000020', 'Solar Panel 4 Connected'),      # sp4_connected
    ('0x00000040', 'Fuel Cell 1 Connected'),        # fc1_connected
    ('0x00000080', 'Fuel Cell 2 Connected'),        # fc2_connected
    ('0x00000100', 'CVT Connected'),                # cvt_connected
    ('0x00000200', 'CVT Reset'),                    # cvt_reset
    ('0x00000400', 'External Charger'),             # external_charger
]

SNAPSHOT_WAKE_CODES = [
    (1, '0x00000001', 'Cold start'),
    (2, '0x00000002', 'Alarm wakeup from CE'),
    (3, '0x00000004', 'Wakeup from SBD'),
    (4, '0x00000008', 'Wakeup from DCL'),
    (5, '0x00000010', 'MPIC Watchdog'),
    (6, '0x00000020', 'PSC Error'),
    (7, '0x00000040', 'Wakeup from IMM'),
    (8, '0x00000080', 'HBEAT Error'),
]

#endregion
