from django.db import models
from django.contrib.auth.models import User

from .asset import Asset
from .variable import Variable
from ..db import exec_void
from ..erddap import Erddap


class PlotType(models.Model):
    name = models.CharField(max_length=25)

    class Meta:
        db_table = 'plot_type'

    def __str__(self):
        return self.name


class PlotClass(models.Model):
    name = models.CharField(max_length=25)

    class Meta:
        db_table = 'plot_class'

    def __str__(self):
        return self.name


class PlotTimeRange(models.Model):
    days = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=50, null=False, blank=False)

    class Meta:
        db_table = 'plot_time_range'

    def __str__(self):
        return self.name


class Plot(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    is_global = models.BooleanField(blank=False, null=False, default=True)
    plot_type = models.ForeignKey(PlotType, blank=True, null=True, on_delete=models.CASCADE)
    plot_class = models.ForeignKey(PlotClass, blank=True, null=True, on_delete=models.CASCADE)
    plot_time_range = models.ForeignKey(PlotTimeRange, blank=True, null=True, on_delete=models.CASCADE)
    plot_relative_to = models.CharField(blank=True, max_length=50, null=True, default="last_reported_data")
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    class Meta:
        db_table = 'plot'

    def __str__(self):
        return self.name

    # TODO(mchagnon): Needs to account for all sorts of variables/settings
    # TODO(mchagnon): Will fail if there are duplicate variables
    def load_data(self):
        data = []

        variables = PlotVariable.objects.filter(plot=self).select_related('variable')
        for variable in variables:
            # TODO(mchagnon): Need to define the time window for preloaded plots somewhere
            data.append(Erddap().get_plot_data(variable.variable.asset, variable.variable, 1))

        return data

    @staticmethod
    def delete(plot_id):
        exec_void("delete_plot", (plot_id,))

class PlatformDefaultPlot(models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE, related_name="platform_default")
    platform_name = models.CharField(max_length=200, null=False, blank=False)
    class Meta:
        db_table = 'platform_default_plot'

class PlotVariable(models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE, related_name="plotvariable")
    variable = models.ForeignKey(Variable, on_delete=models.CASCADE)
    axis = models.CharField(max_length=1, default='y')
    reverse_axis = models.BooleanField(null=False, blank=False, default=False)

    class Meta:
        db_table = 'plot_variable'

class PlotPage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, null=False, blank=False)
    asset = models.ForeignKey(Asset, null=True, blank=True, on_delete=models.CASCADE)
    is_asset_page = models.BooleanField(default=False, null=False, blank=False)
    share_code = models.UUIDField(null=True, blank=True)
    num_columns = models.IntegerField(null=False, blank=False, default=3)

    class Meta:
        db_table = 'plot_page'


class PlotPagePlot(models.Model):
    page = models.ForeignKey(PlotPage, null=False, blank=False, related_name='plots', on_delete=models.CASCADE)
    plot = models.ForeignKey(Plot, null=False, blank=False, on_delete=models.CASCADE)
    order = models.IntegerField(null=False, blank=True, default=0)

    class Meta:
        db_table = 'plot_page_plot'
