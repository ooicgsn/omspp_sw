import math
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from enum import Enum
from ..models.asset import Asset
from ..models.variable import Variable
from ..db import exec_void
from .static import DeliveryMethods, DeliveryMethod, DurationCodes, DurationCode, Severities, Severity,\
    TriggerCategory, TriggerType, TriggerTypes
from ..util import enum_choices
import pandas as pd
from numpy import dtype
import re


class Trigger(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    users = models.ManyToManyField(User, through='TriggerUser')
    severity = models.ForeignKey(Severity,
                                 limit_choices_to=enum_choices(Severities),
                                 null=True, blank=True, on_delete=models.CASCADE)
    duration_code = models.ForeignKey(DurationCode,
                                      limit_choices_to=enum_choices(DurationCodes), on_delete=models.CASCADE)
    asset = models.ForeignKey(Asset, null=True, blank=True, on_delete=models.CASCADE)
    is_global = models.BooleanField(default=False)
    expression = models.TextField(null=True, blank=True)
    mandatory = models.BooleanField(null=False, blank=False, default=False)
    category = models.ForeignKey(TriggerCategory, null=True, blank=True, on_delete=models.CASCADE)

    # TODO(badams): consider using an IntervalField here instead
    duration = models.FloatField(validators=[MinValueValidator(0.0),
                                             MaxValueValidator(999999999.99)])

    roll_up = models.BooleanField(default=False)
    roll_down = models.BooleanField(default=False)
    comments = models.TextField(null=True, blank=True)
    interpolation_amount = models.DurationField(null=True, blank=True)
    change_interval = models.DurationField(null=True, blank=True)
    is_nodata_trigger = models.BooleanField(default=False)
    trigger_type = models.ForeignKey(TriggerType, blank=False, null=True, on_delete=models.CASCADE)

    def get_duration(self):
        """
        Helper function to return a timedelta if time specified, or None to
        signify an alert that if created, will last the entire deployment
        """
        # there is no infinite timedelta, so just return None
        dur_type = self.duration_code.name.upper()
        # FIXME (badams): Need to have integer duration for the time being.
        # don't have an easy fix for this with Pandas .rolling compatibility
        # for the time being
        if dur_type == DurationCodes.HOURS.name:
            return "{}H".format(int(self.duration))
        elif dur_type == DurationCodes.MINUTES.name:
            return "{}Min".format(int(self.duration))
        elif dur_type == DurationCodes.SECONDS.name:
            return "{}S".format(int(self.duration))
        elif dur_type == DurationCodes.CURRENT_DEPLOYMENT.name:
            return None
        else:
            return None

    def split_expression(self, expression):
        """
        Splits an expression into a 2-tuple of quantity to be evaluated,
        followed by a predicate afterwards (i.e. ("2*a+b", "> 13")).  Mainly
        used for splitting an expression to evaluate it and window it prior to
        determining if it exceeds or matches some threshold.
        """
        # should run validate beforehand
        if expression is None:
            raise ValueError('No expression is defined for variable {}'.format(
                               self.name))
        # not bulletproof, may want to store predicate in separate field in
        # a later database design
        matches = self._split_trigger_logic(expression)
        if matches is None:
            raise ValueError('Expression splitting into predicate failed')
        else:
            return matches

    @classmethod
    def _split_trigger_logic(cls, expression):
        # TODO: (badams) this is extremely complicated to maintain so consider
        # splitting the main part of the expression and predicate into two
        # separate database fields
        pat = r'(.+\s*)(?<=[\w\s])((?:(?:(?!>>)>(?<!>>)|(?!<<)<(?<!<<))=?|(?<![<>])(?:==?|!=))[\w\s]\s*.+?)$'
        return re.match(pat, expression)

    def validate_expression(self, test_expr):
        """
        Validates the trigger expression, ensuring the syntax is properly
        formed, the variables defined in the expression have corresponding
        counterparts in the trigger variables, and that the expression itself
        returns a boolean type.  Returns a 2-tuple with the first element a
        boolean indicating whether the expression is valid or not, and the
        second element with any messages in the event of a failure.
        """
        var_names = [tv.name for tv
                     in TriggerVariable.objects.filter(trigger=self)]
        # create an empty dataframe with only the columns to test
        dummy_df = pd.DataFrame(columns=var_names, dtype=dtype('float64'))
        try:
            res = dummy_df.eval(test_expr)
        except Exception as e:
            return False, str(e)
        if res.dtype != dtype('bool'):
            return False, "Expression must return a boolean type"
        else:
            return True, None

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'alert_trigger'

    @property
    def duration_label(self):
        if self.duration_code.id == DurationCodes.CURRENT_DEPLOYMENT.value:
            return 'Current Deployment'

        return "{} {}".format(self.duration, self.duration_code.name)

    @staticmethod
    def delete(trigger_id):
        exec_void("delete_trigger", (trigger_id, ))

    @property
    def duration_in_seconds(self):
        if not self.duration_code or not self.duration:
            return -1

        if self.duration_code.id == DurationCodes.SECONDS.value:
            return math.floor(self.duration)
        elif self.duration_code.id == DurationCodes.MINUTES.value:
            return math.floor(self.duration * 60)
        elif self.duration_code.id == DurationCodes.HOURS.value:
            return math.floor(self.duration * 60 * 60)
        else:
            return -1


class PlatformDefaultTrigger(models.Model):
    trigger = models.ForeignKey(Trigger, on_delete=models.CASCADE, related_name="platform_default")
    platform_name = models.CharField(max_length=200, null=False, blank=False)
    class Meta:
        db_table = 'platform_default_trigger'


class TriggerUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    trigger = models.ForeignKey(Trigger, on_delete=models.CASCADE)
    delivery_method = models.ForeignKey(DeliveryMethod, limit_choices_to=enum_choices(DeliveryMethods), on_delete=models.CASCADE)

    class Meta:
        db_table = 'alert_trigger_user'


class Rule(models.Model):
    trigger = models.ForeignKey(Trigger, on_delete=models.CASCADE, related_name='rules')
    variables = models.ManyToManyField(Variable, through='RuleVariable', related_name='variables')

    # TODO(mchagnon): expression needs to be flushed out further
    expression = models.TextField()

    class Meta:
        db_table = 'rule'


class RuleVariable(models.Model):
    rule = models.ForeignKey(Rule, on_delete=models.CASCADE)
    variable = models.ForeignKey(Variable, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'rule_variable'


class TriggerVariable(models.Model):
    trigger = models.ForeignKey(Trigger, related_name='variables', on_delete=models.CASCADE)
    variable = models.ForeignKey(Variable, related_name='+', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True, blank=False)

    class Meta:
        db_table = 'alert_trigger_variable'

