from django.db import models


class Website(models.Model):
    """Pseudo model solely to hold permissions that aren't directly tied to another, more specific model"""

    class Meta:
        # This will prevent a database table from being created for the model
        managed = False
