from django.db import models
from .asset import Asset, Group
from .static import Severity


class Summary(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'summary'


class SummaryPlatform(models.Model):
    summary = models.ForeignKey(Summary, null=False, blank=False, on_delete=models.CASCADE)
    platform = models.ForeignKey(Asset, null=False, blank=False, on_delete=models.CASCADE)
    telemetry_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)
    data_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)
    power_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)
    functional_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)

    class Meta:
        db_table = 'summary_platform'


class SummaryGroup(models.Model):
    summary = models.ForeignKey(Summary, null=False, blank=False, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, null=False, blank=False, on_delete=models.CASCADE)
    status_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)

    class Meta:
        db_table = 'summary_group'


class SummaryAlertCount(models.Model):
    summary = models.ForeignKey(Summary, null=False, blank=False, on_delete=models.CASCADE)
    platform = models.ForeignKey(Asset, null=False, blank=False, on_delete=models.CASCADE)
    num_alerts = models.IntegerField(default=0, null=False, blank=False)
    severity = models.ForeignKey(Severity, null=False, blank=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'summary_alert_count'