from django import forms

class UploadFileForm(forms.Form):
    file = forms.FileField()
    def clean(self):
        cleaned_data = super(UploadFileForm, self).clean()
        file = cleaned_data.get('file')

        if file:
            filename = file.name.lower()
            
            if not filename.endswith('.json'):
                self.add_error('file', "File is not JSON. Please upload only json files")
                # raise forms.ValidationError("File is not JSON. Please upload only json files")

        return file