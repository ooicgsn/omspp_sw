import re
from django import forms

from ..models import Asset
from ..models.asset import AssetDisposition, AssetDispositions, AssetCategory, AssetSubcategory, AssetClassification
from ..models.asset import Group


class PlatformForm(forms.Form):
    disposition = forms.ChoiceField()
    category = forms.ChoiceField()
    subcategory = forms.ChoiceField(required=False)

    deployment_date = forms.CharField(required=False, label='Deployment Date', widget=forms.DateTimeInput(attrs={'class': 'datetimepicker deployment_date'}, format="%m/%d/%y %I:%M%p"))
    deployment_cruise = forms.CharField(required=False, label='Deployment Cruise', max_length=100)
    recovery_date = forms.CharField(required=False, label='Recovery Date', widget=forms.DateTimeInput(attrs={'class': 'datetimepicker recovery_date'}, format="%m/%d/%y %I:%M%p"))
    recovery_cruise = forms.CharField(required=False, label='Recovery Cruise', max_length=100)

    lat = forms.FloatField(required=False, widget=forms.TextInput())
    lon = forms.FloatField(required=False, widget=forms.TextInput())
    depth = forms.FloatField(required=False, widget=forms.TextInput())
    watch_circle_radius = forms.FloatField(required=False, widget=forms.TextInput())

    def __init__(self, *args, **kwargs):
        super(PlatformForm, self).__init__(*args, **kwargs)

        self.fields['disposition'] = forms.ChoiceField(
            choices=tuple(AssetDisposition.objects.all().values_list('id', 'name')),
            widget=forms.Select(attrs={'class': 'form-control'})
        )

        categories = AssetCategory.objects.all().values_list("id", "name")
        categories = [(0, "Select a Category")] + list(categories)

        self.fields['category'] = forms.ChoiceField(
            choices=categories,
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )

        subcategories = AssetSubcategory.objects.all().values_list("id", "name")
        subcategories = [(0, "Select a Subcategory")] + list(subcategories)

        self.fields['subcategory'] = forms.ChoiceField(
            choices=subcategories,
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )
    
    def clean(self):
        self.cleaned_data = super(PlatformForm, self).clean()

        if (self.cleaned_data.get('lat') and not self.cleaned_data.get('lon') or
            self.cleaned_data.get('lon') and not self.cleaned_data.get('lat')):
            raise forms.ValidationError("Please enter both Latitude and Longitude")

        return self.cleaned_data


class EditAssetForm(forms.ModelForm):
    GLOB_REGEX = r'^[a-zA-Z0-9\_\-\/\*\.]{5,}$'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_raw_data_glob(self):
        data = self.cleaned_data.get('raw_data_glob')

        if data and not re.match(self.GLOB_REGEX, data):
            raise forms.ValidationError("Please enter a valid path")

        return data

    def clean_parsed_data_glob(self):
        data = self.cleaned_data.get('parsed_data_glob')

        if data and not re.match(self.GLOB_REGEX, data):
            raise forms.ValidationError("Please enter a valid path")

        return data

    def clean_reference_designator(self):
        data = self.cleaned_data.get('reference_designator')

        if data and not re.match(r'^[a-zA-Z0-9-]+$', data):
            raise forms.ValidationError("Only letters, numbers and dashes are allowed")

        return data

    def clean_serial_number(self):
        data = self.cleaned_data.get('serial_number')

        if data and not re.match(r'^[a-zA-Z0-9-]+$', data):
            raise forms.ValidationError("Only letters, numbers and dashes are allowed")

        return data

    class Meta:
        model = Asset
        fields = ('parsed_data_glob', 'raw_data_glob', 'reference_designator', 'serial_number', )
        widgets = {
            'parsed_data_glob': forms.TextInput(attrs={'class': 'form-control'}),
            'raw_data_glob': forms.TextInput(attrs={'class': 'form-control'}),
            'reference_designator': forms.TextInput(attrs={'class': 'form-control'}),
            'serial_number': forms.TextInput(attrs={'class': 'form-control'}),
        }


class ClassificationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationForm, self).__init__(*args, **kwargs)

        self.fields['description'].required = False
        self.fields['pattern'].required = False

    def clean(self):
        data = self.cleaned_data

        return data

    class Meta:
        model = AssetClassification
        fields = ('name', 'abbreviation', 'description', 'is_virtual', 'pattern')
        widgets = {
            'description': forms.Textarea(attrs={'rows': 10, 'style': 'width:100%;height:150px'})
        }


class ImportPlatformForm(forms.Form):
    platform_code = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    deployment_code = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    disposition = forms.ModelChoiceField(
        queryset=AssetDisposition.objects.all(),
        required=False,
        empty_label=None,
        widget=forms.Select(attrs={'class': 'form-control'}))
    category = forms.ChoiceField(required=False)
    subcategory = forms.ChoiceField(required=False)
    group = forms.ChoiceField(required=False)

    def __init__(self, *args, **kwargs):
        super(ImportPlatformForm, self).__init__(*args, **kwargs)

        cats = AssetCategory.objects.all()
        cat = []
        cat.append([0, 'Select a Category'])
        for c in cats:
            cat.append([c.id, c.name])

        self.fields['category'] = forms.ChoiceField(
            choices=tuple(cat),
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )
        subcat = []
        subcat.append([0, 'Select a Subcategory'])
        subcats = AssetSubcategory.objects.all()
        for sub in subcats:
            subcat.append([sub.id, sub.name])
        self.fields['subcategory'] = forms.ChoiceField(
            choices=tuple(subcat),
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )

        groups = [
            (g['id'], g['name'])
            for g in Group.objects.order_by('name').values('id', 'name')
        ]

        self.fields['group'] = forms.ChoiceField(
            choices=[('', 'Auto-Detect'), *groups],
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )

