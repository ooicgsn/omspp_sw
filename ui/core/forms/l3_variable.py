import json
from django import forms
from django.db.models import Q
from ..models.variable import Variable
from ..util import enum_choices
from ..models.static import L3Duration
from ..models.account import User, format_user_full_name


class UserChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        if not obj.first_name and not obj.last_name:
            return obj.username
        else:
            return obj.first_name + ' ' + obj.last_name


class L3VariableForm(forms.ModelForm):
    duration_list = list(enum_choices(L3Duration))
    duration_list.insert(0, ('', ''))
    duration_tuple = tuple(duration_list)
    duration_code = forms.ChoiceField(
        choices=duration_tuple,
        widget=forms.Select(attrs={'class': 'form-control'}),
        required=False
    )

    owner = forms.ChoiceField(
        choices=[],
        required=False
    )

    def __init__(self, is_admin, *args, **kwargs):
        super(L3VariableForm, self).__init__(*args, **kwargs)

        users = User.objects.filter()
        users_list = []

        for user in users:
            if ('owner' in self.initial and user.id == self.initial['owner']) or user.is_active:
                name = format_user_full_name(user)
                users_list.append([user.id, name])

        users_list = sorted(users_list, key=lambda x: x[1])
        users_list.insert(0, ['', 'Please select an owner'])

        self.fields['owner'].choices = tuple(users_list)

        self.fields['expression'].required = True
        self.fields['units'].required = True
        if is_admin:
            self.fields['is_global'].required = False
        else:
            self.initial['is_global'] = True

    def clean(self):
        cleaned_data = super(L3VariableForm, self).clean()
        error_msg = 'Please add a component.'
        error_msg_duration = 'Please select a duration code.'
        error_msg_owner = 'Please select an owner. '
        error_msg_duration_number = 'Interpolation value is invalid.'
        components_obj = self.data['saveComponents']
        components_obj_list = json.loads(components_obj)
        duration_valid = True

        if self.data['duration']:
            try:
                float(self.data['duration'])
            except ValueError:
                duration_valid = False

        if not components_obj_list:
            raise forms.ValidationError(error_msg)
        if self.data['duration'] and not self.data['duration_code']:
            raise forms.ValidationError(error_msg_duration)
        if self.cleaned_data['is_global'] is False \
                and (self.cleaned_data['owner'] is None or self.cleaned_data['owner'] == '') \
                and (self.initial['owner'] is None or self.initial['owner'] == ''):
            raise forms.ValidationError(error_msg_owner)
        if duration_valid is False:
            raise forms.ValidationError(error_msg_duration_number)

        return cleaned_data

    class Meta:
        model = Variable
        fields = ('name', 'expression', 'units', 'is_global', 'user')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'expression': forms.Textarea(attrs={'class': 'form-control'}),
            'units': forms.TextInput(attrs={'class': 'form-control'}),
            'is_global': forms.CheckboxInput()
        }
