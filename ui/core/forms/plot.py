from django import forms
from ..models.plot import Plot, PlotType, PlotClass, PlotTimeRange
from ..models.variable import Variable
from ..models.account import User, format_user_full_name
from ..models.asset import Asset, AssetTypes
from ..models.static import TimeChoices, BoolChoices, YAxisChoices, PlotRelativeToChoices

class PlotForm(forms.ModelForm):

    variables_x = forms.CharField(label='X-Variable(s)', widget=forms.HiddenInput())
    variables_y = forms.CharField(label='Y-Variable(s)', widget=forms.HiddenInput())

    #timeframe = forms.CharField(label='Time Frame', widget=forms.Select(
        #choices=[(e.value, e.name.title().replace('_',' ')) for e in TimeChoices]))

    owner = forms.ChoiceField(
        choices=[],
        required=False,
        widget=forms.Select(attrs={'class': 'owner'})
    )
    
    plot_type = forms.ModelChoiceField(queryset=PlotType.objects.all(), label='Plot Type', empty_label=None)
    plot_class = forms.ModelChoiceField(queryset=PlotClass.objects.all(), label='Plot Class', empty_label=None,
                                        widget=forms.HiddenInput())


    start_date = forms.CharField(label='Start Date', required=False, widget=forms.DateInput(attrs={'class': 'datepicker start_date'}))
    end_date = forms.CharField(label='End Date', required=False, widget=forms.DateInput(attrs={'class': 'datepicker end_date'}))

    timeframe = forms.ModelChoiceField(queryset=PlotTimeRange.objects.order_by('-days'), label='Time Frame', empty_label=None, widget=forms.Select(attrs={'class': 'time-window'}),)

    plot_relative_to = forms.ChoiceField(choices=[(e.name, e.value ) for e in PlotRelativeToChoices], label='Relative To', required=True, widget=forms.Select(attrs={'class': 'relative-to'}),)

    yaxis_orientation = forms.CharField(label='Y-Axis Orientation', widget=forms.Select(
        choices=[(e.value, e.name.title()) for e in YAxisChoices]))


    _platformList = list(Asset.objects.filter(type_id=AssetTypes.PLATFORM.value).values_list("name", flat=True).order_by("name").distinct())
    default_on_platforms = forms.MultipleChoiceField(choices=zip(_platformList, _platformList), required=False, label='Default on Platform', 
        widget=forms.SelectMultiple(attrs={'class': 'select2_multiple'}))

    def __init__(self, *args, **kwargs):
        super(PlotForm, self).__init__(*args, **kwargs)

        users = User.objects.filter()
        users_list = []

        for user in users:
            if ('owner' in self.initial and user.id == self.initial['owner']) or user.is_active:
                name = format_user_full_name(user)
                users_list.append([user.id, name])

        users_list = sorted(users_list, key=lambda x: x[1])
        users_list.insert(0, ['', 'Please select an owner'])

        self.fields['owner'].choices = tuple(users_list)


    class Meta:
        model = Plot
        fields = ['name', 'is_global']
        labels = {
            "name": "Name",
            "is_global": "Available to Everyone",
        }

        widgets = {
            'is_global': forms.Select(choices=[(e.value, e.name.title()) for e in BoolChoices])
        }