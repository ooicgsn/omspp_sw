from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User, Group
from ..models.account import MobileCarrier

from ..models.account import Profile


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Username', 'class': 'form-control', 'autocomplete': 'off'})
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Password', 'class': 'form-control', 'autocomplete': 'off'})
    )


class UserForm(forms.ModelForm):
    is_new_user = forms.BooleanField(required=False, widget=forms.HiddenInput())
    new_password = forms.CharField(required=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))
    confirm_password = forms.CharField(required=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'autocomplete': 'off'}))
    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all(),
        widget=forms.CheckboxSelectMultiple(),
        required=True
    )

    def __init__(self, is_admin, *args, **kwargs) -> object:
        """

        :rtype:
        """
        super(UserForm, self).__init__(*args, **kwargs)
        if self.instance.id:
            self.fields['groups'].initial = (
                Group.objects.filter(user=self.instance).values_list('id', flat=True)
            )
        else:
            self.fields['is_new_user'].initial = True
        if not is_admin:
            self.fields['groups'].required = False

    def clean(self):
        cleaned_data = super().clean()
        for user_email in User.objects.filter(email=cleaned_data['email']):
            if user_email.id is not self.instance.id:
                self._errors['email'] = self.error_class(
                    ['A user with that email already exists.'])
        return cleaned_data

    def clean_first_name(self):
        if not self.cleaned_data['first_name']:
            raise forms.ValidationError("First name is required")

        return self.cleaned_data['first_name']

    def clean_last_name(self):
        if not self.cleaned_data['last_name']:
            raise forms.ValidationError("Last name is required")

        return self.cleaned_data['last_name']

    def clean_new_password(self):
        if self.cleaned_data['is_new_user'] and self.cleaned_data['new_password'] == '':
            raise forms.ValidationError("A password is required for all new users")

        return self.cleaned_data['new_password']

    def clean_confirm_password(self):
        if 'new_password' not in self.cleaned_data or self.cleaned_data['new_password'] == '':
            return ''

        if self.cleaned_data['new_password'] != self.cleaned_data['confirm_password']:
            raise forms.ValidationError("Passwords do not match")

    # TODO(mchagnon): jquery validation is less restrictive than Django
    # e.g. example@site will pass the former but not the latter
    def clean_email(self):
        if not self.cleaned_data['email']:
            raise forms.ValidationError("Email is required")

        return self.cleaned_data['email']

    def clean_groups(self):
        if self.fields['groups'].required == True:
            if not self.cleaned_data['groups']:
                raise forms.ValidationError("A group is required")

        return self.cleaned_data['groups']

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
        }


class ProfileForm(forms.ModelForm):
    def __init__(self, is_admin, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)

        if not is_admin:
            self.fields['redmine_api_key'].required = False
        carriers = MobileCarrier.objects.all()

        carrier_list = []
        carrier_list.append(['', 'Please select a carrier'])
        for carrier in carriers:
            carrier_list.append([carrier.id, carrier.name])
        self.fields['sms_carrier'].choices = tuple(carrier_list)



    sms_carrier = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        required=False
    )

    class Meta:
        model = Profile
        fields = ('phone', 'position', 'affiliation', 'redmine_api_key', 'sms_number')
        widgets = {
            'phone': forms.TextInput(attrs={'class':'form-control'}),
            'position': forms.TextInput(attrs={'class': 'form-control'}),
            'affiliation': forms.TextInput(attrs={'class': 'form-control'}),
            'redmine_api_key': forms.TextInput(attrs={'class': 'form-control'}),
            'sms_number': forms.TextInput(attrs={'class': 'form-control'})
        }
