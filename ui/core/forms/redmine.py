from django.shortcuts import get_object_or_404
from django import forms

from ..models.redmine import Project, Organization, Platform, Location, Condition, Tracker, Priority
from ..redmine import Issue, Redmine


class RedmineIssueForm(forms.Form):
    project = forms.ModelChoiceField(queryset=Project.objects.all(), required=True, widget=forms.RadioSelect(), empty_label=None)
    tracker = forms.IntegerField(required=False, widget=forms.RadioSelect())
    subject = forms.CharField(max_length=100, required=True)
    description = forms.CharField(widget=forms.Textarea(), required=True)
    serial_number = forms.CharField(label="Serial #", max_length=100, required=False)
    item = forms.CharField(max_length=100, required=False)
    item_type = forms.CharField(label="Item type/ID", max_length=100, required=False)
    oem = forms.CharField(label="OEM", max_length=100, required=False)
    reference_designator = forms.CharField(label="Reference Designator", max_length=100, required=False)
    disposition = forms.CharField(max_length=255, required=False,
                                  widget=forms.Textarea(attrs={'rows':3}))
    report_date = forms.DateField(label="Date of Report", required=False,
                                  widget=forms.DateInput(attrs={'class': 'datepicker'}))
    issue_date = forms.DateField(label="Date of Problem", required=False,
                                  widget=forms.DateInput(attrs={'class': 'datepicker'}))
    change_date = forms.DateField(label="Date of Change", required=False,
                                  widget=forms.DateInput(attrs={'class': 'datepicker'}))
    completion_date = forms.DateField(label="Est. Date to Close", required=False,
                                  widget=forms.DateInput(attrs={'class': 'datepicker'}))
    location = forms.ModelChoiceField(queryset=Location.objects.all(), required=False)
    platform = forms.ModelChoiceField(queryset=Platform.objects.all(), required=False)
    organization = forms.ModelChoiceField(queryset=Organization.objects.all(), required=False)
    condition = forms.ModelChoiceField(queryset=Condition.objects.all(), required=False)
    priority = forms.ModelChoiceField(queryset=Priority.objects.all(), required=False, empty_label=None)

    def __init__(self, *args, **kwargs):
        super(RedmineIssueForm, self).__init__(*args, **kwargs)

        self.fields['priority'].initial = Priority.objects.get(is_default=True)

    def _cleaned_data_name(self, field):
        if not self.cleaned_data[field]:
            return None

        return self.cleaned_data[field].name

    def _cleaned_data_external_id(self, field):
        if not self.cleaned_data[field]:
            return None

        return self.cleaned_data[field].external_id

    def save(self, user, alert, commit=True):
        tracker = get_object_or_404(Tracker, pk=self.cleaned_data['tracker'])

        # Set up the Redmine issue based on input
        issue = Issue()
        issue.project_id = self.cleaned_data['project'].external_id
        issue.tracker_id = tracker.external_id
        issue.subject = self.cleaned_data['subject']
        issue.description = self.cleaned_data['description']
        issue.priority_id = self._cleaned_data_external_id('priority')
        issue.custom_fields['serial_number'] = self.cleaned_data['serial_number']
        issue.custom_fields['item'] = self.cleaned_data['item']
        issue.custom_fields['disposition'] = self.cleaned_data['disposition']
        issue.custom_fields['item_type'] = self.cleaned_data['item_type']
        issue.custom_fields['oem'] = self.cleaned_data['oem']
        issue.custom_fields['reference_designator'] = self.cleaned_data['reference_designator']
        issue.custom_fields['report_date'] = str(self.cleaned_data['report_date'])
        issue.custom_fields['issue_date'] = str(self.cleaned_data['issue_date'])
        issue.custom_fields['change_date'] = str(self.cleaned_data['change_date'])
        issue.custom_fields['completion_date'] = str(self.cleaned_data['completion_date'])
        issue.custom_fields['location'] = self._cleaned_data_name('location')
        issue.custom_fields['platform'] = self._cleaned_data_name('platform')
        issue.custom_fields['organization'] = self._cleaned_data_name('organization')
        issue.custom_fields['condition'] = self._cleaned_data_name('condition')
        issue.custom_fields['disposition'] = self.cleaned_data['disposition']

        # Call Redmine
        redmine = Redmine(user.profile.redmine_api_key)
        issue_id, errors = redmine.submit_issue(issue)

        # Make sure the Redmine call was successful
        if not issue_id:
            [self.add_error(None, error) for error in errors]
            return False

        # Update the Redmine (issue) ID on the alert
        if alert:
            alert.redmine_id = issue_id
            alert.save()

        return issue_id
