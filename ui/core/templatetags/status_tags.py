from collections import OrderedDict
from functools import reduce
import json

from django import  template

from ..models.asset import  Asset, AssetTypes, AssetTypes

register = template.Library()

@register.inclusion_tag(name='plot_tooltip', filename='asset/templatetags/_plot_tooltip.html')
def plot_tooltip(asset_id, alerts):
    return {
        'template': '_plot_tooltip.html',
        'asset_id': asset_id,
        'alerts_dict': alerts
    }

@register.inclusion_tag(name='mini_status_grid', filename='asset/templatetags/_mini_status_grid.html')
def mini_status_grid(asset, deployment):
    platform = asset.platform or asset
    first_children = list(Asset.objects.filter(parent=platform))

    first_children_alerts = []
    #TODO(dmaher) Move this to a stored procedure
    for child in first_children:
        #Convery the response of get_asset_alerts from bytes to a list
        alerts_bytes = Asset().get_asset_alerts(child.id, True)
        alerts_string = alerts_bytes.content.decode('utf-8')
        alerts_list = json.loads(alerts_string)['data']
        #Only count alerts that are Open
        child.alert_count = len([alert for alert in alerts_list if alert[3] == 'Open'])
        first_children_alerts.append(child)

    return {
        'template': 'asset/templatetags/_mini_status_grid.html',
        'mini_assets': first_children,
        'deployment': deployment,
    }

@register.inclusion_tag(name='status_grid', filename='core/templatetags/extender.html')
def status_grid(asset_id, platform_id, deployment_code, callback=None, show_platform=False):
    # TODO(mchagnon): grids are currently based on group, but this may have to change to be based on other factors
    #
    # if asset.group_id == AssetGroups.ENDURANCE.value or asset.group_id == AssetGroups.PIONEER.value:
    #     return _get_endurance_status_grid(asset, deployment)

    # TODO(mchagnon): create templates for the other groups; default to Endurance so we at least have something
    return _get_status_grid_default(asset_id, platform_id, deployment_code, callback, show_platform)


# TODO(mchagnon):
#   This is a rather complex process to transform the data coming from the database into the structure
#   needed for the UI. We might want to consider caching this object somewhere instead of building each call
# TODO(mchagnon): There are still issues on some platforms, specifically gi01sumo
def _get_status_grid_default(asset_id, platform_id, deployment_code, callback=None, show_platform=False):
    assets = Asset().get_assets_for_status_grid(platform_id)
    platform = Asset.objects.get(pk=platform_id)

    # Start with top-level assets that are directly under the mooring/platform
    rows = [(list(filter(lambda asset: asset['parent_id'] == platform_id, assets))),]
    last_row = rows[-1]

    # Extra assets are ones that don't quite fit the normal structure; they'll be added to the bottom of the grid
    extra_assets = dict()

    # If any assets are removed, we show them at bottom (since their old port could now be reused)
    deleted_assets = dict()

    # Process everything under the top level assets, down to the DCLs (the items that have ports)
    while (True):
        row = []

        for item in last_row:
            # Only pull items that are not marked as instruments; anything else doesn't fit into the top
            # layout of the grid, and needs to be added as extra items at the end
            row.extend(filter(None, [
                asset for asset in assets
                if asset['parent_id'] == item['id'] and asset['port'] == 0 and asset['type_id'] != AssetTypes.INSTRUMENT.value
            ]))

        if len(row) == 0:
            break

        rows.append(row)
        last_row = rows[-1]

    # Number of "header" rows in the grid before the ports start being listed. Need to offset the row counter
    #   on the default text used to fill in missing ports
    port_row_offset = -len(rows)

    # Find any DCLs/STCs that are not in the last row, hide them, and copy them to the last row so they show up
    #   inline with the rest of the DCLs/STCs. To find the correct position, the code needs to loop through the first
    #   row (MFN/BUOY/NSIF) and calculate the number of DCLs each has before it gets to the one in question
    for i in range(len(rows) - 1):
        for j in range(len(rows[i])):
            if rows[i][j]['type_id'] in [AssetTypes.DCL.value, AssetTypes.STC.value]:
                dup = rows[i][j].copy()
                _apply_asset_formatting(dup, platform, deployment_code)

                offset = 0
                for parent in rows[0]:
                    if parent["id"] == dup["parent_id"]:
                        break
                    elif parent["type_id"] in [AssetTypes.BUOY.value, AssetTypes.NSIF, AssetTypes.MFN]:
                        offset += parent["num_dcls"]

                last_row.insert(offset, dup)

                rows[i][j]['hidden'] = True


    # The last row so far will be the last row that is not broken out into port/instrument pairs. Now go through each
    #   port number and find any attached instruments
    for port_num in range(1, Asset.NUM_PORTS_PER_DCL + 1):
        row = []
        for item in last_row:
            port = next(
                (asset for asset in assets \
                    if asset['parent_id'] == item['id'] \
                    and asset['port'] == port_num \
                    and asset['type_id'] == AssetTypes.PORT.value
                 ), None
            )

            # This assumes that there is only going to be one instrument attached to a port
            instrument = None

            if port:
                instruments = [asset for asset in assets if asset['parent_id'] == port['id']]
                for port_instrument in instruments:
                    if port_instrument['is_deleted']:
                        deleted_assets[port_instrument['id']] = port_instrument
                        continue

                    instrument = port_instrument

            row.extend([port, instrument])

        rows.append(row)


    # Build simple hierarchy from remaining assets
    all_ids = [asset['id'] for row in rows for asset in filter(None, row) ]
    for asset in assets:
        if asset['id'] in all_ids:
            continue

        if asset['is_deleted']:
            if asset['id'] in deleted_assets:
                continue

            deleted_assets[asset['id']] = asset
            continue

        parent_id = asset['parent_id']
        if not parent_id in extra_assets:
            parent = next((asset for asset in assets if asset['id'] == parent_id), None)
            if not parent:
                continue

            extra_assets[parent_id] = next(asset for asset in assets if asset['id'] == parent_id)
            extra_assets[parent_id]['children'] = []

        extra_assets[parent_id]['children'].append(asset)

    extra_assets = OrderedDict(sorted(extra_assets.items()))

    # Run throw each asset and determine the css class (based on status and type, and how many columns it spans. Anything
    # under a dcl is 1 column, DCLs are always 2 (one for the port, one for the instruments), and the rest are twice the
    # number of DCLs
    assets = [_apply_asset_formatting(asset, platform, deployment_code) for asset in assets]

    # Find the maximum amount of columns in any row, which will serve as the colspan for the platform line
    platform_colspan = reduce(lambda row1,row2: max(row1, row2), [len(row) for row in rows])
    platform_css_class = Asset.get_status_grid_class(platform.status_id, AssetTypes.PLATFORM)

    return {
        'show_platform': show_platform,
        'platform': platform,
        'platform_colspan': platform_colspan,
        'platform_css_class': platform_css_class,
        'template': 'asset/templatetags/_status_grid_default.html',
        'rows': rows,
        'deployment_code': deployment_code,
        'extra_assets': extra_assets,
        'port_row_offset':port_row_offset,
        'callback': callback,
        'deleted_assets': deleted_assets,
    }

# TODO(mchagnon): Handle port naming and parent in extr assets for long_name
def _apply_asset_formatting(asset, platform, deployment_code):
    is_port = asset['type_id'] == AssetTypes.PORT.value
    is_dcl = asset['type_id'] in [AssetTypes.DCL.value, AssetTypes.STC.value]

    asset['item_css_class'] = Asset.get_status_grid_class(asset['status_id'], asset['type_id'])
    asset['column_css_class'] = 'status-grid-port' if is_port else ''
    asset['text'] = asset['port'] if is_port else asset['code']
    asset['long_name'] = '{} on {} ({})'.format(asset['text'], platform.name, deployment_code)

    if 'hidden' in asset:
        asset['item_css_class'] = 'hidden'

    if is_dcl:
        asset['col_span'] = 2
    elif int(asset['num_dcls']) > 0:
        asset['col_span'] = asset['num_dcls'] * 2
