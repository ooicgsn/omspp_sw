// Default formats for DataTables
var DEFAULT_DATE_TIME_FORMAT = 'M/D/YY h:mma UTC';
var DEFAULT_DATE_FORMAT = 'M/D/YY';

var DOCUMENT_SCROLLTOP = 0;

// Color-blind friendly colors for plotting
var cbColors = ["#56b4e9","#000000","#e69f00","#009e73"];

var CUSTOM_TIME_FRAME='custom_time_frame';


// Helper function to generate custom csv output depending on plot class
function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    //Generate a file name
    var fileName = "Chart_Data_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");

    var csv = CSV;
    blob = new Blob([csv], { type: 'text/csv' });
    var csvUrl = window.URL.createObjectURL(blob);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");

    link.href = csvUrl;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

//define these globally to dynamically update
var varID, plotTitleList=[], plotObject

// Menu items for standard (global) chart hamburger menu
var standardContextMenuItems = [
    {
        textKey: 'printChart',
        text: 'Print',
        onclick: function() {
            this.print();
        }
    },
    {
        separator: true
    },
    {
        textKey: 'disableToolTip',
        text: 'Disable Tooltip',
        onclick: function(e) {
            // use e.target to toggle the text
            this.tooltip.options.enabled = !this.tooltip.options.enabled;
            if (this.tooltip.options.enabled) {
                $(e.target).text('Disable Tooltip');
            } else {
                $(e.target).text('Enable Tooltip');
            }
        }
    },
    {
        separator: true
    },
    {
        textKey: 'downloadPNG',
        text: 'Download PNG',
        onclick: function() {
            this.exportChart();
        }
    },
    {
        textKey: 'downloadCSV',
        text: 'Download CSV',
        onclick: function() {
            var seriesData = this.series;

            var x_extremes = this.xAxis[0].getExtremes();
            // could be multiple y-axis

            var y_extremes, y_extremes_arr = [];
            for (var i=0; i<seriesData.length; i++) {
                y_extremes = this.yAxis[i].getExtremes();
                y_extremes_arr.push(y_extremes)
            }

            var jsonOutput = generateJSONfromChart(seriesData,x_extremes,y_extremes_arr);
            var outputTitle = this.title.textStr;

            JSONToCSVConvertor(jsonOutput, outputTitle, true)
            //this.downloadCSV(); // dont need this anymore
        }
    }
];

// Helper function to find unique array items and sort them
function sort_unique(arr) {
    if (arr.length === 0) return arr;
        arr = arr.sort(function (a, b) { return a*1 - b*1; });
        var ret = [arr[0]];
        for (var i = 1; i < arr.length; i++) { //Start loop at 1: arr[0] can never be a duplicate
            if (arr[i-1] !== arr[i]) {
                ret.push(arr[i]);
            }
        }
    return ret;
}

// Helper function to determine if two arrays are equal
function arraysEqual(arr1, arr2) {
    if(arr1.length !== arr2.length)
        return false;
    for(var i = arr1.length; i--;) {
        if(arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

// Helper function to transform data from arrays to an object form which is much more
// performant for lookups
function createDataObj(series_data, plot_class) {
    var fullSeriesObj={};
    for (var i=0; i<series_data.length; i++){
        var subSeriesObj = {}, elem_obj, time;
        for (var j=0; j < series_data[i].data.length; j++){
            if (plot_class === 'time_series') {
                time = series_data[i].data[j].options.x;
            } else {
                time = series_data[i].data[j].options.time;
            }
            subSeriesObj[time] = series_data[i].data[j].options;
        }
        fullSeriesObj[i]=subSeriesObj;
    }
    return fullSeriesObj
}


function generateJSONfromChart(series_data,x_extremes,y_extremes_arr) {
    // compile all times from all series into a single array and uniqueify it + sort in ascending order
    // then i will loop through and check each series for a specific time... only necessary
    var all_times = [], filtered_times = [];
    for (var ind=0; ind<series_data.length; ind++) {
        var checkType = series_data[ind].data[0].options;
        var plot_class = Object.keys(checkType).indexOf('time') === -1 ? 'time_series': 'non-timeseries';
        // if its not a timeseries then use the x-value for time
        for (var inner_ind=0; inner_ind<series_data[ind].data.length; inner_ind++) {
            if (plot_class === 'time_series') {
                var obs_time = series_data[ind].data[inner_ind].options.x;
                var x_data_point = obs_time;
                var y_data_point = series_data[ind].data[inner_ind].options.y;
            } else {
                var obs_time = series_data[ind].data[inner_ind].options.time;
                var x_data_point = series_data[ind].data[inner_ind].options.x;
                var y_data_point = series_data[ind].data[inner_ind].options.y;
            }
            // only use the points which are visible on the chart
            if ((x_data_point >= x_extremes.min && x_data_point <= x_extremes.max && y_data_point >= y_extremes_arr[ind].min &&
                                                                                     y_data_point <= y_extremes_arr[ind].max)) {
                filtered_times.push(obs_time)
            }
            all_times.push(obs_time)
        }
    }

    // var unique_sorted_times = sort_unique(all_times);
    var unique_sorted_times = sort_unique(filtered_times);

    // if unique_sorted_times is equal to all_times then we don't want to use indexOf
    var eq_arr = arraysEqual(unique_sorted_times, all_times)

    // if time isn't in the first series then it won't be in any because of the way this is designed...
    // timeseries plots are made up of series without a 'time paramater'
    var options = series_data[0].data[0].options;
    var plot_class = Object.keys(options).indexOf('time') === -1 ? 'time_series': 'non-timeseries';

    // create an object series with the key being the time if arrays aren't equal
    if (!eq_arr) {
        var seriesObj = createDataObj(series_data,plot_class);
    }

    // for each time sort each series for it and if its not there then build an empty row
    var concat_data_array = [];
    for (var t=0; t < unique_sorted_times.length; t++) {

        var data_row={}, ind_data_obj, filtered_time_indx, series_times, series_name, unit_name
        var ind_data_obj;
        for (var i=0; i<series_data.length; i++ ) {

            if (plot_class === 'time_series') {
                unit_name = series_data[i].userOptions.description;
                if (eq_arr) {
                    ind_data_obj = series_data[i].data[t];
                } else {
                    ind_data_obj= seriesObj[i][unique_sorted_times[t]];
                }

                series_name = series_data[i].name + ' (' + unit_name + ')';
                data_row['Time'] = new Date(unique_sorted_times[t]).toISOString();

                if (ind_data_obj !== undefined) {
                    data_row[series_name] = ind_data_obj.y;
                } else {
                    data_row[series_name] = '';
                }
            } else {
                if (eq_arr) {
                    ind_data_obj = series_data[i].data[t].options;
                } else {
                    ind_data_obj= seriesObj[i][unique_sorted_times[t]];
                }

                data_row['Time'] = new Date(unique_sorted_times[t]).toISOString();
                data_row[ind_data_obj.x_title + ' ( ' + ind_data_obj.x_unit_name + ')'] = ind_data_obj.x;
                data_row[ind_data_obj.y_title + ' (' + ind_data_obj.y_unit_name + ')'] = ind_data_obj.y;

            }
        }
        // push the completed row into the full array
        concat_data_array.push(data_row)
    }

    // JSONify the array
    var output = JSON.stringify(concat_data_array);
    return output
}

// menu items for custom (non-global) chart hamburger menu
function buildCustomContextMenuItems(standardContextMenuItems,edit_url,from_where) {

    var customContextMenuItems = standardContextMenuItems.concat({
        textKey: 'updatePlot',
        text: 'Edit Plot',
        onclick: function() {
            var plotid = $(this.renderTo).attr('id').replace('plot','');
            var editPage = edit_url.replace('plotid', plotid) + '?' + 'from=' + from_where
            window.location.href=editPage;
        }
    },
    {
        textKey: 'deletePlot',
        text: 'Delete',
        onclick: function() {

            if(confirm("Are you sure you want to delete this plot?")) {
                var plotid = $(this.renderTo).attr('id').replace('plot','');

                if (Number(plotid) != NaN) {
                    permanentlyDelete(Number(plotid));
                }
            }
        }
    }
    );

    return customContextMenuItems
}

// default exporting object
var default_exporting = {
    sourceWidth: 600,
    sourceHeight: 400,
    buttons: {
        contextButton: {
            enabled: true,
            _title: 'ExportImageOrData',
            menuItems: standardContextMenuItems
        }
    },
    csv: {
        columnHeaderFormatter: function (series, key) {
        },
    },
    chartOptions: { // specific options for the exported image
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false
                }
            }
        }
    },
    fallbackToExportServer: false,
};

// default exporting object
var no_data_exporting = {
    sourceWidth: 600,
    sourceHeight: 400,
    buttons: {
        contextButton: {
            enabled: true,
            _title: 'ExportImageOrData',
            menuItems: []
        }
    },
    csv: {
        columnHeaderFormatter: function (series, key) {
        },
    },
    chartOptions: { // specific options for the exported image
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false
                }
            }
        }
    },
    fallbackToExportServer: false,
};

// default navigation object
var default_navigation = {
    buttonOptions: {
        enabled: true,
        theme: {
            'stroke-width': 1,
            stroke: 'silver',
            r: 5,
        }
    }
};

// General helper methods for dialog boxes
function init_dialog_box(selector, height) {
    $(selector).dialog({
        modal: true,
        autoOpen: false,
        draggable: false,
        resizable: false,
        height: typeof height !== 'undefined' ? height : 200,
        title: '',
        position: {
            my: "center",
            at: "top+30%",
            of: window
        },
    });
}
function init_jquery_select2() {
    if( typeof ($().select2) === 'undefined'){  return; }
    $(".select2_multiple").select2({
        placeholder: "Select a Value...",
        allowClear: true
    });

    
};

function init_asset_picker_dialog_box(selector) {
    $(selector).dialog({
        modal: true,
        autoOpen: false,
        draggable: false,
        resizable: false,
        width: 730,
        title: 'Select an Asset',
        position: {
            my: "center",
            at: "top+20%",
            of: window
        },
    });
}

function open_dialog_box(e, selector, url) {
   e.preventDefault();
    $(selector).load(url, function() {
        $(selector).dialog('open');
    });

    return false;
}

// Helper methods for specific dialog boxes
function init_trigger_dialog_box(selector) {
    init_dialog_box(selector, 300);
}

// Helper method for formatting DataTables columns with the default date/time format
function dt_format_date_time(data, type, row) {
    if (type !== "display")
        return data;

    var date_value = moment.utc(data);

    return date_value.isValid() ? date_value.format(DEFAULT_DATE_TIME_FORMAT) : "";
}

// Helper method for formatting DataTables columns with the default date format
function dt_format_date(data, type, row) {
    return moment.utc(data).format(DEFAULT_DATE_FORMAT);
}

// Helper method for formatting DataTables asset column with the correct link
function asset_link_creator(data, type, row) {
    var linkStr= '<a href="/asset/overview/' + row['asset_id'] + '/' + row['deployment_code'] + '/">'+ data + '</a>';
    return linkStr;
}

// Helper method for formatting DataTables platform column with the correct link
function platform_link_creator(data, type, row) {
    var linkStr= '<a href="/asset/overview/' + row['platform_id'] + '/' + row['deployment_code'] + '/">'+ data + '</a>';
    return linkStr;
}

// Helper to add stretch-pagination class to pager on grids to allow it to span more than the col-md-7 class. This
//   is needed on grids that are only half the width of the screen, which cause the paging to break to a second line
function stretch_pagination(pager) {
    $("#" + pager.attr('id') + "_paginate").parent().addClass('pagination-stretch');
}

// Helper method to get a cookie value
// From: https://docs.djangoproject.com/en/1.10/ref/csrf/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// CSRF helper methods so the token doesn't have to be passed manually to all AJAX requests
// From: https://docs.djangoproject.com/en/1.10/ref/csrf/
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


// Helper method to add a success notification (using PNotify)
function add_success_message(msg) {
    $("#notification-area").show(function() {
        new PNotify({
            title: 'Success!',
            text: msg,
            type: 'success',
            icon: 'fa fa-check',
            delay: 5000,
        });
    });
}

// Helper method to add a error notification (using PNotify)
function add_failed_message(msg) {
    $("#notification-area").show(function() {
        new PNotify({
            title: 'Failed!',
            text: msg,
            type: 'error',
            icon: 'fa fa-exclamation-circle',
            delay: 5000,
        });
    });
}


// Helper method to create a fake time array to be used when we don't have any data
function spacedTimeArray(start_date, end_date, time_delta_hours) {
    var start_hour = start_date.getHours();

    if (start_hour % 2 === 0) {
        var start_date_ms = start_date.getTime() - (1000 * 60 *60 * time_delta_hours)
    } else {
        var start_date_ms = start_date.getTime() - (1000 * 60 *60 * (time_delta_hours-1))
    }

    var end_date_ms = end_date.getTime();

    var date_array=[];
    var timeArrElem = start_date_ms;
    while (timeArrElem < end_date_ms) {
        timeArrElem = timeArrElem + (60*60*1000*time_delta_hours)
        date_array.push(timeArrElem)
    }
    return date_array;
}

// Helper method to get query parameter from url
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// Helper method to determine if data is non-numeric and therefor not plottable
function stringCheck(n) {
    return n === 'string';
}

// Helper method to determine the position that the yTitle and axis should be plotted on
function determineOpposite(unitList) {
    var uniqueUnits = unitList.filter(function (item, i, ar) {
        if (item === 'n/a') {
            return true
        }
        return ar.indexOf(item) === i;
    });

    if (uniqueUnits.length % 2 === 0 && uniqueUnits.length !== 0) {
        var opposite = true;
    } else {
        var opposite = false;
    }

    return opposite;
}

// Universal plotting method for a plot with no data
function no_data_plot(plotObject) {
    var chart = Highcharts.chart(plotObject.container_id, {
        chart: {
          type: 'scatter',
          zoomType: 'xy',
          ignoreHiddenSeries : false,
          events: {
            load: function(chart) {
                $('#' + plotObject.container_id).find('.highcharts-container').append('<div id="no-data-plot"><h1>No Data</h1></div>')
                // append the error text
                appendErrorToContainer(plotObject.container_id, plotObject.error_html)
            }
          }
        },
        title: plotObject.title,
        xAxis: plotObject.xData,
        yAxis: plotObject.yData,
        lang: LANG,
        navigation: default_navigation,
        legend: {
            enabled: false
        },
        series: plotObject.seriesData,
        exporting: plotObject.exporting,
        credits: {
            enabled: false
        }
    });
}

// Universal plotting method. Provide a plotObject as the argument (see overview.html for example on how this is built)
function plot(plotObject) {
    chart = Highcharts.chart(plotObject.container_id, {
        chart: {
          type: plotObject.type,
          zoomType: 'xy',
          events: {
            beforePrint:function() {
                DOCUMENT_SCROLLTOP = $(document).scrollTop();

                // perform some resizing to make print output consistent
                this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);

                this.exportSVGElements[0].box.hide();
                this.exportSVGElements[1].hide();
                if (this.exportSVGElements.length > 2) {
                    this.exportSVGElements[2].hide();
                }
            },
            afterPrint:function() {
                // set scrollTop to previous value
                $(document).scrollTop(DOCUMENT_SCROLLTOP);

                // reset the scroll to 0
                DOCUMENT_SCROLLTOP = 0;

                // resize back to original to ensure proper fit on the page
                this.setSize.apply(this, this.resetParams);
                this.hasUserSize = this.oldhasUserSize;

                this.exportSVGElements[0].box.show();
                this.exportSVGElements[1].show();
                if (this.exportSVGElements.length > 2) {
                    this.exportSVGElements[2].show();
                }
            }
          }
        },
        title: plotObject.title,
        subtitle: plotObject.subtitle,
        xAxis: plotObject.xData,
        yAxis: plotObject.yData,
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> {point.x:%Y-%m-%d %H:%M:%S} UTC: <b>{point.y:f} {point.series.yAxis.axisTitle.textStr}</b><br/>',
            shared: false,
        },
        lang: LANG,
        legend: {
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: plotObject.seriesData,
        navigation: default_navigation,
        exporting: plotObject.exporting,
        plotOptions: {
            scatter: {
                marker: {
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        credits: {
            enabled: false
        }
    });
}

// Universal plotting method. Provide a plotObject as the argument (see overview.html for example on how this is built)

function comparison_plot(plotObject) {
    chart = Highcharts.chart(plotObject.container_id, {
        chart: {
          type: 'scatter',
          zoomType: 'xy',
          events: {
            beforePrint:function() {
                DOCUMENT_SCROLLTOP = $(document).scrollTop();

                // perform some resizing to make print output consistent
                this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(600, 400, false);

                this.exportSVGElements[0].box.hide();
                this.exportSVGElements[1].hide();
                if (this.exportSVGElements.length > 2) {
                    this.exportSVGElements[2].hide();
                }
            },
            afterPrint:function() {
                // set scrollTop to previous value
                $(document).scrollTop(DOCUMENT_SCROLLTOP);

                // reset the scroll to 0
                DOCUMENT_SCROLLTOP = 0;

                // resize back to original to ensure proper fit on the page
                this.setSize.apply(this, this.resetParams);
                this.hasUserSize = this.oldhasUserSize;

                this.exportSVGElements[0].box.show();
                this.exportSVGElements[1].show();
                if (this.exportSVGElements.length > 2) {
                    this.exportSVGElements[2].show();
                }
            }
          }
        },
        title: plotObject.title,
        subtitle: plotObject.subtitle,
        xAxis: plotObject.xData,
        yAxis: plotObject.yData,
        lang: LANG,
        legend: {
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: [plotObject.seriesData],
        navigation: default_navigation,
        exporting: plotObject.exporting,
        plotOptions: {
            series: {
              turboThreshold: 0
            },
            scatter: {
                marker: {
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.time:%Y-%m-%d %H:%M:%S} UTC<br><span style="color:{point.color}">\u25CF</span> <b>{point.x:f} {point.x_unit_name}</b>, <b>{point.y:f} {point.y_unit_name}</b><br/>',
                    shared: false,
                },
            }
        },
        credits: {
            enabled: false
        }
    });
}


function formatPlotData(data, x_axis_index, y_axis_index, x_title, x_unit_name, y_title, y_unit_name, colorize) {
    var final_data_struct=[];

    for (var i = 0; i< data.length; i++) {
        var time = data[i][0];
        var ydata = data[i][y_axis_index];
        var xdata = data[i][x_axis_index];

        if (colorize){
            var data_color = cbColors[0]; // this is a place holder.. use convertValToColor() // TODO (christensen): build this function
        } else {
            var data_color = cbColors[0];
        }

        var data_obj = {'x': xdata, 'y': ydata, 'x_unit_name': x_unit_name,
                        'x_title': x_title, 'y_title': y_title,
                        'y_unit_name': y_unit_name, 'time': time, 'color': data_color};

        final_data_struct.push(data_obj);
    }
    return final_data_struct
}

function checkData(data){

    if (data.length === 0 ) {
        return false
    } else {
        var number_of_columns = data[0].length
        var true_array=[];

        for (var i=1; i<number_of_columns; i++) {
            dataCheck=data.map(function(x) { return typeof(x[i])})
            numericData = !dataCheck.every(stringCheck)
            true_array.push(numericData)
        }

        var data_good = true_array.every(function(x){return x === true});
        if (!data_good) {
            return false
        }
    }
    return true
}

function buildDataErrorText(error_obj) {
    var error_html ='';
    var full_hidden_content='';
    var standard_content = '<strong>No data available&nbsp;</strong>'

    var counter=0
    for (key in error_obj) {
        var content = error_obj[key];
        if (counter>0) {
            full_hidden_content += '<hr style="margin: 2px">' + content;

        } else {
            full_hidden_content += content;
        }
        counter++;
    }

    error_html += '<div class="plot-error" style="text-align: center"><p class="plot-error-text-standard">' + standard_content +
            '<a class="details-button" href="#" onclick="toggleErrorText(event)">(Details)</a>' +
            '<div class="hidden-error-text plot-error-text-extra"> ' + full_hidden_content + '</div>' +
            '</p></div>';

    return error_html
}

function toggleErrorText(e) {
    e.preventDefault();

    var targetEl = $(e.target);
    var targetElText = $(e.target).text().toLowerCase().replace(' ','');

    var nearest_chart_container =$(targetEl).closest('.plot-error').parent();
    var nearest_chart = $(nearest_chart_container).highcharts();

    if (targetElText === '(details)') {
        targetEl.text('(Hide Details)');
        targetEl.closest('div').find('div').removeClass('hidden-error-text');
    } else {
        targetEl.text('(Details)');
        targetEl.closest('div').find('div').addClass('hidden-error-text');
        $(nearest_chart_container).css({'overflow-y':'hidden','overflow-x':'hidden'})
    }
}

function dataStatusInfo(resp) {
    // Initially loop through the response to create an object identifying *useable* data
    // *(data are numeric and the length of the data array > 0)*

    var currentResp, numericData, varID, data_info = {};
    var varID=Object.keys(resp);

    for (var i=0; i<varID.length; i++) {
        currentResp=resp[varID[i]];

        // only use variables with numeric data
        numericData = checkData(currentResp.data)

        // determine the L3 status and build appropriate string if necessary
        var L3_status = currentResp.custom_variable ? ' (L3)' : '';

        // determine interpolation status and time interval if necessary
        var interp_status = '';
        for (var_id in currentResp.interp_values) {
            if (currentResp.interp_values[var_id] !== null) {
                interp_status = '  *interpolated @ ' + currentResp.interp_values[var_id] + ' hrs';
            } else {
                interp_status = '';
            }
        }

        // build series names here
        if (currentResp.present_asset !== currentResp.parent_asset) {
            var series_name = currentResp.present_asset + ' on ' +
                        currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' + currentResp.title +
                        interp_status + L3_status;
        } else {
             var series_name = currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' +
                         currentResp.title + interp_status + L3_status;
        }

        data_info[currentResp.variable_id]= {};
        data_info[currentResp.variable_id]['name']=series_name;
        if (currentResp['errors'].length === 0 && numericData) {
           data_info[currentResp.variable_id]['status'] = 'good';
           data_info[currentResp.variable_id]['name'] = series_name;
        } else {
            data_info[currentResp.variable_id]['status']='error';
            var error_msg = '';
            if (!numericData && currentResp['errors'].length === 0) {
                error_msg = 'Non-numeric data';
            } else {
                for (var j=0; j<currentResp['errors'].length; j++) {
                    error_msg += currentResp['errors'][j] + ', ';
                }
                error_msg = error_msg.replace(/,\s*$/, "");
            }
            data_info[currentResp.variable_id]['name'] += ' - ' + error_msg;
        }
    }

    // determine if any *useable data are present
    for (key in data_info) {
        if (data_info[key]['status'] === 'good') {
            var data_all_bad = false;
            break;
        } else {
            var data_all_bad = true;
        }
    }

    return {
            data_info: data_info,
            data_all_bad: data_all_bad
            }
}

function appendErrorToContainer(container_id, error_html) {
    $('#'+container_id).parent().append(error_html)
}

// function to create a 'No Data Plot'
function createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, exporting) {

    // build HTML for no data message which will be put into x axis title
    var error_html = buildDataErrorText(error_obj);


    if(start_date==undefined || end_date == undefined){
        start_date = new Date(new Date().setDate(new Date().getDate()+Number(time_window)));
        end_date = new Date();
    }else if( typeof start_date =="string" || typeof end_date == "string"){
        start_date = new Date(start_date);
        end_date = new Date(end_date);
    }

    var fake_times = spacedTimeArray(start_date, end_date, 1)

    // zip the data together and plot
    var zipped_data = [];
    for (var i = 0; i < fake_times.length; i++) {
        rand_data =[fake_times[i], i*100];
        zipped_data.push(rand_data);
    }

    var title = {
        text: plotname
    }

     var xData = {
        gridLineWidth: 1,
        type: 'datetime',
        title: {
            useHTML: true,
            text: error_html,
            style: {
                'white-space': 'normal',
            }
        },
        labels: {
            enabled: false
        }
     };

     var xData = {
        gridLineWidth: 1,
        labels: {
            enabled: false
        },
        minorTickLength: 0,
        tickLength: 0,
     };

     var yData = {
        gridLineWidth: 1,
        title: {
            text: null,
        },
        labels: {
            enabled: false
        }
     };

     var seriesData = [{
        data: zipped_data,
        visible: false
     }];

    var plotObject_ind = {
        container_id: container_id,
        title: title,
        xData: xData,
        yData: yData,
        seriesData: seriesData,
        exporting: exporting,
        error_html: error_html,
        lang: LANG
    }

    // Pass the plot object to the plot function
    no_data_plot(plotObject_ind);
}

$(".datetimepicker").datetimepicker({
	controlType: 'select',
	oneLine: true,
	timeFormat: 'hh:mmtt',
    dateFormat: "mm/dd/y"
});
$(".datepicker").datepicker({
    dateFormat: "yy-mm-dd"
});

function relative_to(obj) {
    $select = $(obj);
    
    $parent = $select.closest('.x_panel')
    if($select.val()==CUSTOM_TIME_FRAME){
        $parent.find('.dates_wrapper').show();
        $parent.find('.time_window_wrapper').hide();
    }else{
        $parent.find('.end_date').val($parent.find('.end_date').attr("value"));
        $parent.find('.start_date').val($parent.find('.start_date').attr("value"));
        $parent.find('.dates_wrapper').hide();
        $parent.find('.time_window_wrapper').show();
    }  
};
function relative_to_event(e) {
    relative_to(e.currentTarget);
}
$(".relative-to").on('change', relative_to_event);
$.each($(".relative-to"), function(i, obj){
    relative_to(obj);
});

// Universal function to plot profile (ie. salinity vs depth) data in a highchart container
// update_plot(asset_id, deployment_id, variable_id, time_window, container_id, plotname, plotglobal, plotType)


var LANG = {
    CustomSave: "save custom chart to database",
    ExportImageOrData: "export chart or data",
    CancelPlot: "cancel",
    UpdatePlot: "update"
}

function update_plot(asset_id, deployment_id, x_var, y_var, time_window, container_id, plotname, plotglobal, plotType, plotClass, y_direction, start_date, end_date, plot_relative_to) {

     
    // remove error msg div if present
    if ($('#' + container_id).parent().find('.plot-error')) {
        $('#' + container_id).parent().find('.plot-error').remove();
    }
    $("#" + container_id).html(
        // $("<img>", { src: '/static/images/loading.gif' }).css('margin','auto')
        // '<i class="fa fa-5x fa-spinner fa-spin" style="margin: auto"></i>'

        // use css spinner
        `<div class="lds-css ng-scope">
            <div class="lds-spinner" style="100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>`
    );

    if(plotClass=="Comparison"){
        $('#' + container_id).closest('.x_panel').find('.plot-type-window').hide();
    } else {
        $('#' + container_id).closest('.x_panel').find('.plot-type-window').show();
    }

    // Customize HighCharts export button
    if (container_id === 'plot0') {
        var exporting = additional_plot_exporting;
        var navigation = default_navigation;

    } else {

        var navigation = default_navigation;

        if ((plotglobal === 'global_False') && container_id === 'plot0') {
            var exporting = default_exporting;
        } else if ((plotglobal === 'global_False') && container_id !== 'plot-preview-container') {
            var exporting = custom_exporting;
        } else {
            var exporting = default_exporting;
        }
    }

    var ajaxCall = $.ajax({
        url: '/asset/plot-data',
        method: 'POST',
        data: {
            'asset_id': asset_id,
            'x_var': String(x_var),
            'y_var': String(y_var),   
            'time_window': time_window,
            'start_date': start_date,
            'end_date': end_date,
            'deployment_id': deployment_id,
            'plot_type': plotClass,
            'plot_relative_to': plot_relative_to
        }
    });

    ajaxCall.done(function(resp,textStatus,jqXHR) {
        ajax_plot_data(resp, x_var, y_var, time_window, container_id, plotname, plotType, plotClass, y_direction, exporting, start_date, end_date);
    });
    ajaxCall.fail(function(jqXHR, textStatus, errorThrown) {
        var error_obj = {'Error': errorThrown + ' (' + jqXHR.status + ')' };

        var replace_exporting = $.extend(true, {}, no_data_exporting)
        replace_exporting.buttons.contextButton.menuItems = replace_exporting.buttons.contextButton.menuItems.concat(get_menu_items_for_erddap_urls(resp))

        createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, replace_exporting)
        return
    });
}

function normalizeUnitName(unitName){
    var unitMap = { //map of different unit names to normals. positon 0 is the canonical.
        degrees: ['degrees_', 'degree_'],
        siemens: ['S/m', 'S m-1']
    }


    for (const key in unitMap) {
        possibleReplacements = unitMap[key]
        for(var i=1; i<possibleReplacements.length; i++){
            if(unitName.indexOf(possibleReplacements[i])!=-1){
                return unitName.replace(possibleReplacements[i], possibleReplacements[0]);
            }
        }        
    };
    return unitName;
}
function get_menu_items_for_erddap_urls(resp){

    varID=Object.keys(resp);
    var errdap_url_context_menu = [];
    for (var i=0; i<varID.length; i++) {
        currentResp=resp[varID[i]];
        errdap_url_context_menu.push(
            {
                text:  "<a class='erddap_url' href='" + currentResp.erddap_url + "' >" + currentResp.subtitle + ' ' + currentResp.title + '  <u>(download)</u></a>',
            }
        );
    }
    return errdap_url_context_menu;

}
function ajax_plot_data(resp, x_var, y_var, time_window, container_id, plotname, plotType, plotClass, y_direction, exporting, start_date, end_date) {


    var replace_exporting = $.extend(true, {}, exporting)
    replace_exporting.buttons.contextButton.menuItems = replace_exporting.buttons.contextButton.menuItems.concat({separator: true}, get_menu_items_for_erddap_urls(resp))

    var no_data_replace_exporting = $.extend(true, {}, no_data_exporting)
    no_data_replace_exporting.buttons.contextButton.menuItems = no_data_replace_exporting.buttons.contextButton.menuItems.concat(get_menu_items_for_erddap_urls(resp))
    if (Object.getOwnPropertyNames(resp).length == 0) {
        // create fake data..
        createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, no_data_replace_exporting)
        return
    }

    varID=Object.keys(resp);
    var subtitle = {text: ''};

    var currentResp, numericData;

    if (container_id === 'plot0') {
        var title = {text: plotname, style: {'visibility': 'hidden'}};
    } else {
        var title = {text: plotname};
    }

    if (y_direction) {
        var y_axis_reversed = true;
    } else {
        var y_axis_reversed = false;
    }

    switch(plotClass) {
        case 'Timeseries':
            var yData = [], xData = [], seriesData = [], full_unit_list= [] ;
            var opposite=false, gridLineWidth = 1, yAxisCounter=0, good_data = [];

            xData = {
                type: 'datetime',
                title: {
                    text: 'Date'
                }
            };

            var data_info = dataStatusInfo(resp);
            // if all data is bad then generate the error messages and call createEmptyPlot
            if (data_info['data_all_bad']) {
                // create a simple object containing error messages
                var error_obj = {};
                for (key in data_info['data_info']) {
                    error_obj[key]  = data_info['data_info'][key]['name'];
                }
                createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, no_data_replace_exporting)
                return
            }

            for (var i=0; i<varID.length; i++) {
                currentResp=resp[varID[i]];
                var unit_name = currentResp.units || 'n/a';
                unit_name = normalizeUnitName(unit_name);
                // check if the same units are already present and adjust the yData object accordingly
                if (unit_name !== 'n/a' && unit_name !== '1' && unit_name !== 'nan' ) {
                    var foundAtIndex = full_unit_list.indexOf(unit_name);
                } else {
                    var foundAtIndex = -1;
                }

                plotColor = cbColors[yAxisCounter];
                full_unit_list.push(unit_name)
                opposite = determineOpposite(full_unit_list)

                if (foundAtIndex > -1) {
                    var useYaxis = seriesData[foundAtIndex].yAxis;
                    var yLabelTitleText = '';
                    var labelsEnabled = false;
                } else {
                    var useYaxis = seriesData.length;
                    var yLabelTitleText = unit_name;
                    var labelsEnabled = true;
                }

                yData.push({
                    reversed: y_axis_reversed,
                    gridLineWidth: gridLineWidth,
                    labels: {
                        enabled: labelsEnabled,
                        style: {
                            color: varID.length < 2 ? "#606060" : plotColor
                        }
                    },
                    title: {
                        text: yLabelTitleText,
                        style: {
                            color: varID.length < 2 ? "#606060" : plotColor
                        }
                    },
                    opposite: opposite
                });

                // update this in special cases...
                // removed by ticket #97 - Fix default range of plots to not max y-axis at 100% for "percentage" variables
                // if (unit_name === 'percent' || unit_name === '%') {
                //     yData[yData.length -1].max = 100;
                //     yData[yData.length -1].endOnTick = false;
                // }

                var series_name = data_info['data_info'][currentResp.variable_id]['name'];

                seriesData.push({
                    yAxis: useYaxis,
                    name: series_name,
                    data: currentResp.data,
                    color: plotColor,
                    description: unit_name,
                });
                // no gridline for subsequent variables
                gridLineWidth = 0;

                // increment the yAxisCounter
                yAxisCounter+=1;
            }

            plotObject = {
                fullUnitList: full_unit_list,
                container_id: container_id,
                title: title,
                subtitle: subtitle,
                type: plotType,
                xData: xData,
                yData: yData,
                seriesData: seriesData,
                navigation: default_navigation,
                exporting: replace_exporting,
                lang: LANG
            }

            // Pass the plot object to the plot function
            plot(plotObject);

            break;
        case 'Comparison':
            currentResp=resp[varID[0]];

            var unit_name = currentResp.units || 'n/a';

            // only use variables with numeric data.. check non-time columns
            numericData = checkData(currentResp.data)

            var colorize = false;
            var formattedTitle='';
            // TODO: remove try catch block once it is determined how to handle variables from dif assets
            try {
                var x_var_id = currentResp.variable_id.indexOf(Number(currentResp.x_var));
                var x_unit_name = currentResp.units.split(',')[x_var_id] || 'n/a';
                var x_is_custom = currentResp.custom_variable[x_var_id] ? ' (L3)' : '';

                var y_var_id = currentResp.variable_id.indexOf(Number(currentResp.y_var));
                var y_unit_name = currentResp.units.split(',')[y_var_id] || 'n/a';
                var y_is_custom = currentResp.custom_variable[y_var_id] ? ' (L3)' : '';

                if (currentResp.data[0].length == 2) {
                    var x_var_index = x_var_id;
                    var y_var_index = y_var_id;
                } else {
                    var x_var_index = x_var_id + 1; //shift by 1 since time comes first
                    var y_var_index = y_var_id + 1; //shift by 1 since time comes first
                }

                // correctly format the x and y title text as well as the title (maintain x vs y order)
                if (currentResp.title.split(',')[x_var_id].indexOf('(' + x_unit_name + ')') > -1) {
                    var x_title_txt = currentResp.title.split(',')[x_var_id];
                } else {
                    var x_title_txt = currentResp.title.split(',')[x_var_id] + ' (' + x_unit_name + ')';
                }

                 if (currentResp.title.split(',')[y_var_id].indexOf('(' + y_unit_name + ')') > -1) {
                    var y_title_txt = currentResp.title.split(',')[y_var_id];
                } else {
                    var y_title_txt = currentResp.title.split(',')[y_var_id] + ' (' + y_unit_name + ')';
                }

                formattedTitle = currentResp.title.split(',')[x_var_id] + x_is_custom +
                 ' vs. ' + currentResp.title.split(',')[y_var_id] + y_is_custom;
            } catch (err) {
                formattedTitle='Error ';
            }

            if (!numericData || currentResp['errors'].length > 0 ) {
                if (!numericData && currentResp['errors'].length === 0) {
                    formattedTitle += ' - Non-numeric data';
                } else {
                    formattedTitle += ' - ' + currentResp['errors'][0];
                }

                var error_obj = {'Custom': formattedTitle};

                if(start_date==undefined || end_date == undefined){
                    start_date = new Date(new Date().setDate(new Date().getDate()+Number(time_window)));
                    end_date = new Date();
                }
                createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, no_data_replace_exporting)
                return
            }

            // create objects for plotting
            var xData = {
                gridLineWidth: 1,
                type: 'x-axis',
                description: unit_name.split(',')[0],
                title: {
                    enabled: true,
                    text: x_title_txt
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true
            };

            var yData = {
                reversed: y_axis_reversed,
                gridLineWidth: 1,
                labels: {
                    enabled: true,
                    style: {
                        color:  "#606060"
                    }
                },
                title: {
                    enabled: true,
                    text: y_title_txt,
                    style: {
                        color: "#606060"
                    }
                }
            };

            if (currentResp.present_asset !== currentResp.parent_asset) {
                var series_name = currentResp.present_asset + ' on ' +
                currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' + formattedTitle;
            } else {
                 var series_name = currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' +
                  formattedTitle;
            }

            var x_title = currentResp.title.split(',')[x_var_id];
            var y_title = currentResp.title.split(',')[y_var_id];

            var seriesData = {
                name: series_name,
                data: formatPlotData(currentResp.data, x_var_index, y_var_index, x_title, x_unit_name, y_title, y_unit_name, colorize),
                description: unit_name, // here check this
            };

            plotObject = {
                container_id: container_id,
                title: title,
                subtitle: subtitle,
                xData: xData,
                yData: yData,
                seriesData: seriesData,
                navigation: default_navigation,
                exporting: replace_exporting,
                lang: LANG
            }

            // Pass the plot object to the comparison_plot function -- this handles profile and comparison type plots
            comparison_plot(plotObject);

            break;
        default:
            // do nothing
    }
}

// Helper function to reflow highchart after a print event
var printUpdate = function () {
        $('#' + plotObject.container_id).highcharts().reflow();
    };

function change_deployment(lst) {
    var asset_id = $(lst).val();
    var deployment_code = $("option:selected", $(lst)).text();

    // TODO(mchagnon): Only goes to overview page and URL is not using a route
    location.href = '/asset/overview/' + asset_id + '/' + deployment_code;
}

function updateSort(event, ui){
    var sorted = $("#plot-list").sortable("serialize", {});
    var ajaxCall = $.ajax({
        url: '/plot_page/' + $("#plot-list").attr('data-plot-page-id') + '/ajax/reorder_plots',
        method: 'POST',
        data: sorted
    });

    ajaxCall.done(function(resp,textStatus,jqXHR) {
        $("#plot-list").attr('data-plot-page-id', resp.plot_page_id);
        $(".customize-plots-button").text("Edit Plots");
    });

    ajaxCall.fail(function(jqXHR, textStatus, errorThrown) {
        add_failed_message("Reorder save failed!")
    });
}
function startSort(e, ui){
    $('.highcharts-series-group').hide();
    // Needed when clearfix present on overview:
    $('#plot-list>.clearfix').remove();

}
function stopSort(e, ui){
    $('.highcharts-series-group').show();
    // Needed when clearfix present on overview:
    $.each($('#plot-list > .plot-list-plot'), function(i, obj){
        if(i%2==1){
            $(obj).after('<div class="clearfix"><!-- clearfix all breakpoints --></div>');
        }
    })

}
$("#plot-list").sortable({
    containment: "parent", 
    handle: ".x_title",
    cursor:"move",
    delay:200,
    distance:10,
    items:"> .plot-list-plot",
    helper: function(){return '<div style="background-color:rgba(0, 0, 0 , 0.2);" class="col-md-6"></div>';},
    placeholder: "col-md-6 plot-list-plot",
    revert: true,
    snap: true, 
    snapMode: "inner",
    start:startSort,
    stop:stopSort,
    tolerance: "pointer",
    update:updateSort
});

