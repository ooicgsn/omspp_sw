;(function (win) {
    const page = {
        config: null
    };

    let alertsTable;
    let statusHistoryTable;


    let COL_PREV_STATUS = 4;
    let COL_USERNAME = 5;
    page.init = (config) => {
        page.config = config
        PlotCommon.setup_time_window();

        let plotArray = page.config.plots.map(plt => plt.id);
        PlotCommon.setup_close_all(page.config.asset.id, page.config.plot_page_name, page.config.plot_page_id, plotArray);
        if(query("#default_plots")){
            query("#default_plots").value = plotArray;
        }

        // build custom context menu items object and include the edit url as an argument
        let edit_url = window.location.origin + django_js_urls['edit_plot_plot_id'].replace('assetid', page.config.asset.id).replace('deploymentcode', page.config.deployment.code)
        

        let customContextMenuItems = PlotCommon.buildCustomContextMenuItems(PlotCommon.standardContextMenuItems, edit_url, 'asset')

        // custom exporting object
        PlotCommon.custom_exporting = jQuery.extend(true, {}, PlotCommon.default_exporting);
        PlotCommon.custom_exporting.buttons.contextButton.menuItems = customContextMenuItems

        PlotCommon.additional_plot_exporting= jQuery.extend(true, {}, PlotCommon.default_exporting)
        PlotCommon.additional_plot_exporting.buttons.exportButton = {
            text: 'Save Custom Plot',
            _titleKey: 'CustomSave',
            menuItems: null,
            onclick: function() {
                
                let selectedOptions_x = query('#plot-variable-x').selectedOptions
                let x_vars = Array.from(selectedOptions_x).map(option => option.value)
                let xvar_str = String(x_vars);

               
                let selectedOptions_y = query('#plot-variable-y').selectedOptions
                let y_vars = Array.from(selectedOptions_y).map(option => option.value)
                let yvar_str = String(y_vars);

                
                let plot_type = query('.plot-type-window0').selectedOptions[0].value
                let plot_class_val = PlotCommon.variablesObject.plot_class; //$('#plot-button').attr('data-plot-class');
                let y_direction = query('.y-axis-orientation0').selectedOptions[0].value
                let time_window_id = query('.time-window0').value
                let plot_relative_to = query('.relative-to0').value
                let start_date = query('.start_date0').value;
                let end_date = query('.end_date0').value;
    
                window.location.href = django_js_urls['edit_plot'].replace('assetid', page.config.asset.id).replace('deploymentcode', page.config.deployment.code) +
                    '?' + 'type=create' + '&' + 'x_var=' + xvar_str + '&' + 'y_var=' + yvar_str + '&' + 'from=' + 'asset' +
                    '&' + 'plot_type=' + plot_type + '&' + 'plot_class=' + plot_class_val + '&' + 'plot_relative_to=' + plot_relative_to +
                    '&' + 'y_axis_reversed=' + y_direction + '&' + 'time_range_id=' + time_window_id + 
                    '&' + 'start_date=' + start_date + '&' + 'end_date=' + end_date;
            }
        }

        for (let plt of page.config.plots){
           
            PlotCommon.update_plot(
                plt.asset_id, // TODO(mchagnon): what if a plot covers more than one asset (instrument)?
                plt.deployment_id,
                plt.x_var.replace(/[\[\]']+/g, ''),
                plt.y_var.replace(/[\[\]']+/g, ''),
                plt.time_window,
                "plot" + plt.id,
                plt.name,
                plt.global,
                plt.plot_type,
                plt.plot_class,
                plt.y_direction,
                plt.start_date,
                plt.end_date,
                plt.relative_to
            );
                 
        }

        PlotCommon.getMenuVariables(page.config.asset.id, page.config.userId );

        if(location.href.indexOf("?confirm=1") !== -1) {
            Common.add_success_message("Plot page saved.");
        }
        if(location.href.indexOf("?confirm=2") !== -1) {
            Common.add_success_message("Plot page deleted");
        }
        
        if(location.href.indexOf("?asset_purge=1") !== -1) {
            Common.add_success_message("Asset successfully purged.");
        }
        if(location.href.indexOf("?asset_purge=0") !== -1) {
            Common.add_failed_message("Asset purge failed");
        }

        SetupCurrentSideNavMenu(page.config.asset.group_id, page.config.asset.platform_id);

        query("#active-alerts-filter input[type=radio]").addEventListener('click', function() {
            alertsTable.ajax.reload();
        });

        query("#y-axis-orientation").addEventListener('change', collect_plot_data);
        query("#id_plot_type").addEventListener('change', collect_plot_data);
        query("#relative-to").addEventListener('change', collect_plot_data);
        query("#time-window-template").addEventListener('change', collect_plot_data);

        let elements = document.querySelectorAll(".update-plot")
        elements.forEach((ele) => {
                ele.addEventListener('click', collect_plot_data);
        });
        

        elements = document.querySelectorAll(".toggle-status-grid")
        elements.forEach((ele) => {
            ele.addEventListener('click', function(){
                
            query("#alert-container").classList.toggle("col-md-11");
            query("#alert-container").classList.toggle("col-md-5");
            query("#status-grid-container").classList.toggle("col-md-7");
            query("#status-grid-container").classList.toggle("col-md-1");

            let elements = document.querySelectorAll("#status-grid-mini, #status-grid, .status-grid-extra, " +
                "#show-status-grid, #hide-status-grid, #platform-status-header"
            )
            elements.forEach((ele) => {
                ele.classList.toggle('hidden');
            });

            let collapsed = query("#status-grid").classList.contains('hidden');
            statusHistoryTable.column(COL_PREV_STATUS).visible(collapsed);
            statusHistoryTable.column(COL_USERNAME).visible(collapsed);
            alertsTable.column(4).visible(collapsed);
            alertsTable.column(5).visible(collapsed);
            });
        });
        
        Common.setupErrorFlags('snapshot');
        
        elements = document.querySelectorAll(".wake-codes")
        elements.forEach((ele) => {
            ele.addEventListener('click', function(e) {
                e.preventDefault();

                Common.openWakeCodeModal();
            });
        });


        $("#wake-code-modal").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: false,
            height: 300,
            width: 450,
            title: 'Wake Codes',
            position: {
                my: "center",
                at: "top+30%",
                of: window
            },
        });

        $("#sbd-mo-description-modal").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: false,
            height: 200,
            width: 450,
            title: 'SBD MO Status',
            position: {
                my: "center",
                at: "top+30%",
                of: window
            },
        });

        query('.sbd-log-table')?.addEventListener('click', function(e) {
            if(e.target.classList.contains('sbd-mo')){
                e.preventDefault();
                openSbdMoDescriptionModel(e.target.textContent);                
            }
        });

        alertsTable = $("#alerts").DataTable({
            pageLength: 10,
            order: [[0, 'desc']],
            paging: true,
            searching: false,
            info: false,
            ajax: {
                url: '/asset/ajax/alert_history_data',
                method: 'POST',
                data: function(d) {
                    d.asset_id = page.config.asset.id,
                    d.include_all = $("#all-alerts").is(':checked')
                }
            },
            columns: [
                {
                    title: "Date",
                    data: "created",
                    render: Common.dt_format_date_time
                },
                {
                    title: "Asset",
                    data: "asset"
                },
                {
                    title: "Alert",
                    data: "trigger_name"
                },
                {
                    title: "Status",
                    data: "status"
                },
                {
                    title: "First Occurrence",
                    data: "created",
                    visible: false,
                    render: Common.dt_format_date_time
                },
                {
                    title: "Last Occurrence",
                    data: "last_occurrence",
                    visible: false,
                    render: Common.dt_format_date_time
                },
                {
                    data: "alert_id",
                    orderable: false,
                    render: (data) =>  `<a href='/alert/${data}'>View</a>`
                }
            ],
            createdRow: function (row, data, index) {
                $(row).addClass(data.row_css_class);
            },
            drawCallback: function() { Common.stretch_pagination($(this)); }
        });

        statusHistoryTable = $("#status-history").DataTable({
            pageLength: 10,
            order: [[0, 'desc']],
            paging: true,
            searching: false,
            info: false,
            ajax: `/asset/ajax/status_history_data/${page.config.asset.id}`,
            columns: [
                {
                    title: "Date",
                    data: 'created',
                    render: Common.dt_format_date_time
                },
                {
                    title: "Asset",
                    data: 'asset_code',
                    render: (data, type, row) => `<a href="/asset/overview/${data}/${row.deployment_code}/">${row.asset_code}</a>`, 
                },
                {
                    title: "Status",
                    data: 'status'
                },
                {
                    title: "Reason",
                    data: 'reason'
                },
                {
                    title: "Prev Status",
                    visible: false,
                    data: 'previous_status'
                },
                {
                    title: "Changed By",
                    visible: false,
                    data: 'username'
                }
            ],
            initComplete: function(settings, json) {
                $(this).closest(".hidden").toggleClass('hidden', false);
            },
            drawCallback: function() { Common.stretch_pagination($(this)); }
        });
        for (let log of page.config.sbd_logs){
            $(`#sbd-${log.filename}-table`).DataTable({
                pageLength: 10,
                order: [[0, 'desc']],
                paging: true,
                searching: false,
                info: false,
                ajax: `/asset/ajax/get-sbd-log-data?log=${log.path}`,
                columns: [
                    {
                        title: "MOMSN",
    
                        data: 'momsn'
                    },
                    {
                        title: 'Date',
                        data: 'timestamp',
                        render: function(data, type, row) {
                            return type == 'display' ? moment(data).format("M/D/YY h:mma") : data;
                        }
                    },
                    {
                        title: 'MO',
                        orderable: false,
                        data: 'mo',
                        render: function(data) {
                            return `<a href='#' class='sbd-mo'>` + data + `</a>`;
                        }
                    },
                    {
                        title: 'Transfer',
                        orderable: false,
                        data: 'transfer_status',
                        render: function(data, type, row) {
                            return row.transfer_status + ", " + row.transfer_bytes + " bytes";
                        },
                        className: 'transfer-column'
                    },
                    {
                        title: 'Lat/Lng',
                        data: 'latitude',
                        orderable: false,
                        render: function(data, type, row) {
                            let latitude = row['latitude'] == null ? '' : row['latitude'];
                            let longitude = row['longitude'] == null ? '' : row['longitude'];
    
                            return latitude + '<br />' + longitude;
                        }
                    },
                    {
                        title: 'CEP',
                        orderable: false,
                        data: 'cep_radius'
                    },
                    {
                        title: 'Data',
                        data: 'data',
                        width: '100%',
                        className: 'data-column'
                    }
                ],
                initComplete: function(settings, json) {
                    $(this).closest(".hidden").toggleClass('hidden', false);
                },
                drawCallback: function() { Common.stretch_pagination($(this)); }
            });
    
        }

        page.plotSort = new Sortable(query("#plot-list-overview"), 
            {
                onEnd: recieveUpdateSortEvent,
                handle: ".x_title",
                dragoverBubble: true,
                invertSwap: false,
                swapThreshold:1,
                animation: 250,
                // containment: "parent", 
                cursor: "move",
                // delay:200,
                // distance:10,
                // items:"> .plot-list-plot",
                // helper: function(){return '<div style="background-color:rgba(0, 0, 0 , 0.2);" class="col-md-6"></div>';},
                // placeholder: "col-md-6 plot-list-plot",
                // revert: true,
                // snap: true, 
                // snapMode: "inner",
                // start:startSort,
                // stop:stopSort,
                // tolerance: "pointer",
                // update:updateSort
            }
        );


        $(window).on('unload', function() {
            let scrollPosition = $(document).scrollTop();
            localStorage.setItem("scrollPosition", scrollPosition);
        });
    
        if(localStorage.scrollPosition) {
           $(document).scrollTop(localStorage.getItem("scrollPosition"));
        }
    
        if(query('#plot-column-select')){
            query('#plot-column-select').value = query('#plot-column-select').getAttribute('data-num-columns')
        }
        query('#plot-column-select')?.addEventListener('change', function(e) {
            let plots = document.querySelectorAll('.plot-list-plot');
            plots.forEach((ele) => {
                ele.classList.remove("plot-col-2", "plot-col-3", "plot-col-4", "plot-col-5");
                ele.classList.add("plot-col-" + e.target.value);                
            });
    
            updateSort(true);
        });
    }

    function recieveUpdateSortEvent(event){
        updateSort();
    }

    function updateSort(reflow_highcharts=false){
        const order = page.plotSort.toArray();

        const plotPageId = $("#plot-list-overview").attr('data-plot-page-id');

        $.ajax({
            url: `/plot_page/${plotPageId}/ajax/customize_plot_page`,
            method: 'POST',
            data: {
                plot_ids: order,
                num_columns: query('#plot-column-select').value,
                asset_id:page.config.asset.id
            }

        })
            .done((resp) => {
                $("#plot-list-overview").attr('data-plot-page-id', resp.plot_page_id);

                // The text show on the edit button is "Customize Plots" when there is no plot page for the current
                //   asset. After one is set up, the text becomes "Edit Plots"
                $(".customize-plots-button").text("Edit Plots");

                const wasSuccessful = order.every((value, index) => value == resp.after_order[index]);
                if (!wasSuccessful) {
                    Common.add_failed_message(`Reorder save failed: before: ${order} after: ${resp.after_order}`);
                    return;
                }

                Common.add_success_message('Plot settings have been updated successfully');
            })
            .fail(() => {
                Common.add_failed_message("Reorder save failed!")
            })
            .always(() => {
                if(reflow_highcharts){
                    let highcharts = document.querySelectorAll('.pre-loaded-plot');
                    highcharts.forEach((highchart) => {
                        $(highchart).highcharts()?.reflow();
                    });
                }
            });
    }
    // function startSort(e, ui){
    //     $('.highcharts-series-group').hide();

    // }
    // function stopSort(e, ui){
    //     $('.highcharts-series-group').show();
    // }

    function checkPlottingParams() {
        let plotting_param_error = false;

        let noneSelected_x = PlotCommon.variablesObject.variable_id_array_x.length === 0;
        let noneSelected_y = PlotCommon.variablesObject.variable_id_array_y.length === 0;

        let tooManySelected = PlotCommon.variablesObject.plot_class === 'Comparison' &&
            PlotCommon.variablesObject.variable_id_array_y.length > 1;

        if (noneSelected_x || noneSelected_y || tooManySelected) {
            plotting_param_error = true;
        }

        return plotting_param_error
    }

    function openSbdMoDescriptionModel(value) {
        const url = '/asset/ajax/get-sbd-mo-description?value=' + value;

        $.get(url, function(response) {
            let modal = $("#sbd-mo-description-modal");

            modal.find('.description').text(value + ": " + response.description);
            modal.dialog('open');
        });
    }




    function update_plot_data(plot_id, asset_id, plotname, plotglobal, deployment_id, time_window, plot_type_window,
        y_direction, plot_class, selector, start_date, end_date, relative_to ) {
        
        if (plot_id === '0') {
            let plotting_param_errors = checkPlottingParams();
            if (plotting_param_errors) {
                // this is a check.. just in case a user tries to plot without updating the menus
                if ($('#additional-plots').find('span[class="error-message"]').length === 0) {
                    PlotCommon.toggleMenuErrorText();
                }

                // clear #plot0 innerHTML and don't start the spinner if a selection is missing
                $('#plot0').html('');
                return
            }
        }

        let x_var;
        let y_var;
        switch (plot_class) {
            case 'Timeseries':
                if (plot_id === '0') {
                    x_var = PlotCommon.variablesObject.variable_id_array_x
                    y_var = PlotCommon.variablesObject.variable_id_array_y;
                    plotname = PlotCommon.variablesObject.variable_name_array_y.join(', ');
                } else {
                    y_var = [$(selector).attr('data-y-variable-id').replace(/[\[\]']+/g, '')];
                }
                PlotCommon.update_plot(asset_id, deployment_id, 'time', y_var, time_window, 'plot' + plot_id, plotname,
                        plotglobal, plot_type_window, plot_class, y_direction, start_date, end_date, relative_to);

                break;
            case 'Comparison':
                if (plot_id === '0') {
                    // update the plotname
                    plotname = PlotCommon.variablesObject.variable_name_array_x[0] + ' vs. ' +
                        PlotCommon.variablesObject.variable_name_array_y[0];

                    x_var = PlotCommon.variablesObject.variable_id_array_x[0] || [];
                    y_var = PlotCommon.variablesObject.variable_id_array_y[0] || [];
                } else {
                    x_var = $(selector).attr('data-x-variable-id').replace(/[\[\]']+/g, '');
                    y_var = $(selector).attr('data-y-variable-id').replace(/[\[\]']+/g, '');
                }

                if (x_var === y_var) {
                    alert('Plotting a variable against itself is not allowed');
                    return
                }
                PlotCommon.update_plot(asset_id, deployment_id, x_var, y_var, time_window, 'plot' + plot_id, plotname,
                    plotglobal, 'scatter', plot_class, y_direction, start_date, end_date, relative_to);
                break
            default:
                // do nothing
        }

        // prevents the text in the plot area from starting off highlighted
        setTimeout(function () {$('#additional-plots').click();}, 500);
    }

    function collect_plot_data() {
        let plot_button = query("#plot-button");
        let selector = this.closest('.x_panel').querySelector('.update-plot');
        PlotCommon.handle_change_time_window_event(selector);
        if((query('#plot0').innerHTML.trim() !== '')|| (plot_button == selector)  || selector.classList.contains('update-plot')) {
            let plot_id = selector.getAttribute('data-plot-id');
            let asset_id = selector.getAttribute('data-asset-id');
            let plotname = selector.getAttribute('data-plot-name');
            let plotglobal = selector.getAttribute('data-plot-global') === 'global_True' && !page.config.editShared ?
                'global_True' : 'global_False';
            let deployment_id = plot_button.getAttribute('data-deployment-id');

            let time_window_el = query(".time-window" + plot_id);
            let relative_to_el = query(".relative-to" + plot_id);
            let start_date = undefined;             
            let end_date = undefined;
            let time_window = undefined;
            let dates_wrapper = query(".start_date"+ plot_id).parentElement;
            if(relative_to_el.value==PlotCommon.CUSTOM_TIME_FRAME){
                if(!query(".start_date"+ plot_id).value && !query(".end_date"+ plot_id).value){
                    if (dates_wrapper.querySelectorAll('div[class="error-message"]').length === 0) {
                        let div = document.createElement('div');
                        div.classList.add('error-message');
                        div.innerHTML = '&nbsp;Must set either start or end date';
                        dates_wrapper.appendChild(div);
                    }
                    return;
                }else{
                    
                    if (dates_wrapper.querySelector('div[class="error-message"]')){
                        dates_wrapper.removeChild(dates_wrapper.querySelector('div[class="error-message"]'));
                    }
                }

                start_date = query(".start_date"+ plot_id).value;                
                end_date = query(".end_date"+ plot_id).value; 
                time_window = null
            }else{
                if (dates_wrapper.querySelector('div[class="error-message"]')){
                    dates_wrapper.removeChild(dates_wrapper.querySelector('div[class="error-message"]'));
                }
                time_window = PlotCommon.time_choices_dict[time_window_el.value]['days']
            }

            let relative_to = relative_to_el.value

            let plot_type_window = query(".plot-type-window" + plot_id).selectedOptions[0].textContent?.toLowerCase() || 'line';

            let y_direction = query(".y-axis-orientation" + plot_id).value === 'true' ? true: false;

            let plot_class = selector.getAttribute('data-plot-class') || 'Timeseries';
            update_plot_data(plot_id, asset_id, plotname, plotglobal, deployment_id, time_window, plot_type_window,
                y_direction, plot_class, selector, start_date, end_date, relative_to);
        }
    };


    win.Pages = win.Pages || {};
    win.Pages.Overview = page;
})(window);