;(function (win) {
    const page = {
        platformId: null
    };

    page.init = (platformId) => {
        page.platformId = platformId;
        page.setupEvents();
    }

    page.addSubcategory = (name, value, isSelected) => {
        const option = $("<option />")
            .val(value)
            .text(name)
            .prop("selected", isSelected);

        $("#id_subcategory").append(option);
    }

    page.bindSubcategories = (subcategoryId) => {
        $.ajax({
            url: '/asset/ajax/asset_subcategories',
            method: 'POST',
            data: { 'id': $("#id_category option:selected").val() },
            success: function(response) {
                const data = response.data;
                if (!data[0])
                {
                    return;
                }

                const selectedId = subcategoryId ?? $("#id_subcategory option:selected").val();
                $("#id_subcategory").find('option').remove();

                $.each(data, function(idx, value){
                    page.addSubcategory(value.name, value.id, value.id === parseInt(selectedId));
                });
            }
        });
    }

    page.refreshDetails = () => {
        $.ajax({
            url: `/asset/ajax/get_platform_details/${page.platformId}`,
            method: 'get',
            success: (response) => {

                document.querySelector('[name=disposition]').value = response.disposition;
                document.querySelector('[name=category]').value = response.category;
                document.querySelector('[name=deployment_date]').value = response.deployment_date;
                document.querySelector('[name=deployment_cruise]').value = response.deployment_cruise;
                document.querySelector('[name=recovery_date]').value = response.recovery_date;
                document.querySelector('[name=recovery_cruise]').value = response.recovery_cruise;
                document.querySelector('[name=lat]').value = response.lat;
                document.querySelector('[name=lon]').value = response.lon;
                document.querySelector('[name=depth]').value = response.depth;
                document.querySelector('[name=watch_circle_radius]').value = response.watch_circle_radius;

                // Subcategory can't be selected until the list is bound after changing the category value
                page.bindSubcategories(response.subcategory);
            }
        })
    }

    page.reloadConfiguration = () => {
        $("#reload-success").hide();
            $("#reload-error").hide();
            $("#reload-configuration").hide();
            $("#loading-icon").show();

            const form = document.querySelector('#reload-form');
            const formData = new FormData(form);

            $.ajax({
                url: '/asset/ajax/reload-configuration',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    $("#loading-icon").hide();
                    $("#reload-configuration").show();

                    if (!data.success) {
                        $("#reload-error").html(data.message);
                        $("#reload-error").show();
                        return;
                    }

                    page.refreshDetails();
                    $("#reload-success").show();
                }
            });
    }

    page.setupEvents = () => {
        page.bindSubcategories();

        $("#id_category").on('change', function() {
            page.bindSubcategories();
        });

        $("#reload-configuration").click(function(){
            page.reloadConfiguration();
        });

        Array.from(document.querySelectorAll('[name=source]')).forEach((e) => {
            e.addEventListener('change', (e) => {
                document.querySelector('[name=file]').disabled = e.target.value !== 'yaml';
            })
        })
    }

    win.Pages = win.Pages || {};
    win.Pages.EditPlatform = page;
})(window);