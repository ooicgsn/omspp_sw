;(function (win) {
    const page = {
        assetId: null,
        parsedDataTable: null,
        enablePaging: true,
        parsedDataPlot: null,
        hasSubsectionBeenSelected: false,
    };

    const assetTypeErrorClass = {
        '2':'cpm-mpic-error-flags', 
        '3':'dcl-mpic-error-flags', 
    }

    page.init = (assetId, loadDataOnInit, assetName, assetType) => {
        page.assetId = assetId;
        page.assetName = assetName;
        page.assetType = assetType;

        page.setupEvents();

        $("#plot-data-modal").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: true,
            height: 550,
            width: 650,
            title: 'Plot Data',
            position: {
                my: "center",
                at: "top+30%",
                of: window
            },
        });

        if (loadDataOnInit.toLowerCase() === 'true') {
            page.loadData();
        }
    }

    page.safeString = (value) => {
        return value === undefined ? '' : value;
    }

    page.safeDateTime = (value) => {
        try {
            // Values are in seconds and need to be changed to milliseconds for moment
            let adjustedValue = Number(value) * 1000;

            return moment(adjustedValue).format("M/D/YYYY h:mma") + " UTC"
        } catch (err) {
            return '';
        }
    }

    page.loadData = () => {
        const path = $('#data-file').val();
        const subSection = $('[name=subSection]:checked').val();

        $.ajax({
            url: `/asset/parsed_data_for_file/${page.assetId}/`,
            type: 'POST',
            data: { path: path, sub: subSection },
            success: function (data) {
                const idx = path.lastIndexOf('/');

                $('#location-path').text(path.slice(0, idx));
                $('#location-file').text(path.slice(-path.length + idx + 1));

                let container = $('#parsed-data-table');

                let errorMessage = $('#parsed-data-message');
                if (!data.parsed) {
                    container.hide();
                    errorMessage.text(data.parse_error);
                    errorMessage.show();
                    return;
                }

                if (page.parsedDataTable) {
                    page.parsedDataTable.destroy();
                    $('#parsed-data').empty();
                }

                const common = data.common;
                if (Object.keys(common).length > 0) {
                    const isProfile = common.type === 'profile';
                    const isSummaryData = common.type === 'summarydata';

                    if (isProfile) {
                        document.querySelector('#parsed-profile-id').textContent = page.safeString(common.profile_id);
                        document.querySelector('#parsed-profile-status').textContent = page.safeString(common.profile_status);
                        document.querySelector('#parsed-pretty-profile-status').textContent = page.safeString(common.pretty_profile_status);
                        document.querySelector('#parsed-ramp-status').textContent = page.safeString(common.ramp_status);
                        document.querySelector('#parsed-sensor-on').textContent = page.safeDateTime(common.sensor_on);
                        document.querySelector('#parsed-sensor-off').textContent = page.safeDateTime(common.sensor_off);
                        document.querySelector('#parsed-motion-start').textContent = page.safeDateTime(common.motion_start);
                        document.querySelector('#parsed-motion-end').textContent = page.safeDateTime(common.motion_stop);
                        document.querySelector('#parsed-start-depth').textContent = page.safeString(common.start_depth);
                        document.querySelector('#parsed-end-depth').textContent = page.safeString(common.end_depth);

                        if (!page.hasSubsectionBeenSelected) {
                            document.querySelector('#edata').checked = true;
                            page.hasSubsectionBeenSelected = true;
                        }
                    }

                    if (isSummaryData) {
                        document.querySelector('#parsed-id').textContent = page.safeString(common.ID);
                        document.querySelector('#parsed-serial-number').textContent = page.safeString(common.serial_number);
                        document.querySelector('#parsed-date-time').textContent = page.safeString(common.datetime);
                        document.querySelector('#parsed-voltage').textContent = page.safeString(common.voltage);
                        document.querySelector('#parsed-records').textContent = page.safeString(common.records);
                        document.querySelector('#parsed-length').textContent = page.safeString(common.length);
                        document.querySelector('#parsed-events').textContent = page.safeString(common.events);

                        if (!page.hasSubsectionBeenSelected) {
                            document.querySelector('#scidata').checked = true;
                            page.hasSubsectionBeenSelected = true;
                        }
                    }

                    document.querySelectorAll('[data-section=profile]').forEach((e) => {
                        e.classList.toggle('hide', !isProfile);
                    });

                    document.querySelectorAll('[data-section=summarydata]').forEach((e) => {
                        e.classList.toggle('hide', !isSummaryData);
                    });
                }

                errorMessage.hide()
                container.show();
                let parsed_data_columns = data.columns.map((col) => ({
                    title: col,
                    orderable: true
                }));

      
                if (assetTypeErrorClass[page.assetType]!==undefined){
                    let error_flags = parsed_data_columns.find((element) => element.title== "error_flags");
                    error_flags.render = (data) =>{
                        return `<a href="#" class="error-flags" data-endpoint="${assetTypeErrorClass[page.assetType]}" data-title="${page.assetName} Error Flags">${data.toString(16).padStart(8, '0')}</a>`
                    }
                }

                page.parsedDataTable = $('#parsed-data').DataTable({
                    order: [[0, 'desc']],
                    paging: page.enablePaging,
                    pageLength: 25,
                    searching: false,
                    ordering: true,
                    info: true,
                    scrollX: true,
                    data: data.data,
                    columns: parsed_data_columns,
                });
                Common.setupErrorFlags('parsed-data');
                page.updatePlotParameters(data.columns);
            }
        });
    }

    page.updatePlotParameters = (variables) => {
        let $variables = $('#plot-variable');
        $variables.empty();

        variables.forEach(function(x) {
            // Filter out some time options
            if (x === 'time' || x === 'dcl_date_time_string') {
                return;
            }

            let $option = $('<option />', { val: x, text: x });

            $variables.append($option);
        });

        let multiselectObject = {
            enableCaseInsensitiveFiltering: true,
            maxHeight: 275,
            nonSelectedText: 'Select variable(s) to plot',
            buttonWidth: '65%',
            buttonTitle: function(options, select) {
                let labels = [];
                options.each(function () {
                    labels.push($(this).text());
                });
                return labels.join(', ');
            },
            onChange: function(option, checked) {
                let $container = $('#plot-variable').parent();

                let checkedItems = $container.find('input[type="checkbox"]:checked').length;
                if (checkedItems >= 4) {
                    $container.find('input[type="checkbox"]:not(:checked)').prop('disabled', true);
                } else {
                    $container.find('input[type="checkbox"]').prop('disabled', false);
                }
            }
        }

        $("#plot-variable").multiselect(multiselectObject);
        $("#plot-variable").multiselect('rebuild');
    }

    page.plot = () => {
        const selectedVariables = $('#plot-variable').val();
        if (selectedVariables == '') {
            return;
        }

        const filename = $('#data-file').val();
        const subSection = $('[name=subSection]:checked').val();

        $.ajax({
            url: `/asset/parsed_data_for_plot/${page.assetId}/`,
            type: 'POST',
            data: { path: filename, sub: subSection, variables: selectedVariables },
            success: function (data) {
                if (!data.success) {
                    $('#plot-error').text(data.error).toggleClass('hide', false);
                    return;
                }

                let series = [];
                let seriesDefinitions = [];
                const xdata = data.x_axis;

                Object.keys(data.y_axis).forEach(function(variableName, axisIndex) {
                    let ydata = data.y_axis[variableName];

                    series.push({
                        yAxis: axisIndex,
                        name: variableName,
                        data: data.x_axis.map(function(_, i) {
                            return [xdata[i], ydata[i]];
                        })
                    });

                    seriesDefinitions.push({
                        // Very basic logic to alternate sides for y-axis (even/odd)
                        opposite: (axisIndex % 2),
                        title: { text: variableName },
                    });
                });

                page.parsedDataPlot = Highcharts.chart('plot-container', {
                    title: { text: '', },
                    xAxis: { title: { text: 'Date/Time' }, type: 'datetime' },
                    yAxis: seriesDefinitions,
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },
                    series: series,
                    responsive: {
                        rules: [{
                            condition: { maxWidth: 500, },
                        }]
                    }
                });
            }
        });
    }

    page.openPlotModal = () => {
        page.destroyPlot();
        $("#plot-variable").multiselect('clearSelection');
        $("#plot-data-modal").dialog('open');
    }

    page.destroyPlot = () => {
        if (page.parsedDataPlot) {
            try {
                page.parsedDataPlot.destroy();
            } catch (err) {
                // Do nothing
            }


        }
    }

    page.setupEvents = () => {
        $('#data-file').change(function(){
            page.loadData();
        });

        $('#enable-paging').change(function() {
            page.enablePaging = $(this).is(':checked');

            page.loadData();
        });

        $('[name=subSection]').change(function() {
            page.destroyPlot();
            page.loadData();
        })

        $('#plot-data').click(function() {
            page.openPlotModal();
        })

        $('#plot').click(function() {
            page.plot();
        })
    }

    win.Pages = win.Pages || {};
    win.Pages.ParsedData = page;
})(window);