;(function (win) {
    const page = {};
    const userId = JSON.parse(document.querySelector('#user_id').textContent);
    const data_url = `/alert/ajax/hotlist_alert_list/${userId}`;
    page.init = () => {
        page.recentAlertsTable = $("#recent-alerts").DataTable({
            order: [[5, 'desc']], // Default sort: Created
            paging: false,
            searching: false,
            info: false,
            ajax: {
                url: data_url,
                type: 'GET',
                data: data => ({'is_view_acknowledged': document.querySelector('#chkBxAcknowledged').checked})
            },
            columns: [
                {
                    title: "Name",
                    data: "trigger_id",
                    render: (data, type, row) => {
                        if (!data)
                            return row.name;
                        return `<a href="#" data-id="${data}" class="view-trigger">${row.trigger_name}</a>`;
                    }
                },
                {
                    title: "Asset",
                    data: "asset_id",
                    render: (data, type, row) => `<a href="/asset/overview/${data}/${row.deployment_code}/">${row.asset_code}</a>`,
                },
                {
                    title: "Platform",
                    data: "platform_id",
                    render: (data, type, row) => `<a href="/asset/overview/${data}/${row.deployment_code}/">${row.platform_code}</a>`,
                },
                {
                    title: "Deployment",
                    data: "deployment_code",
                },
                {
                    title: "Details",
                    data: "details"
                },
                {
                    title: "Severity",
                    data: "severity_name"
                },
                {
                    title: "Created",
                    data: "alert_date",
                    render: Common.dt_format_date_time
                },
                {
                    title: "Last Occurrence",
                    data: "last_occurrence",
                    render: Common.dt_format_date_time
                },
                {
                    // View
                    data: "alert_id",
                    render: data => `<a href="/alert/${data}">View</a>`,
                },
                {
                    // Acknowledge
                    data: "alert_notification_id",
                    render: (data, type, row) => {
                        if (row.severity_name === "Alarm" || row.severity_name === "High")
                            return "Acknowledge";

                        if (row.has_acknowledged)
                            return "Ack'd";

                        return `<a href="#" class="clear-alert" data-alert-notification-id="${data}" >Acknowledge</a>`;
                    }
                },
                {
                    // Resolve
                    data: "alert_id",
                    render: data => `<a href="/alert/${data}/True">Resolve</a>`,
                }
            ],
            initComplete: function(settings, json) {
                $(this).closest(".hidden").toggleClass('hidden', false);
            },
            createdRow: function (row, data, index) {
                $(row).addClass(data['css_class']);
            },
        });

        document.querySelector("#recent-alerts").addEventListener('click', event => {
            if (event.target.matches('.clear-alert')) {
                event.preventDefault();
                let formData = new FormData();
                formData.append('alert_notification_id', event.target.getAttribute('data-alert-notification-id'));
                fetch('/alert/acknowledge', {
                    method: 'POST',
                    credentials : 'same-origin',
                    headers: {
                        "X-CSRFToken": csrftoken,
                    },
                    body: formData,

                }).then((data) => {
                    if(data.status === 200) {
                        page.recentAlertsTable.ajax.reload();
                    }
                });
            }
            if (event.target.matches(".view-trigger")) {
                event.preventDefault();
                var url = django_js_urls['view_trigger'].replace(/\d+$/i,  event.target.getAttribute('data-id'));
                return Common.open_dialog_box(event, '#trigger-detail', url);
            }
        });

        Common.init_dialog_box("#trigger-detail", 300);

        document.querySelector("#chkBxAcknowledged").addEventListener('click', event => {
            page.recentAlertsTable.ajax.reload();
        });
    }

    win.Pages = win.Pages || {};
    win.Pages.Hotlist = page;
})(window);