import Common from '../common'

;(function (win) {
    const page = {};

    page.init = () => {
        page.setupEvents();
        page.filterDurationCode();
    }

    page.changeTriggerType = () => {
        const triggerTypeId = document.querySelector('[name=trigger_type]').value;
        const isLastUpdateTrigger = triggerTypeId == Common.TriggerTypes.LastUpdate;

        document.querySelector('#interpolation-panel').style.display = isLastUpdateTrigger ? 'none' : '';
        document.querySelector('#variables-panel').style.display = isLastUpdateTrigger ? 'none' : '';
        document.querySelector('#expression-panel').style.display = isLastUpdateTrigger ? 'none' : '';
        document.querySelector('#expression-help-panel').style.display = isLastUpdateTrigger ? 'none' : '';
        document.querySelector('#btnValidate').style.display = isLastUpdateTrigger ? 'none' : '';

        page.filterDurationCode();
    }

    page.filterDurationCode = () => {
        const $durationCode = document.querySelector('[name=duration_code]');
        if ($durationCode.value == Common.DurationCodes.CurrentDeployment) {
            $durationCode.value = ''
        }
        const $options = Array.from($durationCode.querySelectorAll(`option`));
        const $option = $options.filter((x) => x.value == Common.DurationCodes.CurrentDeployment)[0]

        const triggerTypeId = document.querySelector('[name=trigger_type]').value;
        $option.style.display = triggerTypeId == Common.TriggerTypes.LastUpdate ? 'none' : '';
    }

    page.onAssetSelected = (e) => {
        $.ajax({
            type: "GET",
            url: '/variable/ajax/variable_list_by_asset/' + $("#varpicker-value").val(),
            data: {},
            success: function(response) {
                page.updateVariables(response);
            }
        });
    }

    page.updateVariables = (response) => {
        const selectVariable = $("#selectVariable");
        const selectedAssetId = +query("#varpicker-value").value;
        const variables = response.variables;

        selectVariable.empty();
        selectVariable.find('option').remove();

        if (selectedAssetId < 1) {
            selectVariable.append($("<option value='0'>Please select an asset</option>"));
            return;
        }

        if (!variables[0]) {
            selectVariable.append($("<option value='0'>No variables associated with this asset</option>"));
            return;
        }

        selectVariable.append($('<option value="0"></option>'));
        for (let i in variables) {
            let unitText = (variables[i][8]) ? ` (${variables[i][8]})` : "";

            if(variables[i][1].substring(variables[i][1].length -1, variables[i][1].length) === ')') {
                unitText = '';
            }

            selectVariable.append($(
                `<option value="${variables[i][0]}" ` +
                    `asset-id="${variables[i][2]}" ` +
                    `asset-name="${variables[i][3]}"` +
                    `platform-id="${variables[i][4]}" ` +
                    `platform-name="${variables[i][5]}" ` +
                    `deployment-id="${variables[i][6]}" ` +
                    `deployment-name="${variables[i][7]}" ` +
                    `erddap-id="${variables[i][9]}" ` +
                `>${variables[i][1]}${unitText}</option>`));
        }
    }

    page.setupEvents = () => {
        query('[name=trigger_type]').addEventListener('change', (e) => {
            page.changeTriggerType();
        });

        query("#varpicker-name").addEventListener('asset-selected', (e) => {
            page.onAssetSelected(e);
        });

        query("#select-all-email")?.addEventListener("change", (e) => {
            document.querySelectorAll(".email_immediately")
                .forEach((item) => item.checked = e.target.checked);
        });

        query("#select-all-text")?.addEventListener("change", (e) => {
            document.querySelectorAll(".text_immediately")
                .forEach((item) => item.checked = e.target.checked);
        });

        query("#select-all-summary")?.addEventListener("change", (e) => {
            document.querySelectorAll(".email_daily")
                .forEach((item) => item.checked = e.target.checked);
        });
    }

    win.Pages = win.Pages || {};
    win.Pages.EditTrigger = page;
})(window);