;(function (win) {
    const page = {
        config : null,
    }
      
    page.init = (config) => {
        page.config = config;

        // build custom context menu items object and include the edit url as an argument
        let edit_url = window.location.origin + django_js_urls['edit_plot_plot_id'].replace('assetid', page.config.asset.id).replace('deploymentcode', page.config.deployment.code)
        let customContextMenuItems = PlotCommon.buildCustomContextMenuItems(PlotCommon.standardContextMenuItems, edit_url, 'plot_page' + page.config.plot_page_id)


        PlotCommon.custom_exporting = jQuery.extend(true, {}, PlotCommon.default_exporting);
        PlotCommon.custom_exporting.buttons.contextButton.menuItems = customContextMenuItems

        PlotCommon.additional_plot_exporting= jQuery.extend(true, {}, PlotCommon.default_exporting)

        page.setupEvents();

        PlotCommon.setup_time_window();

        var groupDataId = 'group-' + page.config.asset.group_id;
        var platformId = 'platform-' + page.config.asset.platform_id;
        var url = location.href;
        if(url.indexOf("confirm") !== -1) {
            Common.add_success_message("Plot page saved.");
        }
        let closeLinks = document.querySelectorAll(".delete-plot-link");

        closeLinks.forEach((ele) => {
            ele.addEventListener("click", removePlot);
        })

        SetupCurrentSideNavMenu(groupDataId, platformId);

        for (let plt of page.config.plots){
            PlotCommon.update_plot(
                plt.asset_id, // TODO(mchagnon): what if a plot covers more than one asset (instrument)?
                plt.deployment_id,
                plt.x_var.replace(/[\[\]']+/g, ''),
                plt.y_var.replace(/[\[\]']+/g, ''),
                plt.time_window,
                "plot" + plt.id,
                plt.name,
                plt.global,
                plt.plot_type,
                plt.plot_class,
                plt.y_direction,
                plt.start_date,
                plt.end_date,
                plt.relative_to
            );
        }

        
        

    }

    page.setupEvents = () => {

        document.getElementById("delete_plot_page").addEventListener('click', () => {
            if(confirm("Would you like to delete this plot page?")) {
                $.ajax({
                    url: django_js_urls['delete_plot_page'],
                    method: 'POST',
                    data : {
                        'plot_page_id': page.config.plot_page_id
                    },
                    success: function() {
                        let asset_id = page.config.asset.id;
                        let code = page.config.deployment.code;
                        let url = django_js_urls['asset_overview'].replace('assetid', asset_id).replace('deploymentcode', code);
                        url += '?confirm=2'
                        location.href = url;
                    }
                });
            }
        });

        document.getElementById('disable_sharing')?.addEventListener('click', () => {
            $.ajax({
                url: django_js_urls['disable_sharing'],
                method: 'POST',
                data: {
                    'plot_page_id': page.config.plot_page_id
                },
                success:function() {
                    Common.add_success_message("Sharing disabled.");
                }
            });
        });

        document.getElementById("share_plot").addEventListener('click', () => {
            document.getElementById("email_share").removeAttribute("hidden"); 
        });

        document.getElementById('send_email').addEventListener('click', () => {
            let notification = document.getElementById("notification");
            document.getElementById("notification").textContent = '' ;
            let email = document.getElementById('email_textarea').value;
            if(email.length == 0) {
                notification.textContent = 'Please enter an email address';
                notification.classList.add("alert", "alert-danger");
            } else if(validate_email(email)) {
                $.ajax({
                    url: django_js_urls['share_plot_page'],
                    method: 'POST',
                    data: {
                        'email_recip': email,
                        'plot_page_id': page.config.plot_page_id
                    },
                    success:function(data) {
                        Common.add_success_message("Plot page shared.");
                    }
                });

                document.getElementById("email_share").addAttribute("hidden"); 
                document.getElementById('email_textarea').value = '';
                notification.textContent = '';
            } else {
                notification.textContent = 'Please enter valid email addresses';
                notification.classList.add("alert", "alert-danger");
            }
        });

        document.body.addEventListener('click', (e) => {

            if (e.target.matches('.update-plot')) {
                let targ = e.target;

                let plot_id = targ.getAttribute('data-plot-id');
                let asset_id = targ.getAttribute('data-asset-id');
                let deployment_id = targ.getAttribute('data-deployment-id');
                let plotname = targ.getAttribute('data-plot-name');
                let plotglobal = targ.getAttribute('data-plot-global') === 'global_True' && !page.config.editShared ?
                    'global_True' : 'global_False';
                let plot_type_window = query(".plot-type-window" + plot_id).selectedOptions[0].textContent?.toLowerCase() || 'line';
                let plot_class = targ.getAttribute('data-plot-class');;
                let y_direction = query(".y-axis-orientation" + plot_id).value === 'true' ? true: false;

                let x_vars = targ.getAttribute('data-x-variable-id').replace(/[\[\]']+/g, '');
                let y_vars = targ.getAttribute('data-y-variable-id').replace(/[\[\]']+/g, '');

                let time_window_el = query(".time-window" + plot_id);
                let relative_to_el = query(".relative-to" + plot_id);
                let start_date = undefined;
                let end_date = undefined;
                let time_window = undefined;
                let dates_wrapper = query(".start_date"+ plot_id).closest('.dates_wrapper');
                if(relative_to_el.value==PlotCommon.CUSTOM_TIME_FRAME){
                    if(!query(".start_date"+ plot_id).value && !query(".end_date"+ plot_id).value){
                        if (dates_wrapper.querySelectorAll('div[class="error-message"]').length === 0) {
                            let div = document.createElement('div');
                            div.classList.add('error-message');
                            div.innerHTML = '&nbsp;Must set either start or end date';
                            dates_wrapper.appendChild(div);
                        }
                        return;
                    }else{
                        
                        if (dates_wrapper.querySelector('div[class="error-message"]')){
                            dates_wrapper.removeChild(query('div[class="error-message"]'));
                        }
                    }

                    start_date = query(".start_date"+ plot_id).value;                
                    end_date = query(".end_date"+ plot_id).value; 
                    time_window = null
                }else{
                    if (dates_wrapper.querySelector('div[class="error-message"]')){
                        dates_wrapper.removeChild(query('div[class="error-message"]'));
                    }
                    time_window = PlotCommon.time_choices_dict[time_window_el.value]['days']
                }

                let relative_to = relative_to_el.value

                if (plot_class === 'Timeseries') {
                    x_vars = 'time'
                }
                
                PlotCommon.update_plot(asset_id, deployment_id, x_vars, y_vars, time_window,'plot' + plot_id, plotname,
                    plotglobal, plot_type_window, plot_class, y_direction, start_date, end_date, relative_to);
            }
        });
    }
    function removePlot(event) {

        let available_plots = page.config.plots.map(function(i) {
            return i.id;
        });
        if(confirm("Would you like to remove this plot?")) {
            let index = available_plots.indexOf(event.currentTarget.id);
            
            available_plots.splice(index,1);
            if(available_plots.length != 0) {
                $.ajax({
                    url: django_js_urls['save_plot_page'],
                    method: 'POST',
                    data: {
                        'plot_ids': available_plots,
                        'plot_page_name': page.config.plot_page_name,
                        'plot_page_id': page.config.plot_page_id,
                        'asset_id': page.config.asset.id,
                        'asset_overview': "False"
                    },
                    success:function(d) {
                        Common.add_success_message("Plot page saved.");
                    }
                });
                let div_panel = event.currentTarget.closest('.col-lg-6');
                div_panel.parentElement.removeChild(div_panel);
                document.getElementById('plot_ids').val(available_plots);
            } else {
                document.getElementById('plot_page_alert').textContent = "You cannot have a plot page without any plots. Please click the Delete button to delete the page.";
                document.getElementById('plot_page_alert').classList.add("alert", "alert-danger");
            }
        }
    }

    function validate_email(email_list) {
        let regex = /^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;|,| |; |, \s*|\s*$))*$/;
        return regex.test(email_list);
    }

    win.Pages = win.Pages || {};
    win.Pages.PlotPage = page;
})(window);