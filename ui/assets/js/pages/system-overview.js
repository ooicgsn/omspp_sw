import Common from '../common'

;(function (win) {
    const page = {
        config: null
    };

    page.init = (config) => {
        page.config = config;
        page.setupCharts();
        page.setupEvents();
    }


    page.setupCharts = () => {

        page.config.assets.forEach(function (asset, index) {

            var low = 0;
            var med = 0;
            var high = 0;
            var alarm = 0;

            var test = page.config.alerts[asset.id];
            var i;
            for(i in test) {
                switch(i) {
                    case '1':
                        low = test[i];
                        break;
                    case '2':
                        med = test[i];
                        break;
                    case '3':
                        high = test[i];
                        break;
                    case '4':
                        alarm = test[i];
                        break;
                }
            }

            // chartObj[asset.id] = Highcharts.chart(`asset_${asset.id}`, {
            Highcharts.chart(`asset_${asset.id}`, {
                chart: {
                    type: 'bar',
                    height: 200
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: ['Telemetry', 'Data', `<a href="/asset/overview/${asset.id }/${asset.deployment_code}">Functional</a>`]
                },
                yAxis: {
                    max: 100,
                    title: {
                        text: ''
                    },
                    tickInterval: 25,
                    labels: {
                        format: '{value}%'
                    }
                },
                series: [{
                    title: false,
                    data: [0, 0, asset.average_platform_status]
                }],
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['printChart', 'separator', 'downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG', 'downloadCSV']
                        }
                    }
                },
                plotOptions: {
                    series: {
                        zones: [
                            {value: 50, className: 'zone-critical'},
                            {value: 70, className: 'zone-needs-attention'},
                            {className: 'zone-ok'}
                        ]
                    }
                }
            });

            Highcharts.chart(`asset_${ asset.id }_alerts`,                
                {
                chart: {
                    height: 60,
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: ['Alert Status']
                },
                yAxis: {
                    max: 25,
                    title: {
                        text: false
                    }
                },
                credits: {
                        enabled: false
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    },
                    bar: {
                        pointWidth: 25
                    }
                },
                exporting: {
                    buttons: {
                        contextButton: {
                            enabled: false
                        }
                    }
                },
                series: [{
                    name: 'ALARM',
                    data: [alarm],
                    color: '#ff0000'
                }, {
                    name: 'HIGH',
                    data: [high],
                    color: '#ff9900'
                }, {
                    name: 'MEDIUM',
                    data: [med],
                    color: '#ffff00'
                }, {
                    name: 'LOW',
                    data: [low],
                    color: '#00cc00'
                }]
            });

            query(`#asset_${asset.id}`).setAttribute('title', query(`.asset_${asset.id}`).innerHTML);
        });


        Highcharts.chart("platform_status", {
            chart: {
                type: 'bar',
                pointWidth: 20,
            },
            title: {
                text: page.config.page_title
            },
            xAxis: {
                categories: ['Array Status']
            },
            yAxis: {
                max: 100,
                title: {
                    text: false
                }
            },
            series: [{
                title: false,
                data: [page.config.array_status]
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    }
                }
            },
            plotOptions: {
                series: {
                    zones: [
                        {value: 50, className: 'zone-critical'},
                        {value: 70, className: 'zone-needs-attention'},
                        {className: 'zone-ok'}
                    ]
                }
            }
        });

        var low = 0;
        var med = 0;
        var high = 0;
        var alarm = 0;


        page.alertStatusChart = Highcharts.chart("alert_status", {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Alert Status'
            },
            xAxis: {
                categories: ['Alert Status']
            },
            yAxis: {
                max: 25,
                title: {
                    text: false
                }
            },
            credits: {
                    enabled: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                },
                bar: {
                    pointWidth: 25
                }
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'ALARM',
                data: [alarm],
                color: '#ff0000'
            }, {
                name: 'HIGH',
                data: [high],
                color: '#ff9900'
            }, {
                name: 'MEDIUM',
                data: [med],
                color: '#ffff00'
            }, {
                name: 'LOW',
                data: [low],
                color: '#00cc00'
            }]
        });

    }
    page.setupEvents = () => {

        var groupDataId = `group-${page.config.group_id}`;
        SetupCurrentSideNavMenu(groupDataId, null);

        $.widget("ui.tooltip", $.ui.tooltip, {
            options: {
                track: true,
                content: function() {
                    return $(this).prop('title');
                }
            }
        });

        query("#graph_preference").addEventListener('change', () => {
            var pref_id = query("#graph_preference input:checked").value;
            $.ajax({
                url: django_js_urls['update_system_overview_preference'],
                method: 'POST',
                data: {
                    'preference_id': pref_id
                },
                success: function(resp) {
                    if(pref_id == 1) {
                        $$(".functional").forEach((ele)=>{ele.style.display='block'});
                        $$(".alert_status").forEach((ele)=>{ele.style.display='none'});
                    } else if(pref_id == 2) {
                        $$(".functional").forEach((ele)=>{ele.style.display='none'});
                        $$(".alert_status").forEach((ele)=>{ele.style.display='block'});
                    } else {
                        $$(".functional").forEach((ele)=>{ele.style.display='block'});
                        $$(".alert_status").forEach((ele)=>{ele.style.display='block'});
                    }
                }
            });
    
        });
        
        $$("#dispositions input[type=checkbox]").forEach((checkbox) => {
            checkbox.addEventListener('change', page.handleCheckedDisps);
        });
        page.handleCheckedDisps();

        $(document).tooltip();
    }
    page.handleCheckedDisps = () => {
        let checked_disps = [];
        let checkboxes = $$("#dispositions input[type=checkbox]")
        checkboxes.forEach((checkbox) => {
            var id = checkbox.getAttribute('data-value');
            let platforms = document.querySelectorAll("div[data-disposition-id='" + id + "']")
            platforms.forEach((platform) => {
                if(checkbox.checked){
                    checked_disps.push(id);
                    platform.classList.remove('hide')
                }else{
                    platform.classList.add('hide')
                }
            });
        });
        page.updateAlertStatusChart(page.config.group_id, checked_disps, page.alertStatusChart);
    }

    page.updateAlertStatusChart = (group_id, checked_disps, statusChart) => {
        const disp_id_string = checked_disps.join(',');
        $.ajax({
            url: django_js_urls['update_alert_by_disposition'],
            method: 'POST',
            data: {
                'group_id': group_id,
                'alert_disposition': disp_id_string
            },
            success: function(resp) {
                if (resp.alerts_arr) {
                    const sevTotal = resp.alerts_arr.reduce((a, b) => { return a + b })
                    const newMax = (sevTotal >= 25 ? sevTotal + 5 : 25)
                    resp.alerts_arr.map((value, index) => {
                        statusChart.series[index].setData([value]);
                    });
                    statusChart.yAxis[0].update({
                        max: newMax
                    });
                }
            }
        });
    }

    win.Pages = win.Pages || {};
    win.Pages.SystemOverview = page;

})(window);