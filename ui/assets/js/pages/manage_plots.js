;(function (win) {
    const page = {
        config: null
    };


    let currentUserId

    page.init = (config) => {
        page.config = config
        $(window).on('unload', function() {
            var plotTablePage = plotsTable.page();
            var searchText = query('.dataTables_filter input').value
            localStorage.setItem("plotPageNumber", plotTablePage);
            localStorage.setItem("searchText", searchText);
        });

        // Define this globally to dynamically update
        var platform_id =  Common.getParameterByName('platform_id', window.location.href) || 'no asset';
        var plot_success =  Common.getParameterByName('saved', window.location.href) || false;
    

        currentUserId = JSON.parse(query('#user_id').textContent);

        var plotsTable = null;

        

        
        if (platform_id !== '') {
            query('.asset-select').value = platform_id;
        }

        if (plot_success) {
            Common.add_success_message("Plot successfully saved");
        }

        plotsTable = $("#plots").DataTable({
            dom: 'frtip',
            pageLength: 25,
            order: ([1,'asc']),
            paging: true,
            select: true,
            searching: true,
            ordering: true,
            info: true,
            ajax: {
                    url: '/plot_list/ajax/plot_list',
                    method: 'POST',
                    data: function(d) {
                        d.asset_id = $(".asset-select").val()

                        let checked_disps = []
                        $("#dispositions input[type=checkbox]").each(function(){
                            const id = $(this).attr('data-value');
                            $("div[data-disposition-id=" + id + "]").toggleClass('hide', !$(this).is(":checked"));
                            if ($(this).is(":checked")) {
                                checked_disps.push(id)
                            }
                        })
                        let checked_disps_sql_list = checked_disps.join(',')
                        d.asset_disposition = checked_disps_sql_list
                    }
            },
            columns: [
                {
                    title: "",
                    data: "id",
                    checkboxes: {
                        selectRow: true,
                    }
                },
                {
                    data: "id",
                    visible: false
                },
                {
                    title: "Name",
                    data: "name"
                },
                {
                    title: "Is Shared",
                    data: "is_global"
                },
                {
                    data: "platform_id",
                    visible: false,
                    searchable: false
                },
                {
                    title: "Platform",
                    data: "platform_name",
                    render: Common.platform_link_creator
                },
                {
                    data: "asset_id",
                    visible: false,
                    searchable: false
                },
                {
                    title: "Asset",
                    data: "asset_name",
                    render: Common.asset_link_creator
                },
                {
                    title: "Deployment",
                    data: "deployment_code"
                },
                {
                    data: "deployment_id",
                    visible: false,
                    searchable: false
                },
                {
                    title: "Date Created",
                    data: "created",
                    render: Common.dt_format_date_time
                },
                {
                    title: "User",
                    data: "user"
                },
                {
                    data: "id",
                    render: function(data, type, row) {
                        if (!canClone(row.is_global == "Yes", row.user_id))
                            return "";

                        var url = '/asset/clone_plot/' + row.asset_id +'/' + row.deployment_code + '/' + data;

                        return "<a href='" +  url + "'>Clone</a>";
                    }
                },
                {
                    data: "id",
                    render: function(data, type, row) {
                        var text = canEdit(row.is_global == "Yes", row.user_id) ? "Edit" : "View";
                        var url = '/asset/edit_plot/' + row.asset_id + '/' + row.deployment_code + '/' + data +
                                '?from=admin_manager';

                        return "<a href='" + url + "'>" + text +"</a>";
                    }
                },
                {
                    data: "id",
                    render: function(data, type, row) {
                        if (!canDelete(row.is_global == "Yes", row.user_id))
                            return "";

                        return "<a href='#' data-id='" + data + "' class='delete-link'>Delete</a>"
                    }
                }
            ]
        })

        $('#plots').on( 'order.dt', function () {
            const $table = $('#plots').DataTable();
            const order = $table.order();

            if (order && order.length == 1) {
                const $column_id = 7 // Deployment is column 7

                // Only sort by deployment when sorting by 6-Asset, 4-Platform
                if ([6, 4].includes(order[0][0])) {
                    $table.order([[order[0][0], order[0][1]], [$column_id, order[0][1]]]).draw();
                }
            }
        });

        $('#plots tbody').on('click', '.delete-link', function() {
            if (!confirm('Are you sure you want to delete this plot?'))
                return;

            const plotId = $(this).data("id");

            if (!PlotCommon.permanentlyDeletePlot(plotId)) {
                Common.add_failed_message('Failed to delete plot.');
                return;
            }

            plotsTable.ajax.reload();
            Common.add_success_message('Plot deleted successfully.');
        });

        query('#addplot').addEventListener('click', function() {
            var url = django_js_urls['edit_plot']
                    .replace('assetid','asset=none')
                    .replace('deploymentcode','deployment=none');

            window.location.href = url + '?type=create&from=admin_manager';
        });

        query('.asset-select').addEventListener('change', function(){
            var updatedUrl = django_js_urls['manage_plots'] + "?platform_id=" + this.value

            window.history.replaceState(null, null, updatedUrl);

            plotsTable.ajax.reload();
        })

        query("#dispositions input[type=checkbox]").addEventListener('click', function(){
            plotsTable.ajax.reload()
        });

        query('#btn-export-selected').addEventListener('click', function(e) {
            var target_entity_ids = [];
            var data = plotsTable.rows('.selected').data().toArray();
            if(data.length == 0 ){
                    alert('Please select at least one plot to export.');
                    return;
            }

            var csrftoken = Common.getCookie('csrftoken');
            var inputs = `<input type="hidden" name="csrfmiddlewaretoken" value="${csrftoken}">`;
            for(var i=0;i<data.length; i++){
                inputs += '<input type="hidden" name="target_entity_id" value="'+data[i].id+'">'
            }           
            var baseURL = django_js_urls['export_plots'];

            Common.add_success_message("Plots exported to json file.");
            let div = document.createElement('div');
            div.innerHTML = '<form id="export_form" action="'+baseURL+'" method="POST">'+inputs+'</form>'
            document.body.appendChild(div);
            query("#export_form").submit();
            plotsTable.rows().deselect();
            

        });

    }

    function canEdit(isGlobal, userId)
    {
            if (isGlobal && !page.config.editShared) return false;
            if (!isGlobal && currentUserId !== userId && !page.config.editOthers) return false;

            return true;
        }

    function canDelete(isGlobal, userId)
    {
        if (isGlobal && !page.config.deleteShared) return false;
        if (!isGlobal && currentUserId !== userId && !page.config.deleteOthers) return false;

        return true;
    }

    function canClone(isGlobal, userId)
    {

        if (isGlobal && !page.config.cloneShared) return false;
        if (!isGlobal && currentUserId !== userId && !page.config.cloneOthers) return false;

        return true;
    }

    win.Pages = win.Pages || {};
    win.Pages.ManagePlots = page;
})(window);