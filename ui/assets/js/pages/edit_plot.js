
;(function (win) {
    const page = {
        config: null
    };
    
    let counter=0;
    let var_counter=0;

    page.init = (config) => {
        page.config = config;
        PlotCommon.setup_time_window();
        // pre-fill asset and variable pickers if possible
        if (page.config.asset.id) {

            PlotCommon.assetsObject.base_asset_id = parseInt(page.config.asset.id);
            PlotCommon.assetsObject.base_deployment_code = page.config.deployment.code;

            query("#assetpicker-value").value = page.config.asset.id;

            if (page.config.asset.name !== page.config.platform.name) {
                query("#assetpicker-name").textContent =`${page.config.asset.name} on ${page.config.platform.name} ${page.config.deployment.code }`;
            } else {
                query("#assetpicker-name").textContent = `${page.config.asset.name} ${page.config.deployment.code }`;
            }

            query("#varpicker-value").value = page.config.asset.id;
            if (page.config.asset.name !== page.config.platform.name) {
                query("#varpicker-name").textContent = `${page.config.asset.name} on ${page.config.platform.name} ${page.config.deployment.code }`;
            } else {
                query("#varpicker-name").textContent = `${page.config.asset.name} ${page.config.deployment.code }`;
            }

            PlotCommon.assetsObject.asset_id = PlotCommon.assetsObject.base_asset_id;
            PlotCommon.assetsObject.asset_name = page.config.asset.name;
            PlotCommon.assetsObject.base_asset_name = page.config.asset.name;
            PlotCommon.assetsObject.deployment_code = page.config.deployment.code;

            let xvarArr = page.config.xvarArr
            let yvarArr = page.config.yvarArr

            PlotCommon.variablesObject.variable_id_array_x = xvarArr.map(function(xv) {return String(xv.id)});
            PlotCommon.variablesObject.variable_name_array_x = xvarArr.map(function(xv) {return xv.name});
            PlotCommon.variablesObject.variable_id_array_y = yvarArr.map(function(yv) {return String(yv.id)});
            PlotCommon.variablesObject.variable_name_array_y = yvarArr.map(function(yv) {return yv.name});

            // update hidden field for later use with form submission
            query('#id_variables_x').value = String(PlotCommon.variablesObject.variable_id_array_x)
            query('#id_variables_y').value = String(PlotCommon.variablesObject.variable_id_array_y)
        }

        query('#assetpicker-button').style.margin = 0;
        query('#varpicker-button').style.margin = 0;

        let plot_success = Common.getParameterByName('saved', window.location.href) || false;

        Common.init_jquery_select2();

        if (plot_success) {
            Common.add_success_message("Plot successfully saved");
        }
        if (PlotCommon.assetsObject.asset_id) {
            PlotCommon.getMenuVariables(PlotCommon.assetsObject.asset_id, page.config.userId)
        }

        query('#id_is_global').addEventListener('change', isGlobalChange)  

        query("#assetpicker-name").addEventListener('asset-selected', function() {
            // remove the warning css
            query('.asset-chooser').classList.remove('invalid-input');
            query('.asset-chooser-text').style.color='#73879C';

            PlotCommon.assetsObject.base_asset_id = parseInt(query('#assetpicker-value').value);
            // PlotCommon.assetsObject.base_deployment_code = query('#assetpicker-value').dataset.deployment;
            //This is magically getting the data-deployment even though it is not present on the element, so the above vanilla does not work
            PlotCommon.assetsObject.base_deployment_code = $('#assetpicker-value').data('deployment');

            // update the url
            let updated_url = django_js_urls['edit_plot']
                    .replace('assetid',PlotCommon.assetsObject.base_asset_id)
                    .replace('deploymentcode',PlotCommon.assetsObject.base_deployment_code) +
                    '?type=create&variables=&from=' + page.config.from_where;

            window.history.replaceState(null, null, updated_url);
        });

        query("#varpicker-name").addEventListener('asset-selected', function() {
            query('.variable-chooser').classList.remove('invalid-input');
            query('.variable-chooser-text').style.color='#73879C';

            // make sure plot button is enabled
            query('#plot-button').classList.remove('disabled');

            PlotCommon.assetsObject.asset_id = parseInt(query('#varpicker-value').value);
            PlotCommon.assetsObject.deployment_code = $('#varpicker-value').data('deployment');
            PlotCommon.assetsObject.asset_name = $('#varpicker-value').data('name');

            let split_name = PlotCommon.assetsObject.asset_name.split(' ');
            if (split_name.indexOf('on')>-1) {
                PlotCommon.assetsObject.asset_name = split_name[split_name.indexOf('on')+1];
            } else {
                PlotCommon.assetsObject.asset_name = split_name[0];
            }

            // update plot button attribute
            query('#plot-button').setAttribute('data-deployment-id', PlotCommon.assetsObject.deployment_code)
            query('#plot-button').setAttribute('data-asset-id', PlotCommon.assetsObject.asset_id)

            PlotCommon.getMenuVariables(PlotCommon.assetsObject.asset_id, page.config.userId)
        });

        let init_plot_class = query('#id_plot_class').value;
        PlotCommon.variablesObject.plot_class = Object.keys(PlotCommon.plot_class_dict).filter(function(key) {
            return PlotCommon.plot_class_dict[key] === Number(init_plot_class)})[0];

        const var_list_listener = function(e) {
            if (e.target && !e.target.matches("a")) return
            
            let axis_list = this.closest('div[id^=variables_list_]').getAttribute('id');
            
            if (axis_list === 'variables_list_x') {
                let parent = 'plot-variable-x';
            } else {
                let parent = 'plot-variable-y';
            }
            let variable_id = String(e.target.dataset.varId);

            let optionObj = {val: variable_id, text: e.target.nextElementSibling.textContent, parent: parent}

            if (axis_list === 'variables_list_x') {
                PlotCommon.updateMultiselect_x(optionObj, false)
            } else {
                PlotCommon.updateMultiselect_y(optionObj, false)
            }
        }


        query('[id^=variables_list_y]').addEventListener('click', var_list_listener);
        query('[id^=variables_list_x]').addEventListener('click', var_list_listener);

        query('.update-plot').addEventListener('click', function() {
            if (!PlotCommon.assetsObject.asset_id) {
                alert('Select a variable source')
                return
            }
            update_plot_data();
        });

        query('.multiselect.dropdown-toggle')?.addEventListener('click',function() {
            if (!PlotCommon.assetsObject.asset_id) {
                alert('Select a variable source')
            }
        })

        // Make form read only
        if (page.config.is_readonly) {
            let elements = document.querySelectorAll('#id_name, #id_plot_type, #id_yaxis_orientation, #id_timeframe, #id_plot_relative_to, #plot-variable-x, #plot-variable-y')
            elements.forEach((ele) => {
                ele.setAttribute('disabled', true);
            });
            let hide_elements = document.querySelectorAll('.delete-variable, #varpicker-button, #assetpicker-button')
            hide_elements.forEach((ele) => {
                ele.style.display = 'none';
            });
        }

        if(page.config.asset.id!=''){
            let x_var_ids = page.config.xvarArr.map(function(xv) {return xv.id});
            let y_var_ids = page.config.yvarArr.map(function(yv) {return yv.id});
            let global_str = page.config.is_global
            let global = global_str === 'True' ? 'global_True' : 'global_False'
            let plot_type = query('#id_plot_type').options[query('#id_plot_type').selectedIndex].textContent.toLowerCase()
            let plot_class_val = query('#id_plot_class').value  // (this will be hidden)

            let plot_class = Object.keys(PlotCommon.plot_class_dict).filter(function(key) {
                return PlotCommon.plot_class_dict[key] === Number(plot_class_val)})[0];
            let y_direction = query('#id_yaxis_orientation').value.toLowerCase() === 'true' ? true : false;

            let start_date = page.config.plot.start_date;
            let end_date = page.config.plot.end_date;

            let time_window = page.config.plot.plot_time_range;

            let plot_relative_to = page.config.plot.plot_relative_to;
            
            let create_view = Common.getParameterByName('type', window.location.href) || false;
            if(!create_view){
                PlotCommon.update_plot(
                    page.config.asset.id,
                    page.config.deployment.id,
                    x_var_ids,
                    y_var_ids,
                    time_window,
                    'plot-preview-container',
                    query('#id_name').value,
                    global,
                    plot_type,
                    plot_class,
                    y_direction,
                    start_date,
                    end_date,
                    plot_relative_to
                );
            }
            isGlobalChange();
        }

        query('#id_owner').addEventListener('change', function() {            
            if (!query('#id_owner').value) {
                let children = query('label[for="id_owner"]').closest('.control-label').children
                for (const child of children) {
                    child.classList.add('invalid-input');
                };
                let div = document.createElement('p');
                div.innerHTML = '<p style="font-style: italic" class="invalid-color">' + 'Select an owner' + '</p>';
                query('#owner-div').appendChild(div);
            } else {
                let children = query('label[for="id_owner"]').closest('.control-label').children
                for (const child of children) {
                    child.classList.remove('invalid-input');
                };
                if(query('#owner-div>p')){
                    query('#owner-div')?.removeChild(query('#owner-div>p'));
                }
            }
        })


        if (page.config.is_readonly){
            $("#custom-save-form").submit(function(e){
                if (!$("#custom-save-form").valid() || PlotCommon.variablesObject.variable_id_array_x.length === 0
                || PlotCommon.variablesObject.variable_id_array_y.length === 0 || !query('#id_owner').value) {
                    e.preventDefault();
                    if (PlotCommon.variablesObject.variable_id_array_x.length === 0 &&
                        !query('label[for="id_variables_x"]').classList.contains('invalid-input')) {
                        PlotCommon.toggleMenuErrorText();
                    }
                    if (PlotCommon.variablesObject.variable_id_array_y.length === 0 &&
                        !query('label[for="id_variables_y"]').classList.contains('invalid-input')) {
                        PlotCommon.toggleMenuErrorText();
                    }

                    if (!query('#id_owner').value &&
                        !query('label[for="id_owner"]').classList.contains('invalid-input')) {
                        let children = query('label[for="id_owner"]').closest('.control-label').children;
                        for (const child of children) {
                            child.classList.add('invalid-input');
                        };
                        let p = document.createElement('p');
                        p.classList.add('invalid-color');
                        p.style.fontStyle = 'italic';
                        p.innerHTML = 'Select an owner';
                        query('#owner-div').appendChild(p);

                    }
                    return false;
                }
                let dates_wrapper = query(".start_date").closest('.dates_wrapper');
                if(query('#id_plot_relative_to').value==PlotCommon.CUSTOM_TIME_FRAME){
                    if(!query('.start_date').value && !query('.end_date').value){
                        if (dates_wrapper.querySelectorAll('div[class="error-message"]').length === 0) {

                            let div = document.createElement('div');
                            div.classList.add('error-message');
                            div.innerHTML = '&nbsp;Must set either start or end date';
                            dates_wrapper.appendChild(div);
                        }
                        return false;
                    }else{
                        dates_wrapper.removeChild(dates_wrapper.querySelector('div[class="error-message"]'));
                    }
                }else{
                    dates_wrapper.removeChild(dates_wrapper.querySelector('div[class="error-message"]'));
                }
            });
        }
        $("#custom-save-form").validate({
            rules: {
                name: "required",
                variables_x: "required",
                variables_y: "required",
                timeframe: "required",
            },
            messages: {
                name: "Plot name is required",
                variables_x: "Variable(s) are required",
                variables_y: "Variable(s) are required",
                timeframe: "Timeframe required",
    
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");
                if ( element.prop("type") === "checkbox" ) {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function (element, errorClass, validClass ) {
                $(element).closest(".form-group").addClass("has-error");
                $(element).closest('input').addClass('validation-problem');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest(".form-group").removeClass("has-error");
                $(element).closest('input').removeClass('validation-problem');
            }
        });
    
    
        query("#add-another-button")?.addEventListener('click', function(e){
            e.preventDefault();
            if($("#custom-save-form").valid() || PlotCommon.variablesObject.variable_id_array_x.length < 0){
                let url = window.location.href;
                url = url.replace("type=create", "type=create&add_another=true");
                window.history.replaceState(null, null, url);
                $("#custom-save-form").first().submit();
            }
        });
    
        query("#cancel-button").addEventListener('click', function(e){
            e.preventDefault();
    
            let from_where = page.config.from_where;
            // check if we are dealing with a plot page
            if (from_where.indexOf('plot_page') >-1) {
                from_where = 'plot_page';
                let plot_page_number = from_where.replace('plot_page','');
            }
    
            switch (from_where) {
            case 'asset':
                if (page.config.asset){
                    window.location.href = django_js_urls['edit_plot'].replace('assetid',PlotCommon.assetsObject.base_asset_id).replace('deploymentcode',PlotCommon.assetsObject.base_deployment_code)
                }
                break;
    
            case 'admin_manager':
                    window.location.href =  django_js_urls['manage_plots']
                break;
    
            case 'plot_page':
                    window.location.href = django_js_urls['plot_page'].replace('page_id',plot_page_number);
                break;
    
            default:
                if (page.config.asset){
                    window.location.href = django_js_urls['edit_plot'].replace('assetid',PlotCommon.assetsObject.base_asset_id).replace('deploymentcode',PlotCommon.assetsObject.base_deployment_code)
                }
    
            }
        });
        let check_if_empty_then_update =  function() {
            if((query('#plot-preview-container').innerHTML.trim()) !== '') {
                update_plot_data();
            }
        }
        query("#id_plot_type").addEventListener('change', check_if_empty_then_update);
        query("#id_yaxis_orientation").addEventListener('change', check_if_empty_then_update);
        query("#id_timeframe").addEventListener('change', check_if_empty_then_update);
        query("#id_plot_relative_to").addEventListener('change', check_if_empty_then_update);
    
   
    }


    // toggle the owner div on/off depending on if the plot is global or not
    function isGlobalChange() {
        if (query('#id_is_global').value === 'True') {
            query('#owner-div').classList.add('not-visible'); // change this class
            query('#id_owner').value = 1
        } else {
            query('#owner-div').classList.remove('not-visible');
        }
    }

    function checkPlottingParams() {
        let plotting_param_error = false;

        let noneSelected_x = PlotCommon.variablesObject.variable_id_array_x.length === 0;
        let noneSelected_y = PlotCommon.variablesObject.variable_id_array_y.length === 0;

        let tooManySelected = PlotCommon.variablesObject.plot_class === 'Comparison' &&
            PlotCommon.variablesObject.variable_id_array_y.length > 1;

        if (noneSelected_x || noneSelected_y || tooManySelected) {
            plotting_param_error = true;
        }

        return plotting_param_error
    }

    

    function update_plot_data() {
        let plot_id = 'plot-preview-container';
        let asset_id = query('#plot-button').getAttribute('data-asset-id');
        let deployment_id = query('#plot-button').getAttribute('data-deployment-id');
        let plotname = query('#id_name').value || 'Custom Plot'
        let plot_type = query('#id_plot_type').options[query('#id_plot_type').selectedIndex].textContent.toLowerCase()
        let plotglobal = 'global_' + query('#id_is_global').value;
        let plot_class_val = query('#id_plot_class').value;
        let y_direction = query('#id_yaxis_orientation').value.toLowerCase() === 'true' ? true : false;

        let time_window_el = query("#id_timeframe");
        let plot_relative_to = query("#id_plot_relative_to").value
        let start_date = undefined;
        let end_date = undefined;
        let time_window = undefined;

        let dates_wrapper = query(".start_date").closest('.dates_wrapper');
        if(plot_relative_to==PlotCommon.CUSTOM_TIME_FRAME){
            if(!query(".start_date").value && !query(".end_date").value){
                if (!dates_wrapper.querySelector('span[class="error-message"]')) {
                    let div = document.createElement('div');
                    div.classList.add('error-message');
                    div.innerHTML = '&nbsp;Must set either start or end date';
                    dates_wrapper.appendChild(div);
                }
                return;
            }else{
                if(dates_wrapper.querySelector('div[class="error-message"]')){
                    dates_wrapper.removeChild(dates_wrapper.querySelector('div[class="error-message"]'));
                }
            }
            start_date = query("#id_start_date").value;
            end_date = query("#id_end_date").value;
        }else{
            if(dates_wrapper.querySelector('div[class="error-message"]')){
                dates_wrapper.removeChild(dates_wrapper.querySelector('div[class="error-message"]'));
            }
            time_window = PlotCommon.time_choices_dict[time_window_el.value]['days']
        }

        // clear #plot-preview-container innerHTML and don't start the spinner if no variables selected
        if (plot_id === 'plot-preview-container') {
            let plotting_param_errors = checkPlottingParams();
            if (plotting_param_errors) {
                // this is a check.. just in case a user tries to plot without updating the menus
                if (PlotCommon.variablesObject.variable_id_array_x.length === 0 ||
                    PlotCommon.variablesObject.variable_id_array_y.length === 0) {
                    PlotCommon.toggleMenuErrorText();
                }

                // clear #plot0 innerHTML and don't start the spinner if a selection is missing
                query('#plot-preview-container').innerHTML = ''
                return
            }
        }

        let plot_class = Object.keys(PlotCommon.plot_class_dict).filter(function(key) {
            return PlotCommon.plot_class_dict[key] === Number(plot_class_val)})[0];

        let x_var;
        let y_var;
        switch (plot_class) {
            case 'Timeseries':
                // need to do some checking to make sure we have selected items in both menus
                x_var = 'time';
                y_var =  query('#id_variables_y').value;

                PlotCommon.update_plot(asset_id, deployment_id, x_var, y_var, time_window, plot_id, plotname, plotglobal,
                    plot_type, plot_class, y_direction, start_date, end_date, plot_relative_to);
                break;
            case 'Comparison':
                // grab both the x-axis and y-axis variable
                x_var = query('#id_variables_x').value;
                y_var = query('#id_variables_y').value;

                // dont allow a variable to be plotted against itself
                if (x_var === y_var) {
                    alert('Plotting a variable against itself is not allowed');
                    return
                }

                // TODO: check if the user is trying to do a comparison plot with variables from different assets
                // (use asset_id saved as data attr in menu items --not hooked up yet--  prob the easiest way to do this)

                PlotCommon.update_plot(asset_id, deployment_id, x_var, y_var, time_window, plot_id, plotname, plotglobal,
                    'scatter', plot_class, y_direction, start_date, end_date, plot_relative_to);
                break
            default:
                console.log('plot class was not specified please investigate')
        }
    }



    win.Pages = win.Pages || {};
    win.Pages.EditPlot = page;
})(window);