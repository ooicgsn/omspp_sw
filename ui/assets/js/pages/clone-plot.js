;(function (win) {
    const page = {
        plotId: null
    };

    page.init = (plotId) => {
        page.plotId = plotId;

        page.setupEvents();
    }

    page.clone = () => {
        let selectedAssetIds = [];

        $('input:checkbox[name=target_asset]:checked').each(function() {
            selectedAssetIds.push(+this.value);
        });

        if (selectedAssetIds.length === 0) {
            alert('Please select at least one asset.');
            return;
        }

        query('#loading-panel').classList.toggle('hidden', false);
        query('#clone-details').classList.toggle('hidden', true);

        $.ajax({
            url: '/plot/execute_clone_plot',
            method:'POST',
            data: {
                'plot_id': page.plotId,
                'target_asset_ids': selectedAssetIds.join(',')
            },
            success: function(data) {
                if (!data) {
                    Common.add_failed_message('Failed to clone plot.');
                    return;
                }

                if (data.success && data.successful_assets.length > 0) {
                    Common.add_success_message('Plot has been successfully cloned to ' + data.successful_assets.length + ' asset(s).' );
                } else {
                    Common.add_failed_message('Plot was not successfully cloned. Please verify assets are compatible.');
                }

                query('#loading-panel').classList.toggle('hidden', true);
                query('#complete-details').classList.toggle('hidden', false);

                let successful_assets = data.successful_assets;
                if (successful_assets.length > 0) {
                    const $success_table_body = $('#success-assets-table-body');

                    for (let i = 0; i < successful_assets.length; i++) {
                        const asset = successful_assets[i];

                        $success_table_body.append("<tr><td><a href='/asset/edit_plot/"
                            + asset.AssetID + "/"
                            + asset.DeploymentCode + "/"
                            + asset.PlotID + "'>"
                            + asset.AssetName
                            + "</a></td></tr>");
                    }

                    query('#success-table').classList.toggle('hidden', false);
                }

                let failed_assets = data.failed_assets;
                if (failed_assets.length > 0) {
                    let $failed_table_body = $('#failed-assets-table-body');
                    for (let i = 0; i < failed_assets.length; i++) {
                        $failed_table_body.append("<tr><td>" + failed_assets[i].Message + "</td></tr>");
                    }

                    $('#failed-table').toggleClass('hidden', false);
                }
            }
        });
    }

    page.setupEvents = () => {
        const $dispositions = document.querySelectorAll('#dispositions input[type=checkbox]');
        $dispositions.forEach(($disposition) => {
            $disposition.addEventListener('click', (e) => {
                $("#dispositions input[type=checkbox]").each(function(){
                    const id = $(this).attr('data-value');

                    $("tr[data-disposition-id=" + id + "]").toggleClass('hide', !$(this).is(":checked"));

                    if (!$(this).is(":checked")) {
                        $("tr[data-disposition-id=" + id + "] input[type=checkbox]").prop('checked', false);
                    }
                });
            });
        });

        query('#btn-clone').addEventListener('click', (e) => {
            e.preventDefault();
            page.clone();
        });
        query('#btn-clone-bottom').addEventListener('click', (e) => {
            e.preventDefault();
            page.clone();
        })
    }

    win.Pages = win.Pages || {};
    win.Pages.ClonePlot = page;
})(window);