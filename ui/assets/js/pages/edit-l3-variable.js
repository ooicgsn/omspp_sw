;(function (win) {
    const page = {
        assetId: null,
        assetName: null,
        platformName: null,
        deploymentCode: null,
    };

    page.init = (assetId, assetName, platformName, deploymentCode) => {
        page.assetId = assetId;
        page.assetName = assetName;
        page.platformName = platformName;
        page.deploymentCode = deploymentCode;

        if (assetId) {
            page.initializeAssetPicker();
        }

        page.setupEvents();
    }

    page.initializeAssetPicker = () => {
        const platformText = page.assetName !== page.platformName ? `on ${page.platformName} ` : "";

        query("#assetpicker-value").value = page.assetId;
        query("#assetpicker-name").textContent = `${page.assetName} ${platformText}(${page.deploymentCode})`;

        page.onAssetSelected();
    }

    page.onAssetSelected = (e) => {
        const assetId = query("#assetpicker-value").value;
        const url = `/variable/ajax/variable_list_by_asset/${assetId}?exclude_custom=True`;

        $.ajax({
            url: url,
            method: 'GET',
            success: (response) => {
                updateVariables(response);
            }
        });
    }

    page.setupEvents = () => {
        query("#assetpicker-name")
            .addEventListener("asset-selected", page.onAssetSelected);
    }

    win.Pages = win.Pages || {};
    win.Pages.EditL3Variable = page;
})(window);