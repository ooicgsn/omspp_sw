;(function (win) {
    const page = {
        config: null
    };
    page.init = (config) => {
        page.config = config
        page.drawMap();
    }

    page.drawMap = () => {
        // Create the map
        let map = L.map("map").setView(page.config.fallback_position, 6);

        // Create the ESRI World Ocean Base tile layer
        let esriOceanBaseMap = L.tileLayer('https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}', {
            attribution: '<a href="https://www.arcgis.com/home/item.html?id=67ab7f7c535c4687b6518e6d2343e8a2">ESRI World Ocean Basemap</a>',
            transparent: true,
            bounceAtZoomLimits: false
        })

        // Create the ESRI World Ocean Reference tile layer
        let esriOceanReference = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Ocean/World_Ocean_Reference/MapServer/tile/{z}/{y}/{x}', {
            attribution: '<a href="https://www.arcgis.com/home/item.html?id=94329802cbfa44a18f423e6f1a0b875c">ESRI World Ocean Reference</a>'
        });

        // Add the layers
        map.addLayer(esriOceanBaseMap);
        map.addLayer(esriOceanReference);

        // Add a scalebar
        // L.control.scale(position="bottomleft", maxWidth=1000).addTo(map)

        page.circleMarkerStyle = {
            radius: 5,
            fillColor: '#a6a6a6',
            color: '#000000',
            weight: 0.1,
            opacity: 1,
            fillOpacity: 1
        };

        // marker popup template
        let popupTemplate=`<div>
            <a href="{overview-href}">
                <p style="margin: 0">
                    <b>
                        <span class="platform-code">{platform-code}</span>
                        <span class="platform-deployment-code">({platform-deployment-code})</span>
                    </b>
                </p>
            </a>
            <hr style="margin: 4px 0"/>
            <p style="margin: 0">
                <b>Time: </b>
                <span class="platform-last-reported-time">{platform-last-reported-time}</span>
            </p>
            <p style="margin: 0">
                <b>Latitude: </b>
                <span class="platform-latitude">{platform-latitude}</span>
            </p>
            <p style="margin: 0">
                <b>Longitude: </b>
                <span class="platform-longitude">{platform-longitude}</span>
            </p>
            <p class="alerts" style="margin: 0">
                <b>Active Alerts: </b>
                <span class="platform-alerts">{platform-alerts}</span>
            </p>
        </div>`;

        var allMooringsGroup = L.featureGroup([]);
        var activeMooringGroup = L.featureGroup([]);

        // add the initially empty layer to be filled while looping
        allMooringsGroup.addTo(map);

        let time_position_array;

        let popupClone;
        for (var i=0; i<page.config.moorings.length; i++) {

            let watch_circle = page.config.moorings[i].watch_circle;

            L.circle([watch_circle[0], watch_circle[1]], watch_circle[2],
                {color:'#FF0000', weight:1, opacity:0.5, fillColor: '#FF0000', fillOpacity:0.2}).addTo(map)

            time_position_array = page.config.moorings[i].time_position_array;

            for (var j=0; j<time_position_array.length; j++) {
                // create the marker and popup content
                var coords = [time_position_array[j][1], time_position_array[j][2]];

                // var popupClone = popupClone.querySelectorpopupTemplate).clone();
                // var popupClone = new DOMParser().parseFromString(popupTemplate, "text/html");
                popupClone = popupTemplate

                popupClone = popupClone.replace('{platform-code}', page.config.moorings[i].platform_code)
                popupClone = popupClone.replace('{platform-deployment-code}', page.config.moorings[i].deployment_code)
                popupClone = popupClone.replace('{platform-last-reported-time}', formatUTCdate(new Date(time_position_array[j][0])))
                popupClone = popupClone.replace('{platform-latitude}', coords[0].toFixed(4))
                popupClone = popupClone.replace('{platform-longitude}', coords[1].toFixed(4))
                popupClone = popupClone.replace('{platform-alerts}', page.config.moorings[i].alerts)
                
                let overview_url = django_js_urls['asset_overview'].replace('assetid', page.config.moorings[i].platform_id).replace('deploymentcode', page.config.moorings[i].deployment_code)
                
                popupClone = popupClone.replace('{overview-href}', overview_url)

                if (j<time_position_array.length-1) {
                    // use a different marker for previous positions
                    popupClone = popupClone.replace('<p class="alerts" style="margin: 0">', '<p class="alerts" style="margin: 0" hidden>')

                    // marker factory
                    var headMarker = colorCircleMarker(time_position_array[j][0], coords);
                } else {
                    var present_time = new Date().getTime();
                    let head_opacity;
                    // change opacity if data is older than 12 hours
                    if ((present_time - time_position_array[j][0]) > (12*60*60*1000)) {
                        head_opacity = 0.5;
                    } else {
                        head_opacity = 1.0;
                    }
                    var headMarker = L.marker(coords, {opacity: head_opacity});
                }
                headMarker.bindPopup(popupClone)
                activeMooringGroup.addLayer(headMarker)
            }
        }

        allMooringsGroup.addLayer(activeMooringGroup)
        if (Object.keys(activeMooringGroup.getBounds()).length > 0) {
            map.fitBounds(activeMooringGroup.getBounds(),{maxZoom: 7, padding: [100, 100]});
        } else {
            document.getElementById('no-data-tag').style.display='block';
        }

    } 


    function formatUTCdate(d) {
        let year = d.getUTCFullYear().toString().substr(2,2);
        let month = (d.getUTCMonth()+1);
        let day = d.getUTCDate();
        let hour_24 = d.getUTCHours();
        let hour = ((hour_24 + 11) % 12 + 1);
        let minutes_raw = d.getUTCMinutes();
        let minutes = minutes_raw < 10 ? '0' + minutes_raw : minutes_raw;
        let identifier = hour_24 >= 12 ? 'pm' : 'am';
        let formatted_time = month + '/' + day + '/' + year + ' ' + hour + ':' + minutes + identifier + ' ' + 'UTC';
        return formatted_time
    }

    function colorCircleMarker(input_time, coords) {
        let present_time = new Date().getTime();
        let color;
        // greater|equal to 0 and less than 6
        if ((present_time - input_time) >= (0*60*60*1000) && (present_time - input_time) < (6*60*60*1000)) {
            color = '#333333';
        // greater|equal to 6 and less than 12
        } else if ((present_time - input_time) >= (6*60*60*1000) && (present_time - input_time) < (12*60*60*1000)) {
            color = '#595959';
        // greater|equal to 12 and less than 18
        } else if ((present_time - input_time) >= (12*60*60*1000) && (present_time - input_time) < (18*60*60*1000)) {
            color = '#808080';
        // greater|equal to 18 and less than 24
        } else if ((present_time - input_time) >= (18*60*60*1000) && (present_time - input_time) < (24*60*60*1000)) {
            color = '#a6a6a6';
        } else if ((present_time - input_time) >= (24*60*60*1000)) {
            color = '#cccccc';
        } else {
            // default
            color = '#cccccc';
        }

        let circleMarkerStyleClone = JSON.parse(JSON.stringify(page.circleMarkerStyle))

        // update the marker fill color
        circleMarkerStyleClone.fillColor = color;
        let headMarker = L.circleMarker(coords, circleMarkerStyleClone);
        return headMarker
    }

    win.Pages = win.Pages || {};
    win.Pages.MooringLocations = page;

})(window);