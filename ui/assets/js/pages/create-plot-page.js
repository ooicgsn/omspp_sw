;(function (win) {
    const page = {
        config : null,
    };

    page.init = (config) => {

        page.config = config;
        page.setupEvents();
    }

    page.setupEvents = () => {

        let plot_page_id = 0;
        document.getElementById('cancel').addEventListener('click', function() {
            let referrer_url = document.referrer;
            if(referrer_url.indexOf('asset/overview/') != -1) {
                let asset_id = page.config.asset.id;
                let code = page.config.deployment.code;
                let url = django_js_urls['asset_overview'].replace('assetid', asset_id).replace('deploymentcode', code);
                location.href = url;
            } else if(referrer_url.indexOf('asset/plot_page/') != -1) {
                let plotPageStrIndex = document.referrer.indexOf('plot_page');
                let plot_page_num = page.config.plot_page_id ? page.config.plot_page_id :
                    document.referrer.slice(plotPageStrIndex).split('/')[1];
                let url = django_js_urls['plot_page'].replace('page_id', plot_page_num);
    
                location.href = url;
            }
        });


        document.getElementById('add_plot').addEventListener('click', () => {
            let selected = Array.prototype.slice.call(document.getElementById("plot_select").selectedOptions);
            for (let ele of selected) {
                document.getElementById('selected_plots').appendChild(ele)
            };
        });
    
        document.getElementById("plot_select").addEventListener('dblclick', (event) => {
            let ele = event.target
            if (ele.tagName === "OPTION") {
                document.getElementById('selected_plots').appendChild(ele);
            }
        });
    
        document.getElementById('remove_plot').addEventListener('click', () => {
            let selected = Array.prototype.slice.call(document.getElementById("selected_plots").selectedOptions);
            for (let ele of selected) {
                let asset_id = ele.getAttribute('data-asset-id')
                if(asset_id==document.getElementById("assetpicker-value").value){
                    document.getElementById('plot_select').appendChild(ele);
                }else{
                    document.getElementById('selected_plots').removeChild(ele);
                }
            };
        });
        
        document.getElementById("selected_plots").addEventListener('dblclick', (event) => {
            let ele = event.target
            if (ele.tagName === "OPTION") {

                let asset_id = ele.getAttribute('data-asset-id')
                if(asset_id==document.getElementById("assetpicker-value").value){
                    document.getElementById('plot_select').appendChild(ele);
                }else{
                    document.getElementById('selected_plots').removeChild(ele);
                }
            }

        });
        const config = { attributes: false, childList: true, subtree: false };
        const callback = (mutationList, observer) => {
            for (const mutation of mutationList) {
              if (mutation.type === "childList") {
                getPlots();
                break;
              } 
            }
          };
    
        const observer = new MutationObserver(callback);
        observer.observe(query("#assetpicker-name"), config);

        
        let groupDataId = 'group-' + page.config.asset.group_id;
        let platformId = 'platform-' + page.config.asset.platform_id;
        SetupCurrentSideNavMenu(groupDataId, platformId);

        document.getElementById('plot-page').addEventListener('click', function(e){
            let plot_ids = [];
            let selected_plot_els = document.querySelectorAll("#selected_plots option");

            selected_plot_els.forEach((ele) => {
                plot_ids.push(ele.value);
            });
            let plot_page_name = query("#plot-page-name").value;
            let asset_overview = page.config.asset_overview;
            if( page.config.plot_page_id !== "") // come back and rework? 
            {
                plot_page_id =  page.config.plot_page_id;
            }
            if(document.querySelectorAll("#selected_plots option").length == 0)
            {
                document.getElementById("empty_notification").innerText = "Please select a plot";
            }
            else if(plot_page_name.length > 0)
            {
                $.ajax({
                url: django_js_urls['save_plot_page'],
                method: 'POST',
                data: {
                    'plot_ids': plot_ids,
                    'plot_page_name': plot_page_name,
                    'plot_page_id': plot_page_id,
                    'asset_id': page.config.asset.id,
                    'asset_overview': asset_overview,
                },
                success:function(data) {
                    plot_page_id = data['plot_page_id'];
                    let id = data['plot_page_id'];
    
                    if(asset_overview == "True") {
                        let asset = page.config.asset.id;
                        let url = django_js_urls['asset_overview'];
                        url = url.replace('assetid', asset).replace('deploymentcode', page.config.deployment.id);
                        url = url + "?confirm=1";
                        location.href = url;
                    } else {
                        let url = django_js_urls['plot_page'].replace('page_id', id);

                        url = url + "?confirm=1";
                        location.href = url;
                    }
                    }
                });
    
            }
            else
            {
                document.getElementById("empty_notification").innerText = "Please enter a name";
            }
        });
        query('#preview-available').addEventListener('click', showPreview);
        query('#preview-selected').addEventListener('click', showPreview);

    }

    function showPreview(event) {
        let btn = event.target
        let option = btn.parentElement.querySelector("select").selectedOptions[0];
        if (!option) return;
       
        let x_vars = option.getAttribute('data-variable-x');
        let y_vars = option.getAttribute('data-variable-y');

        let asset_id = option.getAttribute('data-asset-id');
        let time_window = -1;
        let container = "plot-preview-div";

        let plot_type = option.getAttribute('data-plot-type');
        let plot_class = option.getAttribute('data-plot-class');
        let text = option.textContent;

        if (plot_class === 'Timeseries') {
            x_vars = 'time'
        }

        let y_direction = option.getAttribute('data-yaxis-direction').toLowerCase() === 'true' ? true : false;

        PlotCommon.update_plot(asset_id, page.config.deployment.id, x_vars, y_vars, time_window, container,
            text,'global_True', plot_type, plot_class, y_direction);


    }
        
          
    
    function getPlots() {
        let asset = document.getElementById("assetpicker-value").value;
        $.ajax({
            url: django_js_urls['plot_list_by_asset'],
            method: "POST",
            data: {'asset_id': asset},
            success: function(response) {
                updatePlots(response);
            }
        });
    };

    function updatePlots(response) {
        let selectPlot = document.getElementById("plot_select");

        let nodes = Array.prototype.slice.call(selectPlot.childNodes);
        nodes.forEach((child) => {
            selectPlot.removeChild(child);
        });

        let plots = response.plots;
        let selected_plot_ids = [];
        document.querySelectorAll("#selected_plots option").forEach((ele) => {
            selected_plot_ids.push(ele.value); //'" data-plot-type="' + plots[i][4]
        });

        if(plots[0]) {
            for(let i in plots) {
                if(selected_plot_ids.indexOf(String(plots[i][0])) == -1) {

                    let plot_class = Object.keys(PlotCommon.plot_class_dict).filter(function(key) {
                        return PlotCommon.plot_class_dict[key] === Number(plots[i][6])
                    })[0];

                    let option = document.createElement("option");

                    option.value =  plots[i][0];
                    option.text =  plots[i][1];
                    option.setAttribute('data-asset-id', plots[i][2]);
                    option.setAttribute('data-variable-x', plots[i][3]);
                    option.setAttribute('data-variable-y', plots[i][4]);
                    option.setAttribute('data-plot-type', (plots[i][5]).toLowerCase());
                    option.setAttribute('data-plot-class', plot_class);
                    option.setAttribute('data-yaxis-direction', plots[i][7]);

                    selectPlot.appendChild(option);
                }
            }
        } else {
            let option = document.createElement("option");
            option.value = "0"
            option.text = "No plots associated with this asset"
            selectPlot.appendChild(option);
        }
    };

    
        


    win.Pages = win.Pages || {};
    win.Pages.CreatePlotPage = page;
})(window);