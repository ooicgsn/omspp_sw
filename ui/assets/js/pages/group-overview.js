;(function (win) {
    const page = {
        config: null
    };
    page.init = (config) => {
        page.config = config;
        page.setupEvents();
    }

    page.setupEvents = () => {
        SetupCurrentSideNavMenu(page.config.groupDataId, null);

        let checkboxes = document.querySelectorAll("#dispositions input[type=checkbox]")
        checkboxes.forEach((ele) => {
            ele.addEventListener('change', function(e) {
                checkboxes.forEach((checkbox) => {
                    var id = checkbox.getAttribute('data-value');
                    let platforms = document.querySelectorAll("div[data-disposition-id='" + id + "']")
                    platforms.forEach((platform) => {
                        if(checkbox.checked){
                            platform.classList.remove('hide')
                        }else{
                            platform.classList.add('hide')
                        }
                    });
                });
            });
        });
    } 


    win.Pages = win.Pages || {};
    win.Pages.GroupOverview = page;

})(window);