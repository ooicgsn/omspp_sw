;(function (win) {
    const page = {};

    page.init = () => {
        page.setupEvents();
    }

    page.setDefaultPage = () => {
        if(!confirm('Would you like to set this as your default starting page?')) {
            return;
        }

        $.ajax({
            url: '/account/default',
            method: 'POST',
            success: () => {
                Common.add_success_message("Default starting page saved");
            }
        });
    }

    page.updateCurrentDateTime = () => {
        const current = moment.utc().format("YYYY-M-DD HH:mm:ss") + " UTC"

        document.querySelector('#current-time').textContent = current;
    }

    page.setupEvents = () => {
        query('#default-page')?.addEventListener('click', e => {
            e.preventDefault();
            page.setDefaultPage();
        });

        const $clock = document.querySelector('#current-time');
        if ($clock) {
            setInterval(page.updateCurrentDateTime, 1000);
        }
    }

    win.Pages = win.Pages || {};
    win.Pages.Shared = page;
})(window);