import './pages/shared'
import './pages/hotlist'
import './pages/overview'
import './pages/edit_plot'
import './pages/manage_plots'
import './pages/create-plot-page'
import './pages/plot-page'
import './pages/edit-trigger'
import './pages/clone-plot'
import './pages/system-overview'
import './pages/mooring-locations'
import './pages/group-overview'
import './pages/parsed-data'
import './pages/edit-asset'
import './pages/edit-platform'
import './pages/edit-l3-variable'

import PlotCommon from './plot_common'
import Common from './common'
import Sortable from 'sortablejs'

// import jQuery from 'jquery';

// window.jQuery = jQuery;

window.Sortable = Sortable;
window.PlotCommon = PlotCommon;
window.Common = Common;

window.query = document.querySelector.bind(document);
window.$$ = document.querySelectorAll.bind(document);

const csrftoken = Common.getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(".datetimepicker").datetimepicker({
    controlType: 'select',
    oneLine: true,
    timeFormat: 'hh:mmtt',
    dateFormat: "mm/dd/y"
});

$(".datepicker").datepicker({
    dateFormat: "yy-mm-dd"
});

