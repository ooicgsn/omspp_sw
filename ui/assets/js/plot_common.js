import jQuery from "jquery";

class PlotCommon {
    constructor() {}
    static custom_exporting;
    static additional_plot_exporting;
    static plot_class_dict;
    static time_choices_dict;

    static multiselectObject= { //leaving this as JQ until multiselect library is updated
        enableCaseInsensitiveFiltering: true,
        maxHeight: 275,
        nonSelectedText: 'Select variable(s) to plot',
        buttonWidth: '95%',
        buttonTitle: function(options, select) {
            let labels = [];
            options.each(function () {
                labels.push($(this).text());
            });
            return labels.join(', ');
        },
        onChange: function(option, checked) {
            let option_parent = $(option).parent().attr('id');
            let option_custom = $(option).data()['custom'];
            let optionObj = {val: option.val(), custom: option_custom, text: option.text(), parent: option_parent}
            if (option_parent === 'plot-variable-x') {
                PlotCommon.updateMultiselect_x(optionObj, checked)
            } else {
                PlotCommon.updateMultiselect_y(optionObj, checked)
            }
        }
    }   
    static assetsObject = {
        _base_asset_id: '',
        _base_deployment_code: '',
        _base_asset_name: '',
        _asset_id: '',
        _asset_name: '',
        _base_deployment_code: '',
        _deployment_code: '',
        set base_asset_name(asset_name) {
            this._base_asset_name = asset_name;
        },
        get base_asset_name() {
            return this._base_asset_name;
        },
        set asset_name(asset_name) {
            this._asset_name = asset_name;
        },
        get asset_name() {
            return this._asset_name;
        },
        set base_asset_id(id) {
            this._base_asset_id=id;
        },
        get base_asset_id() {
            return this._base_asset_id
        },
        set asset_id(id) {
            this._asset_id = id;
        },
        get asset_id() {
            return this._asset_id;
        },
        set base_deployment_code(dcode) {
            this._base_deployment_code = dcode;
        },
        get base_deployment_code() {
            return this._base_deployment_code
        },
        set deployment_code(dcode) {
            this._deployment_code = dcode;
        },
        get deployment_code() {
            return this._deployment_code
        }
    }
    static variablesObject={
        _variable_id_array_x: [],
        _variable_id_array_y: [],
        _variable_name_array_x: [],
        _variable_name_array_y: [],
        _plot_class: '',
        set variable_id_array_x(x_var) {
            for (let i=0; i<x_var.length; i++) {
                this._variable_id_array_x.push(x_var[i]);
            }
        },
        get variable_id_array_x() {
            return this._variable_id_array_x;
        },
        set variable_name_array_x(name) {
            for (let i=0; i<name.length; i++) {
                this._variable_name_array_x.push(name[i]);
            }
        },
        get variable_name_array_x() {
            return this._variable_name_array_x;
        },
        set variable_id_array_y(y_var) {
            for (let i=0; i<y_var.length; i++) {
                this._variable_id_array_y.push(y_var[i]);
            }
        },
        get variable_id_array_y() {
            return this._variable_id_array_y;
        },
        set variable_name_array_y(name) {
            for (let i=0; i<name.length; i++) {
                this._variable_name_array_y.push(name[i]);
            }
        },
        get variable_name_array_y() {
            return this._variable_name_array_y;
        },
        set plot_class(ptype) {
            this._plot_class=ptype;
        },
        get plot_class() {
            return this._plot_class;
        },
    }


    static CUSTOM_TIME_FRAME='custom_time_frame';
    static LANG = {
        CustomSave: "save custom chart to database",
        ExportImageOrData: "export chart or data",
        CancelPlot: "cancel",
        UpdatePlot: "update"
    }
    
    // default navigation object
    static default_navigation = {
        buttonOptions: {
            enabled: true,
            theme: {
                'stroke-width': 1,
                stroke: 'silver',
                r: 5,
            }
        }
    };
    // menu items for custom (non-global) chart hamburger menu
    static buildCustomContextMenuItems(standardContextMenuItems,edit_url,from_where) {
        return standardContextMenuItems.concat(
            {
                textKey: 'updatePlot',
                text: 'Edit Plot',
                onclick: function() {
                    let plotid = $(this.renderTo).attr('id').replace('plot','');
                    let editPage = edit_url.replace('plotid', plotid) + '?' + 'from=' + from_where
                    window.location.href=editPage;
                }
            },
            {
                textKey: 'deletePlot',
                text: 'Delete',
                onclick: function() {
                    if (!confirm('Are you sure you want to delete this plot?')) {
                        return;
                    }

                    const plotId = $(this.renderTo).attr('id').replace('plot','');

                    if (!PlotCommon.permanentlyDeletePlot(+plotId)) {
                        Common.add_failed_message('Failed to delete plot.');
                        return;
                    }

                    // Remove the plot from the UI
                    const panel = document.querySelector(`div[id="plot${plotId}"]`).closest('.plot-list-plot');
                    panel.parentNode.removeChild(panel);

                    Common.add_success_message('Plot deleted successfully.');
                }
            }
        );
    }

    // Menu items for standard (global) chart hamburger menu
    static standardContextMenuItems = [
        {
            textKey: 'printChart',
            text: 'Print',
            onclick: function() {
                this.print();
            }
        },
        {
            separator: true
        },
        {
            textKey: 'disableToolTip',
            text: 'Disable Tooltip',
            onclick: function(e) {
                // use e.target to toggle the text
                this.tooltip.options.enabled = !this.tooltip.options.enabled;
                if (this.tooltip.options.enabled) {
                    e.target.textContent = 'Disable Tooltip';
                } else {
                    e.target.textContent = 'Enable Tooltip';
                }
            }
        },
        {
            separator: true
        },
        {
            textKey: 'downloadPNG',
            text: 'Download PNG',
            onclick: function() {
                this.exportChart();
            }
        },
        {
            textKey: 'downloadCSV',
            text: 'Download CSV',
            onclick: function() {
                let seriesData = this.series;

                let x_extremes = this.xAxis[0].getExtremes();
                // could be multiple y-axis

                let y_extremes, y_extremes_arr = [];
                for (let i=0; i<seriesData.length; i++) {
                    y_extremes = this.yAxis[i].getExtremes();
                    y_extremes_arr.push(y_extremes)
                }

                let jsonOutput = PlotCommon.generateJSONfromChart(seriesData,x_extremes,y_extremes_arr);
                let outputTitle = this.title.textStr;

                Common.JSONToCSVConvertor(jsonOutput, outputTitle, true)
                //this.downloadCSV(); // dont need this anymore
            }
        }
    ];
    

    // default exporting object
    static default_exporting = {
        sourceWidth: 600,
        sourceHeight: 400,
        buttons: {
            contextButton: {
                enabled: true,
                _title: 'ExportImageOrData',
                menuItems: PlotCommon.standardContextMenuItems
            }
        },
        csv: {
            columnHeaderFormatter: function (series, key) {
            },
        },
        chartOptions: { // specific options for the exported image
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: false
                    }
                }
            }
        },
        fallbackToExportServer: false,
    };


    // default exporting object
    static no_data_exporting = {
        sourceWidth: 600,
        sourceHeight: 400,
        buttons: {
            contextButton: {
                enabled: true,
                _title: 'ExportImageOrData',
                menuItems: []
            }
        },
        csv: {
            columnHeaderFormatter: function (series, key) {
            },
        },
        chartOptions: { // specific options for the exported image
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: false
                    }
                }
            }
        },
        fallbackToExportServer: false,
    };


    static getMenuVariables(asset_id, user_id) {
        if (query('#variable-loading-text-x')?.classList.contains('hidden')) {
            // add the text to the DOM and make it visible
            query('#variable-loading-text-x').classList.remove('hidden');
            query('#variable-loading-text-y').classList.remove('hidden');

            // add hidden class to the preview button
            query('#plot-button').classList.add('hidden')
        }

        // if it doesn't exist then need to append it (this is the case when coming to page with no asset and no variables)
        if (document.querySelectorAll('#variable-loading-text-x').length === 0) {

            let cont_x = query('#container-x')
            let p = document.createElement('p');
            p.setAttribute('id', 'variable-loading-text-x');
            p.style.color = '#0000ff';
            p.innerHTML =  'menu loading...'
            cont_x.parentNode.insertBefore(p, cont_x.nextSibling);

            let cont_y = query('#container-y')
            p = document.createElement('p');
            p.setAttribute('id', 'variable-loading-text-y');
            p.style.color = '#0000ff';
            p.innerHTML =  'menu loading...'
            cont_y.parentNode.insertBefore(p, cont_y.nextSibling);
        }

        query('.multiselect.dropdown-toggle')?.classList.add('disabled')

        let ajaxCall = $.ajax({
                url: '/asset/ajax/get_asset_variables',
                method: 'POST',
                data: {
                    'asset_id': asset_id,
                    'user_id': user_id
                }
        });

        ajaxCall.done(function(resp) {
            let menuVariables = resp.variables;

            let menuVarIds=[];
            for (let i=0; i<menuVariables.length; i++) {
                if (menuVariables[i] !== undefined && typeof(menuVariables[i]['id'] === 'number')) {
                    menuVarIds.push(String(menuVariables[i].id));
                }
            }

            let menuVarIds_x = menuVarIds.slice(0);
            menuVarIds_x.unshift('Time');
            let menuVarIds_y = menuVarIds.slice(0)

            PlotCommon.buildMenu(menuVariables, asset_id);
            PlotCommon.toggleOnMenuItems(PlotCommon.variablesObject.variable_id_array_x,PlotCommon.variablesObject.variable_id_array_y);
            query('#variable-loading-text-y').classList.add('hidden');
            query('#variable-loading-text-x').classList.add('hidden');
            query('.multiselect.dropdown-toggle').classList.remove('disabled')
            query('#plot-button')?.classList.remove('hidden')

            // make time the default X-Axis
            if (PlotCommon.variablesObject.variable_id_array_x.length === 0) {
                $('#plot-variable-x').multiselect('select', 'Time');
                let optionObj = {val: 'Time', text: 'Time', parent: 'plot-variable-x'};
                PlotCommon.updateMultiselect_x(optionObj, true);
            }
        })
    }
        // function to turn multiselector checkboxes on for the necessary variables
    static toggleOnMenuItems(variable_id_list_x, variable_id_list_y) {
        for (let i=0; i<variable_id_list_x.length; i++) {
            $('#plot-variable-x').multiselect('select', String(variable_id_list_x[i]))
            if (variable_id_list_x[i] === 'Time') {
                $('#plot-variable-x').multiselect('select', String('Time'))
            }
        }

        for (let i=0; i<variable_id_list_y.length; i++) {
            $('#plot-variable-y').multiselect('select', String(variable_id_list_y[i]))
        }

        // disable the rest of the checkboxes if 4 are already selected
        if (PlotCommon.variablesObject.variable_id_array_y.length >= 4) {
            let eles = document.querySelectorAll('#container-y input[type="checkbox"]:not(:checked)')
            eles.forEach((ele) => {
                ele.disabled = true;
            });
        }
    }
    
    static setup_close_all(asset_id, plot_page_name, plot_page_id, plotArray){
        const closeLinks = Array.from(document.querySelectorAll(".close-link-all"));

        closeLinks.forEach((element) => {
            element.addEventListener('click', (e) => {
                if (!confirm("Would you like to remove this plot?")) {
                    return;
                }

                const panel = e.target.closest('.plot-list-plot');
                const index = plotArray.indexOf(panel.dataset.id);
                plotArray.splice(index, 1);

                $.ajax({
                    url: django_js_urls['save_plot_page'],
                    method: 'POST',
                    data: {
                        'plot_ids': plotArray,
                        'plot_page_name': plot_page_name,
                        'plot_page_id': plot_page_id,
                        'asset_id':  asset_id,
                        'asset_overview': "True",
                    },
                    success: () => {
                        Common.add_success_message("Plot page saved.");
                    }
                });

                panel.remove();
            });
        });

    }
        // TODO: bootstrap multiselect is rather slow for these huge lists.. why are there so many variables
    // in these dropdowns?
    static buildMenu(menuItems, asset_id) {
        let list_item, html_text='';

        // build time checkbox
        let timeItem_text = '<option value="Time">Time</option>'

        for (let i=0; i<menuItems.length; i++){
            let data_custom = menuItems[i].is_custom ? true : false;

            let listItem = '<option value="' + (menuItems[i].id).toString() + '" ' + 'data-custom=' + data_custom + '>' +
                menuItems[i].name + '</option>';
            html_text += listItem;
        }

        $("#plot-variable-x").multiselect('destroy');
        $("#plot-variable-y").multiselect('destroy');

        // make sure this element is emptied out
        $('#plot-variable-x').empty();
        $('#plot-variable-x').append(timeItem_text + html_text);
        $('#plot-variable-x').multiselect(PlotCommon.multiselectObject);

        $('#plot-variable-y').empty();
        $('#plot-variable-y').append(html_text);
        $('#plot-variable-y').multiselect(PlotCommon.multiselectObject);
    }
    

    static get_menu_items_for_erddap_urls(resp){

        let varID=Object.keys(resp);
        let errdap_url_context_menu = [];
        for (let i=0; i<varID.length; i++) {
            let currentResp=resp[varID[i]];
            errdap_url_context_menu.push(
                {
                    text:  "<a class='erddap_url' href='" + currentResp.erddap_url + "' >" + currentResp.subtitle + ' ' + currentResp.title + '  <u>(download)</u></a>',
                }
            );
        }
        return errdap_url_context_menu;

    }

    static dataStatusInfo(resp) {
        // Initially loop through the response to create an object identifying *useable* data
        // *(data are numeric and the length of the data array > 0)*

        let currentResp, numericData, varID, data_info = {};
        varID=Object.keys(resp);

        for (let i=0; i<varID.length; i++) {
            currentResp=resp[varID[i]];

            // only use variables with numeric data
            numericData = Common.checkData(currentResp.data)

            // determine the L3 status and build appropriate string if necessary
            let L3_status = currentResp.custom_variable ? ' (L3)' : '';

            // determine interpolation status and time interval if necessary
            let interp_status = '';
            for (let var_id in currentResp.interp_values) {
                if (currentResp.interp_values[var_id] !== null) {
                    interp_status = '  *interpolated @ ' + currentResp.interp_values[var_id] + ' hrs';
                } else {
                    interp_status = '';
                }
            }

            // build series names here
            let series_name;
            if (currentResp.present_asset !== currentResp.parent_asset) {
                series_name = currentResp.present_asset + ' on ' +
                            currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' + currentResp.title +
                            interp_status + L3_status;
            } else {
                series_name = currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' +
                            currentResp.title + interp_status + L3_status;
            }

            data_info[currentResp.variable_id]= {};
            data_info[currentResp.variable_id]['name']=series_name;
            if (currentResp['errors'].length === 0 && numericData) {
            data_info[currentResp.variable_id]['status'] = 'good';
            data_info[currentResp.variable_id]['name'] = series_name;
            } else {
                data_info[currentResp.variable_id]['status']='error';
                let error_msg = '';
                if (!numericData && currentResp['errors'].length === 0) {
                    error_msg = 'Non-numeric data';
                } else {
                    for (let j=0; j<currentResp['errors'].length; j++) {
                        error_msg += currentResp['errors'][j] + ', ';
                    }
                    error_msg = error_msg.replace(/,\s*$/, "");
                }
                data_info[currentResp.variable_id]['name'] += ' - ' + error_msg;
            }
        }

        let data_all_bad
        // determine if any *useable data are present
        for (let key in data_info) {
            if (data_info[key]['status'] === 'good') {
                data_all_bad = false;
                break;
            } else {
                data_all_bad = true;
            }
        }

        return {
                data_info: data_info,
                data_all_bad: data_all_bad
                }
    }
        
    static update_plot(asset_id, deployment_id, x_var, y_var, time_window, container_id, plotname, plotglobal, plotType, plotClass, y_direction, start_date, end_date, plot_relative_to) {
        
         
        // remove error msg div if present
        let errorEle = query('#' + container_id).parentElement.querySelector('.plot-error')
        if (errorEle) {
            errorEle.parentElement.removeChild(errorEle);
        }
        query('#' + container_id).innerHTML =
            `<div class="lds-css ng-scope">
                <div class="lds-spinner" style="100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>`;
        
    
        if(plotClass=="Comparison"){
            if (query('#' + container_id).closest('.x_panel').querySelector('.plot-type-window')){
                query('#' + container_id).closest('.x_panel').querySelector('.plot-type-window').style.display = 'none';
            }
        } else {
            if (query('#' + container_id).closest('.x_panel').querySelector('.plot-type-window')){
                query('#' + container_id).closest('.x_panel').querySelector('.plot-type-window').style.display='initial';
            }
        }
        let exporting;
        let navigation;
        // Customize HighCharts export button
        if (container_id === 'plot0') {
            exporting = PlotCommon.additional_plot_exporting;
            navigation = PlotCommon.default_navigation;
    
        } else {
            navigation = PlotCommon.default_navigation;
            if ((plotglobal === 'global_False') && container_id === 'plot0') {
                exporting = PlotCommon.default_exporting;
            } else if ((plotglobal === 'global_False') && container_id !== 'plot-preview-container') {
                exporting = PlotCommon.custom_exporting;
            } else {
                exporting = PlotCommon.default_exporting;
            }
        }
        let ajaxCall = $.ajax({
            url: '/asset/plot-data',
            method: 'POST',
            data: {
                'asset_id': asset_id,
                'x_var': String(x_var),
                'y_var': String(y_var),   
                'time_window': time_window,
                'start_date': start_date,
                'end_date': end_date,
                'deployment_id': deployment_id,
                'plot_type': plotClass,
                'plot_relative_to': plot_relative_to
            }
        });
        ajaxCall.done(function(resp,textStatus,jqXHR) {
            PlotCommon.ajax_plot_data(resp, x_var, y_var, time_window, container_id, plotname, plotType, plotClass, y_direction, exporting, start_date, end_date);
        });
        ajaxCall.fail(function(jqXHR, textStatus, errorThrown) {
            let error_obj = {'Error': errorThrown + ' (' + jqXHR.status + ')' };
            let replace_exporting = $.extend(true, {}, PlotCommon.no_data_exporting)
            replace_exporting.buttons.contextButton.menuItems = replace_exporting.buttons.contextButton.menuItems.concat(PlotCommon.get_menu_items_for_erddap_urls(resp))
    
            PlotCommon.createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, replace_exporting)
            return
        });
    }
    static ignore_time_changes = false;
    // This requires a checkbox with a name of "sync_plots" to function properly
    static handle_change_time_window_event(plot_button) {
        if (PlotCommon.ignore_time_changes) return;

        // Don't bother if sync'ing is not enabled
        if (!document.querySelector("[name=sync_plots]")?.checked ?? false) {
            return;
        }

        let plots = Array.from(document.querySelectorAll('.plot-list-plot [data-highcharts-chart], #plot0'));

        let plot_id_source = plot_button.getAttribute('data-plot-id');

        let time_window_source = query(".time-window" + plot_id_source);
        let relative_to_source = query(".relative-to" + plot_id_source);
        let start_date_source = query(".start_date"+ plot_id_source);
        let end_date_source = query(".end_date"+ plot_id_source);

        plots.forEach(plot => {
            try {
                let selector = plot.closest('.x_panel').querySelector('.update-plot');
                if (plot_button===selector) {//exclude the plot that triggered the event
                    return;
                }            

                if (!selector) {
                    return;
                }

                let plot_id = selector.getAttribute('data-plot-id');
                let time_window = query(".time-window" + plot_id);
                let relative_to = query(".relative-to" + plot_id);
                let start_date = query(".start_date"+ plot_id);
                let end_date = query(".end_date"+ plot_id);

                time_window.value = time_window_source.value;
                relative_to.value = relative_to_source.value;

                relative_to.dispatchEvent(new Event('change'));
                if (relative_to_source.value==PlotCommon.CUSTOM_TIME_FRAME){
                    start_date.value = start_date_source.value;
                    end_date.value = end_date_source.value;
                }
                PlotCommon.ignore_time_changes = true;
                selector.dispatchEvent(new Event("click"));
                PlotCommon.ignore_time_changes = false;

            }
            catch (error)
            {
                console.error('Error: ', error);
            }
        });
    }
    // This requires a checkbox with a name of "sync_plots" to function properly
    static handle_zoom_plot_event(event) {
        // Only check zoom event
        if (!event.trigger || event.trigger !== "zoom") {
            return;
        }

        // Don't bother if sync'ing is not enabled
        if (!document.querySelector("[name=sync_plots]")?.checked ?? false) {
            return;
        }

        let plots = Array.from(document.querySelectorAll('.plot-list-plot [data-highcharts-chart], #plot0'));

        plots.forEach(plot => {
            try {
                const chart = $(plot).highcharts();
                if (!chart) {
                    return;
                }

                // Ignore charts not using date/time for the x-axis (e.g., comparison plots)
                if (chart.xAxis[0].userOptions.type.toLowerCase() !== "datetime") {
                    return;
                }

                chart.xAxis[0].setExtremes(event.min, event.max);
            }
            catch (error)
            {
                console.error('Error: ', error);
            }
        });
    }

    static ajax_plot_data(resp, x_var, y_var, time_window, container_id, plotname, plotType, plotClass, y_direction, exporting, start_date, end_date) {

        let replace_exporting = $.extend(true, {}, exporting)
        replace_exporting.buttons.contextButton.menuItems = replace_exporting.buttons.contextButton.menuItems.concat({separator: true}, PlotCommon.get_menu_items_for_erddap_urls(resp))

        let no_data_replace_exporting = $.extend(true, {}, PlotCommon.no_data_exporting)
        no_data_replace_exporting.buttons.contextButton.menuItems = no_data_replace_exporting.buttons.contextButton.menuItems.concat(PlotCommon.get_menu_items_for_erddap_urls(resp))
        if (Object.getOwnPropertyNames(resp).length == 0) {
            // create fake data..
            PlotCommon.createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, no_data_replace_exporting)
            return
        }

        let varID=Object.keys(resp);
        let subtitle = {text: ''};

        let currentResp, numericData;

        let title;
        if (container_id === 'plot0') {
            title = {text: plotname, style: {'visibility': 'hidden'}};
        } else {
            title = {text: plotname};
        }

        let y_axis_reversed;
        if (y_direction) {
            y_axis_reversed = true;
        } else {
            y_axis_reversed = false;
        }

        let yData = [], xData = [], seriesData = [], full_unit_list= [] ;
        let opposite=false, gridLineWidth = 1, yAxisCounter=0, good_data = [];
        let plotObject;
        
        switch(plotClass) {
            case 'Timeseries':

                xData = {
                    type: 'datetime',
                    title: {
                        text: 'Date'
                    }
                };

                let data_info = PlotCommon.dataStatusInfo(resp);
                // if all data is bad then generate the error messages and call createEmptyPlot
                if (data_info['data_all_bad']) {
                    // create a simple object containing error messages
                    let error_obj = {};
                    for (let key in data_info['data_info']) {
                        error_obj[key]  = data_info['data_info'][key]['name'];
                    }
                    PlotCommon.createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, no_data_replace_exporting)
                    return
                }
                let foundAtIndex;
                for (let i=0; i<varID.length; i++) {
                    currentResp=resp[varID[i]];
                    let unit_name = currentResp.units || 'n/a';
                    unit_name = Common.normalizeUnitName(unit_name);
                    // check if the same units are already present and adjust the yData object accordingly
                    if (unit_name !== 'n/a' && unit_name !== '1' && unit_name !== 'nan' ) {
                        foundAtIndex = full_unit_list.indexOf(unit_name);
                    } else {
                        foundAtIndex = -1;
                    }

                    let plotColor = Common.cbColors[yAxisCounter];
                    full_unit_list.push(unit_name)
                    opposite = PlotCommon.determineOpposite(full_unit_list)

                    let useYaxis;
                    let yLabelTitleText;
                    let labelsEnabled;
                    if (foundAtIndex > -1) {
                        useYaxis = seriesData[foundAtIndex].yAxis;
                        yLabelTitleText = '';
                        labelsEnabled = false;
                    } else {
                        useYaxis = seriesData.length;
                        yLabelTitleText = unit_name;
                        labelsEnabled = true;
                    }

                    yData.push({
                        reversed: y_axis_reversed,
                        gridLineWidth: gridLineWidth,
                        labels: {
                            enabled: labelsEnabled,
                            style: {
                                color: varID.length < 2 ? "#606060" : plotColor
                            }
                        },
                        title: {
                            text: yLabelTitleText,
                            style: {
                                color: varID.length < 2 ? "#606060" : plotColor
                            }
                        },
                        opposite: opposite
                    });

                    // update this in special cases...
                    // removed by ticket #97 - Fix default range of plots to not max y-axis at 100% for "percentage" variables
                    // if (unit_name === 'percent' || unit_name === '%') {
                    //     yData[yData.length -1].max = 100;
                    //     yData[yData.length -1].endOnTick = false;
                    // }

                    let series_name = data_info['data_info'][currentResp.variable_id]['name'];

                    seriesData.push({
                        yAxis: useYaxis,
                        name: series_name,
                        data: currentResp.data,
                        color: plotColor,
                        description: unit_name,
                    });
                    // no gridline for subsequent variables
                    gridLineWidth = 0;

                    // increment the yAxisCounter
                    yAxisCounter+=1;
                }

                plotObject = {
                    fullUnitList: full_unit_list,
                    container_id: container_id,
                    title: title,
                    subtitle: subtitle,
                    type: plotType,
                    xData: xData,
                    yData: yData,
                    seriesData: seriesData,
                    navigation: PlotCommon.default_navigation,
                    exporting: replace_exporting,
                    lang: PlotCommon.LANG
                }

                // Pass the plot object to the plot function
                PlotCommon.plot(plotObject);

                break;
            case 'Comparison':
                currentResp=resp[varID[0]];

                let unit_name = currentResp.units || 'n/a';

                // only use variables with numeric data.. check non-time columns
                numericData = Common.checkData(currentResp.data)

                let colorize = false;
                let formattedTitle='';
                let x_title_txt;
                let y_title_txt;
                let x_var_id;
                let y_var_id;
                let x_var_index;
                let y_var_index;
                let x_unit_name;
                let x_is_custom;
                let y_unit_name;
                let y_is_custom;
                // TODO: remove try catch block once it is determined how to handle variables from dif assets
                try {
                    x_var_id = currentResp.variable_id.indexOf(Number(currentResp.x_var));
                    x_unit_name = currentResp.units.split(',')[x_var_id] || 'n/a';
                    x_is_custom = currentResp.custom_variable[x_var_id] ? ' (L3)' : '';

                    y_var_id = currentResp.variable_id.indexOf(Number(currentResp.y_var));
                    y_unit_name = currentResp.units.split(',')[y_var_id] || 'n/a';
                    y_is_custom = currentResp.custom_variable[y_var_id] ? ' (L3)' : '';

                    if (currentResp.data[0].length == 2) {
                        x_var_index = x_var_id;
                        y_var_index = y_var_id;
                    } else {
                        x_var_index = x_var_id + 1; //shift by 1 since time comes first
                        y_var_index = y_var_id + 1; //shift by 1 since time comes first
                    }

                    // correctly format the x and y title text as well as the title (maintain x vs y order)
                    if (currentResp.title.split(',')[x_var_id].indexOf('(' + x_unit_name + ')') > -1) {
                        x_title_txt = currentResp.title.split(',')[x_var_id];
                    } else {
                        x_title_txt = currentResp.title.split(',')[x_var_id] + ' (' + x_unit_name + ')';
                    }

                    if (currentResp.title.split(',')[y_var_id].indexOf('(' + y_unit_name + ')') > -1) {
                        y_title_txt = currentResp.title.split(',')[y_var_id];
                    } else {
                        y_title_txt = currentResp.title.split(',')[y_var_id] + ' (' + y_unit_name + ')';
                    }

                    formattedTitle = currentResp.title.split(',')[x_var_id] + x_is_custom +
                    ' vs. ' + currentResp.title.split(',')[y_var_id] + y_is_custom;
                } catch (err) {
                    formattedTitle='Error ';
                }

                if (!numericData || currentResp['errors'].length > 0 ) {
                    if (!numericData && currentResp['errors'].length === 0) {
                        formattedTitle += ' - Non-numeric data';
                    } else {
                        formattedTitle += ' - ' + currentResp['errors'][0];
                    }

                    let error_obj = {'Custom': formattedTitle};

                    if(start_date==undefined || end_date == undefined){
                        start_date = new Date(new Date().setDate(new Date().getDate()+Number(time_window)));
                        end_date = new Date();
                    }
                    PlotCommon.createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, no_data_replace_exporting)
                    return
                }

                // create objects for plotting
                xData = {
                    gridLineWidth: 1,
                    type: 'x-axis',
                    description: unit_name.split(',')[0],
                    title: {
                        enabled: true,
                        text: x_title_txt
                    },
                    startOnTick: true,
                    endOnTick: true,
                    showLastLabel: true
                };

                yData = {
                    reversed: y_axis_reversed,
                    gridLineWidth: 1,
                    labels: {
                        enabled: true,
                        style: {
                            color:  "#606060"
                        }
                    },
                    title: {
                        enabled: true,
                        text: y_title_txt,
                        style: {
                            color: "#606060"
                        }
                    }
                };
                let series_name;
                if (currentResp.present_asset !== currentResp.parent_asset) {
                    series_name = currentResp.present_asset + ' on ' +
                    currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' + formattedTitle;
                } else {
                    series_name = currentResp.parent_asset + ' (' + currentResp.deployment_code + '): ' +
                    formattedTitle;
                }

                let x_title = currentResp.title.split(',')[x_var_id];
                let y_title = currentResp.title.split(',')[y_var_id];

                seriesData = {
                    name: series_name,
                    data: PlotCommon.formatPlotData(currentResp.data, x_var_index, y_var_index, x_title, x_unit_name, y_title, y_unit_name, colorize),
                    description: unit_name, // here check this
                };

                plotObject = {
                    container_id: container_id,
                    title: title,
                    subtitle: subtitle,
                    xData: xData,
                    yData: yData,
                    seriesData: seriesData,
                    navigation: PlotCommon.default_navigation,
                    exporting: replace_exporting,
                    lang: PlotCommon.LANG
                }

                // Pass the plot object to the comparison_plot function -- this handles profile and comparison type plots
                PlotCommon.comparison_plot(plotObject);

                break;
            default:
                // do nothing
        }
    }

    // Universal plotting method. Provide a plotObject as the argument (see overview.html for example on how this is built)
    static plot(plotObject) {

        plotObject.xData.events = plotObject.xData.events || {}
        plotObject.xData.events.afterSetExtremes = this.handle_zoom_plot_event;

        let DOCUMENT_SCROLLTOP
        let chart = Highcharts.chart(plotObject.container_id, {
            chart: {
            type: plotObject.type,
            zoomType: 'xy',
            events: {
                beforePrint:function() {
                    DOCUMENT_SCROLLTOP = window.scrollY;
                    // perform some resizing to make print output consistent
                    this.oldhasUserSize = this.hasUserSize;
                    this.resetParams = [this.chartWidth, this.chartHeight, false];
                    this.setSize(600, 400, false);

                    this.exportSVGElements[0].box.hide();
                    this.exportSVGElements[1].hide();
                    if (this.exportSVGElements.length > 2) {
                        this.exportSVGElements[2].hide();
                    }
                },
                afterPrint:function() {
                    // set scrollTop to previous value
                    window.scrollTo({top:DOCUMENT_SCROLLTOP});

                    // reset the scroll to 0
                    DOCUMENT_SCROLLTOP = 0;

                    // resize back to original to ensure proper fit on the page
                    this.setSize.apply(this, this.resetParams);
                    this.hasUserSize = this.oldhasUserSize;

                    this.exportSVGElements[0].box.show();
                    this.exportSVGElements[1].show();
                    if (this.exportSVGElements.length > 2) {
                        this.exportSVGElements[2].show();
                    }
                    chart.reflow();// moved this here from mediaquerylist listener. did not test. CN
                }
            }
            },
            title: plotObject.title,
            subtitle: plotObject.subtitle,
            xAxis: plotObject.xData,
            yAxis: plotObject.yData,
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {point.x:%Y-%m-%d %H:%M:%S} UTC: <b>{point.y:f} {point.series.yAxis.axisTitle.textStr}</b><br/>',
                shared: false,
            },
            lang: PlotCommon.LANG,
            legend: {
                align: 'center',
                verticalAlign: 'bottom'
            },
            series: plotObject.seriesData,
            navigation: PlotCommon.default_navigation,
            exporting: plotObject.exporting,
            plotOptions: {
                scatter: {
                    marker: {
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            credits: {
                enabled: false
            }
        });
    }

    static toggleMenuErrorText() {
        let errorMsgs = {
            noneSelected: 'Select a variable',
            tooManySelected: 'Limit: 1 variable',
            tooManySelected4: 'Limit: 4 variables'
        }

        let noneSelected_x = PlotCommon.variablesObject.variable_id_array_x.length === 0;
        let noneSelected_y = PlotCommon.variablesObject.variable_id_array_y.length === 0;

        let tooManySelected = PlotCommon.variablesObject.plot_class === 'Comparison' &&
            PlotCommon.variablesObject.variable_id_array_y.length > 1;

        let tooManySelected4 = PlotCommon.variablesObject.plot_class === 'Timeseries' &&
            PlotCommon.variablesObject.variable_id_array_y.length > 4;

        if (noneSelected_x) {
            if (document.querySelectorAll('#container-x span[class="error-message"]').length === 0) {
                let span = document.createElement('span');
                span.classList.add('error-message');
                span.id="x-error-text";
                span.innerHTML = '&nbsp; ' + errorMsgs['noneSelected'];
                query('#container-x p[class="plot-menu-label"]').appendChild(span);
            }
        } else {
            let span = query('#container-x span[id="x-error-text"]');
            if(span){
                span.parentNode.removeChild(span)
            }
        }

        if (noneSelected_y) {
            if (document.querySelectorAll('#container-y span[class="error-message"]').length === 0) {
                let span = document.createElement('span');
                span.classList.add('error-message');
                span.id="y-error-text";
                span.innerHTML = '&nbsp; ' + errorMsgs['noneSelected'];
                query('#container-y p[class="plot-menu-label"]').appendChild(span);
            }
        } else if (tooManySelected) {
            if (document.querySelectorAll('#container-y span[class="error-message"]').length === 0) {
                let span = document.createElement('span');
                span.classList.add('error-message');
                span.id="y-error-text";
                span.innerHTML = '&nbsp; ' + errorMsgs['tooManySelected'];
                query('#container-y p[class="plot-menu-label"]').appendChild(span);
             }
        } else if (tooManySelected4) {
            if (document.querySelectorAll('#container-y span[class="error-message"]').length === 0) {
                let span = document.createElement('span');
                span.classList.add('error-message');
                span.id="y-error-text";
                span.innerHTML = '&nbsp; ' + errorMsgs['tooManySelected4'];
                query('#container-y p[class="plot-menu-label"]').appendChild(span);
            }
        } else {
            let span = query('#container-y span[id="y-error-text"]')
            if(span){
                span.parentNode.removeChild(span)
            }   
        }

        return
    }
    // Universal plotting method. Provide a plotObject as the argument (see overview.html for example on how this is built)

    static comparison_plot(plotObject) {
        let chart = Highcharts.chart(plotObject.container_id, {
            chart: {
            type: 'scatter',
            zoomType: 'xy',
            events: {
                beforePrint:function() {
                    DOCUMENT_SCROLLTOP = $(document).scrollTop();

                    // perform some resizing to make print output consistent
                    this.oldhasUserSize = this.hasUserSize;
                    this.resetParams = [this.chartWidth, this.chartHeight, false];
                    this.setSize(600, 400, false);

                    this.exportSVGElements[0].box.hide();
                    this.exportSVGElements[1].hide();
                    if (this.exportSVGElements.length > 2) {
                        this.exportSVGElements[2].hide();
                    }
                },
                afterPrint:function() {
                    // set scrollTop to previous value
                    $(document).scrollTop(DOCUMENT_SCROLLTOP);

                    // reset the scroll to 0
                    DOCUMENT_SCROLLTOP = 0;

                    // resize back to original to ensure proper fit on the page
                    this.setSize.apply(this, this.resetParams);
                    this.hasUserSize = this.oldhasUserSize;

                    this.exportSVGElements[0].box.show();
                    this.exportSVGElements[1].show();
                    if (this.exportSVGElements.length > 2) {
                        this.exportSVGElements[2].show();
                    }
                }
            }
            },
            title: plotObject.title,
            subtitle: plotObject.subtitle,
            xAxis: plotObject.xData,
            yAxis: plotObject.yData,
            lang: PlotCommon.LANG,
            legend: {
                align: 'center',
                verticalAlign: 'bottom'
            },
            series: [plotObject.seriesData],
            navigation: PlotCommon.default_navigation,
            exporting: plotObject.exporting,
            plotOptions: {
                series: {
                turboThreshold: 0
                },
                scatter: {
                    marker: {
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        pointFormat: '{point.time:%Y-%m-%d %H:%M:%S} UTC<br><span style="color:{point.color}">\u25CF</span> <b>{point.x:f} {point.x_unit_name}</b>, <b>{point.y:f} {point.y_unit_name}</b><br/>',
                        shared: false,
                    },
                }
            },
            credits: {
                enabled: false
            }
        });
    }
    // function to create a 'No Data Plot'
    static createEmptyPlot(plotname, container_id, time_window, error_obj, start_date, end_date, exporting) {
        // build HTML for no data message which will be put into x axis title
        let error_html = PlotCommon.buildDataErrorText(error_obj);


        if(start_date==undefined || end_date == undefined){
            start_date = new Date(new Date().setDate(new Date().getDate()+Number(time_window)));
            end_date = new Date();
        }else if( typeof start_date =="string" || typeof end_date == "string"){
            start_date = new Date(start_date);
            end_date = new Date(end_date);
        }

        let fake_times = Common.spacedTimeArray(start_date, end_date, 1)

        // zip the data together and plot
        let zipped_data = [];
        for (let i = 0; i < fake_times.length; i++) {
            let rand_data =[fake_times[i], i*100];
            zipped_data.push(rand_data);
        }

        let title = {
            text: plotname
        }

        let xData = {
            gridLineWidth: 1,
            type: 'datetime',
            title: {
                useHTML: true,
                style: {
                    'white-space': 'normal',
                }
            },
            labels: {
                enabled: false
            },
            minorTickLength: 0,
            tickLength: 0,
        };


        let yData = {
            gridLineWidth: 1,
            title: {
                text: null,
            },
            labels: {
                enabled: false
            }
        };

        let seriesData = [{
            data: zipped_data,
            visible: false
        }];

        let plotObject_ind = {
            container_id: container_id,
            title: title,
            xData: xData,
            yData: yData,
            seriesData: seriesData,
            exporting: exporting,
            error_html: error_html,
            lang: PlotCommon.LANG
        }

        // Pass the plot object to the plot function
        PlotCommon.no_data_plot(plotObject_ind);
    }
    static no_data_plot(plotObject) {
        let chart = Highcharts.chart(plotObject.container_id, {
            chart: {
              type: 'scatter',
              zoomType: 'xy',
              ignoreHiddenSeries : false,
              events: {
                load: function(chart) {
                    let div = document.createElement('div')
                    div.innerHTML = '<div id="no-data-plot"><h1>No Data</h1></div>'
                    query('#' + plotObject.container_id + ' .highcharts-container').appendChild(div)
                    // append the error text
                    PlotCommon.appendErrorToContainer(plotObject.container_id, plotObject.error_html)
                }
              }
            },
            title: plotObject.title,
            xAxis: plotObject.xData,
            yAxis: plotObject.yData,
            lang: PlotCommon.LANG,
            navigation: PlotCommon.default_navigation,
            legend: {
                enabled: false
            },
            series: plotObject.seriesData,
            exporting: plotObject.exporting,
            credits: {
                enabled: false
            }
        });
    }
    static formatPlotData(data, x_axis_index, y_axis_index, x_title, x_unit_name, y_title, y_unit_name, colorize) {
        let final_data_struct=[];
        let data_color;
        for (let i = 0; i< data.length; i++) {
            let time = data[i][0];
            let ydata = data[i][y_axis_index];
            let xdata = data[i][x_axis_index];
    
            if (colorize){
                data_color = Common.cbColors[0]; // this is a place holder.. use convertValToColor() // TODO (christensen): build this function
            } else {
                data_color = Common.cbColors[0];
            }
    
            let data_obj = {'x': xdata, 'y': ydata, 'x_unit_name': x_unit_name,
                            'x_title': x_title, 'y_title': y_title,
                            'y_unit_name': y_unit_name, 'time': time, 'color': data_color};
    
            final_data_struct.push(data_obj);
        }
        return final_data_struct
    }


    static buildDataErrorText(error_obj) {
        let error_html ='';
        let full_hidden_content='';
        let standard_content = '<strong>No data available&nbsp;</strong>'

        let counter=0
        for (let key in error_obj) {
            let content = error_obj[key];
            if (counter>0) {
                full_hidden_content += '<hr style="margin: 2px">' + content;

            } else {
                full_hidden_content += content;
            }
            counter++;
        }

        error_html += '<div class="plot-error" style="text-align: center"><p class="plot-error-text-standard">' + standard_content +
                '<a class="details-button" href="#" onclick="PlotCommon.toggleErrorText(event)">(Details)</a>' +
                '<div class="hidden-error-text plot-error-text-extra"> ' + full_hidden_content + '</div>' +
                '</p></div>';

        return error_html
    }
    // Helper method to determine the position that the yTitle and axis should be plotted on
    static determineOpposite(unitList) {
        let uniqueUnits = unitList.filter(function (item, i, ar) {
            if (item === 'n/a') {
                return true
            }
            return ar.indexOf(item) === i;
        });
        let opposite;
        if (uniqueUnits.length % 2 === 0 && uniqueUnits.length !== 0) {
            opposite = true;
        } else {
            opposite = false;
        }

        return opposite;
    }
    static toggleErrorText(e) {
        e.preventDefault();

        let targetEl = e.target;
        let targetElText = e.target.textContent.toLowerCase().replace(' ','');

        let nearest_chart_container =targetEl.closest('.plot-error').parentElement;
        let nearest_chart = $(nearest_chart_container).highcharts();

        if (targetElText === '(details)') {
            targetEl.textContent = '(Hide Details)';
            targetEl.closest('div').querySelector('div').classList.remove('hidden-error-text');
        } else {
            targetEl.textContent = '(Details)';
            targetEl.closest('div').querySelector('div').classList.add('hidden-error-text');
            nearest_chart_container.style.overflowY = 'hidden';
            nearest_chart_container.style.overflowX = 'hidden';
        }
    }
    static appendErrorToContainer(container_id, error_html) {
        let div = document.createElement('div')
        div.innerHTML = error_html
        query('#'+container_id).parentElement.appendChild(div)
    }

    static relative_to(obj) {   
        let panel = obj.closest('.x_panel')

        if(obj.value==PlotCommon.CUSTOM_TIME_FRAME){
            panel.querySelector('.dates_wrapper').style.display='';
            panel.querySelector('.time_window_wrapper').style.display='none';
        }else{
            // //I do not understand what this ever did
            // $parent.find('.end_date').val($parent.find('.end_date').attr("value"));
            // $parent.find('.start_date').val($parent.find('.start_date').attr("value"));
            // //so I converted to it Vanilla here, but left it commented.
            // panel.querySelector('.end_date').value = panel.querySelector('.end_date').getAttribute("value");
            // panel.querySelector('.start_date').value = panel.querySelector('.start_date').getAttribute("value");
            panel.querySelector('.dates_wrapper').style.display='none';
            panel.querySelector('.time_window_wrapper').style.display='initial';
        }  
    };
    static relative_to_event(e) {
        PlotCommon.relative_to(e.currentTarget);
    }
    static setup_time_window(){
        let allRels = document.querySelectorAll(".relative-to");
        for (const rel of allRels) {
            rel.addEventListener('change', PlotCommon.relative_to_event);
            PlotCommon.relative_to(rel);
        }
    }


    static updateMultiselect_y(optionObj, checked) {
        // Remove unselected variable from global arrays
        if (!checked) {
            let unchecked_option = optionObj.val
            let unchecked_option_indx = PlotCommon.variablesObject.variable_id_array_y.indexOf(unchecked_option);

            PlotCommon.variablesObject.variable_id_array_y.splice(unchecked_option_indx,1);
            PlotCommon.variablesObject.variable_name_array_y.splice(unchecked_option_indx,1);

            $('#plot-variable-y').multiselect('deselect', unchecked_option);

        } else {
            // only allow 1 menu item to be checked when dealing with plots that aren't timeseries
            if (PlotCommon.variablesObject.plot_class !== 'Timeseries') {
                let i = 0;
                let number_yvars = PlotCommon.variablesObject.variable_id_array_y.slice(0).length;
                while (i <= number_yvars) {
                    let previous_y_variable = PlotCommon.variablesObject.variable_id_array_y.pop()
                    PlotCommon.variablesObject.variable_name_array_y.pop()
                    $('#plot-variable-y').multiselect('deselect', previous_y_variable);
                    i++;
                }
            }
            let full_variable_name = PlotCommon.nameFabrication(optionObj);
            PlotCommon.variablesObject.variable_id_array_y.push(optionObj.val);
            PlotCommon.variablesObject.variable_name_array_y.push(full_variable_name);
        }
        if (PlotCommon.variablesObject.variable_id_array_y.length >= 4) {
            // Disable all other checkboxes
            let eles = document.querySelectorAll('#container-y input[type="checkbox"]:not(:checked)')
            eles.forEach((ele) => {
                ele.disabled = true;
            });
        } else {
            let eles = document.querySelectorAll('#container-y input[type="checkbox"]')
            eles.forEach((ele) => {
                ele.disabled = false;
            });
        }

        // Y-Axis Logic
        if (PlotCommon.variablesObject.plot_class !== 'Timeseries') {
            PlotCommon.variablesObject.plot_class = 'Comparison';

            // change the markers to scatter
            query('#id_plot_type').value = 2;
        }

        PlotCommon.syncMenusWithList()
    }


    static updateMultiselect_x(optionObj, checked) {
        // Remove unselected variable from global arrays
        if (!checked) {
            let unchecked_option = optionObj.val;
            let unchecked_option_indx = PlotCommon.variablesObject.variable_id_array_x.indexOf(unchecked_option);

            PlotCommon.variablesObject.variable_id_array_x.splice(unchecked_option_indx,1);
            PlotCommon.variablesObject.variable_name_array_x.splice(unchecked_option_indx,1);

            $('#plot-variable-x').multiselect('deselect', unchecked_option);
        } else {
            // only 1 variable is allowed to be checked in the x-axis dropdown
            let previous_x_variable = PlotCommon.variablesObject.variable_id_array_x.pop()
            PlotCommon.variablesObject.variable_name_array_x.pop()
            $('#plot-variable-x').multiselect('deselect', previous_x_variable);

            let full_variable_name = PlotCommon.nameFabrication(optionObj);
            PlotCommon.variablesObject.variable_id_array_x.push(optionObj.val);
            PlotCommon.variablesObject.variable_name_array_x.push(full_variable_name);
        }

        // X-Axis Logic
        if (PlotCommon.variablesObject.variable_id_array_x.length) {

            // Enable Y-Axis button
            query('#container-y').querySelector('.multiselect.dropdown-toggle')?.classList.remove('disabled');

            if (optionObj.val === 'Time'){
                PlotCommon.variablesObject.plot_class = 'Timeseries';
            } else {
                PlotCommon.variablesObject.plot_class = 'Comparison';
                // change plot type to scatter
                query('#id_plot_type').value = 2;
            }

            document.getElementById('plot-button').setAttribute('data-plot-class', PlotCommon.variablesObject.plot_class);
        }

        PlotCommon.syncMenusWithList()
    }


    static syncMenusWithList() {
        // update hidden plot class dropdown
        PlotCommon.toggleMenuErrorText();
        if(!query('#id_variables_x')){
            return
        }
        if(query('#id_plot_class')){
            query('#id_plot_class').value = PlotCommon.plot_class_dict[PlotCommon.variablesObject.plot_class];
        }
        
        // need to update the plot button with an attribute specifying its type
        query('#plot-button')?.setAttribute('plot-class', PlotCommon.variablesObject.plot_class)
        if(query('#id_variables_x')){
        
            query('#id_variables_x').value = String(PlotCommon.variablesObject.variable_id_array_x)
            query('#id_variables_y').value = String(PlotCommon.variablesObject.variable_id_array_y)
            
        }

        let outerList_x = query('#variables_list_x');
        let for_label_x = 'id_variables_x';
        let outerList_y = query('#variables_list_y');
        let for_label_y = 'id_variables_y';

        let multiselect_x = query('#container-x').querySelector('.multiselect-container');
        let multiselect_y = query('#container-y').querySelector('.multiselect-container');

        // clear the variable list
        outerList_x.innerHTML = '';
        outerList_y.innerHTML = '';

        // update error text if necessary

        // ****build x-list****

        let variableDivTemplate=document.createElement('div');
        let closeTagStr = '<a style="text-decoration: none; margin-right: 5px; font-size: 20px" href="#" aria-label="close">&times;</a>';
        variableDivTemplate.innerHTML = '<p style="margin-bottom: 5px; margin-left: 5px">' + closeTagStr + '<span>varNAME</span>' + '</p>';
    
        if (PlotCommon.variablesObject.variable_id_array_x.length > 0) {
            // rebuild variables_list div
            for (let i=0; i<PlotCommon.variablesObject.variable_name_array_x.length; i++) {
                // TODO: clean this up can be simpler
                let variableDivTemplateCloneOuter = variableDivTemplate.cloneNode(true);
                let variableDivTemplateCloneHTML = variableDivTemplateCloneOuter.outerHTML
                let elem_doc = new DOMParser().parseFromString(variableDivTemplateCloneHTML, 'text/html');

                let variableDivTemplateClone = elem_doc.body.firstChild;

                variableDivTemplateClone.setAttribute('data-var-id', String(PlotCommon.variablesObject.variable_id_array_x[i]))
                variableDivTemplateClone.querySelector('a').setAttribute('data-var-id', String(PlotCommon.variablesObject.variable_id_array_x[i]))
                variableDivTemplateClone.querySelector('span').textContent = PlotCommon.variablesObject.variable_name_array_x[i];
                query('#variables_list_x').appendChild(variableDivTemplateClone);
            }
        }

        // ****build y-list****
        if (PlotCommon.variablesObject.variable_id_array_y.length > 0) {
            // rebuild variables_list div
            for (let i=0; i<PlotCommon.variablesObject.variable_name_array_y.length; i++) {
                // TODO: clean this up can be simpler
                let variableDivTemplateCloneOuter = variableDivTemplate.cloneNode(true);
                let variableDivTemplateCloneHTML = variableDivTemplateCloneOuter.outerHTML
                let elem_doc = new DOMParser().parseFromString(variableDivTemplateCloneHTML, 'text/html');

                let variableDivTemplateClone = elem_doc.body.firstChild;

                variableDivTemplateClone.setAttribute('data-var-id', String(PlotCommon.variablesObject.variable_id_array_y[i]))
                variableDivTemplateClone.querySelector('a').setAttribute('data-var-id', String(PlotCommon.variablesObject.variable_id_array_y[i]))
                variableDivTemplateClone.querySelector('span').textContent = PlotCommon.variablesObject.variable_name_array_y[i];
                query('#variables_list_y').appendChild(variableDivTemplateClone);
            }
        }
    }


    static nameFabrication(optionObj) {
        let fullCheckBoxName = optionObj.text;
        let splitName = fullCheckBoxName.split(':')
        let instrument = splitName[0].trim();
        let baseVarName;
        if (splitName.length === 1) {
            baseVarName = query('#varpicker-name')?.textContent.split(' ')[0];
        } else {
            baseVarName = splitName[1].trim();
        }

        let plot_deployment_code_str = optionObj.option_custom ? ': ' : ' (' + PlotCommon.assetsObject.deployment_code + '): ';
        
        let detailedName;
        // for Time we don't need the full string
        if (instrument === PlotCommon.assetsObject.asset_name) {
            detailedName = instrument + plot_deployment_code_str + baseVarName;
        } else {
            detailedName = instrument + ' on ' + PlotCommon.assetsObject.asset_name + plot_deployment_code_str + baseVarName;
        }

        if (splitName.length === 1) {
            detailedName = splitName[0];
        }
        return detailedName
    }

    static generateJSONfromChart(series_data,x_extremes,y_extremes_arr) {
        // compile all times from all series into a single array and uniqueify it + sort in ascending order
        // then i will loop through and check each series for a specific time... only necessary
        let all_times = [], filtered_times = [];
        for (let ind=0; ind<series_data.length; ind++) {
            let checkType = series_data[ind].data[0].options;
            let plot_class = Object.keys(checkType).indexOf('time') === -1 ? 'time_series': 'non-timeseries';
            // if its not a timeseries then use the x-value for time
            for (let inner_ind=0; inner_ind<series_data[ind].data.length; inner_ind++) {
                if (plot_class === 'time_series') {
                    let obs_time = series_data[ind].data[inner_ind].options.x;
                    let x_data_point = obs_time;
                    let y_data_point = series_data[ind].data[inner_ind].options.y;
                } else {
                    let obs_time = series_data[ind].data[inner_ind].options.time;
                    let x_data_point = series_data[ind].data[inner_ind].options.x;
                    let y_data_point = series_data[ind].data[inner_ind].options.y;
                }
                // only use the points which are visible on the chart
                if ((x_data_point >= x_extremes.min && x_data_point <= x_extremes.max && y_data_point >= y_extremes_arr[ind].min &&
                                                                                        y_data_point <= y_extremes_arr[ind].max)) {
                    filtered_times.push(obs_time)
                }
                all_times.push(obs_time)
            }
        }

        let unique_sorted_times = Common.sort_unique(filtered_times);

        // if unique_sorted_times is equal to all_times then we don't want to use indexOf
        let eq_arr = Common.arraysEqual(unique_sorted_times, all_times)

        // if time isn't in the first series then it won't be in any because of the way this is designed...
        // timeseries plots are made up of series without a 'time paramater'
        let options = series_data[0].data[0].options;
        let plot_class = Object.keys(options).indexOf('time') === -1 ? 'time_series': 'non-timeseries';

        // create an object series with the key being the time if arrays aren't equal
        if (!eq_arr) {
            let seriesObj = Common.createDataObj(series_data,plot_class);
        }

        // for each time sort each series for it and if its not there then build an empty row
        let concat_data_array = [];
        for (let t=0; t < unique_sorted_times.length; t++) {

            let data_row={}, ind_data_obj, filtered_time_indx, series_times, series_name, unit_name
            for (let i=0; i<series_data.length; i++ ) {

                if (plot_class === 'time_series') {
                    unit_name = series_data[i].userOptions.description;
                    if (eq_arr) {
                        ind_data_obj = series_data[i].data[t];
                    } else {
                        ind_data_obj= seriesObj[i][unique_sorted_times[t]];
                    }

                    series_name = series_data[i].name + ' (' + unit_name + ')';
                    data_row['Time'] = new Date(unique_sorted_times[t]).toISOString();

                    if (ind_data_obj !== undefined) {
                        data_row[series_name] = ind_data_obj.y;
                    } else {
                        data_row[series_name] = '';
                    }
                } else {
                    if (eq_arr) {
                        ind_data_obj = series_data[i].data[t].options;
                    } else {
                        ind_data_obj= seriesObj[i][unique_sorted_times[t]];
                    }

                    data_row['Time'] = new Date(unique_sorted_times[t]).toISOString();
                    data_row[ind_data_obj.x_title + ' ( ' + ind_data_obj.x_unit_name + ')'] = ind_data_obj.x;
                    data_row[ind_data_obj.y_title + ' (' + ind_data_obj.y_unit_name + ')'] = ind_data_obj.y;

                }
            }
            // push the completed row into the full array
            concat_data_array.push(data_row)
        }

        // JSONify the array
        let output = JSON.stringify(concat_data_array);
        return output
    }

    static async permanentlyDeletePlot(plotId) {
        const response = await fetch('/asset/ajax/delete_custom_plot', {
            method: 'POST',
            body: JSON.stringify({'plot_id': plotId}),
            headers: { 'X-CSRFToken': Common.getCookie('csrftoken') }
        });

        const data = await response.json();

        return data.success;
    }
}

export default PlotCommon;