import jQuery from "jquery";

class Common {
    constructor() { }

    static DEFAULT_DATE_TIME_FORMAT = 'M/D/YY h:mma UTC';
    static DEFAULT_DATE_FORMAT = 'M/D/YY';

    static cbColors = ["#56b4e9","#000000","#e69f00","#009e73"];

    static getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            let cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                let cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    // Helper method to get query parameter from url
    static getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    static init_jquery_select2() {
        if( typeof ($().select2) === 'undefined'){  return; }
        $(".select2_multiple").select2({
            placeholder: "Select a Value...",
            allowClear: true
        });
    
        
    };
    static setupErrorFlags(id) {
        let ele = document.getElementById(id);
        ele.addEventListener('click', function(e) {
            e.preventDefault();
            if(e.target.classList.contains('error-flags')){

                const endpoint = $(e.target).data('endpoint');
                const value = $(e.target).text()
    
                let title = $(e.target).data('title');
    
                if (title == undefined || title == '') {
                    title = 'Error Flags';
                }

                Common.openErrorFlagModal(title, endpoint, value);
            }
        });

        $("#error-flag-modal").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: false,
            height: 300,
            width: 450,
            title: 'Error Flags',
            position: {
                my: "center",
                at: "top+30%",
                of: window
            },
        });
    }


    static openErrorFlagModal(title, endpoint, value) {
        const url = '/asset/ajax/' + endpoint + '?flags=' + value;

        $.get(url, function(response) {
            let modal = $("#error-flag-modal");
            let body = modal.find('tbody');

            body.empty();

            const isPowerSystemController = (endpoint == "psc-error-flags");
            const isMpea = (endpoint == 'mpea-error-flags');
            const isWakeCodes = (endpoint == 'wake-codes');

            if (isPowerSystemController) {
                Common.addErrorFlagRows(body, response.errors1, 'F1');
                Common.addErrorFlagRows(body, response.errors2, 'F2');
                Common.addErrorFlagRows(body, response.errors3, 'F3');
            } else if (isMpea) {
                Common.addErrorFlagRows(body, response.errors1, 'F1');
                Common.addErrorFlagRows(body, response.errors2, 'F2');
            } else if (isWakeCodes) {
                Common.addWakeCodeRows(body, response.codes);
            } else {
                Common.addErrorFlagRows(body, response.errors);
            }

            modal.find('.flag-number').toggle(isPowerSystemController);
            modal.find('.value-name').text(isWakeCodes ? 'Wake Bits' : 'Flag (Hex)');
            modal.dialog('open', { title: '' });
            modal.dialog('option', 'title', title)
        });
    }

    static addErrorFlagRows(body, rows, flagNumber) {
        $.each(rows, function (_, item) {
            let row = $('<tr />');

            row.append($('<td />', {text: flagNumber, 'class': 'flag-number'}));
            row.append($('<td />', {text: item[0]}));
            row.append($('<td />', {text: item[1]}));

            body.append(row);
        })
    }
    static openWakeCodeModal() {
        $.get('/asset/ajax/wake-codes', function(response) {
            let modal = $("#wake-code-modal");
            let body = modal.find('tbody');

            body.empty();

            Common.addWakeCodeRows(body, response.codes);

            modal.dialog('open', { title: '' });
        });
    }

    static addWakeCodeRows(body, rows) {
        $.each(rows, function (_, item) {
            let row = $('<tr />');

            row.append($('<td />', {text: item[0]}));
            row.append($('<td />', {text: item[1]}));
            row.append($('<td />', {text: item[2]}));

            body.append(row);
        })
    }

    static init_asset_picker_dialog_box(selector) {
        $(selector).dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: false,
            width: 730,
            title: 'Select an Asset',
            position: {
                my: "center",
                at: "top+20%",
                of: window
            },
        });
    }

    static init_dialog_box(selector, height) {
        $(selector).dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: false,
            height: typeof height !== 'undefined' ? height : 200,
            title: '',
            position: {
                my: "center",
                at: "top+30%",
                of: window
            },
        });
    }
    
    // Helper method to create a fake time array to be used when we don't have any data
    static spacedTimeArray(start_date, end_date, time_delta_hours) {
        let start_hour = start_date.getHours();
        let start_date_ms;
        if (start_hour % 2 === 0) {
            start_date_ms = start_date.getTime() - (1000 * 60 *60 * time_delta_hours)
        } else {
            start_date_ms = start_date.getTime() - (1000 * 60 *60 * (time_delta_hours-1))
        }

        let end_date_ms = end_date.getTime();

        let date_array=[];
        let timeArrElem = start_date_ms;
        while (timeArrElem < end_date_ms) {
            timeArrElem = timeArrElem + (60*60*1000*time_delta_hours)
            date_array.push(timeArrElem)
        }
        return date_array;
    }
    // Helper method for formatting DataTables columns with the default date/time format
    static dt_format_date_time(data, type, row) {
        if (type !== "display")
            return data;

        let date_value = moment.utc(data);

        return date_value.isValid() ? date_value.format(Common.DEFAULT_DATE_TIME_FORMAT) : "";
    }
    // Helper to add stretch-pagination class to pager on grids to allow it to span more than the col-md-7 class. This
    //   is needed on grids that are only half the width of the screen, which cause the paging to break to a second line
    static stretch_pagination(pager) {
        $("#" + pager.attr('id') + "_paginate").parent().addClass('pagination-stretch');
    }

    // Helper method for formatting DataTables columns with the default date format
    static dt_format_date(data, type, row) {
        return moment.utc(data).format(DEFAULT_DATE_FORMAT);
    }

    // Helper method for formatting DataTables asset column with the correct link
    static asset_link_creator(data, type, row) {
        let linkStr= '<a href="/asset/overview/' + row['asset_id'] + '/' + row['deployment_code'] + '/">'+ data + '</a>';
        return linkStr;
    }

    // Helper method for formatting DataTables platform column with the correct link
    static platform_link_creator(data, type, row) {
        let linkStr= '<a href="/asset/overview/' + row['platform_id'] + '/' + row['deployment_code'] + '/">'+ data + '</a>';
        return linkStr;
    }
        
    static checkData(data){
    
        if (data.length === 0 ) {
            return false
        } else {
            let number_of_columns = data[0].length
            let true_array=[];
    
            for (let i=1; i<number_of_columns; i++) {
                let dataCheck=data.map(function(x) { return typeof(x[i])})
                let numericData = !dataCheck.every(Common.stringCheck)
                true_array.push(numericData)
            }
    
            let data_good = true_array.every(function(x){return x === true});
            if (!data_good) {
                return false
            }
        }
        return true
    }

    // Helper method to determine if data is non-numeric and therefor not plottable
    static stringCheck(n) {
        return n === 'string';
    }
    static normalizeUnitName(unitName){
        let unitMap = { //map of different unit names to normals. positon 0 is the canonical.
            degrees: ['degrees_', 'degree_'],
            siemens: ['S/m', 'S m-1']
        }
    
    
        for (const key in unitMap) {
            let possibleReplacements = unitMap[key]
            for(let i=1; i<possibleReplacements.length; i++){
                if(unitName.indexOf(possibleReplacements[i])!=-1){
                    return unitName.replace(possibleReplacements[i], possibleReplacements[0]);
                }
            }        
        };
        return unitName;
    }
    static open_dialog_box(e, selector, url) {
        e.preventDefault();
        $(selector).load(url, function() {
            $(selector).dialog('open');
        });
    
        return false;
    }
    // Helper method to add a success notification (using PNotify)
    static add_success_message(msg) {
        $("#notification-area").show(function() {
            new PNotify({
                title: 'Success!',
                text: msg,
                type: 'success',
                icon: 'fa fa-check',
                delay: 5000,
            });
        });
    }

    // Helper method to add a error notification (using PNotify)
    static add_failed_message(msg) {
        $("#notification-area").show(function() {
            new PNotify({
                title: 'Failed!',
                text: msg,
                type: 'error',
                icon: 'fa fa-exclamation-circle',
                delay: 5000,
            });
        });
    }
    static change_deployment(lst) {
        let asset_id = $(lst).val();
        let deployment_code = $("option:selected", $(lst)).text();
    
        // TODO(mchagnon): Only goes to overview page and URL is not using a route
        location.href = '/asset/overview/' + asset_id + '/' + deployment_code;
    }
    static sort_unique(arr) {
        if (arr.length === 0) return arr;
            arr = arr.sort(function (a, b) { return a*1 - b*1; });
            let ret = [arr[0]];
            for (let i = 1; i < arr.length; i++) { //Start loop at 1: arr[0] can never be a duplicate
                if (arr[i-1] !== arr[i]) {
                    ret.push(arr[i]);
                }
            }
        return ret;
    }
    // Helper function to determine if two arrays are equal
    static arraysEqual(arr1, arr2) {
        if(arr1.length !== arr2.length)
            return false;
        for(let i = arr1.length; i--;) {
            if(arr1[i] !== arr2[i])
                return false;
        }

        return true;
    }
    // Helper function to transform data from arrays to an object form which is much more
    // performant for lookups
    static createDataObj(series_data, plot_class) {
        let fullSeriesObj={};
        for (let i=0; i<series_data.length; i++){
            let subSeriesObj = {}, elem_obj, time;
            for (let j=0; j < series_data[i].data.length; j++){
                if (plot_class === 'time_series') {
                    time = series_data[i].data[j].options.x;
                } else {
                    time = series_data[i].data[j].options.time;
                }
                subSeriesObj[time] = series_data[i].data[j].options;
            }
            fullSeriesObj[i]=subSeriesObj;
        }
        return fullSeriesObj
    }


    // Helper function to generate custom csv output depending on plot class
    static JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        let arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

        let CSV = '';
        //Set Report title in first row or line

        CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header
        if (ShowLabel) {
            let row = "";

            //This loop will extract the label from 1st index of on array
            for (let index in arrData[0]) {

                //Now convert each value to string and comma-seprated
                row += index + ',';
            }

            row = row.slice(0, -1);

            //append Label row with line break
            CSV += row + '\r\n';
        }

        //1st loop is to extract each row
        for (let i = 0; i < arrData.length; i++) {
            let row = "";

            //2nd loop will extract each column and convert it in string comma-seprated
            for (let index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }

            row.slice(0, row.length - 1);

            //add a line break after each row
            CSV += row + '\r\n';
        }

        //Generate a file name
        let fileName = "Chart_Data_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g,"_");

        let csv = CSV;
        let blob = new Blob([csv], { type: 'text/csv' });
        let csvUrl = window.URL.createObjectURL(blob);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension

        //this trick will generate a temp <a /> tag
        let link = document.createElement("a");

        link.href = csvUrl;

        //set the visibility hidden so it will not affect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}

const TriggerTypes = Object.freeze({
    Expression:  1,
    LastUpdate: 2,
});

const DurationCodes = Object.freeze({
    Minutes: 1,
    Hours: 2,
    CurrentDeployment: 3,
    Seconds: 4,
});

export default Common;
Common.TriggerTypes = TriggerTypes;
Common.DurationCodes = DurationCodes;
