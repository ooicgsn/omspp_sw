import os

# Django settings
DEBUG = os.environ.get("DEBUG", "")

ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "*").split()
CSRF_TRUSTED_ORIGINS = os.environ.get("CSRF_TRUSTED_ORIGINS", "*").split()

# Email settings
DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL", "")
EMAIL_HOST = os.environ.get("EMAIL_HOST", "")
EMAIL_PORT = os.environ.get("EMAIL_PORT", "")
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD", "")
EMAIL_USE_SSL = os.environ.get("EMAIL_USE_SSL", "")

SITE_URL = os.environ.get("SITE_URL", "")

# ERDDAP settings
ERDDAP_URL = os.environ.get("ERDDAP_URL", "")
ERDDAP_USERNAME = os.environ.get("ERDDAP_USERNAME", "")
ERDDAP_PASSWORD = os.environ.get("ERDDAP_PASSWORD", "")
LOG_ERDDAP_REQUESTS = os.environ.get("LOG_ERDDAP_REQUESTS", "")
LOG_ERDDAP_DETAILS = os.environ.get("LOG_ERDDAP_DETAILS", "")

# Redmine settings
# The Use Live Projects setting will enable a connection to the production instances
#   of the Redmine instance; when disabled, test projects are used
REDMINE_URL = os.environ.get("REDMINE_URL", "")
REDMINE_USE_LIVE_PROJECTS = os.environ.get("REDMINE_USE_LIVE_PROJECTS", False)

# Url to view raw log files (hosted outside of OMS++ application
DATA_DIRECTORY = os.environ.get("DATA_DIRECTORY", "")

# To disable feature (namely external connections) for use when operating the application while at sea
LAPTOP_MODE = os.environ.get("LAPTOP_MODE", False)

# Whether to render front end in development mode (with hot module reloading)
DJANGO_VITE_DEV_MODE = os.environ.get("DJANGO_VITE_DEV_MODE").lower() == 'true'

