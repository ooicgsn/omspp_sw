import re
import datetime
import math
from django import db
from core.models.alert_trigger import Trigger, TriggerVariable, TriggerUser
from core.models.alert import Alert, Status, Occurrence, OccurrenceItem, Statuses
from core.models.asset import AssetDispositions
from core.alerts import recalculate_asset_status
from core.erddap import Erddap, interpolate_continuous_series
from core.db import exec_void
from core.models.static import TriggerTypes, DurationCodes, TriggerType
from core.models.asset import AssetDispositions
from pytz import timezone
import pandas as pd
from core.alerts import send_user_alert_notification
import multiprocessing

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def create_alert(trigger, failures, is_last_update_failure=False):
    """Creates an alert, alert_occurrence(s), and alert notifications"""
    # If there are no failures of any kind, there's no reason to create an alert
    if (failures is None or failures.empty) and not is_last_update_failure:
        return

    # Get an existing alert or create a new one
    alert, is_new = Alert.objects.get_or_create(
        trigger=trigger,
        status_id=Statuses.OPEN.value,
        severity=trigger.severity,
        deployment=trigger.asset.deployment,
        asset=trigger.asset,
        roll_up=trigger.roll_up,
        roll_down=trigger.roll_down
    )

    occurrence = Occurrence.objects.create(created=pd.Timestamp.utcnow(), alert=alert)
    timestamp_utc = None

    # Handle failures from expression based triggers
    if failures is not None and not failures.empty:
        for timestamp, failure in failures.iteritems():
            # Pandas appears to parse CSV TZ-naive even if ISO8601 is used. Convert each TS from naive to UTC here,
            # as Django doesn't like it naive TS being used in the model fields
            timestamp_utc = timestamp.to_pydatetime().replace(tzinfo=timezone('UTC'))

            OccurrenceItem.objects.create(
                occurrence=occurrence,
                occurred_at=timestamp_utc,
                value=failure
            )

    # Handle failures from last update triggers
    if is_last_update_failure:
        timestamp_utc = pd.Timestamp.utcnow()

        OccurrenceItem.objects.create(
            occurrence=occurrence,
            occurred_at=timestamp_utc,
            value=None
        )

    alert.created = timestamp_utc if is_new else alert.created
    alert.last_occurrence = timestamp_utc
    alert.save()

    # raise new alert notifications
    exec_void('add_alert_notifications', (alert.id,))

    # Recalculate the asset's status if this is a new alert. If this is just a new occurrence, then leave the
    #   status as is. Additionally, we only send out alert notifications if this is a new alert so users
    #   do not get spammed with messages each time a new occurrence is added
    if is_new:
        log_info(trigger, "Updating status for linked alert asset id {}".format(alert.asset.id))

        recalculate_asset_status(alert.asset.id, alert.details, None, False, alert.id)

        # send email notification if there are any users subscribed
        send_alert_notifications(trigger, alert)

    return alert


def send_alert_notifications(trigger, alert):
    try:
        trigger_users = TriggerUser.objects.filter(trigger_id=trigger.id)

        if len(trigger_users) > 0:
            for trigger_user in trigger_users:
                send_user_alert_notification(trigger_user.user, alert, trigger_user.delivery_method_id)

    except Exception as e:
        log_info(trigger, "  [!] Error sending alert notifications: " + str(e))


def get_windowed_data(raw_data, interval):
    """
    Takes raw data and a string interval and resamples data within a window
    to the given interval.  The next window is spaced one half the interval
    length away, causing 50% overlap between adjacent windows.
    Returns a resampled time series indexed pandas DataFrame or Series.
    """

    # NB: (badams) This might not be the most efficient implementation, but it
    #              works reasonably well for an out of the box Pandas
    #              implementation
    # TODO: (badams) prefer pandas.Timedelta intervals to string specifications
    #                since the former appear more flexible
    resamp = raw_data.resample(interval).mean()
    half_interval = pd.Timedelta(interval) / 2

    # shift back by half the time interval, resample on original interval, and
    # then shift forward another 15 minutes on this result.
    # Discard the first data point as it is missing some data.
    # This results in times that overlap with the original resampling.
    # NB: .ix[1:] won't throw an error if the Series or DataFrame is empty
    # have to shift by pd.Timedelta object, string has odd results

    # Logic split to separate lines for clarity. Also replaced ".ix" with ".iloc" to to the former being
    #   deprecated in newer releases of pandas
    resamp_shift = raw_data.shift(freq=half_interval)
    resamp_shift = resamp_shift.resample(interval, offset=half_interval)
    resamp_shift = resamp_shift.mean()
    resamp_shift = resamp_shift.shift(freq=-half_interval * 2)
    resamp_shift = resamp_shift.iloc[1:]

    # combine the two series and sort in chronologically ascending order
    resamp_combined = pd.concat([resamp, resamp_shift]).sort_index()

    return resamp_combined


# Does a simple replace of platform specific variables with the data from the platform. This is done ahead of the
#   logic to substitute ERDDAP data based on the variables included in the expression
def prefill_expression(trigger):

    # Only prefill if the predefined variables are included somewhere. This saves having to load the platform
    #   data if there are no replacements necessary
    pattern = r'\bplatform_(?:lat|latitude|lon|longitude|depth|watch_circle_radius)\b'
    if not re.search(pattern, trigger.expression, re.IGNORECASE):
        return trigger.expression

    platform = trigger.asset.platform
    replacements = [
        ['platform_lat', platform.lat],
        ['platform_latitude', platform.lat],
        ['platform_lon', platform.lon],
        ['platform_longitude', platform.lon],
        ['platform_depth', platform.depth],
        ['platform_watch_circle_radius', platform.watch_circle_radius],
    ]

    expression = trigger.expression

    for replacement in replacements:
        result, count = re.subn(replacement[0], str(replacement[1]), expression, re.IGNORECASE)

        # If the platform variable was found, make sure there is a valid value set for said variable on the platform
        if count > 0 and not replacement[1]:
            if not replacement[1] or replacement[1].strip() == '':
                raise Exception(f'{replacement[0]} is included in the expression, but no value is set on the platform.')

            # Safety check to make sure the value is a number/float, however, the UI does prevent a non-numerical value
            #   from being saved in the first place
            try:
                float(replacement[1])
            except:
                raise Exception(f'{replacement[0]} is included in the expression, but the platform value is not a number.')

        expression = result

    return expression


def fetch_data_trigger(trigger, time_span_days=-2):
    """
    trigger: A trigger object to process
    time_span_days: a negative number indicating the number of days from the current time to search for observations
    """
    trig_vars = TriggerVariable.objects.filter(trigger=trigger)
    tvar_dict = {tv.name: tv.variable for tv in trig_vars}

    try:
        expression = prefill_expression(trigger)
    except Exception as e:
        print(f'  [!] Could not prefill trigger expression with platform data: {e}')
        return pd.DataFrame()

    # any interpolation must be performed prior to evaluating the predicate
    try:
        expr, pred = trigger.split_expression(expression).groups()
    except Exception as e:
        log_info(trigger,
                 "  [!] Error parsing trigger expression: " + (trigger.expression if trigger.expression else ""))
        return pd.DataFrame()

    # combined may initially be either a dataframe or a series depending on
    # if it had an expression associated with it
    try:
        combined, _, _ = Erddap().fetch_data(tvar_dict, expr, trigger.name,
                                             time_span_days=time_span_days,
                                             deployment_id=trigger.asset.deployment.code,
                                             interp_interval=trigger.interpolation_amount,
                                             change_interval=trigger.change_interval)
    except Exception as e:
        log_info(trigger, f'  [!] Exception connecting to ERDDAP: {e}')
        return pd.DataFrame()

    if combined.empty:
        log_info(trigger, "  [!] No data returned from ERDDAP")
        return pd.DataFrame()

    # return an empty series for the trigger if the result is empty.
    # often this will be due to having time out of range for the data fetch
    if combined.empty:
        return pd.Series(dtype=bool)

    formula_remaps = {
        "{}_{}".format(tv.variable.asset.erddap_id, tv.variable.erddap_id): tv.name
        for tv in trig_vars
    }

    # Columns can only be renamed like this if we're working with a DataFrame (vs a Series)
    #    On a series (or on everything?) the name may actually not be important
    if isinstance(combined, pd.DataFrame):
        combined.rename(columns=formula_remaps, inplace=True)

    window_duration = trigger.get_duration()

    # if there's windowed data, we need to apply the window prior to
    # applying the predicate
    if window_duration:
        windowed_results = get_windowed_data(combined, window_duration)

        # evaluate the windowed results against the predicate
        windowed_results.name = 'val'
        fails = windowed_results.to_frame('val').eval("val {}".format(pred), engine='numexpr')
        return windowed_results[fails]

    # just return the evaluated expression if there is no windowing
    # TODO: Determine if this is ever used? window_duration is required...
    # ensure that combined is a dataframe so we can call eval
    res = combined.to_frame('val')
    fails = res.eval(f'val {pred}', engine='numexpr')

    return res[fails].val


def check_last_update_trigger(trigger):
    # Basic sanity checks
    if not trigger.asset or not trigger.duration_code or not trigger.duration:
        print('  Missing required data to check the alert trigger')
        return False

    # Make sure the asset is marked as deployed, or it should never fire the trigger (for many reasons, but
    #   specifically because non-deployed assets will not get updated last_update times.
    if trigger.asset.disposition_id != AssetDispositions.DEPLOYED.value:
        return False

    # If there is no last update, then nothing needs to be calculated - just trigger the alert
    if trigger.asset.last_update is None:
        print('  Last update triggered because there is no last_update time')
        return True

    allowed_duration = trigger.duration_in_seconds
    if allowed_duration < 0:
        print("  Failed to check trigger because the duration code is not valid for this type of alert.")
        return False

    current_time = datetime.datetime.now(datetime.timezone.utc)
    timespan = current_time - trigger.asset.last_update
    actual_duration = math.floor(timespan.total_seconds())

    return actual_duration > allowed_duration


def process_trigger(trigger):
    trigger.log_entries = []

    # If no trigger type is set, default to expression
    if not trigger.trigger_type:
        trigger.trigger_type = TriggerType.objects.get(pk=TriggerTypes.EXPRESSION.value)

    """Run the tests specified by the alert trigger condition"""
    log_info(trigger, "[+] Processing {} trigger  {} ({}) for {} on {} ({})".format(
        trigger.trigger_type.name.lower(),
        trigger.name,
        trigger.id,
        trigger.asset if trigger.asset else "",
        trigger.asset.platform if trigger.asset else "",
        trigger.asset.deployment.code if trigger.asset and trigger.asset.deployment else ""
    ))

    try:
        # raise exception if there is no asset associated with this trigger
        if trigger.asset is None:
            raise ValueError(f'  [!] Alert trigger {trigger.name} must have an associated asset')

        alert = None
        if trigger.trigger_type.id == TriggerTypes.LAST_UPDATE.value:
            last_update_triggered = check_last_update_trigger(trigger)

            if last_update_triggered:
                log_info(trigger, f'  [+] Alert (last update) raised')
                alert = create_alert(trigger, failures=None, is_last_update_failure=True)

        else:
            failures = fetch_data_trigger(trigger)

            if not failures.empty:
                log_info(trigger, f'  [+] Alert (expression) raised for "{trigger.expression}"')
                alert = create_alert(trigger, failures)

        flush_logs(trigger)
        return alert

    # we want to continue and log any exceptions, not kill the process should an error arise
    except Exception as e:
        log_info(trigger, str(e))
        flush_logs(trigger)
        return None


def log_info(trigger, msg):
    trigger.log_entries.append(msg)


def flush_logs(trigger):
    logger.info("\n" + "\n".join(trigger.log_entries))


def process_triggers(parallel=True):
    """
    Simple method for running all triggers in the database that have
    a deployed or burn-in asset disposition

    Args: parallel - Use multiprocessing to evaluate the alert trigger conditions in parallel
    """
    enabled_dispositions = [
        AssetDispositions.BURN_IN.value,
        AssetDispositions.DEPLOYED.value
    ]

    triggers = Trigger.objects \
        .exclude(asset__is_deleted=True) \
        .filter(asset__platform__disposition__id__in=enabled_dispositions)

    if parallel:
        db.connections.close_all()
        pool = multiprocessing.Pool()
        pool.map(process_trigger, triggers)
        return

    for trigger in triggers:
        process_trigger(trigger)
