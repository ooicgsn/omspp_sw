
import os, sys

import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("projectLocation")
# parser.add_argument("user")
# parser.add_argument("host")
# parser.add_argument("password")

args = parser.parse_args()

#begin web service gateway
proj_path = args.projectLocation

# pointer to django settings.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.settings")
sys.path.append(proj_path)

# Load Local settings
os.chdir(proj_path)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

#end webservice gateway

from tasks import test_triggers

# def runSetUpTask():
#     t = test_tasks.TestTasks()
#     t.setUp()
#     return;

def runTest_Window():
    try:
        t = test_triggers.TestTriggers()
        t.setUp()
        t.test_window()
        t.test_window_l3()
        t.test_window_makes_alert()
        t.test_no_window()
    except AssertionError as e:
        raise e
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)
    finally:
        t.tearDown()
        pass



runTest_Window()