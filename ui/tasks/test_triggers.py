from django.test import TestCase
# this is rather long, consider namespaced?
from core.models.alert import (Alert, Status, Notification, Occurrence,
                               OccurrenceItem)
from core.models.alert_trigger import (DurationCode, Severity, Trigger, TriggerUser,
                                       TriggerVariable)
from core.models.asset import Asset, AssetType, AssetStatus, Group, Deployment
from core.models.variable import Variable, VariableComponent
from pandas import Timestamp
from django.contrib.auth.models import User
from urllib.parse import urljoin, quote
from tasks import run_alerts
import pandas as pd
import requests
import requests_mock
from django.conf import settings


# (badams): possibly split into a windowed and non-windowed case inheriting base
#           setup of variables, etc.
class TestTriggers(TestCase):

    def tearDown(self):
        if self.deployment:
            self.deployment.delete()
        if self.assetPlatform:
            self.assetPlatform.delete()
        if self.asset:
            self.asset.delete()
        if self.voltage_variable:
            self.voltage_variable.delete()
        if self.current_variable:
            self.current_variable.delete()

    def setUp(self):
        self.user = User.objects.get(id=1)

        self.deployment = Deployment(name='Test Deployment', code='test_deploy')
        self.deployment.save()

        self.assetPlatform = Asset(name='GA01SUMO', code='test-platform',
                      type=AssetType.objects.get(name='Platform'),
                      group=Group.objects.get(name='Coastal - Pioneer'),
                      status=AssetStatus.objects.get(name='Ok'),
                      deployment=self.deployment, erddap_id='test-platform')

        self.assetPlatform.save()

        self.asset = Asset(name='CPM', code='CPM',
                      type=AssetType.objects.get(name='Instrument'),
                      group=Group.objects.get(name='Coastal - Pioneer'),
                      status=AssetStatus.objects.get(name='Ok'),
                      deployment=self.deployment, erddap_id='test-asset',
                      platform_id=self.assetPlatform.id)

        self.asset.save()

        self.voltage_variable = Variable(name='voltage', asset=self.asset,
                                 erddap_id='voltage')
        self.voltage_variable.save()

        self.current_variable = Variable(name='port1_current', asset=self.asset,
                                 erddap_id='port1_current')
        self.current_variable.save()

        # test_url = 'https://cgoms.coas.oregonstate.edu/erddap/'
        test_url = settings.ERDDAP_URL
        # Rely on default ERDDAP URL in livesettings for the time being.
        # A bit of a hack, perhaps, but there didn't seem to be an easy way to
        # alter livesettings in Python
        # http://pinebank.whoi.net/erddap/tabledap/test-asset.csv?time,voltage&time%3E=now-2days&deploy_id=%22test_deploy%22
        self.ds_url = urljoin(test_url, 'erddap/tabledap/test-asset.csv?time,voltage&time%3E=now-2days&deploy_id=%22test_deploy%22')
        self.ds_url_l3 = urljoin(test_url, 'erddap/tabledap/test-asset.csv?time,voltage,port1_current&time%3E=now-2days&deploy_id=%22test_deploy%22')
        # mock response for the CSV
        self.csv_mock = '''time,voltage
UTC,V
2017-03-20T00:00:00Z,14.01
2017-03-20T00:01:00Z,14.0
2017-03-20T00:02:00Z,12.7
2017-03-20T00:03:00Z,14.0
2017-03-20T00:04:00Z,13.99998
2017-03-20T00:05:00Z,13.99998
2017-03-20T00:06:00Z,13.9543'''

        self.csv_mock_l3 = '''time,voltage,port1_current
UTC,V,A
2017-03-20T00:00:00Z,14.01,20
2017-03-20T00:01:00Z,14.0,20
2017-03-20T00:02:00Z,12.7,20
2017-03-20T00:03:00Z,14.0,20
2017-03-20T00:04:00Z,13.99998,20
2017-03-20T00:05:00Z,13.99998,20
2017-03-20T00:06:00Z,13.9543,20'''


    def test_window_l3(self):
        """
        Tests whether a window test will function properly.  Should not raise
        alert, due to averaging of window on outlier.
        """

        # trigger with window
        minute_duration = DurationCode.objects.get(name='Minutes')
        self.trig_window_l3 = Trigger(name='trig-test-window-l3', duration=2,
                                   asset=self.asset, created_by=self.user,
                                   expression='wattage > 200',
                                   duration_code=minute_duration,
                                   severity=Severity.objects.get(name='Medium'))
        self.trig_window_l3.save()
        
        self.trig_window_l3_variable = Variable(
            name='wattage',
            expression='wattage = voltage * port1_current',
            is_custom=True,
            asset=self.asset,
        )
        self.trig_window_l3_variable.save()

        voltage_component = VariableComponent()
        voltage_component.child = self.voltage_variable
        voltage_component.parent = self.trig_window_l3_variable
        voltage_component.name = 'voltage'
        voltage_component.save()
        

        current_component = VariableComponent()
        current_component.child = self.current_variable
        current_component.parent = self.trig_window_l3_variable
        current_component.name = 'port1_current'
        current_component.save()

        self.trig_window_var = TriggerVariable(trigger=self.trig_window_l3,
                                                  variable=self.trig_window_l3_variable,
                                                  name='wattage')
        self.trig_window_var.save()

        with requests_mock.mock() as m:
            m.get(self.ds_url_l3, text=self.csv_mock_l3)
            before_occurences = Occurrence.objects.count()
            before_notifications = Notification.objects.count()
            alert = run_alerts.process_trigger(self.trig_window_l3)
            
            self.assertIsNotNone(alert)
            # check that 1 new occurence has been generated
            self.assertEqual(Occurrence.objects.count()-before_occurences, 1)
            # check that every user got a notification
            self.assertEqual(Notification.objects.count()-before_notifications, User.objects.all().count())


    def test_window(self):
        """
        Tests whether a window test will function properly.  Should not raise
        alert, due to averaging of window on outlier.
        """

        # trigger with window
        minute_duration = DurationCode.objects.get(name='Minutes')
        self.trig_window = Trigger(name='trig-test-window', duration=2,
                                   asset=self.asset, created_by=self.user,
                                   expression='voltage < 13',
                                   duration_code=minute_duration,
                                   severity=Severity.objects.get(name='Medium'))
        self.trig_window.save()
        self.trig_window_var = TriggerVariable(trigger=self.trig_window,
                                                  variable=self.voltage_variable,
                                                  name='voltage')
        self.trig_window_var.save()

        with requests_mock.mock() as m:
            m.get(self.ds_url, text=self.csv_mock)
            before_occurences = Occurrence.objects.count()
            before_notifications = Notification.objects.count()
            alert = run_alerts.process_trigger(self.trig_window)
            # when the window smooths out the alert, the result
            self.assertIsNone(alert)
            #check that no new occurences have been generated
            self.assertEqual(Occurrence.objects.count(), before_occurences)
            self.assertEqual(Notification.objects.count(), before_notifications)



    def test_window_makes_alert(self):
        print("test_window_makes_alert")
        """
        Tests whether a window test will function properly.  Should not raise
        alert, due to averaging of window on outlier.
        """

        # trigger with window
        minute_duration = DurationCode.objects.get(name='Minutes')
        self.trig_window_makes_alert = Trigger(name='trig-test-window-makes-alert', duration=2,
                                   asset=self.asset, created_by=self.user,
                                   expression='voltage > 13',
                                   duration_code=minute_duration,
                                   severity=Severity.objects.get(name='Medium'))
        self.trig_window_makes_alert.save()

        self.trig_window_makes_alert_var = TriggerVariable(trigger=self.trig_window_makes_alert,
                                                  variable=self.voltage_variable,
                                                  name='voltage')
        self.trig_window_makes_alert_var.save()

        self.trigger_user = TriggerUser(
            trigger=self.trig_window_makes_alert, 
            user=self.user,
            delivery_method_id=2,
        )
        self.trigger_user.save()
        with requests_mock.mock() as m:
            m.get(self.ds_url, text=self.csv_mock)
            before_occurences = Occurrence.objects.count()
            before_notifications = Notification.objects.count()
            alert = run_alerts.process_trigger(self.trig_window_makes_alert)

            self.assertIsNotNone(alert)
            #check that no new occurences have been generated
            # print(str(Occurrence.objects.count()) + "-" + str(before_occurences) + " = " + str(Occurrence.objects.count()-before_occurences))
            # print(str(Notification.objects.count()) + "-" + str(before_notifications) + " = " + str(Notification.objects.count()-before_notifications))
            self.assertEqual(Occurrence.objects.count()-before_occurences, 1)
            self.assertEqual(Notification.objects.count()-before_notifications, User.objects.all().count() + 1)
        

    def test_no_window(self):
        """
        Tests whether a no window test will function properly.  Should raise an
        alert due to outlier under threshold.
        """

        # create a trigger with no windowing
        self.trig_no_window = Trigger(name='trig-test-no-window', duration=0,
                                      duration_code=DurationCode.objects.get(name='Current Deployment'),
                                      expression='voltage < 13',
                                      asset=self.asset, created_by=self.user,
                                      severity=Severity.objects.get(name='Medium'))
        self.trig_no_window.save()
        self.trig_no_window_var = TriggerVariable(trigger=self.trig_no_window,
                                                  variable=self.voltage_variable,
                                                  name='voltage')
        self.trig_no_window_var.save()

        with requests_mock.mock() as m:
            before_occurences = Occurrence.objects.count()
            before_notifications = Notification.objects.count()
            before_occurrence_item = OccurrenceItem.objects.count()

            m.get(self.ds_url, text=self.csv_mock)
            alert = run_alerts.process_trigger(self.trig_no_window)

            self.assertIsNotNone(alert)
            self.assertEqual(alert.last_occurrence,
                             Timestamp('2017-03-20T00:02:00Z'))
            # set alert status to resolved
            # 3 pre-existing + 1 new occurrence


            # print(str(Occurrence.objects.count()) + "-" + str(before_occurences) + " = " + str(Occurrence.objects.count()-before_occurences))
            # print(str(Notification.objects.count()) + "-" + str(before_notifications) + " = " + str(Notification.objects.count()-before_notifications))
            # print(str(OccurrenceItem.objects.count()) + "-" + str(before_occurrence_item) + " = " + str(OccurrenceItem.objects.count()-before_occurrence_item))


            self.assertEqual(Occurrence.objects.count()-before_occurences, 1)
            self.assertEqual(OccurrenceItem.objects.count()-before_occurrence_item, 1)
            # for now, new alert should create as many notifications as there
            # are users
            self.assertEqual(Notification.objects.filter(alert=alert).count(),
                             User.objects.count())
            # rerun alert with same status
            alert_same = run_alerts.process_trigger(self.trig_no_window)
            self.assertEqual(alert_same.id, alert.id)

            # new run should create a new Occurrence object
            self.assertEqual(Occurrence.objects.count()-before_occurences, 2)
            self.assertEqual(OccurrenceItem.objects.count()-before_occurrence_item, 2)


            # more notifications should not be raised, as the alert already
            # exists and has not been cleared
            self.assertEqual(Notification.objects.filter(alert=alert).count(),
                             User.objects.count())
            # update alert to resolved status
            alert.status = Status.objects.get(name='Resolved')
            alert.save()
            # simulate another run of the alerts after the first one has been
            # resolved.  Same time window in mock, so this isn't very realistic,
            # but the occurences count should remain
            # the same.
            alert2 = run_alerts.process_trigger(self.trig_no_window)
            # should have created a new alert
            self.assertNotEqual(alert.id, alert2.id)
            self.assertIsInstance(alert2, Alert)
            # set alert status to resolved
            # +1 occurences
            self.assertEqual(Occurrence.objects.count()-before_occurences, 3)
            self.assertEqual(OccurrenceItem.objects.count()-before_occurrence_item, 3)
            # There should be exactly one alert history entry corresponding
            # to the alert condition just created
            self.assertEqual(Notification.objects.filter(alert=alert2).count(),
                             User.objects.count())




# TODO (badams): add a test case for duplicated data
