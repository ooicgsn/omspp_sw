from core.models.alert import Alert
from django.core.mail import EmailMessage
from django.conf import settings
from django.urls import reverse
from datetime import datetime

def run_daily_digest_emails(selected_date=None):
    # Retrieve master list of pending notifications from data
    if selected_date is None:
        pending_notifications = Alert().get_pending_notifications_for_digest()
    else:
        pending_notifications = Alert().get_pending_notifications_for_digest(selected_date)

    # Get list of unique users
    users = set(dic["user_id"] for dic in pending_notifications)

    for user in users:
        # For each unique user get a list of notifications to send to send to that user
        user_notifications = [dic for dic in pending_notifications if dic["user_id"] == user]
        send_digest_email(user_notifications)


def send_digest_email(user_notifications):
    try:
        # Split out alerts into days if there are more than one for better formatting
        unique_dates = set(datetime.strftime(dic["date_created"], '%m/%d/%Y') for dic in user_notifications)
        alert_details = []

        # For each day build a list of alerts to insert into the email body
        for alert_day in unique_dates:
            alert_details.append("<strong>{}</strong><br /><ul>".format(alert_day))
            days_alerts = [dic for dic in user_notifications if datetime.strftime(dic["date_created"], '%m/%d/%Y') == alert_day]
            for alert in days_alerts:
                # Build a link each alert and add a link for it
                url = reverse('alert.detail', kwargs={'alert_id': alert["alert_id"]})
                url = settings.SITE_URL + url
                alert_details.append("<li>{} - <a href='{}'>{}</a></li>".format(
                    "{} ({})".format(alert["asset_name"], alert["platform_name"], alert["deployment"])
                    if alert["asset_name"] == alert["platform_name"]
                    else "{} on {} ({})".format(alert["asset_name"], alert["platform_name"], alert["deployment"]),
                    url,
                    alert["trigger_name"] if alert["alert_name"] is None else alert["alert_name"]))
            alert_details.append("</ul>")

        msg_subject = "OMS++ Alert Summary for {}. {} New Alerts.".format(
            datetime.strftime(datetime.now(), '%m/%d/%Y'), len(user_notifications))

        msg_body = "Dear {},<br /><br />" \
                   "This is a summary of the alerts you have received: <br /><br />" \
                   "{}" \
                   "Thank You,<br /><br />" \
                   "OMS++ Administration".format(
                    user_notifications[0]["user_name"], "".join(alert_details))

        msg_email_address = user_notifications[0]["user_email"]
        msg = EmailMessage(msg_subject, msg_body, settings.DEFAULT_FROM_EMAIL, [msg_email_address])
        msg.content_subtype = 'html'
        msg.send()
    except Exception as e:
        print("Error in send_digest_email:")
        print(e)
