#!/bin/bash

# provide the directory as the first arg

cat "$1"/*.csv | sed '2,${/^variable_name,/d}' > "$1"/csv_report.combined_csv
