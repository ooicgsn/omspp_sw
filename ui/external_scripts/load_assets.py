import os
import sys
# FIXME (badams): multiple datetime imports
import datetime
import argparse
import re
import pandas as pd
import numpy as np
from urllib.parse import urljoin
from io import BytesIO
import requests
import psycopg2
from webapp.settings import DATABASES
from collections import defaultdict
from datetime import datetime
from django.conf import settings

def filter_bad_vars(varname):
    """Boolean check to determine if any invalid variable names are present"""
    var_blacklist = ['^NC_GLOBAL$', '^deploy_id$', '^http.*$', '.*feature.*']
    return not any(re.search(pat, varname) for pat in var_blacklist)

def get_valid_variables(info_df):
    """
    Takes an ERDDAP info dataframe and outputs a pivoted dataframe with only
    valid variables and associated attributes
    """
    # exclude global attributes so that only variable related rows remain
    variable_df = info_df[info_df['Variable Name'].apply(filter_bad_vars)]
    # pivot on vars for index, attrs for columns
    pivot_vars = variable_df.pivot('Variable Name', 'Attribute Name', 'Value')
    # axis info should be present in most datasets
    if 'axis' not in pivot_vars:
        pivot_vars['axis'] = np.NaN
    # We can't reliably establish variable dimension from TableDAP datasets,
    # so platform is used as an arbitrary proxy to determine which variables
    # have dimensions.

    if 'platform' in pivot_vars:
        return pivot_vars[pivot_vars['platform'].notnull() |
                          pivot_vars['axis'].notnull()]
    # if it's not there, anything else goes until there's a reliable way of
    # determining dimension info.
    else:
        return pivot_vars

def load_assets(file_location):
    """Get ERDDAP user info from database"""
    user = settings.ERDDAP_USERNAME
    password = settings.ERDDAP_PASSWORD
    auth = requests.auth.HTTPBasicAuth(user, password)
    base_url = settings.ERDDAP_URL

    # authenticate with ERDDAP
    req = requests.get(urljoin(base_url + '/', 'info/index.csv'), auth=auth)

    # create database connection from settings
    db_settings = DATABASES['default']
    conn = psycopg2.connect(host=db_settings['HOST'],
                               port=db_settings['PORT'] or '5432',
                               dbname=db_settings['NAME'],
                               user=db_settings['USER'],
                               password=db_settings['PASSWORD'])


    # begin load assets
    newFile = open(file_location, 'r')
    FILE = newFile.readlines()
    newFile.close()
    section = ""
    sections = {}
    allsections = {}

    #create sections to split into dicts
    for line in FILE:
        if not line.strip():
            continue

        if ('Platform Deployment Info Section' in line):
            section = "pdis"
            continue
        elif ('Platform Configuration Section' in line):
            section = "pcs"
            continue
        elif ('Power Configuration Section' in line):
            section = "pocs"
            continue
        elif ('Telem Configuration Section' in line):
            section = "tcs"
            continue
        elif ('DCL Port Config  Section' in line):
            section = "dpcs"
            continue
        elif ('STC Port Config' in line):
            section = "stcpc"
            continue
        elif (line[:1] == "#") or (line in ['\n', '\r\n']) or ("######" in line):
            continue
        elif line in ['\n', '\r\n']:
             continue
        else:
            line = line.replace("=", "|").replace('\n', '').split("#", 1)[0]
            values = line.split('|')
            key = values[0].strip()
            val = values[1].strip() if len(values) > 1 else ''

            if section in allsections:
                sections.update({key: val})
                allsections.update({section: sections})
            else:
                sections = {}

                sections.update({key: val})
                allsections.update({section: sections})

    #create seperate dicts
    if 'pdis' in allsections:
        pdis = allsections["pdis"]
    if 'pcs' in allsections:
        pcs = allsections["pcs"]
    if 'pocs' in allsections:
        pocs = allsections["pocs"]
    if 'tcs' in allsections:
        tcs = allsections["tcs"]
    if 'dpcs' in allsections:
        dpcs = allsections["dpcs"]
    if 'stcpc' in allsections:
        stcpc = allsections["stcpc"]

    # establish connection
    # try:
    #     conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (args.dbname, args.user, args.host, args.password))
    # except:
    #     print("Unable to connect to the database")


    # Open a cursor to perform database operations
    cur = conn.cursor()

    #create asset type lists
    cur.execute("SELECT id, name from asset_type")
    assetTypes = cur.fetchall()

    el = [x for x in assetTypes if x[1] == "Buoy"][0][0]

    #Select Group
    if 'Global' in pdis['Deploy.location']:
        cur.execute("SELECT id FROM public.asset_group WHERE name like %s", ( 'Global',))
        groupid = cur.fetchone()

    if 'Pioneer' in pdis['Deploy.location']:
        cur.execute("SELECT id FROM public.asset_group WHERE name like %s", ( 'Coastal - Pioneer',))
        groupid = cur.fetchone()

    if 'Endurance' in pdis['Deploy.location']:
        cur.execute("SELECT id FROM public.asset_group WHERE name like %s", ( 'Coastal - Endurance',))
        groupid = cur.fetchone()

    if 'AUV' in pdis['Deploy.location']:
        cur.execute("SELECT id FROM public.asset_group WHERE name like %s", ( 'AUV',))
        groupid = cur.fetchone()

    if 'Gliders' in pdis['Deploy.location']:
        cur.execute("SELECT id FROM public.asset_group WHERE name like %s", ( 'Gliders',))
        groupid = cur.fetchone()


    #create group if none match
    if groupid is None:
        cur.execute(""" INSERT INTO public.asset_group(name, sort_order)VALUES (%s, %s);;""", (pdis['Deploy.location'], 1))
        cur.execute("SELECT id FROM public.asset_group WHERE name = (%s);", (pdis['Deploy.location'],))
        groupid = cur.fetchone()


    #Load Deployment
    sdate = None
    edate = None

    if ("dd" or "yy" or "mm" in pdis['Deploy.sdate']):
        sdate = None
    else:
        sdate = datetime.datetime.strptime(pdis['Deploy.sdate'], '%Y/%m/%d').date()

    if ("dd" or "yy" or "mm" in pdis['Deploy.edate']):
        edate = None
    else:
        edate = datetime.datetime.strptime(pdis['Deploy.edate'], '%Y/%m/%d').date()

    #Load Deployment
    cur.execute("SELECT id FROM public.deployment WHERE name = %s", (pdis['Deploy.id'],))
    deployid = cur.fetchone()

    if deployid is None:
        cur.execute("""INSERT INTO public.deployment(
            created, modified, name, code, deployment_date, end_date)
            VALUES (%s, %s, %s, %s, %s, %s);;"""
            ,(datetime.now(), datetime.now(), pdis['Deploy.id'], pdis['Deploy.id'], sdate, edate, ))

        cur.execute("SELECT id FROM public.deployment WHERE name = (%s);", (pdis['Deploy.id'],))
        deployid = cur.fetchone()


    #Load Platform
    cur.execute("SELECT id FROM public.asset WHERE name = %s", (pcs['Platform.id'],))
    platformid = cur.fetchone()

    if platformid is None:
        cur.execute("""INSERT INTO public.asset(
            created, modified, name, code, status_is_manually_set,
            current_deployment_id, group_id, type_id, status_id, platform_id)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);;"""
                    , (datetime.now(), datetime.now(), pcs['Platform.id'], pcs['Platform.id'], True, deployid, groupid, [x for x in assetTypes if x[1] == "Platform"][0][0], 1, platformid,))

        cur.execute("SELECT id FROM public.asset WHERE name = (%s);", (pcs['Platform.id'],))
        platformid = cur.fetchone()


    emptyAssets = frozenset(['', 'N/A'])


    #Load Buoy
    if 'Platform.buoy.inst_cfg' in pcs and pcs['Platform.buoy.inst_cfg'].strip() not in emptyAssets:
        buoyName = "BUOY"
        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (buoyName, platformid,))
        buoyId = cur.fetchone()

        if buoyId is None:
            cur.execute("""INSERT INTO public.asset(
                    created, modified, name, code, status_is_manually_set,
                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);;"""
                        , (datetime.now(), datetime.now(), buoyName, buoyName, True,
                           deployid, groupid, [x for x in assetTypes if x[1] == "Buoy"][0][0], 1, platformid,  platformid,))

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (buoyName, platformid,))
            buoyId = cur.fetchone()

            for i in tcs:
                if "xeos" in i:
                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                (i[6:11], buoyId))
                    instId = cur.fetchone()

                    if instId is None:
                        cur.execute("""INSERT INTO public.asset(
                                                        created, modified, name, code, status_is_manually_set,
                                                        current_deployment_id, group_id, type_id, status_id, parent_id, platform_id )
                                                        VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                    , (datetime.now(), datetime.now(), i[6:11], i[6:11], True,
                                       deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, buoyId, platformid))


    # Load NSIF
    if 'Platform.nsif.inst_cfg' in pcs and pcs['Platform.nsif.inst_cfg'].strip() not in emptyAssets:
        nsifName = "NSIF"
        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (nsifName, platformid,))
        nsifId = cur.fetchone()

        if nsifId is None:
            cur.execute("""INSERT INTO public.asset(
                        created, modified, name, code, status_is_manually_set,
                        current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                        VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                            , (datetime.now(), datetime.now(), nsifName, nsifName, True,
                               deployid, groupid, [x for x in assetTypes if x[1] == "NSIF"][0][0], 1, platformid, platformid))

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (nsifName, platformid,))
            nsifId = cur.fetchone()


    # Load MFN
    if 'Platform.mfn.inst_cfg' in pcs and pcs['Platform.mfn.inst_cfg'].strip() not in emptyAssets:
        mfnName = "MFN"
        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (mfnName, platformid,))
        mfnId = cur.fetchone()

        if mfnId is None:
            cur.execute("""INSERT INTO public.asset(
                            created, modified, name, code, status_is_manually_set,
                            current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                            VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                        , (datetime.now(), datetime.now(), mfnName, mfnName, True,
                           deployid, groupid, [x for x in assetTypes if x[1] == "MFN"][0][0], 1, platformid, platformid))

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (mfnName, platformid,))
            mfnId = cur.fetchone()


    # Load WFP
    if 'Platform.wfp.inst_cfg' in pcs and pcs['Platform.wfp.inst_cfg'].strip() not in emptyAssets:
        wfpName = "WFP"
        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (wfpName, platformid,))
        wfpId = cur.fetchone()

        if wfpId is None:
            cur.execute("""INSERT INTO public.asset(
                            created, modified, name, code, status_is_manually_set,
                            current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                            VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                        , (datetime.now(), datetime.now(), wfpName, wfpName, True,
                            deployid, groupid, [x for x in assetTypes if x[1] == "MFN"][0][0], 1, platformid,
                            platformid))

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (wfpName, platformid,))
            wfpId = cur.fetchone()

    # Load IMM
    if 'Platform.imm.inst_cfg' in pcs and pcs['Platform.imm.inst_cfg'].strip() not in emptyAssets:
        immName = "IMM"
        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (immName, platformid,))
        immId = cur.fetchone()

        if immId is None:
            cur.execute("""INSERT INTO public.asset(
                            created, modified, name, code, status_is_manually_set,
                            current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                            VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                        , (datetime.now(), datetime.now(), immName, immName, True,
                           deployid, groupid, [x for x in assetTypes if x[1] == "MFN"][0][0], 1, platformid,
                           platformid))

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (immName, platformid,))
            immId = cur.fetchone()


    #Load buoy Hotel Config
    #cpm is the first instrument. set its parent to buoy
    if 'Platform.buoy.inst_cfg' in pcs and pcs['Platform.buoy.inst_cfg'].strip() not in emptyAssets:
        instrumentParentID = buoyId
        buoyHotelConfig = pcs['Platform.buoy.hotel_cfg'].split(",")

        for inst in filter(None, buoyHotelConfig):
            if "cpm" in inst:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, buoyId))
                instCheckId = cur.fetchone()

                if instCheckId is None:
                    cur.execute("""INSERT INTO public.asset(
                                       created, modified, name, code, status_is_manually_set,
                                       current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                       VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid, [x for x in assetTypes if x[1] == "CPM"][0][0], 1, buoyId, platformid))

                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, buoyId))
                instId = cur.fetchone()
                instrumentParentID = instId

            elif "stc" in inst:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, buoyId))
                instCheckId = cur.fetchone()

                if instCheckId is None:
                    cur.execute("""INSERT INTO public.asset(
                                       created, modified, name, code, status_is_manually_set,
                                       current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                       VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid, [x for x in assetTypes if x[1] == "STC"][0][0], 1, buoyId, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, buoyId))
                    instId = cur.fetchone()
                    instrumentParentID = instId

                    #Loop through the port configurations and add ports and instruments
                    for i in stcpc:
                        if inst in i:
                            if ".inst." in i:
                                continue
                            elif "inst" in i:
                                stcParentID = instId
                                cur.execute("""INSERT INTO public.asset(
                                                created, modified, name, code, status_is_manually_set,
                                                current_deployment_id, group_id, type_id, status_id, parent_id, port, platform_id)
                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), i[5:-5], i[5:-5], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Port"][0][0], 1, stcParentID, i[9:-5], platformid))
                                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                            (i[5:-5], stcParentID))
                                stcinstId = cur.fetchone()

                                stcParentID = stcinstId
                                cur.execute("""INSERT INTO public.asset(
                                                created, modified, name, code, status_is_manually_set,
                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), stcpc[i], stcpc[i], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, stcParentID, platformid))

            elif "dcl" in inst:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                instId = cur.fetchone()

                if instId is None:
                    cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid,  [x for x in assetTypes if x[1] == "DCL"][0][0], 1, instrumentParentID, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                    instId = cur.fetchone()

                    # Loop through the port configurations and add ports and instruments
                    for i in dpcs:
                        if inst in i:
                            if ("inst" in i) and (".inst." not in i):
                                dclParentID = instId
                                cur.execute("""INSERT INTO public.asset(
                                                created, modified, name, code, status_is_manually_set,
                                                current_deployment_id, group_id, type_id, status_id, parent_id, port, platform_id)
                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), i[6:-5], i[6:-5], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Port"][0][0], 1, dclParentID, int(i[10:-5]), platformid))
                                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                            (i[6:-5], dclParentID))
                                dclinstId = cur.fetchone()

                                dclParentID = dclinstId
                                cur.execute("""INSERT INTO public.asset(
                                                created, modified, name, code, status_is_manually_set,
                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), dpcs[i], dpcs[i], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, dclParentID, platformid))

            elif "fb" in inst:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                instId = cur.fetchone()

                if instId is None:
                    cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                    instId = cur.fetchone()
                    instrumentParentID = instId

                    for i in tcs:
                        if "fb" in i:
                            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                        (i[6:9], instrumentParentID))
                            instId = cur.fetchone()

                            if instId is None:
                                cur.execute("""INSERT INTO public.asset(
                                                                created, modified, name, code, status_is_manually_set,
                                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), i[6:9], i[6:9], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))


            elif "isu" in inst:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                instId = cur.fetchone()

                if instId is None:
                    cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                    instId = cur.fetchone()
                    instrumentParentID = instId

                    for i in tcs:
                        if "isu" in i:
                            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                        (i[6:10], instrumentParentID))
                            instId = cur.fetchone()

                            if instId is None:
                                cur.execute("""INSERT INTO public.asset(
                                                                created, modified, name, code, status_is_manually_set,
                                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), i[6:10], i[6:10], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

            elif "sbd" in inst:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                instId = cur.fetchone()

                if instId is None:
                    cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                    instId = cur.fetchone()
                    instrumentParentID = instId

                    for i in tcs:
                        if "sbd" in i:
                            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                        (i[6:10], instrumentParentID))
                            instId = cur.fetchone()

                            if instId is None:
                                cur.execute("""INSERT INTO public.asset(
                                                                created, modified, name, code, status_is_manually_set,
                                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                            , (datetime.now(), datetime.now(), i[6:10], i[6:10], True,
                                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

            else:
                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
                instId = cur.fetchone()

                if instId is None:
                    cur.execute("""INSERT INTO public.asset(
                                        created, modified, name, code, status_is_manually_set,
                                        current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                        VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                , (datetime.now(), datetime.now(), inst, inst, True,
                                   deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))


    # Load NSIF Hotel Config
    # cpm is the first instrument. set its parent to buoy
    if 'Platform.nsif.inst_cfg' in pcs and pcs['Platform.nsif.inst_cfg'].strip() not in emptyAssets:
        instrumentParentID = nsifId
        nsifHotelConfig = pcs['Platform.nsif.hotel_cfg'].split(",")
        for inst in filter(None, nsifHotelConfig):
                if "cpm" in inst:
                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, nsifId))
                    instCheckId = cur.fetchone()

                    if instCheckId is None:
                        cur.execute("""INSERT INTO public.asset(
                                               created, modified, name, code, status_is_manually_set,
                                               current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                               VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                        , (datetime.now(), datetime.now(), inst, inst, True,
                                           deployid, groupid, [x for x in assetTypes if x[1] == "CPM"][0][0], 1, nsifId, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, nsifId))
                    instId = cur.fetchone()
                    instrumentParentID = instId

                elif "dcl" in inst:
                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                    (inst, instrumentParentID))
                    instId = cur.fetchone()

                    if instId is None:
                        cur.execute("""INSERT INTO public.asset(
                                                            created, modified, name, code, status_is_manually_set,
                                                            current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                            VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                    , (datetime.now(), datetime.now(), inst, inst, True,
                                        deployid, groupid, [x for x in assetTypes if x[1] == "DCL"][0][0], 1, instrumentParentID, platformid))

                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                    (inst, instrumentParentID))
                    instId = cur.fetchone()

                    # Loop through the port configurations and add ports and instruments
                    for i in dpcs:
                        if inst in i:
                            if ".inst." in i:
                                continue
                            elif "inst" in i:
                                dclParentID = instId
                                cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, port, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s);;"""
                                                , (datetime.now(), datetime.now(), i[6:-5], i[6:-5], True,
                                                   deployid, groupid, [x for x in assetTypes if x[1] == "Port"][0][0], 1, dclParentID, i[10:-5], platformid))
                                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                                (i[6:-5], dclParentID))
                                dclinstId = cur.fetchone()

                                dclParentID = dclinstId
                                cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                                , (datetime.now(), datetime.now(), dpcs[i], dpcs[i], True,
                                                   deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, dclParentID, platformid))

                    else:
                        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                    (inst, instrumentParentID))
                        instId = cur.fetchone()

                        if instId is None:
                            cur.execute("""INSERT INTO public.asset(
                                                created, modified, name, code, status_is_manually_set,
                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                        , (datetime.now(), datetime.now(), inst, inst, True,
                                           deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

                        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                    (inst, instrumentParentID))

    # Load MFN Hotel Config
    # cpm is the first instrument. set its parent to buoy
    if 'Platform.mfn.inst_cfg' in pcs and pcs['Platform.mfn.inst_cfg'].strip() not in emptyAssets:
        instrumentParentID = mfnId
        mfnHotelConfig = pcs['Platform.mfn.hotel_cfg'].split(",")

        for inst in filter(None, mfnHotelConfig):
                if "cpm" in inst:
                    cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                            (inst, mfnId))
                    instCheckId = cur.fetchone()

                    if instCheckId is None:
                        cur.execute("""INSERT INTO public.asset(
                                                               created, modified, name, code, status_is_manually_set,
                                                               current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                               VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                                , (datetime.now(), datetime.now(), inst, inst, True,
                                                   deployid, groupid, [x for x in assetTypes if x[1] == "CPM"][0][0], 1, mfnId, platformid))

                        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                            (inst, mfnId))
                        instId = cur.fetchone()
                        instrumentParentID = instId

                elif "dcl" in inst:
                        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                            (inst, instrumentParentID))
                        instId = cur.fetchone()

                        if instId is None:
                            cur.execute("""INSERT INTO public.asset(
                                                                            created, modified, name, code, status_is_manually_set,
                                                                            current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                                            VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                                , (datetime.now(), datetime.now(), inst, inst, True,
                                                   deployid, groupid, [x for x in assetTypes if x[1] == "DCL"][0][0], 1, instrumentParentID, platformid))

                            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                            (inst, instrumentParentID))
                            instId = cur.fetchone()

                            # Loop through the port configurations and add ports and instruments
                            for i in dpcs:
                                if inst in i:
                                    if ".inst." in i:
                                        continue
                                    elif "inst" in i:
                                        dclParentID = instId
                                        cur.execute("""INSERT INTO public.asset(
                                                                    created, modified, name, code, status_is_manually_set,
                                                                    current_deployment_id, group_id, type_id, status_id, parent_id, port, platform_id)
                                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s);;"""
                                                        , (
                                                        datetime.now(), datetime.now(), i[6:-5], i[6:-5],
                                                        True,
                                                        deployid, groupid, [x for x in assetTypes if x[1] == "Port"][0][0], 1, dclParentID, i[10:-5], platformid))
                                        cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                                        (i[6:-5], dclParentID))
                                        dclinstId = cur.fetchone()

                                        dclParentID = dclinstId
                                        cur.execute("""INSERT INTO public.asset(
                                                                    created, modified, name, code, status_is_manually_set,
                                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                                        , (
                                                        datetime.now(), datetime.now(), dpcs[i], dpcs[i],
                                                        True,
                                                        deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, dclParentID, platformid))

                else:
                            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                                (inst, instrumentParentID))
                            instId = cur.fetchone()

                            if instId is None:
                                cur.execute("""INSERT INTO public.asset(
                                                                created, modified, name, code, status_is_manually_set,
                                                                current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                                VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                                                    , (datetime.now(), datetime.now(), inst, inst, True,
                                                       deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1, instrumentParentID, platformid))

                                cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                                                (inst, instrumentParentID))


    # Load WFP Inst Config
    # cpm is the first instrument. set its parent to buoy
    if 'Platform.wfp.inst_cfg' in pcs and pcs['Platform.wfp.inst_cfg'].strip() not in emptyAssets:
        instrumentParentID = wfpId
        wfpInstConfig = pcs['Platform.wfp.inst_cfg'].split(",")

        for inst in filter(None, wfpInstConfig):

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s", (inst, instrumentParentID))
            instId = cur.fetchone()

            if instId is None:
                cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                            , (datetime.now(), datetime.now(), inst, inst, True,
                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1,
                               instrumentParentID, platformid))


    # Load WFP Inst Config
    # cpm is the first instrument. set its parent to buoy
    if 'Platform.imm.inst_cfg' in pcs and pcs['Platform.imm.inst_cfg'].strip() not in emptyAssets:
        instrumentParentID = immId
        immInstConfig = pcs['Platform.imm.inst_cfg'].split(",")

        for inst in filter(None, immInstConfig):

            cur.execute("SELECT id FROM public.asset WHERE name = %s and parent_id = %s",
                        (inst, instrumentParentID))
            instId = cur.fetchone()

            if instId is None:
                cur.execute("""INSERT INTO public.asset(
                                                    created, modified, name, code, status_is_manually_set,
                                                    current_deployment_id, group_id, type_id, status_id, parent_id, platform_id)
                                                    VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s);;"""
                            , (datetime.now(), datetime.now(), inst.split(':')[1], inst.split(':')[1], True,
                               deployid, groupid, [x for x in assetTypes if x[1] == "Instrument"][0][0], 1,
                               instrumentParentID, platformid))

    # update platform's platform ID
    cur.execute("""Update Asset SET platform_id = id, name = upper(name), code = upper(code) where name LIKE %s and current_deployment_id = %s;;"""
                , (pcs['Platform.id'], deployid,))

    # Update all loaded assets names to upper
    cur.execute("""Update Asset SET name = upper(name), code = upper(code) where platform_id = %s and current_deployment_id = %s;;"""
                , (platformid, deployid,))

    # update asset deployment table with new assets
    cur.execute("""Insert INTO asset_deployment(asset_id, deployment_id)
                    (SELECT id, %s FROM asset where current_deployment_id = %s and platform_id = %s);;"""
                    , (deployid, deployid, platformid))

    # update asset history table with new assets
    cur.execute("""INSERT INTO public.asset_history(
                    created, status_id, prev_status_id, reason, asset_id, user_id)
                    (SELECT %s, 1, NULL, 'Initial Asset Load', id, 1 FROM asset where current_deployment_id = %s and platform_id = %s );;;"""
                    , (datetime.now(), deployid, platformid))

    #Insert Variables

    #choose fields to load csv
    fields = ['Title', 'tabledap']
    tables_all = pd.read_csv(BytesIO(req.content), header=0, usecols=fields)
    tables = tables_all[tables_all.notnull()]

    for index, row in tables.iterrows():
        # Some of these rows are gridded datasets, which we are not using for alerts and alarms
        if isinstance(row['tabledap'], float):
            # TODO (badams): Use logging instead
            print('Gridded dataset found; skipping (row={} value={})'.format(index, str(row['tabledap'])))
            continue

        if row['tabledap'].endswith('allDatasets'):
            continue
        else:
            ref_des = row['tabledap'].split('/')[-1]
            url_target = urljoin(base_url + '/', 'info/{}/index.csv'.format(ref_des))

        dd_loc = defaultdict(lambda: 1, **{'CP': 2, 'CE': 3})
        # get the first two letters of the asset to get the overall location
        # ("asset_group")
        asset_code = ref_des[:2]
        asset_group_id = dd_loc[asset_code]

        # # TODO: (badams) convert over to use Django ORM instead
        # cur.execute("""INSERT INTO asset (name, created, modified, code, group_id, status_is_manually_set, status_id,
        #                    type_id) VALUES (%s, now(), now(), %s, %s, false, 1, 4) ON CONFLICT DO NOTHING""",
        #             (ref_des, ref_des.split('-')[0], asset_group_id))

        location_path = row['Title'].split(' ')
        platform_name = location_path[0]
        inst_name = location_path[-1]

        cur.execute("""SELECT name FROM asset WHERE id = %s""", (platformid,))
        platform_asset_name = cur.fetchone()

        platform_dataset = pcs['Platform.id'].upper() + '-BUOY-001-PWRSYS'
        platform_flag = False

        if platform_dataset == ref_des:
            #cur.execute("""UPDATE asset SET ERDDAP_ID = %s  WHERE ID  = %s""", (platform_dataset, platformid))
            platform_flag = True
            #print(platform_dataset, platformid)

        if platform_flag or (str(platform_asset_name[0]).lower() in
                             str(platform_name).lower()):

            cur.execute("""SELECT id FROM asset WHERE LOWER (name) = %s AND platform_id = %s LIMIT 1""",
                        (str.lower(str(inst_name)), platformid,))

            asset_id = cur.fetchone()
            if asset_id == None and platform_flag == True:
                cur.execute("""SELECT id FROM asset WHERE LOWER (name) = %s AND platform_id = %s LIMIT 1""",
                            (str.lower(str(pcs['Platform.id'])), platformid,))

                asset_id = cur.fetchone()

            cur.execute("""UPDATE asset SET ERDDAP_ID = %s  WHERE ID  = %s""", (ref_des, asset_id,))


            if asset_id is not None:
                # get units
                req = requests.get(url_target, auth=auth)
                tabledap_info = pd.read_csv(BytesIO(req.content), header=0)
                valid_vars = get_valid_variables(tabledap_info)

                now = datetime.utcnow()
                # use .get in case there's bad metadata lacking long_name
                # and/or units so that exception is not thrown
                ins_rows = [(now, now, var_attrs.get('long_name') or var_name,
                             var_name, var_attrs.get('units'), False,
                             asset_id, platformid, deployid)
                            for var_name, var_attrs in valid_vars.iterrows()]

                cur.executemany("""INSERT INTO variable (created, modified, name, erddap_id, units, is_custom,
                                                         asset_id, is_plottable, platform_id, deployment_id)
                                           VALUES (%s, %s, %s, %s, %s, %s, %s, true, %s, %s)""", ins_rows)


    conn.commit()

    # Close communication with the database
    cur.close()
    conn.close()

    # management.call_command('rebuild_asset_links', '--all', verbosity=0)

    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("fileLocation")
    parser.add_argument("projectLocation")

    args = parser.parse_args()

    #begin web service gateway
    proj_path = args.projectLocation
    file_loc = args.fileLocation

    # pointer to django settings.
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.settings")
    sys.path.append(proj_path)

    # Load Local settings
    os.chdir(proj_path)

    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()

    #end webservice gateway
    load_assets(file_loc)
