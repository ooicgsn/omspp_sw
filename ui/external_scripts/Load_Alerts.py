import psycopg2 as psy
from datetime import datetime as dt
import json, csv
from webapp.settings import DATABASES

def load_alerts():
    alert_id_seed = 1
    try:
        db_settings = DATABASES['default']
        #conn = psy.connect("dbname='omsplusplus' user='omsplusplus' host='localhost' password='password'")
        conn = psy.connect(host=db_settings['HOST'],
                                port=db_settings['PORT'] or '5432',
                                dbname=db_settings['NAME'],
                                user=db_settings['USER'],
                                password=db_settings['PASSWORD'])
        cur = conn.cursor()

        with open('external_scripts/alerts_csv/alerts.csv') as file:
            reader = csv.DictReader(file)
            rows = list(reader)

        alert_dict = {}

        for row in rows:
            if row['description'] != '':
                alert_dict[row['description']] = row

        for alert,definition in alert_dict.items():
            now = dt.now().isoformat()

            cur.execute("SELECT asset.id, variable.id "
                        "FROM asset asset "
                        "INNER JOIN variable variable "
                        "ON asset.erddap_id LIKE '%s' AND "
                        "variable.asset_id = asset.id AND "
                        "variable.erddap_id LIKE '%s';"
                        %(definition['erddap_id'],definition['parameter']))

            try:
                asset_id, variable_id = cur.fetchone()
            except:
                continue


            cur.execute("INSERT INTO alert_trigger"
                        "(id, created, modified, name, duration, is_global, asset_id, "
                        "created_by_id, duration_code_id, severity_id) "
                        "VALUES ('%s', TIMESTAMP '%s', TIMESTAMP '%s', '%s', %s, TRUE, '%s', 1, %s,  '%s');"
                        %(alert_id_seed, now, now, alert, definition['duration'],asset_id,definition['duration_code'],definition['severity']))

            operation = definition['parameter'] + ' ' + definition['operator'] + ' ' + definition['value']

            # TODO(mchagnon): This needs to be changes into just an expression instead of rule/variable links
            cur.execute("INSERT INTO rule "
                        "(id, expression, trigger_id) "
                        "VALUES ('%s', '%s', '%s');"
                        %(alert_id_seed, operation, alert_id_seed))

            cur.execute("INSERT INTO rule_variable "
                        "(id, name, rule_id, variable_id) "
                        "VALUES ('%s','%s','%s','%s');"
                        %(alert_id_seed, definition['parameter'], alert_id_seed, variable_id))

            alert_id_seed = alert_id_seed + 1

        # Reset the table sequence IDs to the next valuerule_variable
        cur.execute("SELECT setval('alert_trigger_id_seq', (SELECT MAX(id) FROM alert_trigger));")
        cur.execute("SELECT setval('rule_id_seq', (SELECT MAX(id) FROM rule));")
        cur.execute("SELECT setval('alert_trigger_id_seq', (SELECT MAX(id) FROM rule_variable));")

        conn.commit()
        conn.close()

    except Exception as e:
        conn.rollback()
        conn.close()
        #print(e.message)
        raise e

if __name__ == '__main__':
    load_alerts()