### Release v1.9.26- February 25th, 2025

### Added
* Need new mooring category "Testbed"
* Add error code parsing modal to parsed data grid

### Release v1.9.25 - November 16th, 2024

### Added
* Sync ERDDAP time windows when changing plot settings (when enable by the user) 

### Changed
* Increase gunicorn and nginx timeouts to allow for longer running ERDDAP requests

### Fixed
* Variable list not populating in more recent version of Chrome

### Release v1.9.24 - October 20th, 2024

### Added
* Add ability to sync time based plots 

### Changed
* Sort parsed file dropdown alphabetical instead of by the file's timestamp

### Release v1.9.23 - September 16th, 2024

### Added
* Reload meta data from configuration yaml files
* Deployed and Recovered Cruise fields on platforms
* Raw and parsed data "globs" to store file locations
* Reference designator and serial number fields
* Edit screen for asset fields

### Changed
* Allow loading XEOS and SBD log files from raw data "glob" location

### Release v1.9.22 - August 2nd, 2024

### Fixed
* Error viewing XEOS instruments on CP11SOSM

### Added
* Add deployment code to hotlist and alert details
* Add support for prawler data to "Parsed Data"

### Changed
* CTDMO parsed data showing time in usecs since 1970.
* Add previously unmerged javascript updates
 
### Release v1.9.21 - May 29th, 2024

### Fixed
* Panda "truthy" errors when running alerts

### Release v1.9.20 - May 21st, 2024

### Fixed
* Handling of missing mooring images
* Navigation bar refreshing when asset selected in grid
* Triggers fail to run due to missing trigger type

### Added
* 14 day time window to pre-defined plot durations
* MAB mooring images
* Parsed data processing logic for WFP data

### Changed
* Navigation bar Array names
* Improvements to plot page front end UI
* Improvements to mooring location front end UI

### Release v1.9.19 - March 16th, 2024

### Fixed
* Pandas warning when ERDDAP returns no data
* SBD/Xeos error when more than one was shown on the page
* Errors when creating standalone plot pages
* Errors when Deleting plots

### Added
* Customize the number of plots shown per row on the overview screen

### Release v1.9.18 - February 24th, 2024

### Fixed
* Cloning spinner never stops and user is not notified of results

### Release v1.9.17 - February 11th, 2024

### Fixed
* Refactor of frontend plot code
* Issues retaining sort order when a user drags and drops plots

### Added
* Display current time (UTC) in sidebar
* Triggers based on last update time

### Changed
* Allow .log, .txt and .cfg files to be viewed rather than downloaded

### Release v1.9.14 - July 13th, 2023

### Fixed
* Basic unit tests

### Added
* Initial default options for asset classifications
* Apply asset classification when importing or reloading moorings

### Changed
* Remove deprecated data files menu item and page
* Improved logic behind mapping folders for parsed data
* Improved logic for loading parsed data files
* Updates to last update field at the top of asset pages



### Release v1.9.13 - May 31st, 2023

### Fixed
* Updated casting logic for map page due to the deprecation of a numpy feature
* Export of .png images from plots
* Incorrectly changing "shared" setting when importing plots

### Added
* Ability to use variables from the database in triggers
* Initial capability to classify assets

### Changed
* Require a unique email address per user account
* Remove max(time) restriction on end date to work around ERDDAP data issues
* Improve reset password functionality
* Mapping logic for importing entities to use email for user mapping 

### Release v1.9.12 - April 8th, 2023

### Fixed
* Error when adding new L3 variables
* Plot data not working when switching "sub" types on WFP instruments

### Added
* Add scale bar to mooring map
* Parsed data not displaying for GI01SUMO IMM instruments
* Parsed data for ADCP on AS03CPSM needs additional processing
* Forgot/lost password functionality

### Changed
* Keep BUOY, NSIF, MFN status tabs visible regardless of selected instrument
 
### Release v1.9.11 - March 17th, 2023

### Fixed
* Mapping of turbidity sensor dataset to instrument
* Remove close icon on additional plots area
* Plots relative to now show misleading selection
* Shared plots appear to be saved initially as not shared
* Fix database migration error
* Hotel values (wake codes) missing from STCs

### Added
* Plotting of parsed data
 
### Changed
* Update logo
* Update nginx configuration to allow ERDDAP port to come from .env

### Release v1.9.10 - February 10th, 2023

### Fixed
* Variables from deleted assets showing in plot dropdown
* Deprecation warnings at startup
* Group check when importing and reloading moorings

### Added
* Alternate path for loading SBD log files
* Modal for PSC override flags
* Display of parsed WFP instrument data

### Release v1.9.9 - January 27th, 2023

### Fixed
* Group checks when importing mooring configuration
* Missing telemetry schedule in snapshots
* CPM1 status file path
* Deployment field when adding L3 variables to a trigger
* Mapping for parsed data on BUOY/IRID-1

### Added
* Improvements to the CPM and DCL snapshots
* Watch circle on map page
* SBD (IRID/XEOS) logs
* Ability to show multivalued data on plots (spectral plotting)
 
### Changed
* Logo and affiliations
* Times for mooring locations on the map 

### Release v1.9.8 - December 13th, 2022

### Fixed
* NaN values in parsed data preventing parsed data grid from rendering
* Errors calling ERDDAP from get_latest_positions
* Ordering of plots not being retained after drag and drop

### Added
* Importing of triggers and plots that contain L3 variables
* Snapshot/landing page content from CPM and DCL status files

### Changed
* Upgraded to Django 4.x

### Release v1.9.7 - October 15th, 2022

### Fixed
* Various issues minor issues with alerts and processing

### Added
* Toggle option for paging on parsed data grid
* Support hierarchy/nested parsed data on some instruments
* Initial support for MAB moorings

### Changed
* Temporary removal of digest email options
* Additional logic around folder mapping for parsed data to support OSU naming conventions
* Hide gliders

### Release v1.9.6 - September 10th, 2022

### Fixed
* Plots w/ non-time x-axis do not handle time period correctly
* Manual plot button changes x-axis to time

### Added
* Support display of parsed data as grid within application
* Show ERDDAP endpoint in context menu of plots
* Add help and documentation to OMS++

### Release v1.9.5 - August 7th, 2022

### Fixed
* Mooring map incorrectly assigns current position icon to oldest point
* Fix Pandas errors with triggers due to the version increase
* Dataset ID duplicates on pinebank
  * Prevent duplicates when importing plots, triggers and L3s
  * Prevent duplicates when reloading configuration
  * Add additional check to prevent importing plots, triggers and L3s when the required ERDDAP dataset IDs are not present in the target system

### Changed
* Proxy ERDDAP through UI docker compose


### Release v1.9.4 - July 5th, 2022

### Fixed
* Ordering of plots not being retained
* Duplicate matching assets on IMM causing error
* Syntax warnings when running Django migrations/dev server
* Save Custom Plot not saving timeframe
* Error on data files page when hitting a broken symlink on the filesystem

### Added
* Multiple updates to import and export plots, triggers and L3 variables
  * Select multiple entities when exporting
  * Export all option
  * Allow name change on all imported entities
  * Allow some information on all entites during the import
* Add "default on platform" to import/export of triggers 

### Changed
* Hosting of data files for Data Dir link

### Release v1.9.3 - May 22nd, 2022

### Fixed
* Asset import failure for BUOY PWRSYS
* Emails from manual alerts are not being sent
* Missing import when creating a manual alert
* LOCAL_DATA_DIR passed through the docker container
* Django warnings when dealing with migrations
* Changing time frame dropdown throws javascript error
* Remove Double Imports from Templates
* Fix Pandas deprecation warnings the 
* Cloning category field on trigger clones
* Debugging info in logs when saving platforms
* Prevent duplicate triggers/plots when reloading platform configuration
* Cloning to similar instruments that different only in the numerical suffix
* Plotting data where the X or Y axis contains NaN's

### Added
* NetRC credential support through docker
* Support new time option on plots: "Last Reported Data"
* Django command to list out mapping matrix between ERDDAP and the UI
* Django command to reload all platforms, and/or by platform and/or by deployment
* Default On field to exported plots/triggers
* Django command to import and export recovery/deployment dates for all moorings

### Changed
* Accept alternate WFPENG dataset title
* All cloning should handle "similar" targets having simple numeric suffix differences
* Mapping logic for some instruments to match new ERDDAP dataset titles/IDs
* Removed asterisk on Default On field since its not required

### Release v1.9.2 - February 2nd, 2022

### Fixed
* Python error when saving a new user
* Unsorted owner dropdown on trigger, L3 and plot edit screens
* Issues with DATA_DIRECTORY setting

### Added
* Import and export of triggers
* Import and export of plots
* Import and export of L3 variables
* Nginx templates for local (non-ssl) vs production/staging (ssl) environments
* Added numexpr to requirements.txt

### Changed
* Prevent L3's using other L3's (restrict to one level deep)
* Moved various settings into environment variables through docker

### Release v1.9.1 - January 18th, 2022 

### Fixed
* Typo on platform import screen
* Error handling from 404 ERDDAP errors
* Platform disappears from deployed list when re-checked

### Added
* Link to platform on manage platforms screen
* Disposition filter on platforms screen
* Remove related platform_default_plot records when deleting platforms

### Changed
* Changed button text on hotlist to "Ack'd"

### Release v1.9.0

### Fixed
* Restored missing items from the v1.2 release

### Added
* Added restart:always flag to docker containers
* More exception handling for ERDDAP errors

### Changed
* Upgraded to Django 3.x
* Upgraded to PostgreSQL 13.x
* Containerized application with Docker
* Add SSL certificate paths based on local environment settings (.env)
* Make local data dir setting configurable through .env and docker
* Changed acknowledgement text on hot list

### Release v1.5.0 - May, 22nd, 2021

### Fixed
* Hot list not showing acknowledged alerts
* Last occurrence value for the hot list and detail page do not match
* During trigger creation: platform disappears from the pulldown list
* Incorrect UTC times in Alert information pages & status page

### Added
* View raw data contents directly from instrument pages
* Add link to platform from manage platforms page
* Add disposition filter checkboxes to Manage Platforms page

### Changed
* Trap errors when trying to pull the last update time from ERDDAP

### Release v1.1.1 - August 20th, 2020

### Fixed
* Deployment dropdown duplicates when viewing the overview page for ports
* Plot page plots showing "no data" until after the plot button is clicked

### Release v1.1.0 - August 2nd, 2020

### Added
* Ability to purge instruments from the database
* Platform support and UI for deployment/recovered dates, lat/lon and depth
* Asset support for deployment/recovered dates, lat/lon and depth (to be provided by Roundabout in the future)
* Ability to set default moorings that plots and triggers will be cloned to once imported

### Changed
* Plot dragging handle to just the title area to allow dragging on the plot to function normally
* Sorting of L3/Trigger/Plot grids to include deployment when sorting by asset or platform
* Plots added to overview page after creation
* Messages when importing a platfo rm that already exists

### Fixed
* Trapped/reduced error messages from get_latest_updates cron job
* Errors cloning assets
* Sorting on asset cloning page

### Release v1.0.0 - June 14th, 2020

### Added
* Ability to clone L3 (custom) variables
* Disposition filter to L3, trigger and plot listing screens
* Ability to completely remove a dataset from an asset (along with all variables)

### Changed
* Show DCL/STC variables when creating plots for port numbers 
* Default variable source assets when choosing a base asset on triggers and plots
* Default to l3s, triggers and plots on deployed moorings

### Fixed
* Fixed placement (not hierarchy) of DCL's on the status grid when the DCL has no direct CPM parent (UI only) 
* Sort order of the platform dropdown on the plot management screen 
* Fixed platform column showing the asset (and not the platform) on the L3 listing screen

## Release v0.17.6 - May 3rd, 2020

### Added
* Added owner name to L3 listing page
* Added name normalization to the units field on some variables to ensure that they were plotted on the same Y axis
* Added a new "Edit Dataset ID" feature to allow for full control over updating the dataset ID on an asset, including:
    * Matching the variables from the new dataset ID to the existing ones on that asset
    * Allowing variable names, IDs and units to be customized as needed
    * Restrictions to prevent removing in use variables (in triggers, L3s and plots)
    * Adding new variables not already present on the asset
    * Confirmation screen to verify the changes that will be made before updates are made
* Linked the dataset ID on the overview page to the new edit dataset ID feature when there is no dataset ID present on an asset

### Changed
* Removed the old, less functional "Update ERDDAP ID" modal window/feature

### Fixed
* Fix default range of plots to not max y-axis at 100% for "percentage" variables
* Added better error handling on the system overview page for alerts that had no severity set

## Release v0.17.5 - March 7th, 2020

### Added
* Added a configuration file to blacklist specific instruments from being imported (specifically ones where there would be no data streams)
* Allow operator users to view the mange plots page to manage their own plots

### Changed
* Added seconds to the timestamps shown when hovering over plot data
* Added dataset ID to the asset overview screen, along with a link to that asset in ERDDAP
* Added filtering by deployment to L3s, plotting and triggers
* Upgraded DataTables (client side grid component) to the newest version and refactored client side code for grids to improve readability and maintainability

### Fixed
* Errors when deleting certain plots and triggers
* Refactored/updated logic surrounding the platform category/subcategory list when modifying platforms
* Fixed misc. bugs found throughout the application related to the grids that were found while upgrading the component
* Fixed view link after importing a new platform

## Release v0.17.4 - October 26th, 2019

### Added
* Ability to purge alerts from the database via the UI
* Added deployment column to trigger and L3 management screens
* Added additional project documentation

### Changed
* Changed sidebar platform listing to group left and right columns by category

### Fixed
* Cleaned up logging of trigger processing cron to make errors more descriptive
* Added validation to make sure trigger duration was not zero
* Fixed issue with importing variables with "None" as the name in ERDDAP

## Release v0.17.3 - August 7th, 2019

### Added
* New content manager access level
* Updates to documentation
* Version indicator in the footer
* Deleting triggers (and related alerts) by name via a Django command
* Deleting plots by name via a Django command

### Changed
* Removed access to edit/import platforms for the operator access level
* Removed the config tool (functionality being moved to a separate project)

### Fixed
* Issue with "none" being displayed when editing an instrument's ERDDAP ID that did not have one set already
* Fixed default name used for ERDDAP variables that did not have "long_name" attribute set in ERDDAP
* Fixed issue determining mooring location (previously not finding the correct GPS instrument)

## Release v0.17.2 - July 18th, 2019

### Added
* Merged in recent updates to configuration data for WHOI moorings

### Changed
* Removed Ocean Leadership logo
* Removed sidebar footer icons
* Changed instances of "global" to "shared" to prevent naming conflict with the Global mooring array
* Set release date for the last (0.17.1) version

### Fixed
* Issue with renaming permissions
* Prevent moorings with a disposition not in deployed or burn-in from having their last update date changed
* Issues with deployment number parsing not accounting for test data without integer values
* Simplified code used in tests to check for certain conditions
* Issues with UTC time conversions related to ERDDAP data

## Release v0.17.1 - June 15th, 2019

### Fixed
* Issues with admin permissions to edit plots, l3 variables and triggers with admins other than the default administrator

## Release v0.17 - September 19th, 2018

#### Added
* Endurange gliders
* Documentation of the production server's configuration

#### Changed
* Logic to filter out triggers from processing on non deployed/recovered platforms and instruments
* Formatting of alert notifications to include additional information about the trigger that caused it

#### Fixed
* Issue with disposition on instruments not matching the disposition of the platform
* Sorting of deployments on the glider listing page
* Issue with importing/updating platforms from the config file in Chrome
* Fixed error when customizing plots on the asset overview page
* Issue when loading new gliders where the group was not being set properly
* Issues with cloning not catching all related assets
* Duration column on trigger listing page
* Issues with SMS alerts not being sent to the correct email accounts
* Issue with SMS alets and emails showing "none" for the related asset

## Release v0.16 - July 28th, 2018

#### Added
* Power system drill-down page

#### Changed
* Updated sort order of glider deployments on the overview pages

#### Fixed
* Issue with re-loading glider configuration file
* Issue with customize plot button not working and throwing an error
* Browser issue that caused the import plots page not to work in Chrome
* Display of the correct disposition on asset overview pages other than the platform itself

## Release v0.15 - May 9th, 2018

#### Added
* Ability to load platform configuration from the files located on the server in the raw data directory
* Saving of time window when creating/modifying plots
* Digest email summaries
* Filtering on glider overview page per group

#### Changed
* ERDDAP variables used to determine GPS lat/lon for plotting mooring location on a map
* Split glider menu on sidebar into separate groups

#### Fixed
* Changed call to pull latest updates on instruments to be deployment specific
* Permissions when editing plots

## Release v0.14 - May 5th, 2018

#### Added
* Documentation of cron jobs necessary to run the application
* Indication of interpolated variables shown on plots
* Enabled emails and texts for triggered alerts
* Redmine production project settings
* Ability to switch between dev and production projects

#### Changed
* Variables used to plot mooring locations on the map
* Improved handling of non-timeseries based plots

#### Fixed
* Various issues related to plotting

## Release v0.13 - April 28th, 2018

#### Added
* Identifying the GPS instrument on the asset load script
* Installation instructions for non-Ubuntu based Linux distributions

#### Changed
* Bolded deployed mooring names on the system overview page
* Improved naming of interpolation values on the L3 edit page

#### Fixed
* Validation formatting on failed logins
* Validation formatting on L3 variable add/edit screen
* Issue with load plots script not correctly identifying the correct deployment for default plots
* Cleanup, fixes and improvement to plots not based on time

## Release v0.12 - April 21st, 2018

#### Added
* Alert notifications of active alerts when new users are created
* Required field indicators to Redmine ticket page
* Deployment to the array status page
* Disposition filtering to the array status page
* Documentation for the release/deployment process
* Data directory links for all gliders on the overview page

#### Changed
* Default the status of newly imported moorings/deployments to inactive
* Error handling for data issues when making calls to ERDDAP


#### Fixed
* Issues when changing the category and subcategory on moorings
* Formatting issues on Redmine tickets
* Performance problems with loading mooring locations on a map
* Missing deployments/moorings on the map
* Problems plotting some instrument data, including WFPs
* Disposition filter on asset selection window
* Data directory link on moorings

## Release v0.11 - March 31st, 2018

#### Added
* Additional details to the alert history grid when the status grid is collapsed
* Support for instruments that were removed from an active deployment
* Link to site for end-users to submit issues with missing units on data
* Support for creating a generic Redmine ticket that is not associated with any alert
* Indication on the glider page as to whether or not the glider has reported data recently

#### Changed
* Only pull last update date for deployed and burn-in moorings
* Mooring links on side bar to link to the most recent deployed mooring, rather than just the most recent overall
* Error messages/reporting when plots cannot be rendered due to missing data within ERDDAP
    
#### Fixed
* Corrected outstanding issues with the global or user owned setting for triggers and plots
* Formatting/layout issues on the Redmine ticket creation page
* Issues submitting Redmine tickets once the external Redmine instance had been upgraded

## Release v0.10 - March 17th, 2018

#### Added
* Change log to the BitBucket repository
* Interpolation amount value to be entered and utilized for L3 plotting and alert processing
* Mooring location map that displays all moorings each of the arrays
* Alert count bar graphs under all moorings on the system overview page
* Additional filters on the system overview page to show alert counts, functional percentage, or both. Also stored the current view as a user preference

#### Changed
* The acknowledge link on the hot list is now only available on low severity alerts. Alerts with a higher severity will need to me reviewed in more detail before it can be acknowledged by a user
* Alert processing will only occur on moorings marked as deployed and burn-in

#### Fixed
* Issue with acknowledgement link on the hotlist page not working has been corrected
* The filters on the asset picker dialog window have been fixed
* Fixed issue with some L3/trigger expression not being accepted even though they were valid
* Improved error handling on plot rendering when data from the specified timeframe is not in ERDDAP
* Added caching to mooring location data (needed for the map) to increase page load times
* Significantly improved the algorithms used to map ERDDAP datasets to OMS++ instruments; many more assets within the application will now have data streams available them to plot and create triggers against

## Release v0.9 – February 24th, 2018

#### Added
* A new page was made to allow the editing of platforms/moorings after their initial import from the platform config files
* An initial alert count status bar has been added to the system overview page. More enhancements will follow, including user preferences for which version to show, the ability to see the alert bar for each mooring
* A category can now be selected for triggers
* A new page was created to allow a user to show the location of a mooring on map
* L3 variables can now be plotted and there is an indication of which values on the plot are custom variables
* Initial changes to allow manual alerts to be sent to specific users
* Initial changes to allow filtering of platforms by disposition when selecting them for triggers and L3s

#### Changed
* The glider load script, “load_gliders”, has been updated to allow updates as well as inserts. This means it can now be run repeatedly without creating duplicate gliders
* A category and subcategory can now be selected on moorings
* When creating new plots, the preview will be updated as parameters are changed
* Layout/formatting of the edit “plot page” page has been updated
* Only include open alerts on the mooring array overview page

#### Fixed
* The glider listing page is grouped so that all deployments for the same glider will appear in the same box
* Improvements to the validation of expressions on custom L3 variables and triggers
* Improvements to cloning including bug fixes and the ability to clone all triggers or plots from a mooring to another mooring
* Additional UI updates, layout cleanup and bugfixes
* Adjust size of alert status bar graph based on the number of alerts (to handle large alert counts)

## Release v0.8 – January 27th, 2018

#### Added
* Hovering over a mooring on the system overview page will provide you with a count of open alerts (grouped by severity)
* There is a new setting on the settings page to enable laptop mode currently will
    * Hide/show the create Redmine ticket button on alerts
    * Hide/show the Redmine API key field when editing users
* The ability to change the Y-axis direction on plots
* The ability to create plots where the X axis is not time
* The platform dropdown on the manage plots page includes the deployment
* There is now a comments field on alert triggers
* You can now acknowledge an alert from the alert details page
* Project Fi has been added as a carrier option for SMS texts

#### Changed
* Global -> Overview and Pioneer -> Overview are linked to separate pages
* The layout of the add/edit alert trigger page has been updated to be better organized
* The layout of the add/edit L3 page has been updated
* On the asset overview page, the deployment dropdown is now sorted
* Operator users are able to set their Redmine API key and create Redmine tickets
* When adding a new variable to an L3 or alert trigger, the variable name for the expression will pre-populate
* Issues with clone in some scenarios found before and during the last demo have been fixed
* The load time on the manage plots page has been increased dramatically
* The times show throughout the site now show UTC to clarify the time zone of the dates/times listed
* Trigger expression parsing has been improved. Specifically, >= and <= are now working
* The hover tooltip on plots can now be enabled/disabled as needed
* Improved handling of ERDDAP data calls when sending out of range dates or when no data is available for the selected plot
* The UI on the manual alert page has been cleaned up
* Additional bug fixes and UI updates
