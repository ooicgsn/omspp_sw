DEBUG = True

ALLOWED_HOSTS =
CSRF_TRUSTED_ORIGINS =

DEFAULT_FROM_EMAIL = 'no-reply@example.com'
EMAIL_HOST = 'example.com'
EMAIL_PORT = '25'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_SSL = False

LOG_ERDDAP_REQUESTS = True
LOG_ERDDAP_DETAILS = True

SITE_URL = 'http://localhost'

# Endpoint for connecting to ERDDAP. If username and password are provided, the application will connect with Basic auth
ERDDAP_URL=http://www.example.com
ERDDAP_USERNAME=
ERDDAP_PASSWORD=
ERDDAP_PORT=8080

# Redmine integration settings. The Use Live Projects setting will enable a connection to the production instances
#   of the Redmine instance; when disabled, test projects are used
REDMINE_URL=http://www.example.com
REDMINE_USE_LIVE_PROJECTS=false

# Url to view raw log files (hosted outside of OMS++ application
DATA_DIRECTORY=http://www.example.com

# To disable feature (namely external connections) for use when operating the application while at sea
LAPTOP_MODE=false

# Location of raw data files, including legacy platform configs
LOCAL_DATA_DIR=/path/to/raw/data

# Location of SSL certs
SSL_CERT_PATH=/dev/null
SSL_KEY_PATH=/dev/null

# Options to enable SSL certs. Nginx will fail if provided a key but no value, which is why the syntax here is to either include the entire line, or nothing
ENABLE_SSL_CERT=ssl_certificate /ssl/ssl.cer;
ENABLE_SSL_KEY=ssl_certificate_key /ssl/ssl.key;

# Hostnames to use for the UI
SERVER_NAMES=localhost

# Folder containing the nginx template to mount inside the docker container (ssl vs non-ssl)
NGINX_TEMPLATE_PATH=./nginx/no-ssl

# Location of the raw database files
DATABASE_PATH=./data

# Location for logs
LOG_PATH=./logs

# Location for backup files (databases)
BACKUP_PATH=./backups

# Location of an optional .netrc file. This path will be mounted inside the ui container at
#   ""/root/oms.netrc". The dummy file must be there for the docker-compose mount to work;
#   stored credentials are fake
NETRC_FILE=./config/oms.netrc

# Enable when parsed data folders are always numbered with -1, -2, etc
USE_NUMBERED_PARSED_DATA_FOLDERS=False

# Enable when parsed data folders for the IMM always start with the prefix "imm-"
USE_IMM_PREFIXED_PARSED_DATA_FOLDERS=True

# The port to connect to ERDDAP on the host machine
ERDDAP_PORT=8080
PARSED_DATA_DIRECTORY=/parsed/
LOCAL_PARSED_DATA_DIR=./data/parsed

# Whether to render front end in development mode (with hot module reloading)
DJANGO_VITE_DEV_MODE = False
