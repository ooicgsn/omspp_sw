FROM continuumio/miniconda3

RUN apt-get update

COPY ./web /code/
COPY ./common /code/common